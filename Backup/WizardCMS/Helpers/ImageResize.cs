﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.Helpers
{
    public class ImageResize
    {
        // Main class method 
        internal byte[] Resize(byte[] p, int htOfImage, int maxOfWidth = 0)
        {
            System.Drawing.Image img = MakeImage(p);
            int width = Convert.ToInt32(Convert.ToDouble(img.Width) *
            (Convert.ToDouble(htOfImage) / Convert.ToDouble(img.Height)));
            if (maxOfWidth != 0)
            {
                if (width > maxOfWidth)
                {
                    htOfImage = Convert.ToInt32(Convert.ToDouble(img.Height) *
                    (Convert.ToDouble(maxOfWidth) / Convert.ToDouble(img.Width)));
                    width = maxOfWidth;
                }
            }
            System.Drawing.Size s = new System.Drawing.Size(width, htOfImage);
            System.Drawing.Image resizedImg = Resize(img, s, true);
            using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
            {
                if (System.Drawing.Imaging.ImageFormat.Png.Equals(img.RawFormat))
                {
                    resizedImg.Save(memStream, System.Drawing.Imaging.ImageFormat.Png);
                }
                else //of course you could check for bmp, jpg, etc depending on what you allowed
                {
                    resizedImg.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return memStream.ToArray();
            }
        }

        private System.Drawing.Image MakeImage(byte[] byteArrayIn)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        private static System.Drawing.Image Resize(System.Drawing.Image image,
            System.Drawing.Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new System.Drawing.Bitmap(newWidth, newHeight);
            using (System.Drawing.Graphics graphicsHandle = System.Drawing.Graphics.FromImage(newImage))
            {
                graphicsHandle.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                graphicsHandle.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
                graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;

                //graphicsHandle.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                graphicsHandle.InterpolationMode =
                           System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
    }
}