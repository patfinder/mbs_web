﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.IO;
using WizardCMS.Const;

namespace WizardCMS.Helpers
{
    public class DataHelper
    {
        public static List<T> SortListByProperty<T>(List<T> lst, string sortBy, string typeSort) where T : class
        {
            try
            {
                if (lst != null && String.IsNullOrWhiteSpace(sortBy) == false)
                {
                    if (typeSort.ToUpper() == Constants.SORT_ASC || typeSort == "")
                    {
                        lst = lst.OrderBy(m => m.GetType().GetProperty(sortBy, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(m, null)).ToList();
                    }
                    else
                    {
                        lst = lst.OrderByDescending(m => m.GetType().GetProperty(sortBy, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(m, null)).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return lst;
        }

        public static bool ReflectiveEquals<T>(T first, T second, string[] ignore) where T : class
        {
            if (first == null && second == null)
            {
                return true;
            }
            if (first == null || second == null)
            {
                return false;
            }
            Type firstType = first.GetType();
            if (second.GetType() != firstType)
            {
                return false; // Or throw an exception
            }
            // This will only use public properties. Is that enough?
            foreach (PropertyInfo propertyInfo in firstType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (!ignore.Contains(propertyInfo.Name) && propertyInfo.CanRead)
                {
                    object firstValue = propertyInfo.GetValue(first, null);
                    object secondValue = propertyInfo.GetValue(second, null);
                    if (!object.Equals(firstValue, secondValue))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool PropertiesEqual<T>(T first, T second, string[] ignore) where T: class
        {
            if (first != null && second != null)
            {
                var type = typeof(T);
                var ignoreList = new List<string>(ignore);
                var unequalProperties =
                    from pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    where !ignoreList.Contains(pi.Name)
                    let selfValue = type.GetProperty(pi.Name).GetValue(first, null)
                    let toValue = type.GetProperty(pi.Name).GetValue(second, null)
                    where selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue))
                    select selfValue;
                return !unequalProperties.Any();
            }
            return first.Equals(second);
        }

        //Copy File
        public static bool CopyFile(string sourceFile, string desFile)
        {
            try
            {
                //
                string appPath = HttpContext.Current.Request.PhysicalApplicationPath;
                string filePath = appPath + sourceFile.Replace('/', '\\');
                string destinationFile = appPath + desFile.Replace('/', '\\');
                if (File.Exists(destinationFile))
                {
                    return true;
                }
                if (File.Exists(filePath))
                {
                    FileInfo fileInfo = new FileInfo(destinationFile);
                    if (!Directory.Exists(fileInfo.DirectoryName))
                    {
                        Directory.CreateDirectory(fileInfo.DirectoryName);
                    }
                    destinationFile = appPath + desFile.Replace('/', '\\');// +fileInfo.Name;
                    File.Copy(filePath, destinationFile);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        //Delete a file
        public static bool DeleteFile(string fileName, string Dir)
        {
            try
            {
                //
                string appPath = HttpContext.Current.Request.PhysicalApplicationPath;
                string filePath = appPath + Dir.Replace('/', '\\') + fileName.Replace('/', '\\');
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        public static bool CheckSizeImageUpload(HttpPostedFileBase namefile)
        {
            try
            {
                if (namefile.ContentLength < Constants.MAX_LENGTH_IMG)
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

    }
}