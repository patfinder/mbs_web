﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.Helpers
{
    public class DatetimeHelper
    {
        public static string DD_MM_YYYY = "dd-MM-yyyy";
        public static string DD_MM_YYYY_HH_MM = "dd-MM-yyyy HH:mm";

        public static DateTime StringToDate(string strDate, string format)
        {
            try
            {
                if (string.IsNullOrEmpty(format))
                    format = DD_MM_YYYY;
                DateTime date = DateTime.ParseExact(strDate, format, null);
                return date;
            }
            catch { }
            return new DateTime();
        }

        public static string LastUpdateTime(DateTime time)
        {
            System.Globalization.DateTimeFormatInfo timeFormat = new System.Globalization.DateTimeFormatInfo();
            timeFormat.ShortTimePattern = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern;
            timeFormat.AMDesignator = "AM";
            timeFormat.PMDesignator = "PM";
            string time_ = String.Format(timeFormat, "{0:t}", time);
            string year_ = time.ToString("dd/MM/yyyy");

            return year_ + " " + time_;
            //return year_;
        }

        public static DateTime GetStartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }

        /// <summary>
        /// Phương thức chuyển từ chuỗi sang kiểu ngày tháng
        /// </summary>
        /// <param name="strDate">chuỗi mô tả ngày</param>
        /// <param name="strFormat">chuỗi định dạng (dd/mm/yyyy, mm/dd/yyyy)</param>
        /// <returns>DateTime ==> ngày tháng của chuỗi</returns>
        public static DateTime getDateTimeWithString(string strDate, string strFormat = "dd/mm/yyyy" )
        {
            try
            {
                strDate = strDate.Replace('-', '/');

                DateTime dt;
                String[] tempArr;
                tempArr = strDate.Split('/');
                int day = 0;
                int month = 0;
                int year = 0;
                if (strFormat == "dd/mm/yyyy")
                {
                    day = int.Parse(tempArr[0]);
                    month = int.Parse(tempArr[1]);
                    year = int.Parse(tempArr[2]);
                }
                else
                {
                    day = int.Parse(tempArr[1]);
                    month = int.Parse(tempArr[0]);
                    year = int.Parse(tempArr[2]);
                }
                dt = new DateTime(year, month, day);
                return dt;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return DateTime.Now;
        }

        /// <summary>
        /// So sánh ngày
        /// </summary>
        /// <param name="dt1">ngày 1</param>
        /// <param name="dt2">ngày 2</param>
        /// <returns>kết quả so sánh giữa 2 ngày</returns>
        public int compareDate(DateTime dt1, DateTime dt2)
        {
            TimeSpan sp = dt1.Subtract(dt2);
            return sp.Days;
        }

        public static string DatetimeToString(DateTime time)
        {
            DateTime thisDate = time;
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("pt-BR");
            return thisDate.ToString("d", culture);
        }

        public static string DatetimeToLongString(DateTime time)
        {
            DateTime thisDate = time;
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("pt-BR");
            return thisDate.ToString("g", culture);
        }

        /// <summary>
        /// Chuyển kiểu datetime sang chuỗi định dạng ddmmyyyy
        /// </summary>
        /// <param name="dt">ngày tháng</param>
        /// <returns>chuỗi định dạng ddmmyyyy</returns>
        public string convertDateToShortString(DateTime dt)
        {
            string dd = dt.Day.ToString();
            if (dd.Length < 2) dd = "0" + dd;

            string mm = dt.Month.ToString();
            if (mm.Length < 2) mm = "0" + mm;

            string yyyy = dt.Year.ToString();
            return dd + mm + yyyy;
        }

        /// <summary>
        /// Chuyển kiểu chuỗi ddmmyyyy sang kiểu datetime
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>ngày</returns>
        public static DateTime convertShortStringToDate(string str)
        {
            int dd = int.Parse(str.Substring(0, 2));
            int mm = int.Parse(str.Substring(2, 2));
            int yyyy = int.Parse(str.Substring(4, 4));
            return (new DateTime(yyyy, mm, dd));
        }

        /// <summary>
        /// Chuyển từ chuỗi sang dạng giờ
        /// </summary>
        /// <param name="strTime">chuỗi giờ: hhmmss</param>
        /// <returns>DateTime 00:00:00hh:mm:ss</returns>
        public DateTime getTimeFromString(string strTime)
        {
            /*
            int hour = int.Parse(strTime.Substring(0, 2)); ;
            int minute = int.Parse(strTime.Substring(2, 2));
            int second = int.Parse(strTime.Substring(4, 2));
            return (new DateTime(1, 1, 1, hour, minute, second));
             */
            string[] time = strTime.Split(':');
            int hour = int.Parse(time[0]); ;
            int minute = int.Parse(time[1]);
            int second = int.Parse(time[2]);
            return (new DateTime(1, 1, 1, hour, minute, second));
        }

        /// <summary>
        /// Chuyển từ giờ sang chuỗi
        /// </summary>
        /// <param name="time">giá trị giờ</param>
        /// <returns>chuỗi mô tả giờ hhmmss</returns>
        public static String convertTimeToString(DateTime time)
        {
            return string.Format("{0:00}{1:00}{2:00}", time.Hour, time.Minute, time.Year);
        }
    }
}