﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;
using System.ComponentModel;
using System.Text.RegularExpressions;
using WizardCMS.Const;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace WizardCMS.Helpers
{
    public class CommonHelper
    {
        /// <summary>
        /// Get segment from an index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="currentPath"></param>
        /// <returns></returns>
        public static string GetSegment(int index, string currentPath)
        {
            string result = string.Empty;
            try
            {
                //url = "http://localhost/admincp/";
                currentPath = currentPath.Replace("//", "");
                if (currentPath.IndexOf('?') > 0)
                    currentPath = currentPath.Substring(0, currentPath.IndexOf('?'));
                string[] parse = currentPath.Split('/');
                if (parse.Count() > 0 && parse.Count() >= index)
                {
                    return parse[index];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return result;
        }

        public static string GetSegment(int index)
        {
            try
            {
                string str = "";
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                url = url.Replace("//", "");
                if (url.IndexOf('?') > 0)
                    url = url.Substring(0, url.IndexOf('?'));
                if (url.Split("/".ToCharArray()).Count() - 1 >= index)
                {
                    str = url.Split("/".ToCharArray())[index];
                }
                return str;
            }
            catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
            return string.Empty;
        }

        /// <summary>
        /// Function get all segment
        /// </summary>
        /// <param name="currentPath"></param>
        /// <returns></returns>
        public static string[] GetListSegment(string currentPath)
        {
            try
            {
                //url = "http://localhost/admincp/";
                currentPath = currentPath.Replace("://", "/");
                currentPath = currentPath.Replace("//", "/");
                string[] parse = currentPath.Split('/');
                if (parse.Count() > 0)
                {
                    return parse;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return null;
        }

        public static string CutText(string input, int length=80)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        /// <summary>
        /// Trim text to an specified length
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CutText2(string source, int length = 80)
        {
            string result = string.Empty;
            try
            {
                if (source.Length <= length)
                {
                    return source;
                }

                source = source.Substring(0, length);
                if (source[length - 1] == ' ')
                {
                    return source.Trim() + "...";
                }
                string[] x = source.Split(' ');
                if (x.Count() <= 1)
                {
                    return source.Trim() + "...";
                }
                x[x.Count() - 1] = "";
                return source.Trim() + "...";

                //$x  = explode(" ", $text);
                //$sz = sizeof($x);
                //if ($sz <= 1)   {
                //    return $text."...";}
                //$x[$sz-1] = '';
                //return trim(implode(" ", $x))."...";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return result;
        }

        /// <summary>
        /// SEO Title
        /// </summary>
        /// <param name="input">input title</param>
        /// <returns>seo title</returns>
        public static string SEO_TITLE(string input)
        {
            string result = string.Empty;
            try
            {
                input = input.Trim();
                input = input.Replace("'", "");
                input = input.Replace("’", "");
                input = input.Replace("\"", "");
                input = input.Replace("“", "");
                input = input.Replace("”", "");
                input = Resources.Resources.META_TITLE + " | " + input;
                return input;
            }
            catch (Exception ex)
            { }
            return result;
        }

        /// <summary>
        /// Tao thumnail, resize, crop images function
        /// </summary>
        /// <param name="input">image url</param>
        /// <param name="width">resize width</param>
        /// <param name="height">resize height</param>
        /// <returns></returns>
        public static string Img(string input, int width, int height, int noImgCode=1)
        {
            string lang = StringHelper.language();
            string noimage = "Assets/images/no-image-" + lang + "-" + noImgCode + ".jpg";
            string appPath = HttpContext.Current.Request.PhysicalApplicationPath;
            string filePath = string.Empty;
            try
            {
                if (input != null && input.Trim() != string.Empty)
                {
                    filePath = appPath + input.Replace('/', '\\');

                    if (!File.Exists(filePath))
                    {
                        //return noimage;//Constants.DIR_ARTICLE + input;
                        filePath = appPath + noimage.Replace('/', '\\');
                    }
                }
                else
                {
                    filePath = appPath + noimage.Replace('/', '\\');
                }

                FileInfo fileInfo = new FileInfo(filePath);

                string uploadDir_Thumb = fileInfo.DirectoryName + "\\" + width + "x" + height; //$fileinfo['dirname'].'/'.$width.'x'.$height.'/';
                //create new folder
                if (!Directory.Exists(uploadDir_Thumb))
                {
                    Directory.CreateDirectory(uploadDir_Thumb);
                }

                string new_file_path = uploadDir_Thumb + "\\" + fileInfo.Name;// +fileInfo.Extension;
                if (!File.Exists(new_file_path) || (new FileInfo(new_file_path)).LastWriteTime < fileInfo.LastWriteTime)
                {
                    int original_width = 0; int original_height = 0;

                    GetImageSize(filePath, out original_width, out original_height);
                    float ratio = (float)original_width / (float)original_height;

                    //The requested sizes
                    int requested_width = width;
                    int requested_height = height;

                    //Initialising
                    double new_width = 0;
                    double new_height = 0;

                    //Calculations
                    if (requested_width > requested_height)
                    {
                        new_width = requested_width;
                        new_height = new_width / ratio;
                        if (requested_height == 0)
                            requested_height = (int)new_height;

                        if (new_height < requested_height)
                        {
                            new_height = requested_height;
                            new_width = new_height * ratio;
                        }
                    }
                    else
                    {
                        new_height = requested_height;
                        new_width = new_height * ratio;
                        if (requested_width == 0)
                            requested_width = (int)new_width;

                        if (new_width < requested_width)
                        {
                            new_width = requested_width;
                            new_height = new_width / ratio;
                        }
                    }

                    new_width = Math.Ceiling(new_width);
                    new_height = Math.Ceiling(new_height);

                    Image img = Image.FromFile(filePath);
                    //resize image
                    img = resizeImage(img, new Size((int)new_width + 1, (int)new_height + 1));

                    //crop image if width and height are not zero
                    if (width != 0 && height != 0)
                    {
                        int crop_x = (int)Math.Floor((new_width - width) / 2);
                        int crop_y = (int)Math.Floor((new_height - height) / 2);

                        img = cropImage(img, new Rectangle(crop_x, crop_y, width, height));

                        //save new image
                        saveJpeg(new_file_path, new Bitmap(img), 100L);
                    }
                }
                new_file_path = new_file_path.Replace(appPath, "");
                new_file_path = new_file_path.Replace("\\", "/");
                return new_file_path;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }

            return noimage;
        }

        public static string Img2(string input, int width, int height, int noImgCode = 1)
        {
            string lang = StringHelper.language();
            string noimage = "Assets/images/no-image-" + lang + "-" + noImgCode + ".jpg";
            string appPath = HttpContext.Current.Request.PhysicalApplicationPath;
            string filePath = string.Empty;
            try
            {
                if (input != null && input.Trim() != string.Empty)
                {
                    filePath = appPath + input.Replace('/', '\\');
                    filePath = filePath.Replace("\\\\","\\");

                    if (!File.Exists(filePath))
                    {
                        //return noimage;//Constants.DIR_ARTICLE + input;
                        filePath = appPath + noimage.Replace('/', '\\');
                    }
                }
                else
                {
                    filePath = appPath + noimage.Replace('/', '\\');
                }

                FileInfo fileInfo = new FileInfo(filePath);

                string uploadDir_Thumb = fileInfo.DirectoryName + "\\" + width + "x" + height; //$fileinfo['dirname'].'/'.$width.'x'.$height.'/';
                //create new folder
                if (!Directory.Exists(uploadDir_Thumb))
                {
                    Directory.CreateDirectory(uploadDir_Thumb);
                }

                string new_file_path = uploadDir_Thumb + "\\" + fileInfo.Name;// +fileInfo.Extension;
                if (!File.Exists(new_file_path) || (new FileInfo(new_file_path)).LastWriteTime < fileInfo.LastWriteTime)
                {
                    Image img = Image.FromFile(filePath);
                    //resize image
                    ResizeImage(img, width, height, new_file_path);
                }
                new_file_path = new_file_path.Replace(appPath, "");
                new_file_path = new_file_path.Replace("\\", "/");
                return new_file_path;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }

            return Img2(noimage, width, height, noImgCode);
        }

        private static void ResizeImage(Image Original, Int32 newWidth, Int32 newHeight, String pathToSave)
        {
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            int originalWidth = Original.Width;
            int originalHeight = Original.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)newWidth / (float)originalWidth);
            nPercentH = ((float)newHeight / (float)originalHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((newWidth -
                              (originalWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((newHeight -
                              (originalHeight * nPercent)) / 2);
            }

            int destWidth = (int)(originalWidth * nPercent);
            int destHeight = (int)(originalHeight * nPercent);


            Bitmap bmp = new Bitmap(newWidth, newHeight, PixelFormat.Format32bppArgb);

            bmp.SetResolution(Original.HorizontalResolution, Original.VerticalResolution);
            using (Graphics Graphic = Graphics.FromImage(bmp))
            {
                Graphic.CompositingQuality = CompositingQuality.HighQuality;
                Graphic.Clear(Color.Transparent);
                Graphic.CompositingMode = CompositingMode.SourceCopy;
                Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

                Graphic.DrawImage(
                    Original,
                    new Rectangle(destX, destY, destWidth, destHeight),
                    new Rectangle(sourceX, sourceY, originalWidth, originalHeight),
                    GraphicsUnit.Pixel
                    );

                bmp.Save(pathToSave, Original.RawFormat);

            }

        }

        //get image size
        public static void GetImageSize(string inFile, out int width, out int height)
        {
            using (Stream stream = new FileStream(inFile, FileMode.Open))
            {
                using (Image src_image = Image.FromStream(stream))
                {
                    width = src_image.Width;
                    height = src_image.Height;
                }
            }
        }

        public static Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
                                            bmpImage.PixelFormat);
            return (Image)(bmpCrop);
        }

        public static Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            using (Graphics Graphic = Graphics.FromImage(b))
            {
                Graphic.CompositingQuality = CompositingQuality.HighQuality;
                Graphic.Clear(Color.Transparent);
                Graphic.CompositingMode = CompositingMode.SourceCopy;
                Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

                Graphic.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            }

            //Graphics g = Graphics.FromImage((Image)b);
            //g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //g.CompositingMode = CompositingMode.SourceCopy;
            //g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            //g.Dispose();

            return (Image)b;
        }

        private static void _clean_cache()
        {
            try
            {
                foreach (string filename in Directory.GetFiles(HttpContext.Current.Request.MapPath(@"/uploads/image_cache/"), "*.*", SearchOption.AllDirectories))
                {
                    if (File.Exists(filename)) {
                        FileInfo fileInfo = new FileInfo(filename);
                        if (DateTime.Now.AddHours(-1) > fileInfo.LastWriteTime)
                        {
                            //delete file
                            File.Delete(filename);
                        }
                    }
                }
            }
            catch { }
        }

        private static void _clear_folder_cache()
        {
            try
            {
                foreach (string file in Directory.GetFiles(HttpContext.Current.Request.MapPath(@"/uploads/image_cache/")))
                {
                    File.Delete(file);
                }
                foreach (string dir in Directory.GetDirectories(HttpContext.Current.Request.MapPath(@"/uploads/image_cache/")))
                {
                    Directory.Delete(dir, true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string ByteToImage(byte[] image, int width = 0, int height = 0, int noImgCode=1)
        {
            try
            {
                //clean old cache image
                //new System.Threading.Thread(() => { _clean_cache(); }).Start();
                _clean_cache();

                ImageFormat format = ImageFormat.Jpeg;
                string extension = ".jpg";
                using (MemoryStream ms = new MemoryStream(image))
                {
                    System.Drawing.Image i = System.Drawing.Image.FromStream(ms);
                    Bitmap bmpImage = new Bitmap(i);

                    if (ImageFormat.Jpeg.Equals(i.RawFormat))
                    {
                        // JPEG
                        format = ImageFormat.Jpeg;
                        extension = ".jpg";
                    }
                    else if (ImageFormat.Png.Equals(i.RawFormat))
                    {
                        // PNG
                        format = ImageFormat.Png;
                        extension = ".png";
                    }
                    else if (ImageFormat.Gif.Equals(i.RawFormat))
                    {
                        // GIF
                        format = ImageFormat.Gif;
                        extension = ".gif";
                    }

                    string filename = "/uploads/image_cache/" + StringHelper.EncryptMD5(DateTime.Now.Ticks.ToString()) + extension;

                    saveJpeg(HttpContext.Current.Request.MapPath(filename), bmpImage, 100);

                    //i.Save(HttpContext.Current.Request.MapPath(filename), format);

                    if (width > 0 && height > 0)
                        return Img(filename, width, height);
                    return filename;
                }
            }
            catch (Exception)
            {
            }
            return Img("assets/images/no-image-" + noImgCode + ".jpg", width, height);
        }

        public static string FetchImage(string html, int width = 0, int height = 0, int noImgCode=1)
        {
            try
            {
                //clean old cache image
                //new System.Threading.Thread(() => { _clean_cache(); }).Start();
                _clean_cache();

                ImageFormat format = ImageFormat.Jpeg;
                string extension = ".jpg";

                string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                MatchCollection matchesImgSrc = Regex.Matches(html, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);

                string image_link = "";
                foreach (Match m in matchesImgSrc)
                {
                    image_link = m.Groups[1].Value;
                }

                if (!string.IsNullOrEmpty(image_link))
                {
                    string filename = _saveImage(image_link);
                    if (width > 0 && height > 0)
                        return Img(filename, width, height, noImgCode);
                    return filename;
                }
            }
            catch (Exception)
            {
            }
            string lang = StringHelper.language();
            return Img("assets/images/no-image-" + lang + "-" + noImgCode + ".jpg", width, height,noImgCode);
        }

        private static string _saveImage(string link)
        {
            try
            {
                byte[] data;
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    data = client.DownloadData(link);
                }

                string extension = "";
                int fileExtPos = link.LastIndexOf(".");
                if (fileExtPos >= 0)
                    extension = link.Substring(fileExtPos);

                string fileName = StringHelper.EncryptMD5(DateTime.Now.Ticks.ToString()) + extension;
                string directory = HttpContext.Current.Request.MapPath("/uploads/image_cache/");
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);

                    //set permission
                    CommonHelper.SetPermission(directory);
                }
                string path = System.IO.Path.Combine(directory, fileName);

                System.IO.File.WriteAllBytes(path, data);

                return "/uploads/image_cache/"+fileName;
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public static void saveJpeg(string path, Bitmap img, long quality)
        {
            img.Save(path, img.RawFormat);
            /*
            // Encoder parameter for image quality
            EncoderParameter qualityParam =
                new EncoderParameter(Encoder.Quality, quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");

            if (jpegCodec == null)
                return;

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            img.Save(path, jpegCodec, encoderParams);
            */
        }

        public static ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

        public static string ResizeImg(string input, int width, int height)
        {
            string noimage = "Assets/images/no-image.jpg";
            string appPath = HttpContext.Current.Request.PhysicalApplicationPath;
            string filePath = string.Empty;
            try
            {
                if (input != null && input.Trim() != string.Empty)
                {
                    filePath = appPath + input.Replace('/', '\\');

                    if (!File.Exists(filePath))
                    {
                        //return noimage;//Constants.DIR_ARTICLE + input;
                        filePath = appPath + noimage.Replace('/', '\\');
                    }
                }
                else
                {
                    filePath = appPath + noimage.Replace('/', '\\');
                }

                FileInfo fileInfo = new FileInfo(filePath);

                string uploadDir_Thumb = fileInfo.DirectoryName + "\\" + width + "x" + height; //$fileinfo['dirname'].'/'.$width.'x'.$height.'/';
                //create new folder
                if (!Directory.Exists(uploadDir_Thumb))
                {
                    Directory.CreateDirectory(uploadDir_Thumb);
                }

                string new_file_path = "\\uploads\\image_catch" + "\\" + fileInfo.Name;// +fileInfo.Extension;
                if (!File.Exists(new_file_path) || (new FileInfo(new_file_path)).LastWriteTime < fileInfo.LastWriteTime)
                {

                    ImageResize imgSize = new ImageResize();
                    byte[] img = imgSize.Resize(imageToBytes(filePath), width, height);

                    ImageFormat format = ImageFormat.Jpeg;
                    string extension = ".jpg";
                    using (MemoryStream ms = new MemoryStream(img))
                    {
                        System.Drawing.Image i = System.Drawing.Image.FromStream(ms);
                        Bitmap bmpImage = new Bitmap(i);

                        if (ImageFormat.Jpeg.Equals(i.RawFormat))
                        {
                            // JPEG
                            format = ImageFormat.Jpeg;
                            extension = ".jpg";
                        }
                        else if (ImageFormat.Png.Equals(i.RawFormat))
                        {
                            // PNG
                            format = ImageFormat.Png;
                            extension = ".png";
                        }
                        else if (ImageFormat.Gif.Equals(i.RawFormat))
                        {
                            // GIF
                            format = ImageFormat.Gif;
                            extension = ".gif";
                        }

                        //string filename = "/uploads/image_catch/" + StringHelper.EncryptMD5(DateTime.Now.Ticks.ToString()) + extension;

                        saveJpeg(new_file_path, bmpImage, 100);

                        //return filename;
                    }
                }
                new_file_path = new_file_path.Replace(appPath, "");
                new_file_path = new_file_path.Replace("\\", "/");
                return new_file_path;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        private static byte[] imageToBytes(string filePath)
        {
            try
            {
                using (MemoryStream mStream = new MemoryStream())
                {
                    Image img = Image.FromFile(filePath);
                    img.Save(mStream, img.RawFormat);
                    return mStream.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return null;
        }

        public static string EDITOR(string input)
        {
            //$str = str_replace('../../../static/uploads/editor/', PATH_URL.'static/uploads/editor/', $str);
            //$str = preg_replace('/http:\/\/www.youtube.com\/watch\?v=([A-Za-z0-9\-\_]+)&amp;feature=([A-Za-z0-9]+)/is', '<iframe width="500" height="289" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>', $str);
            //$str = preg_replace('/http:\/\/www.youtube.com\/watch\?v=([A-Za-z0-9\-\_]+)/is', '<iframe width="500" height="289" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>', $str);
            //return $str;

            return string.Empty;
        }

        //set permission to created folder - update 17/12/2012 - Nghia.Truong
        public static void SetPermission(string directory)
        {
            try
            {
                //set permission to created folder - update 17/12/2012 - Nghia.Truong
                var ds = new DirectorySecurity();
                var sid = new SecurityIdentifier(WellKnownSidType.CreatorOwnerSid, null);
                var ace = new FileSystemAccessRule(sid, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);
                ds.AddAccessRule(ace);

                DirectoryInfo myDirectoryInfo = new DirectoryInfo(directory);

                DirectorySecurity myDirectorySecurity = myDirectoryInfo.GetAccessControl();

                //myDirectorySecurity.AddAccessRule(new FileSystemAccessRule(User, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow));
                myDirectorySecurity.AddAccessRule(ace);

                myDirectoryInfo.SetAccessControl(myDirectorySecurity);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            Double timestamp = ConvertStringToDouble(unixTimeStamp);
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(timestamp).ToLocalTime();
            return dtDateTime;
        }

        public static Double ConvertStringToDouble(string input)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch
            {
                return 0;
            }
        }

        public static void SendEmail(string address, string message, string subject)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                string email = WebConfigurationManager.AppSettings["smtp_email"].ToString(); ;
                string password = WebConfigurationManager.AppSettings["smtp_password"].ToString();
                string client = WebConfigurationManager.AppSettings["smtp_client"].ToString();
                string port = WebConfigurationManager.AppSettings["smtp_port"].ToString();

                NetworkCredential loginInfo = new NetworkCredential(email, password);
                MailMessage msg = new MailMessage();
                SmtpClient smtpClient = new SmtpClient(client, int.Parse(port));

                msg.From = new MailAddress("support@mbs.com.vn");
                msg.To.Add(new MailAddress(address));
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.Subject = subject;
                msg.IsBodyHtml = true;
                msg.Body = message;

                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = loginInfo;
                smtpClient.SendCompleted += SendCompletedCallback;

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

                string userState = "Send mail complete";
                smtpClient.SendAsync(msg, userState);
                //return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
                //return false;
            }
        }

        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // replace with proper validation
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Callback when send mail complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                string msg = string.Format("[{0}] Send canceled.", token);
                Logs.WriteLogDebug(msg);
                //Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                string msg = string.Format("[{0}] {1}", token, e.Error.ToString());
                Logs.WriteLogDebug(msg);
                //Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Logs.WriteLogDebug("Message sent.");
                //Console.WriteLine("Message sent.");
            }
        }

        public static bool Register(DateTime? deadline)
        {
            if (deadline == null)
                return false;

            int check = DateTime.Compare(DateTime.Now, (DateTime)deadline);
            if (check <= 0)
                return true;

            return false;
        }
    }
}