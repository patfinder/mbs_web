﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Security;
using WizardCMS.Models;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Helpers
{
    public class AuthorizeActionFilter : ActionFilterAttribute
    {
        //AdminAccountService accountService = new AccountServiceImpl();
        public static AdminAccountModel AccountLogin = new AdminAccountModel();

        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        string redirectOnSuccess = filterContext.HttpContext.Request.Url.AbsolutePath;
        //        string redirectUrl = string.Format("?returnUrl={0}", redirectOnSuccess);
        //        string loginUrl = FormsAuthentication.LoginUrl + redirectUrl;

        //        filterContext.HttpContext.Response.Redirect(loginUrl, true);
        //    }
        //    else
        //    {
        //        //bool isAuthorized = _service.Authorize(GetUserSession.Id, _authRoles.ToString());
        //        bool isAuthorized = false;
        //        if (!isAuthorized)
        //        {
        //            // TODO: Make custom "Not Authorized" error page.
        //            throw new UnauthorizedAccessException("No access");
        //        }
        //    }

        //    //return RedirectToAction("Index", "Authentication");
        //     Log("OnActionExecuting", filterContext.RouteData);       
        //}

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            // Don't show filter multiple times when using Html.RenderAction or Html.Action.
            if (filterContext.IsChildAction == true)
            {
                return;
            }
            else
            {
                Object account = UserIsAuthenticated();
                if (account == null)
                {
                    // Redirect to login page
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary 
                        { 
                            { "controller", "Admincp" }, 
                            { "action", "login" } 
                        });
                }
                else
                {
                    AccountLogin = (AdminAccountModel)account;
                    bool isAuthorize = UserIsAuthorize(account as AdminAccountModel, filterContext);
                    if (isAuthorize)
                    {

                    }
                    else
                    {

                    }
                }
            }

            //var rc = new RequestContext(filterContext.HttpContext, filterContext.RouteData);
            //string url = RouteTable.Routes.GetVirtualPath(rc,new RouteValueDictionary(routeValues)).VirtualPath;
            Object controller = filterContext.RouteData.Values["controller"];
            Object action = filterContext.RouteData.Values["action"];
            Object nameModule = filterContext.RouteData.Values["nameModule"];



        }

        public static Object UserIsAuthenticated()
        {
            return HttpContext.Current.Session[Constants.AUTHORIZE_NAME];
        }

        public bool UserIsAuthorize(AdminAccountModel account, ActionExecutingContext filterContext)
        {
            Object controller = filterContext.RouteData.Values["controller"];
            Object action = filterContext.RouteData.Values["action"];
            Object nameModule = filterContext.RouteData.Values["nameModule"];
            if (nameModule != null && String.IsNullOrWhiteSpace(nameModule.ToString()) == false)
            {
                string strModule = nameModule.ToString();
                string[] array = strModule.Split('/');
                string act = "";                
                for (int i = 0; i < array.Count();i++ )
                {
                    string mo = array[i];
                    if (i == 0)
                    {
                        if (mo.Trim() != "")
                        {
                            strModule = mo;
                        }
                    }
                    if (i == 1)
                    {
                        if (mo.Trim() != "")
                        {
                            act = mo;
                        }
                    }
                }
                ModuleModel module          = ModuleServices.GetModuleByFunction(strModule);
                AdminAccountModel accountDB = AdminAccountService.GetData(account.USERNAME, account.PASSWORD);
                AccountLogin = accountDB;
            }
            Object moduleId = filterContext.RouteData.Values["moduleId"];
            return true;
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Log("OnActionExecuted", filterContext.RouteData);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Log("OnResultExecuting", filterContext.RouteData);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Log("OnResultExecuted", filterContext.RouteData);
        }


        private void Log(string methodName, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
            Debug.WriteLine(message, "Action Filter Log");
        }

    }
}