﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WizardCMS.Models;

namespace WizardCMS.DTO
{
    public class MenuItemDTO
    {
        public int Id { get; set; }

        public int ParrentId { get; set; }

        public string Title { get; set; }

        public string Slug { get; set; }

        public string Link { get; set; }

        public string Redirect { get; set; }

        public List<MenuItemDTO> lstSubMenu { get; set; }

        public Dictionary<string, ArticleContent> ContentTranslates { get; set; }
 
        public MenuItemDTO() 
        {
            this.lstSubMenu = null;
        }
        
    }

    public class TemplateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}