﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;
using System.Collections;

namespace WizardCMS.Models
{
    public class ArticleViewModel
    {
        public int ID { get; set; }
        public int CID { get; set; }
        public int STATUS { get; set; }
        public int PARENT_ID { get; set; }
        public int PRIORITY { get; set; }
        public int MAIN_MENU { get; set; }
        public int ADMIN_SUBMENU { get; set; }
        public int FEATURED { get; set; }
        public int YEAR_REPORT { get; set; }
        public int TYPE { get; set; }
        public int IS_DELETE { get; set; }
        public int IS_CHANGE_SLUG { get; set; }

        public string ICON { get; set; }
        public string TITLE { get; set; }
        public string SUB_TITLE { get; set; }
        public string SLUG { get; set; }
        public string LINK { get; set; }
        public string CONTENT { get; set; }
        public string DESCRIPTION { get; set; }
        public string TEMPLATE { get; set; }
        public string THUMBNAIL { get; set; }
        public string ATTACHMENT { get; set; }
        public string LOCATION { get; set; }

        public string IMAGE_TABLET { get; set; }
        public string IMAGE_MOBILE { get; set; }

        public DateTime CREATED { get; set; } //Ngày đăng bài viết
        public DateTime MODIFIED { get; set; }
        public DateTime? DEAD_LINE { get; set; }
        public DateTime? DATE_POST { get; set; } //Ngày tạo bài viết
        public DateTime? OPEN_DAY { get; set; } //Ngày diễn ra hội thảo

        public int VIEWS { get; set; }

        //public List<ArticleContent> ContentTranslates { get; set; }
        public Dictionary<string, ArticleContent> ContentTranslates { get; set; }

        public List<ArticleViewModel> Childs { get; set; }

        //Constructor
        public ArticleViewModel()
        {
            this.ContentTranslates = new Dictionary<string, ArticleContent>();
        }
    }

    public class ArticleContent
    {
        public int NID { get; set; }
        public string TITLE { get; set; }
        public string SUB_TITLE { get; set; }
        public string SLUG { get; set; }
        public string LINK { get; set; }
        public string DESCRIPTION { get; set; }
        public string CONTENT { get; set; }
        public string IMAGE { get; set; }
        public string IMAGE_TABLET { get; set; }
        public string IMAGE_MOBILE { get; set; }
        public string ATTACHMENT { get; set; }
        public string LANG { get; set; }

        public ArticleContent()
        { }
    }

    public static class ArticlesServices
    {
        #region Articles services interface
        public static ArticleContent GetArticleContentByLang(Dictionary<string, ArticleContent> lstArticleContent, string lang = "vi")
        {
            try
            {
                if (lstArticleContent != null && lstArticleContent.ContainsKey(lang))
                {
                    return lstArticleContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static List<MenuItemDTO> GetMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Dictionary<string, ArticleContent> GetContentTranslates(int NID)
        {
            try
            {
                return ArticlesDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetAdminSubMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetAdminSubMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetMenuByListId(int[] arrArticleIDs, int depth)
        {
            try
            {
                return ArticlesDAL.Instance.GetMenuByListId(arrArticleIDs, depth);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetFooterMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetFooterMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetAll()
        {
            try
            {
                ArticlesDAL.Instance.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleByLink(string link)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByLink(link);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String GetNodeLink(int id, string lang = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetNodeLink(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetNextNode(int id, int parent, string lang = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetNextNode(id, parent, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetPreviousNode(int id, int parent, string lang = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetPreviousNode(id, parent, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleBySlug(string slug, int parent = 0)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleBySlug(slug, parent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetSiteMap()
        {
            try
            {
                return ArticlesDAL.Instance.GetSiteMap();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetTopParent(ArticleViewModel article)
        {
            try
            {
                return ArticlesDAL.Instance.GetTopParent(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetArticlesByParentId(int parentId, int page, int perpage)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticlesByParentId(parentId, page, perpage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// LẤY DANH SÁCH BÀI VIẾT
        /// </summary>
        /// <param name="parrent">ID NODE CHA</param>
        /// <param name="depth">Độ sâu muốn lấy bài viết con</param>
        /// <param name="current_level"></param>
        /// <returns></returns>
        public static List<ArticleViewModel> GetArticleByParrent(int parrent, int depth = 1, int current_level = 1)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByParrent(parrent, depth, current_level);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetFeaturedContent(int parrent, int page = 1, int perpage = 10, int term = 0, bool getRow = false)
        {
            try
            {
                return ArticlesDAL.Instance.GetFeaturedContent(parrent, page, perpage, term, getRow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetHotNews(int parrent, int limit = 4)
        {
            try
            {
                return ArticlesDAL.Instance.GetHotNews(parrent, limit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "", int[] not_ins = null)
        {
            try
            {
                return ArticlesDAL.Instance.GetSearchContent(parrent, page, perpage, searchText, dateFrom, dateTo, not_ins);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi", int[] not_ins = null)
        {
            try
            {
                return ArticlesDAL.Instance.GetCountSearchContent(parentId, fromDate, toDate, searchText, lang, not_ins);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetSearchContent(Hashtable searchParams)
        {
            try
            {
                return ArticlesDAL.Instance.GetSearchContent(searchParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(Hashtable searchParams)
        {
            try
            {
                return ArticlesDAL.Instance.GetCountSearchContent(searchParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetSearchReport(int parrent, int year_report, List<int> in_years, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "", int other = 0)
        {
            try
            {
                return ArticlesDAL.Instance.GetSearchReport(parrent, year_report, in_years, page, perpage, searchText, dateFrom, dateTo, other);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchReport(int parentId, int year_report, List<int> in_years, string fromDate, string toDate, string searchText, string lang = "vi", int other = 0)
        {
            try
            {
                return ArticlesDAL.Instance.GetCountSearchReport(parentId, year_report, in_years, fromDate, toDate, searchText, lang, other);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountArticlesByParentId(int parentId)
        {
            try
            {
                return ArticlesDAL.Instance.CountArticlesByParentId(parentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleById(int id)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleContentById(int id, string lang = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<int> GetListArticleIdByParentId(int parentId, string lang)
        {
            try
            {
                return ArticlesDAL.Instance.GetListArticleIdByParentId(parentId, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel AdminGetArticalById(int id)
        {
            try
            {
                return ArticlesDAL.Instance.AdminGetArticalById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(ArticleViewModel article)
        {
            try
            {
                return ArticlesDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return ArticlesDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return ArticlesDAL.Instance.GetCount(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetArticleByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang="vi")
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByPaging(parentId, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="articleViewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(ArticleViewModel articleViewModel)
        {
            try
            {
                return ArticlesDAL.Instance.Insert(articleViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return ArticlesDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteContent(int id, string lang)
        {
            try
            {
                return ArticlesDAL.Instance.DeleteContent(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteFile(int id, string field)
        {
            try
            {
                return ArticlesDAL.Instance.DeleteFile(id, field);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lstId)
        {
            try
            {
                return ArticlesDAL.Instance.DeleteList(lstId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSelectList(int selectedId, int? parentId, int currentId = 0, string space = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetSelectList(selectedId, parentId, currentId, space = "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckSlug(string slug, string lang, int parent = 0)
        {
            try
            {
                return ArticlesDAL.Instance.CheckSlug(slug, lang, parent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateLinkCustom(int parentId)
        {
            try
            {
                ArticlesDAL.Instance.UpdateLinkCustom(parentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateMenuLink(int parentId, string parentLink = "", string lang = "vi")
        {
            try
            {
                ArticlesDAL.Instance.UpdateMenuLink(parentId, parentLink = "", lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class ArticlesDAL
        {
            private static ArticlesDAL _instance;

            static public ArticlesDAL Instance
            { 
                get {
                    if (_instance == null)
                        _instance = new ArticlesDAL();

                    return _instance;
                }
            }

            #region constructor
            public ArticlesDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
            /// <summary>
            /// Get All Menu Item
            /// </summary>
            public List<MenuItemDTO> GetMenu()
            {
                List<MenuItemDTO> result = new List<MenuItemDTO>();
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = context.WZ_ARTICLES.Where(a => a.MAIN_MENU == 1 && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).ToList();
                        if (menus != null)
                        {
                            foreach (var item in menus)
                            {
                                MenuItemDTO menu = new MenuItemDTO();
                                menu.Id = (int)item.ID;
                                menu.ParrentId = (int)item.PARENT_ID;

                                menu.ContentTranslates = this.GetContentTranslates((int)item.ID);

                                //get sub menu
                                var submenu = context.WZ_ARTICLES.Where(a => a.PARENT_ID == item.ID && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).Select(a=>new MenuItemDTO
                                {
                                    Id = (int)a.ID,
                                    ParrentId = (int)a.PARENT_ID,
                                    ContentTranslates = this.GetContentTranslates((int)a.ID)
                                });

                                if (submenu != null) menu.lstSubMenu = submenu.ToList();

                                if (menu.ContentTranslates != null) result.Add(menu);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public Dictionary<string, ArticleContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, ArticleContent> result = new Dictionary<string, ArticleContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_ARTICLES_CONTENT
                                    where ct.NID == NID
                                    orderby ct.LANG descending
                                    select new ArticleContent
                                    {
                                        NID = (int)ct.NID,
                                        TITLE = ct.TITLE,
                                        SUB_TITLE = ct.SUB_TITLE,
                                        DESCRIPTION = ct.DESCRIPTION,
                                        CONTENT = ct.CONTENT,
                                        SLUG = ct.SLUG,
                                        LINK = ct.LINK,
                                        IMAGE = ct.IMAGE,
                                        IMAGE_TABLET = ct.IMAGE_TABLET,
                                        IMAGE_MOBILE = ct.IMAGE_MOBILE,
                                        ATTACHMENT = ct.ATTACHMENT,
                                        LANG = ct.LANG
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch(Exception ex) 
                {
                    Logs.WriteLogError(ex.StackTrace);
                    Logs.WriteLogError(ex.Message);
                }
                return null;
            }

            /// <summary>
            /// ADMIN CP GET SUB MENU OF ARTICLES
            /// </summary>
            /// <returns>List MenuItemDTO</returns>
            public List<MenuItemDTO> GetAdminSubMenu()
            {
                try
                {
                    var lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == lang && m.ADMIN_SUBMENU == Constants.IS_SUBMENU && m.PARENT_ID == 0
                                    orderby m.PRIORITY ascending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG
                                    };
                        var temp = menus.ToList();

                        if (temp != null)
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetAdminSubMenu2(item.Id);
                            }
                        }

                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }


            public List<MenuItemDTO> GetAdminSubMenu2(int parrent, int level = 0, int current_level = 1)
            {
                try
                {
                    var lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == lang && m.PARENT_ID == parrent && m.ADMIN_SUBMENU == 1
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null && (level == 0 || level > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetAdminSubMenu2(item.Id, level, current_level+1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<MenuItemDTO> GetMenuByListId(int[] arrArticleIDs, int depth = 1)
            {
                try
                {
                    var lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where m.STATUS == 1 && mc.NID == m.ID && mc.LANG == lang && arrArticleIDs.Contains((int)m.ID) && m.TEMPLATE != Constants.TEMPLATE_HIDDEN
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null)
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetSubMenu2(item.Id, depth, 1);
                            }

                            return temp;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<MenuItemDTO> GetSubMenu2(int parrent, int level = 0, int current_level = 1)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lang = StringHelper.language();
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where m.STATUS == 1 && m.ID == mc.NID && mc.LANG == lang && m.TEMPLATE != Constants.TEMPLATE_HIDDEN && m.PARENT_ID == parrent
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null && (level == 0 || level > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetSubMenu2(item.Id, level, current_level + 1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get Footer Menu
            /// </summary>
            public List<MenuItemDTO> GetFooterMenu()
            {
                List<MenuItemDTO> result = new List<MenuItemDTO>();
                try
                {
                    using (var context = new DBContext())
                    {
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public void GetAll()
            {
                try
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            /// <summary>
            /// Get An Article By Id
            /// </summary>
            /// <param name="link">input link</param>
            /// <returns>result</returns>
            public ArticleViewModel GetArticleByLink(string link)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                   from nc in context.WZ_ARTICLES_CONTENT
                                   where nc.NID == n.ID && nc.LINK == link && nc.LANG == current_lang && n.STATUS == Constants.SHOW
                                   select new ArticleViewModel {
                                      ID = (int)n.ID,
                                      CID = (int)n.CID,
                                      TITLE = nc.TITLE,
                                      SUB_TITLE = nc.SUB_TITLE,
                                      LINK = nc.LINK,
                                      CONTENT = nc.CONTENT,
                                      DESCRIPTION = nc.DESCRIPTION,
                                      ATTACHMENT = nc.ATTACHMENT,
                                      LOCATION = n.LOCATION,
                                      PARENT_ID = (int)n.PARENT_ID,
                                      YEAR_REPORT = (int)n.YEAR_REPORT,
                                      CREATED = n.CREATED,
                                      MODIFIED = n.MODIFIED,
                                      DEAD_LINE = n.DEAD_LINE,
                                      DATE_POST = n.DATE_POST,
                                      OPEN_DAY = n.OPEN_DAY,
                                      TEMPLATE = n.TEMPLATE,
                                      PRIORITY = (int)n.PRIORITY,
                                      FEATURED = (int)n.FEATURED,
                                      MAIN_MENU = (int)n.MAIN_MENU,
                                      ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                      STATUS = (int)n.STATUS,
                                      IS_DELETE = (int)n.IS_DELETE,
                                      IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                   }).FirstOrDefault();
                        if (node != null)
                            node.ContentTranslates = this.GetContentTranslates(node.ID);
                        else
                            node = null;
                        return node;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get An Article By Slug
            /// </summary>
            /// <param name="slug">input slug</param>
            /// <returns>result</returns>
            public ArticleViewModel GetArticleBySlug(string slug, int parent = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var query = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where nc.NID == n.ID && nc.SLUG == slug && nc.LANG == current_lang && n.STATUS == Constants.SHOW
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SUB_TITLE = nc.SUB_TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        CONTENT = nc.CONTENT,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                        THUMBNAIL = nc.IMAGE,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        DEAD_LINE = n.DEAD_LINE,
                                        DATE_POST = n.DATE_POST,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        CID = (int)n.CID,
                                        IS_DELETE = (int)n.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                        //ContentTranslates = this.GetContentTranslates((int)n.ID)
                                    });
                        if (parent > 0) {
                            query = query.Where(n => n.PARENT_ID == parent);
                        }
                        var node = query.FirstOrDefault();
                        if (node != null)
                            node.ContentTranslates = this.GetContentTranslates(node.ID);
                        return node;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Set the site map
            /// </summary>
            /// <returns></returns>
            public List<MenuItemDTO> GetSiteMap()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        List<MenuItemDTO> result = new List<MenuItemDTO>();
                        var menus = context.WZ_ARTICLES.Where(a => a.MAIN_MENU == Constants.MAIN_MENU && a.PARENT_ID == 0 && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a=>a.CREATED).ToList();
                        if (menus != null)
                        {
                            foreach (var item in menus)
                            {
                                MenuItemDTO menu = new MenuItemDTO();
                                menu.Id = (int)item.ID;
                                menu.ParrentId = (int)item.PARENT_ID;
                                menu.ContentTranslates = this.GetContentTranslates((int)item.ID);

                                //get sub menu
                                var submenu = context.WZ_ARTICLES.Where(a => a.PARENT_ID == item.ID && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).Select(a => new MenuItemDTO
                                {
                                    Id = (int)a.ID,
                                    ParrentId = (int)a.PARENT_ID,
                                    ContentTranslates = this.GetContentTranslates((int)a.ID)
                                });

                                if (submenu != null) menu.lstSubMenu = submenu.ToList();

                                if (menu.ContentTranslates != null) result.Add(menu);
                            }
                        }

                        if (result.Count > 0)
                        {
                            return result;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the root parent of an node
            /// </summary>
            /// <param name="article">Current node</param>
            /// <returns></returns>
            public ArticleViewModel GetTopParent(ArticleViewModel article)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (article.PARENT_ID > 0)
                        {
                            var ar = context.WZ_ARTICLES.Where(a => a.ID == (int)article.PARENT_ID && a.STATUS == Constants.SHOW).FirstOrDefault();
                            if (ar != null)
                            {
                                article.ID = (int)ar.ID;
                                article.TEMPLATE = ar.TEMPLATE;
                                article.PARENT_ID = (int)ar.PARENT_ID;
                                article.STATUS = (int)ar.STATUS;
                                article.PRIORITY = (int)ar.PRIORITY;
                                article.FEATURED = (int)ar.FEATURED;

                                while (article.PARENT_ID > 0)
                                {
                                    return this.GetTopParent(article);
                                }

                                return article;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return article;
            }

            public List<ArticleViewModel> GetArticlesByParentId(int parentId, int page, int perpage)
            {
                List<ArticleViewModel> result = new List<ArticleViewModel>();
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticles = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).ThenByDescending(a => a.ID).ToList();
                        if (lstArticles != null)
                        {
                            lstArticles = lstArticles.Skip((page - 1) * perpage).Take(perpage).ToList();
                            foreach (var item in lstArticles)
                            {
                                ArticleViewModel article    = new ArticleViewModel();
                                article.ID                  = (int)item.ID;
                                article.CID                 = (int)item.CID;
                                article.PARENT_ID           = (int)item.PARENT_ID;
                                article.MAIN_MENU           = (int)item.MAIN_MENU;
                                article.STATUS              = (int)item.STATUS;
                                article.DATE_POST           = item.DATE_POST;
                                article.CREATED             = item.CREATED;
                                article.PRIORITY            = (int)item.PRIORITY;
                                article.FEATURED            = (int)item.FEATURED;
                                article.VIEWS               = (int)item.VIEWS;
                                article.ADMIN_SUBMENU       = (int)item.ADMIN_SUBMENU;
                                article.ContentTranslates   = this.GetContentTranslates((int)item.ID);

                                result.Add(article);
                            }
                            return result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public List<ArticleViewModel> GetFeaturedContent(int parrent, int page = 1, int perpage = 10, int term = 0, bool getRow = false)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == lang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW
                                    orderby m.FEATURED descending, m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ICON = m.ICON,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CID = (int)m.CID,
                                        IS_DELETE = (int)m.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)m.IS_CHANGE_SLUG,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED,
                                        DEAD_LINE = m.DEAD_LINE,
                                        DATE_POST = m.DATE_POST
                                    };

                        if (term > 0) {
                            query = query.Where(m=>m.CID == term);
                        }

                        if (page > 0 && perpage > 0)
                        {
                            return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }
                        else
                        {
                            return query.ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<ArticleViewModel> GetArticleByParrent(int parrent, int depth = 1, int current_level = 1, int page = 1, int perpage = 10, int featured = 0)
            {
                try
                {
                    var lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == lang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW

                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ICON = m.ICON,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CID = (int)m.CID,
                                        IS_DELETE = (int)m.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)m.IS_CHANGE_SLUG,
                                        DEAD_LINE = m.DEAD_LINE,
                                        DATE_POST = m.DATE_POST,
                                        MODIFIED = m.MODIFIED,
                                        CREATED = m.CREATED,
                                    };
                        var temp = menus.ToList();

                        if (temp != null && page > 0)
                        {
                            temp = temp.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        if (temp != null && (depth == 0 || depth > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.Childs = this.GetArticleByParrent(item.ID, depth, current_level + 1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<ArticleViewModel> GetHotNews(int parrent, int limit = 4)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == lang && m.PARENT_ID == parrent
                                    orderby m.FEATURED descending, m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ICON = m.ICON,
                                        DATE_POST = m.DATE_POST,
                                        CREATED = m.CREATED,
                                        STATUS = (int)m.STATUS
                                    };
                        return query.Skip(0).Take(limit).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<ArticleViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "", int[] not_ins = null)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        CID = (int)m.CID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED,
                                        DEAD_LINE = m.DEAD_LINE,
                                    };
                        //var result = query.ToList();
                        if (not_ins != null && not_ins.Count() > 0)
                        {
                            query = query.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = from a in query
                                     where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (query.Count() > 0 && page > 0 && perpage > 0)
                        {
                            return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }
                        else
                        {
                            return query.ToList();
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi", int[] not_ins = null)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var result = from m in context.WZ_ARTICLES
                                     from mc in context.WZ_ARTICLES_CONTENT
                                     where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                     select m;
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId);
                        }

                        if (not_ins != null && not_ins.Count() > 0)
                        {
                            result = result.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<ArticleViewModel> GetSearchContent(Hashtable searchParams)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        CID = (int)m.CID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        PRIORITY = (int)m.PRIORITY,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        LOCATION = m.LOCATION,
                                        CREATED = m.CREATED,
                                        DEAD_LINE = m.DEAD_LINE,
                                        DATE_POST = m.DATE_POST,
                                        OPEN_DAY = m.OPEN_DAY,
                                        MODIFIED = m.MODIFIED
                                    };
                        if (searchParams.ContainsKey("parent"))
                        {
                            int parent = (int)searchParams["parent"];
                            query = query.Where(a => a.PARENT_ID == parent);
                        }

                        if (searchParams.ContainsKey("deadline"))
                        {
                            int deadline = (int)searchParams["deadline"];
                            if (deadline==1)
                                query = query.Where(a => a.DEAD_LINE > DateTime.Now);
                        }

                        if (searchParams.ContainsKey("not_ins"))
                        {
                            int[] not_ins = searchParams["not_ins"] as int[];
                            if (not_ins != null && not_ins.Count() > 0)
                                query = query.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("ins"))
                        {
                            int[] ins = searchParams["ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("parent_ins"))
                        {
                            int[] ins = searchParams["parent_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.PARENT_ID));
                        }

                        if (searchParams.ContainsKey("parent_not_ins"))
                        {
                            int[] ins = searchParams["parent_not_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => !ins.Contains((int)a.PARENT_ID));
                        }

                        if (searchParams.ContainsKey("term"))
                        {
                            int term = (int)searchParams["term"];
                            if (term > 0)
                                query = query.Where(a => a.CID == term);
                        }

                        if (searchParams.ContainsKey("dateFrom") && !string.IsNullOrEmpty((string)searchParams["dateFrom"]))
                        {
                            string dateFrom = (string)searchParams["dateFrom"];
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("dateTo") && !string.IsNullOrEmpty((string)searchParams["dateTo"]))
                        {
                            string dateTo = (string)searchParams["dateTo"];
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("searchText"))
                        {
                            string searchText = (string)searchParams["searchText"];
                            if (!string.IsNullOrEmpty(searchText))
                                query = from a in query
                                        where (a.TITLE.ToLower().Contains(searchText.ToLower()) || a.DESCRIPTION.ToLower().Contains(searchText.ToLower()) || a.CONTENT.ToLower().Contains(searchText.ToLower()))
                                        select a;
                        }

                        if (searchParams.ContainsKey("sortby"))
                        {
                            var sortby = searchParams["sortby"] as Hashtable;
                            int i = 0;
                            string strSort = "";
                            foreach (DictionaryEntry sort in sortby)
                            {
                                strSort += i == 0 ? (sort.Key + " " + sort.Value) : ("," + sort.Key + " " + sort.Value);
                                i++;
                            }

                            if (!string.IsNullOrEmpty(strSort))
                                query = query.OrderBy(strSort);
                        }
                        else
                        {
                            query = query.OrderByDescending(a => a.CREATED);
                        }

                        //query = query.OrderByDescending(a => a.CREATED);

                        if (searchParams.ContainsKey("page") && searchParams.ContainsKey("perpage"))
                        {
                            int page = (int)searchParams["page"];
                            int perpage = (int)searchParams["perpage"];

                            if (query.Count() > 0 && page > 0)
                            {
                                return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                            }
                        }
                        else 
                        {
                            return query.ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(Hashtable searchParams)
            {
                try
                {
                    string lang = StringHelper.language();

                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var result = from m in context.WZ_ARTICLES
                                     from mc in context.WZ_ARTICLES_CONTENT
                                     where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                     select m;
                        if (searchParams.ContainsKey("parent"))
                        {
                            int parent = (int)searchParams["parent"];
                            result = result.Where(a => a.PARENT_ID == parent);
                        }

                        if (searchParams.ContainsKey("term"))
                        {
                            int term = (int)searchParams["term"];
                            if (term > 0)
                                result = result.Where(a => a.CID == term);
                        }

                        if (searchParams.ContainsKey("not_ins"))
                        {
                            int[] not_ins = searchParams["not_ins"] as int[];
                            if (not_ins != null && not_ins.Count() > 0)
                            {
                                result = result.Where(a => !not_ins.Contains((int)a.ID));
                            }
                        }

                        if (searchParams.ContainsKey("ins"))
                        {
                            int[] ins = searchParams["ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                result = result.Where(a => ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("parent_ins"))
                        {
                            int[] ins = searchParams["parent_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                result = result.Where(a => ins.Contains((int)a.PARENT_ID));
                        }

                        if (searchParams.ContainsKey("parent_not_ins"))
                        {
                            int[] ins = searchParams["parent_not_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                result = result.Where(a => !ins.Contains((int)a.PARENT_ID));
                        }

                        if (searchParams.ContainsKey("dateFrom") && !string.IsNullOrEmpty((string)searchParams["dateFrom"]))
                        {
                            string dateFrom = (string)searchParams["dateFrom"];
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("dateTo") && !string.IsNullOrEmpty((string)searchParams["dateTo"]))
                        {
                            string dateTo = (string)searchParams["dateTo"];
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("searchText"))
                        {
                            string searchText = (string)searchParams["searchText"];
                            searchText = searchText.ToLower();
                            if (!string.IsNullOrEmpty(searchText))
                                result = from a in result
                                         from ac in context.WZ_ARTICLES_CONTENT
                                         where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.DESCRIPTION.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                         select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<ArticleViewModel> GetSearchReport(int parrent, int year_report, List<int> in_years, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "", int other = 0)
            {
                try
                {
                    if (in_years != null && in_years.Count == 0)
                        return null;

                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED,
                                        YEAR_REPORT = (int)m.YEAR_REPORT
                                    };
                        //var result = query.ToList();
                        if (other > 0)
                        {
                            query = query.Where(m=>m.ID != other);
                        }
                        if (year_report > 0) {
                            query = (from m in query where m.YEAR_REPORT == year_report select m);
                        }
                        if (in_years != null && in_years.Count() > 0)
                        {
                            query = from m in query where in_years.Contains((int)m.YEAR_REPORT) select m;
                        }

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = (from a in query
                                      where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText))
                                      select a);
                        }

                        if (query != null && page > 0)
                        {
                            query = query.Skip((page - 1) * perpage).Take(perpage);
                        }
                        var result = query.ToList();
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchReport(int parentId, int year_report, List<int> in_years, string fromDate, string toDate, string searchText, string lang = "vi", int other=0)
            {
                try
                {
                    if (in_years != null && in_years.Count == 0)
                        return 0;

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES
                                     where a.STATUS == Constants.SHOW
                                     select a;

                        if (other > 0)
                        {
                            result = result.Where(m => m.ID != other);
                        }
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId);
                        }

                        if (year_report > 0)
                        {
                            result = result.Where(m=>m.YEAR_REPORT == year_report);
                        }
                        if (in_years != null && in_years.Count() > 0)
                        {
                            result = from m in result where in_years.Contains((int)m.YEAR_REPORT) select m;
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //Count list article for pagination
            public int CountArticlesByParentId(int parentId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticle = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId && a.STATUS == Constants.SHOW).OrderByDescending(a => a.ID).ToList();
                        if (lstArticle != null)
                        {
                            return lstArticle.Count;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return 0;
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public ArticleViewModel GetArticleById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        CID = (int)n.CID,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        DEAD_LINE = n.DEAD_LINE,
                                        DATE_POST = n.DATE_POST,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        IS_DELETE = (int)n.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id">ID ARTICLE</param>
            /// <returns></returns>
            public ArticleViewModel GetArticleContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SUB_TITLE = nc.SUB_TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        THUMBNAIL = nc.IMAGE,
                                        CONTENT = nc.CONTENT,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        DATE_POST = n.DATE_POST,
                                        DEAD_LINE = n.DEAD_LINE,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        CID = (int)n.CID,
                                        IS_DELETE = (int)n.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public String GetNodeLink(int id, string lang = "") 
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == lang
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            return node.LINK;
                        }
                    }
                }
                catch { }
                return string.Empty;
            }

            public ArticleViewModel GetPreviousNode(int id, int parent, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where n.PARENT_ID == parent && n.ID < id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    orderby n.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SUB_TITLE = nc.SUB_TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        THUMBNAIL = nc.IMAGE,
                                        CONTENT = nc.CONTENT,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        DATE_POST = n.DATE_POST,
                                        MODIFIED = n.MODIFIED,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        CID = (int)n.CID,
                                        IS_DELETE = (int)n.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public ArticleViewModel GetNextNode(int id, int parent, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where n.PARENT_ID == parent && n.ID > id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    orderby n.CREATED ascending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SUB_TITLE = nc.SUB_TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        THUMBNAIL = nc.IMAGE,
                                        CONTENT = nc.CONTENT,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        DATE_POST = n.DATE_POST,
                                        MODIFIED = n.MODIFIED,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        CID = (int)n.CID,
                                        IS_DELETE = (int)n.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)n.IS_CHANGE_SLUG,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public List<int> GetListArticleIdByParentId(int parentId, string lang)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticle = from article in context.WZ_ARTICLES
                                         where article.PARENT_ID == parentId
                                         //orderby article.datepost descending
                                         orderby article.CREATED descending
                                         select (int)article.ID;

                        return lstArticle.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public ArticleViewModel AdminGetArticalById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = (from a in context.WZ_ARTICLES
                                       where (a.ID == id)
                                       select new ArticleViewModel
                                       {
                                           ID = (int)a.ID,
                                           CID = (int)a.CID,
                                           PARENT_ID = (int)a.PARENT_ID,
                                           TEMPLATE = a.TEMPLATE,
                                           STATUS = (int)a.STATUS,
                                           MODIFIED = a.MODIFIED,
                                           CREATED = a.CREATED,
                                           DEAD_LINE = a.DEAD_LINE,
                                           DATE_POST = a.DATE_POST,
                                           OPEN_DAY = a.OPEN_DAY,
                                           LOCATION = a.LOCATION,
                                           PRIORITY = (int)a.PRIORITY,
                                           FEATURED = (int)a.FEATURED,
                                           MAIN_MENU = (int)a.MAIN_MENU,
                                           ADMIN_SUBMENU = (int)a.ADMIN_SUBMENU,
                                           YEAR_REPORT = (int)a.YEAR_REPORT,
                                           VIEWS = (int)a.VIEWS,
                                           IS_DELETE = (int)a.IS_DELETE,
                                           IS_CHANGE_SLUG = (int)a.IS_CHANGE_SLUG,
                                           ICON = a.ICON
                                       }).FirstOrDefault();

                        if (article != null)
                            article.ContentTranslates = this.GetContentTranslates(article.ID);

                        return article;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(ArticleViewModel article)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_ARTICLES.Where(a => a.ID == article.ID).SingleOrDefault();
                        if (node != null)
                        {
                            node.STATUS = article.STATUS;
                            node.CID = article.CID;
                            node.PARENT_ID = article.PARENT_ID;
                            node.YEAR_REPORT = article.YEAR_REPORT;
                            node.MAIN_MENU = article.MAIN_MENU;
                            node.PRIORITY = article.PRIORITY;
                            node.FEATURED = article.FEATURED;
                            node.ICON = article.ICON;
                            node.MODIFIED = DateTime.Now;
                            node.ADMIN_SUBMENU = article.ADMIN_SUBMENU;
                            node.DEAD_LINE = article.DEAD_LINE;
                            node.OPEN_DAY = article.OPEN_DAY;
                            node.CREATED = article.CREATED;
                            node.LOCATION = article.LOCATION;

                            Object accountObj = AuthorizeActionFilter.UserIsAuthenticated();
                            AdminAccountModel account = (AdminAccountModel)accountObj;
                            if (account.USERNAME == "root")
                            {
                                node.IS_DELETE = article.IS_DELETE;
                                node.IS_CHANGE_SLUG = article.IS_CHANGE_SLUG;
                                node.TEMPLATE = article.TEMPLATE;
                            }

                            context.SaveChanges();

                            //update content translates
                            if (article.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, ArticleContent> item in article.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == article.ID && a.LANG == content_translate.LANG).SingleOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.TITLE))
                                    {
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SUB_TITLE = content_translate.SUB_TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.CONTENT = content_translate.CONTENT;
                                        node_content_lang.LINK = content_translate.LINK;
                                        node_content_lang.ATTACHMENT = content_translate.ATTACHMENT;
                                        if (!string.IsNullOrEmpty(content_translate.IMAGE))
                                        {
                                            node_content_lang.IMAGE = content_translate.IMAGE;
                                        }
                                        if (!string.IsNullOrEmpty(content_translate.IMAGE_TABLET))
                                        {
                                            node_content_lang.IMAGE_TABLET = content_translate.IMAGE_TABLET;
                                        }
                                        if (!string.IsNullOrEmpty(content_translate.IMAGE_MOBILE))
                                        {
                                            node_content_lang.IMAGE_MOBILE = content_translate.IMAGE_MOBILE;
                                        }

                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        node_content_lang = new WZ_ARTICLES_CONTENT();
                                        node_content_lang.NID = article.ID;
                                        node_content_lang.LANG = content_translate.LANG;
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SUB_TITLE = content_translate.SUB_TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.CONTENT = content_translate.CONTENT;
                                        node_content_lang.LINK = content_translate.LINK;
                                        node_content_lang.ATTACHMENT = content_translate.ATTACHMENT;
                                        if (!string.IsNullOrEmpty(content_translate.IMAGE))
                                        {
                                            node_content_lang.IMAGE = content_translate.IMAGE;
                                        }
                                        context.WZ_ARTICLES_CONTENT.AddObject(node_content_lang);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.Message);
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_ARTICLES.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang
                                     select a;
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId );
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list article by paging
            public List<ArticleViewModel> GetArticleByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<ArticleViewModel> lstArticlles = new List<ArticleViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES 
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where ac.NID == a.ID && ac.LANG == lang
                                     orderby a.CREATED descending
                                     select new ArticleViewModel {
                                        ID = (int)a.ID,
                                        PARENT_ID = (int)a.PARENT_ID,
                                        CREATED = a.CREATED,
                                        MODIFIED = a.MODIFIED,
                                        DEAD_LINE = a.DEAD_LINE,
                                        DATE_POST = a.DATE_POST,
                                        TEMPLATE = a.TEMPLATE,
                                        PRIORITY = (int)a.PRIORITY,
                                        FEATURED = (int)a.FEATURED,
                                        YEAR_REPORT = (int)a.YEAR_REPORT,
                                        TITLE = ac.TITLE,
                                        LINK = ac.LINK,
                                        CONTENT = ac.CONTENT,
                                        THUMBNAIL = ac.IMAGE,
                                        MAIN_MENU = (int)a.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)a.ADMIN_SUBMENU,
                                        STATUS = (int)a.STATUS,
                                        CID = (int)a.CID,
                                        IS_DELETE = (int)a.IS_DELETE,
                                        IS_CHANGE_SLUG = (int)a.IS_CHANGE_SLUG,
                                    };

                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId);
                        }
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_ARTICLES_CONTENT
                                     where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        //result = result.OrderByDescending(a=>a.CREATED);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        var temp = result.ToList();

                        if (temp != null)
                        {
                            foreach (var item in temp)
                            {
                                item.ContentTranslates = this.GetContentTranslates(item.ID);
                            }
                        }

                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(ArticleViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_ARTICLES article = new WZ_ARTICLES();
                        if (viewModel != null)
                        {
                            article.ID = this._getNextID();
                            article.CID = viewModel.CID;
                            article.PARENT_ID = viewModel.PARENT_ID;
                            article.TEMPLATE = viewModel.TEMPLATE;
                            article.YEAR_REPORT = viewModel.YEAR_REPORT;
                            article.MAIN_MENU = viewModel.MAIN_MENU;
                            article.ADMIN_SUBMENU = viewModel.ADMIN_SUBMENU;
                            article.STATUS = viewModel.STATUS;
                            article.PRIORITY = viewModel.PRIORITY;
                            article.FEATURED = viewModel.FEATURED;
                            article.ICON = viewModel.ICON;
                            article.CREATED = viewModel.CREATED;
                            article.MODIFIED = DateTime.Now;
                            article.DEAD_LINE = viewModel.DEAD_LINE;
                            article.OPEN_DAY = viewModel.OPEN_DAY;
                            article.DATE_POST = DateTime.Now;
                            article.LOCATION = viewModel.LOCATION;

                            context.WZ_ARTICLES.AddObject(article);
                            context.SaveChanges();

                            if (viewModel.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, ArticleContent> item in viewModel.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_ARTICLES_CONTENT article_content = new WZ_ARTICLES_CONTENT();
                                    article_content.NID = article.ID;
                                    article_content.TITLE = node_content.TITLE;
                                    article_content.SUB_TITLE = node_content.SUB_TITLE;
                                    article_content.SLUG = node_content.SLUG;
                                    article_content.LINK = node_content.LINK;
                                    article_content.DESCRIPTION = node_content.DESCRIPTION;
                                    article_content.CONTENT = node_content.CONTENT;
                                    article_content.IMAGE = node_content.IMAGE;
                                    article_content.IMAGE_TABLET = node_content.IMAGE_TABLET;
                                    article_content.IMAGE_MOBILE = node_content.IMAGE_MOBILE;
                                    article_content.ATTACHMENT = node_content.ATTACHMENT;
                                    article_content.LANG = node_content.LANG;

                                    context.WZ_ARTICLES_CONTENT.AddObject(article_content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)article.ID;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.Message);
                    Logs.WriteLogError(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_ARTICLES_SEQ;
                        //    sequence.WZ_ARTICLES_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ARTICLE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ARTICLE";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        var check = context.WZ_ARTICLES.Where(a => a.ID == newID).SingleOrDefault();
                        if (check != null)
                            return this._getNextID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_ARTICLES.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            context.WZ_ARTICLES.DeleteObject(article);
                            context.SaveChanges();

                            //delete article content translate
                            var content_translates = context.WZ_ARTICLES_CONTENT.Where(ac=>ac.NID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_ARTICLES_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            public bool DeleteContent(int id, string lang)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article_content = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == id && a.LANG == lang).FirstOrDefault();

                        if (article_content != null)
                        {
                            context.WZ_ARTICLES_CONTENT.DeleteObject(article_content);
                            context.SaveChanges();

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            public bool DeleteFile(int nid, string field)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (field == "image_vi")
                        {
                            var article_content = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == "vi").FirstOrDefault();
                            if (article_content != null)
                            {
                                article_content.IMAGE = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "image_en")
                        {
                            var article_content = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == "en").FirstOrDefault();
                            if (article_content != null)
                            {
                                article_content.IMAGE = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "file_vi")
                        {
                            var article_content = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == "vi").FirstOrDefault();
                            if (article_content != null)
                            {
                                article_content.ATTACHMENT = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "file_en")
                        {
                            var article_content = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == "en").FirstOrDefault();
                            if (article_content != null)
                            {
                                article_content.ATTACHMENT = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "iconAdmincp")
                        {
                            var article = context.WZ_ARTICLES.Where(a => a.ID == nid).FirstOrDefault();
                            if (article != null)
                            {
                                article.ICON = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lstId)
            {
                try
                {
                    if (lstId != null)
                    {
                        foreach (int id in lstId)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //Get list menu option
            public string GetSelectList(int selectedId, int? parentId, int currentId = 0, string space = "")
            {
                try
                {
                    var lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        string output = string.Empty;
                        string selected = string.Empty;

                        if (parentId == null) parentId = 0;

                        var articles = from a in context.WZ_ARTICLES
                                       from ac in context.WZ_ARTICLES_CONTENT
                                       where ac.NID == a.ID && a.PARENT_ID == parentId && ac.LANG == lang && !string.IsNullOrEmpty(ac.TITLE) && a.ADMIN_SUBMENU == 1
                                       orderby a.PRIORITY ascending
                                       orderby a.CREATED descending
                                       select new ArticleViewModel{ 
                                            ID = (int)a.ID,
                                            PARENT_ID = (int)a.PARENT_ID,
                                            TEMPLATE = a.TEMPLATE,
                                            TITLE = ac.TITLE,
                                            LINK = ac.LINK
                                       };
                        if (articles != null)
                        {
                            foreach (var item in articles)
                            {
                                selected = item.ID == selectedId ? "selected" : string.Empty;
                                var sub = item.LINK.Split('/');
                                int countsub = sub.Count();

                                switch (countsub)
                                {
                                    case 1:
                                        space = "";
                                        break;
                                    case 2:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ ";
                                        break;
                                    case 3:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ";
                                        break;
                                    case 4:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* ";
                                        break;
                                }

                                //De qui lai option list
                                string title = item.TITLE;
                                if (currentId == item.ID)
                                    output += "<option value='" + item.ID + "' " + selected + " disabled='disabled' >" + space + title + "</option>";
                                else
                                    output += "<option value='" + item.ID + "' " + selected + " >" + space + title + "</option>";

                                output += this.GetSelectList(selectedId, item.ID, currentId, space);

                                

                                //output += this.GetSelectList(selectedId, item.id, space);
                            }
                        }
                        return output;
                    }
                }
                catch (Exception ex)
                {

                }
                return string.Empty;
            }

            /// <summary>
            /// Check if article slug already exist following language
            /// </summary>
            /// <param name="slug">Input slug</param>
            /// <param name="lang">Input language</param>
            /// <returns>True is exist</returns>
            public bool CheckSlug(string slug, string lang, int parent = 0)
            {
                try
                {
                    lang = string.IsNullOrEmpty(lang) ? "vi" : lang;
                    using (var context = new DBContext())
                    {
                        if (parent > 0)
                        {
                            var node = (from a in context.WZ_ARTICLES
                                        from ac in context.WZ_ARTICLES_CONTENT
                                        where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug && a.PARENT_ID == parent
                                        select a).FirstOrDefault();
                            if (node != null) return true;
                        }
                        else
                        {
                            var node = (from a in context.WZ_ARTICLES
                                        from ac in context.WZ_ARTICLES_CONTENT
                                        where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug
                                        select a).FirstOrDefault();
                            if (node != null) return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.ToString());

                    return true;
                }
                return false;
            }

            public void UpdateLinkCustom(int parentId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var parent = this.GetArticleById(parentId);
                        if (parent.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in parent.ContentTranslates)
                            {
                                var parent_content = item.Value;
                                this.UpdateMenuLink(parentId, parent_content.LINK, parent_content.LANG);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }

            //update menu link
            public void UpdateMenuLink(int parentId, string parentLink = "", string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var articles = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId).ToList();
                        if (articles != null)
                        {
                            foreach (var item in articles)
                            {
                                int currentId = (int)item.ID;

                                var nodeContent = (from nc in context.WZ_ARTICLES_CONTENT where nc.NID == item.ID && nc.LANG == lang select nc).SingleOrDefault();

                                if (nodeContent == null) continue;

                                string newLink = nodeContent.SLUG;

                                if (parentLink != string.Empty)
                                {
                                    newLink = parentLink + "/" + newLink;
                                }

                                //Neu thay doi link thi cap nhat lai link cua tat ca cac node con
                                if (newLink != nodeContent.LINK)
                                {
                                    this.UpdateLink(newLink, lang, currentId);
                                    this.UpdateMenuLink(currentId, newLink, lang);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { }
            }

            private void UpdateLink(string link, string lang, int nid)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var ar = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == lang).FirstOrDefault();
                        if (ar != null)
                        {
                            ar.LINK = link;
                            //using (TransactionScope scrope = new TransactionScope())
                            //{
                                context.SaveChanges();
                                return;
                            //}
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            #endregion
        }
    }
}