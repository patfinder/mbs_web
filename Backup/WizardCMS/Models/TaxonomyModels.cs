﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class TaxonomyViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public int REFERANCE_ID { get; set; }
        public int PRIORITY { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class TaxonomyServices
    {
        #region service interface
        public static TaxonomyViewModel Get(int Id)
        {
            try
            {
                return TaxonomyDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(TaxonomyViewModel slideViewModel)
        {
            try
            {
                return TaxonomyDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return TaxonomyDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(TaxonomyViewModel slideViewModel)
        {
            try
            {
                return TaxonomyDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return TaxonomyDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TaxonomyViewModel> GetAll()
        {
            try
            {
                return TaxonomyDAL.Instance.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return TaxonomyDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TaxonomyViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return TaxonomyDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class TaxonomyDAL
        {
            #region static instance
            private static TaxonomyDAL _instance;

            static public TaxonomyDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new TaxonomyDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public TaxonomyViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_TAXONOMY
                                where a.ID == Id
                                select new TaxonomyViewModel
                                {
                                    ID = (int)a.ID,
                                    NAME = a.NAME,
                                    STATUS = (int)a.STATUS,
                                    PRIORITY = (int)a.PRIORITY,
                                    REFERANCE_ID = (int)a.REFERANCE_ID,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(TaxonomyViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_TAXONOMY.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.NAME = viewModel.NAME;
                        node.REFERANCE_ID = viewModel.REFERANCE_ID;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(TaxonomyViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_TAXONOMY node = new WZ_TAXONOMY();
                        node.ID = this._getNextID();
                        node.NAME = viewModel.NAME;
                        node.REFERANCE_ID = viewModel.REFERANCE_ID;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;
                        node.CREATED = DateTime.Now;

                        context.WZ_TAXONOMY.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_TAXONOMY.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_TAXONOMY.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_TAXONOMY.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_TAXONOMY
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where SqlMethods.Like(a.NAME.ToLower(), "%" + searchText + "%")
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<TaxonomyViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<TaxonomyViewModel> lsTaxonomyViewModels = new List<TaxonomyViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_TAXONOMY
                                     select new TaxonomyViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME  = a.NAME,
                                         REFERANCE_ID = (int)a.REFERANCE_ID,
                                         PRIORITY = (int)a.PRIORITY,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where SqlMethods.Like(a.NAME.ToLower(), "%" + searchText + "%")
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<TaxonomyViewModel> GetAll()
            {
                try
                {
                    List<TaxonomyViewModel> lsTaxonomyViewModels = new List<TaxonomyViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_TAXONOMY
                                     select new TaxonomyViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME = a.NAME,
                                         PRIORITY = (int)a.PRIORITY,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }
            
            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "TAXONOMY" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "TAXONOMY";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        var check = context.WZ_TAXONOMY.Where(a=>a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
        #endregion
    }
}