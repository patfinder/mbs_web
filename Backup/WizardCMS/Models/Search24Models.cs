﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class MessageViewModel
    {
        public int ID { get; set; }
        public string TITLE { get; set; }
        public string THUMBNAIL { get; set; }
        public string DESCRIPTION { get; set; }
        public string CONTENT { get; set; }
        public string AUTHOR { get; set; }
        public string SOURCE { get; set; }
        public int CATEGORY_ID { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
        public byte[] IMAGE { get; set; }
    }

    public static class Search24Services
    {
        #region service interface
        public static MessageViewModel Get(int Id)
        {
            try
            {
                return Search24DAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(MessageViewModel slideViewModel)
        {
            try
            {
                return Search24DAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return Search24DAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(MessageViewModel slideViewModel)
        {
            try
            {
                return Search24DAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return Search24DAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MessageViewModel> GetAll()
        {
            try
            {
                return Search24DAL.Instance.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return Search24DAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MessageViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return Search24DAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MessageViewModel> GetData(int[] categories, int page, int perpage, string searchText = "", int[] not_ins = null, string to_date = "", string dateFrom = "")
        {
            try
            {
                return Search24DAL.Instance.GetData(categories, page, perpage, searchText, not_ins, to_date, dateFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountData(int[] categories, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return Search24DAL.Instance.CountData(categories, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class Search24DAL
        {
            #region static instance
            private static Search24DAL _instance;

            static public Search24DAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new Search24DAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public MessageViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new Search24Context())
                    {
                        return (from a in context.Messages
                                where a.MessageID == Id
                                select new MessageViewModel
                                {
                                    ID = (int)a.MessageID,
                                    TITLE = a.MsgTitle,
                                    DESCRIPTION = a.MsgLead,
                                    CONTENT = a.MsgContent,
                                    THUMBNAIL = a.MsgHomeImage,
                                    IMAGE = a.MsgHomeImage_Byte,
                                    AUTHOR = a.MsgAuthor,
                                    SOURCE = a.MsgType,
                                    STATUS = a.MsgStatus != null ? (int)a.MsgStatus : 0,
                                    CATEGORY_ID = (int)a.CategoryID != null ? (int)a.CategoryID : 0,
                                    CREATED = a.MsgPostDate != null ? (DateTime)a.MsgPostDate : new DateTime()
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(MessageViewModel viewModel)
            {
                try
                {
                    using (var context = new Search24Context())
                    {
                        var node = context.Messages.Where(a => a.MessageID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.MsgTitle = viewModel.TITLE;
                        node.MsgLead = viewModel.DESCRIPTION;
                        node.MsgContent= viewModel.CONTENT;
                        node.CategoryID = viewModel.CATEGORY_ID;
                        node.MsgStatus = viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(MessageViewModel viewModel)
            {
                try
                {
                    using (var context = new Search24Context())
                    {
                        Message node = new Message();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new Search24Context())
                    {
                        var node = (context.Messages.Where(a => a.MessageID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.Messages.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new Search24Context())
                    {
                        var node = (context.Messages.Where(a => a.MessageID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.MsgStatus = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new Search24Context())
                    {
                        var result = from a in context.Messages
                                     where Constants.CATEGORIES_THE_GIOI.Contains((int)a.CategoryID) || Constants.CATEGORIES_THI_TRUONG.Contains((int)a.CategoryID) || Constants.CATEGORIES_VI_MO.Contains((int)a.CategoryID)
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.MsgPostDate >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.MsgPostDate <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where SqlMethods.Like(a.MsgTitle.ToLower(), "%" + searchText + "%")
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<MessageViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<MessageViewModel> lsMessageViewModels = new List<MessageViewModel>();

                    using (var context = new Search24Context())
                    {
                        var result = from a in context.Messages
                                     where Constants.CATEGORIES_THE_GIOI.Contains((int)a.CategoryID) || Constants.CATEGORIES_THI_TRUONG.Contains((int)a.CategoryID) || Constants.CATEGORIES_VI_MO.Contains((int)a.CategoryID)
                                     select new MessageViewModel
                                     {
                                         ID = (int)a.MessageID,
                                         TITLE = a.MsgTitle,
                                         DESCRIPTION = a.MsgLead,
                                         THUMBNAIL = a.MsgHomeImage,
                                         CONTENT = a.MsgContent,
                                         STATUS = a.MsgStatus != null ? (int)a.MsgStatus : 0,
                                         CATEGORY_ID = (int)a.CategoryID != null ? (int)a.CategoryID : 0,
                                         CREATED = a.MsgPostDate != null ? (DateTime)a.MsgPostDate : new DateTime()
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.TITLE.ToLower().Contains(searchText)
                                     select a;
                        }

                        //if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        //if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        //result = result.OrderBy(sortBy.ToUpper() + " " + sortType);
                        result = result.OrderByDescending(m => m.CREATED);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<MessageViewModel> GetAll()
            {
                try
                {
                    List<MessageViewModel> lsMessageViewModels = new List<MessageViewModel>();

                    using (var context = new Search24Context())
                    {
                        var result = from a in context.Messages
                                     select new MessageViewModel
                                     {
                                         ID = (int)a.MessageID,
                                         TITLE = a.MsgTitle,
                                         DESCRIPTION = a.MsgLead,
                                         CONTENT = a.MsgContent,
                                         STATUS = a.MsgStatus != null ? (int)a.MsgStatus : 0,
                                         CATEGORY_ID = (int)a.CategoryID != null ? (int)a.CategoryID : 0,
                                         CREATED = a.MsgPostDate != null ? (DateTime)a.MsgPostDate : new DateTime()
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<MessageViewModel> GetData(int[] categories, int page, int perpage, string searchText = "", int[] not_ins = null, string to_date = "", string dateFrom = "" )
            {
                try
                {
                    List<MessageViewModel> lsMessageViewModels = new List<MessageViewModel>();

                    using (var context = new Search24Context())
                    {
                        var result = from a in context.Messages
                                     where a.MsgStatus == 2 //only get approve post
                                     select new MessageViewModel
                                     {
                                         ID = (int)a.MessageID,
                                         TITLE = a.MsgTitle,
                                         DESCRIPTION = a.MsgLead,
                                         //CONTENT = a.MsgContent,
                                         THUMBNAIL = a.MsgHomeImage,
                                         //IMAGE = a.MsgHomeImage_Byte,
                                         AUTHOR = a.MsgAuthor,
                                         SOURCE = a.MsgType,
                                         STATUS = a.MsgStatus != null ? (int)a.MsgStatus : 0,
                                         CATEGORY_ID = (int)a.CategoryID != null ? (int)a.CategoryID : 0,
                                         CREATED = a.MsgPostDate != null ? (DateTime)a.MsgPostDate : DateTime.Now
                                     };

                        if (categories != null && categories.Count() > 0)
                        {
                            result = result.Where(m => categories.Contains(m.CATEGORY_ID));
                        }

                        if (not_ins != null && not_ins.Count() > 0)
                        {
                            result = result.Where(m => !not_ins.Contains(m.ID));
                        }

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (String.IsNullOrWhiteSpace(to_date) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(to_date.Trim(), "dd-MM-yyyy HH:mm:ss");
                                //toDate = DatetimeHelper.GetStartOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where SqlMethods.Like(a.TITLE.ToLower(), "%" + searchText + "%")
                                     select a;
                        }

                        result = result.OrderByDescending(m => m.CREATED);

                        if (page > 0 && perpage > 0)
                        {
                            result = result.Skip((page - 1) * perpage).Take(perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public int CountData(int[] categories, string searchText = "", string dateFrom="", string dateTo="")
            {
                try
                {
                    List<MessageViewModel> lsMessageViewModels = new List<MessageViewModel>();

                    using (var context = new Search24Context())
                    {
                        var result = from a in context.Messages
                                     where a.MsgStatus == 2 //only get approve post
                                     select new MessageViewModel
                                     {
                                         ID = (int)a.MessageID,
                                         TITLE = a.MsgTitle,
                                         STATUS = a.MsgStatus != null ? (int)a.MsgStatus : 0,
                                         CATEGORY_ID = (int)a.CategoryID != null ? (int)a.CategoryID : 0,
                                         CREATED = a.MsgPostDate != null ? (DateTime)a.MsgPostDate : new DateTime()
                                     };

                        if (categories != null && categories.Count() > 0)
                        {
                            result = result.Where(m => categories.Contains(m.CATEGORY_ID));
                        }

                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.TITLE.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return 0;
            }

            #endregion
        }
        #endregion
    }
}