﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class MajorViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string OTHER_MAJOR { get; set; }
        public string SOCIAL_ACTIVITIES { get; set; }
        public string ARTICLES { get; set; }
        public string SOFTWARES { get; set; }
        public string TALENT { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReMajorServices
    {
        #region service interface
        public static int Insert(MajorViewModel viewModel)
        {
            try
            {
                return MajorDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(MajorViewModel viewModel)
        {
            try
            {
                return MajorDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return MajorDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MajorViewModel Get(int id)
        {
            try
            {
                return MajorDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MajorViewModel GetByUserId(int uid)
        {
            try
            {
                return MajorDAL.Instance.GetByUserId(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MajorViewModel> GetAll(int uid)
        {
            try
            {
                return MajorDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class MajorDAL
        {
            #region static instance
            private static MajorDAL _instance;

            static public MajorDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new MajorDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public MajorViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_MAJOR
                                where a.ID == Id
                                select new MajorViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    OTHER_MAJOR = a.OTHER_MAJOR,
                                    SOCIAL_ACTIVITIES = a.SOCIAL_ACTIVITIES,
                                    SOFTWARES = a.SOFTWARES,
                                    TALENT = a.TALENT,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public MajorViewModel GetByUserId(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_MAJOR
                                where a.USER_ID == uid
                                select new MajorViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    OTHER_MAJOR = a.OTHER_MAJOR,
                                    SOCIAL_ACTIVITIES = a.SOCIAL_ACTIVITIES,
                                    SOFTWARES = a.SOFTWARES,
                                    TALENT = a.TALENT,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(MajorViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_MAJOR.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        //node.PASSWORD = viewModel.PASSWORD;
                        node.OTHER_MAJOR = viewModel.OTHER_MAJOR;
                        node.SOCIAL_ACTIVITIES = viewModel.SOCIAL_ACTIVITIES;
                        node.SOFTWARES = viewModel.SOFTWARES;
                        node.TALENT = viewModel.TALENT;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<MajorViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_MAJOR
                                     where a.USER_ID == uid
                                     select new MajorViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         OTHER_MAJOR = a.OTHER_MAJOR,
                                         SOCIAL_ACTIVITIES = a.SOCIAL_ACTIVITIES,
                                         SOFTWARES = a.SOFTWARES,
                                         TALENT = a.TALENT,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_MAJOR.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_MAJOR.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(MajorViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_MAJOR node = new WZ_RE_MAJOR();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.OTHER_MAJOR = viewModel.OTHER_MAJOR;
                        node.SOCIAL_ACTIVITIES = viewModel.SOCIAL_ACTIVITIES;
                        node.ARTICLES = viewModel.ARTICLES;
                        node.TALENT = viewModel.TALENT;
                        node.SOFTWARES = viewModel.SOFTWARES;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_MAJOR.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_MAJOR" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_MAJOR";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_MAJOR.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}