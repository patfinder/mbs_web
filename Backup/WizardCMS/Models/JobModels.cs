﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;
using System.Collections;

namespace WizardCMS.Models
{
    public class JobViewModel
    {
        public int ID { get; set; }
        public int CID { get; set; } //Danh muc(vi du: can bo quan ly, chuyen vien, nhan vien...)
        public int TERM_ID { get; set; }//Nganh nghe (vi du: it, ke toan...)
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public int NUM { get; set; } //So luong can tuyen
        public string TITLE { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }

        public string TERM_NAME { get; set; }
        public string CATEGORY_NAME { get; set; }

        public DateTime CREATED { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime DEADLINE { get; set; }
        public DateTime POST_DAY { get; set; }

        //public List<JobContent> ContentTranslates { get; set; }
        public Dictionary<string, JobContent> ContentTranslates { get; set; }
        public List<BranchViewModel> WorkPlaces { get; set; }
        public List<WZ_CT_PROVINCE> Provinces { get; set; }

        //Constructor
        public JobViewModel()
        {
            this.ContentTranslates = new Dictionary<string, JobContent>();
            this.WorkPlaces = new List<BranchViewModel>();
            this.Provinces = new List<WZ_CT_PROVINCE>();
        }
    }

    public class JobContent
    {
        public int NID { get; set; }
        public string LANG { get; set; }
        public string TITLE { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQUIRE { get; set; }
        public string BENEFIT { get; set; }
        public string DOC_REQUIRE { get; set; }
        public string DEADLINE { get; set; }
        public string PLACE_CV { get; set; }

        public JobContent()
        { }
    }

    public static class JobServices
    {
        #region services interface
        public static JobContent GetFormContentByLang(Dictionary<string, JobContent> lstFormContent, string lang = "vi")
        {
            try
            {
                if (lstFormContent != null && lstFormContent.ContainsKey(lang))
                {
                    return lstFormContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static Dictionary<string, JobContent> GetContentTranslates(int NID)
        {
            try
            {
                return JobDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<JobViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return JobDAL.Instance.GetSearchContent(parrent, page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(int parenNID, string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return JobDAL.Instance.GetCountSearchContent(parenNID, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<JobViewModel> GetSearchContent(Hashtable searchParams)
        {
            try
            {
                return JobDAL.Instance.GetSearchContent(searchParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(Hashtable searchParams)
        {
            try
            {
                return JobDAL.Instance.GetCountSearchContent(searchParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static JobViewModel GetById(int id)
        {
            try
            {
                return JobDAL.Instance.GetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static JobViewModel GetContentById(int id, string lang = "")
        {
            try
            {
                return JobDAL.Instance.GetContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static JobViewModel AdminGetById(int id)
        {
            try
            {
                return JobDAL.Instance.AdminGetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(JobViewModel article)
        {
            try
            {
                return JobDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return JobDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parenNID, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return JobDAL.Instance.GetCount(parenNID, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<JobViewModel> GetByPaging(int parenNID, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return JobDAL.Instance.GetByPaging(parenNID, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="viewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(JobViewModel viewModel)
        {
            try
            {
                return JobDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertWorkPlaces(string BranchIds, int jobId)
        {
            try
            {
                return JobDAL.Instance.InsertWorkPlaces(BranchIds, jobId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertJobProvinces(string provinces, int jobId)
        {
            try
            {
                return JobDAL.Instance.InsertJobProvinces(provinces, jobId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return JobDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lsNID)
        {
            try
            {
                return JobDAL.Instance.DeleteList(lsNID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckSlug(string slug, string lang)
        {
            try
            {
                return JobDAL.Instance.CheckSlug(slug, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public class JobDAL
        {
            private static JobDAL _instance;

            static public JobDAL Instance { 
                get {
                    if (_instance == null)
                        _instance = new JobDAL();

                    return _instance;
                }
            }

            #region constructor
            public JobDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
           
            public Dictionary<string, JobContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, JobContent> result = new Dictionary<string, JobContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_RE_JOB_CONTENT
                                    where ct.NID == NID
                                    orderby ct.LANG descending
                                    select new JobContent
                                    {
                                        NID = (int)ct.NID,
                                        TITLE = ct.TITLE,
                                        DESCRIPTION = ct.DESCRIPTION,
                                        SLUG = ct.SLUG,
                                        LANG = ct.LANG,
                                        REQUIRE = ct.REQUIRE,
                                        BENEFIT = ct.BENEFIT,
                                        DOC_REQUIRE = ct.DOC_REQUIRE,
                                        DEADLINE = ct.DEADLINE,
                                        PLACE_CV = ct.PLACE_CV,
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            public List<JobViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText="", string dateFrom="", string dateTo="")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_RE_JOB
                                    from mc in context.WZ_RE_JOB_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new JobViewModel
                                    {
                                        ID = (int)m.ID,
                                        TITLE = mc.TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        SLUG = mc.SLUG,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED,
                                        DEADLINE = m.DEADLINE,
                                        POST_DAY = m.POST_DAY != null ? (DateTime)m.POST_DAY : new DateTime()
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = from a in query
                                    where a.TITLE.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText)
                                    select a;
                        }

                        if (query.Count() > 0 && page > 0)
                        {
                            return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        return query.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(int parenNID, string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var result = from m in context.WZ_RE_JOB
                                     from mc in context.WZ_RE_JOB_CONTENT
                                     where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                     select m;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_RE_JOB_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.DESCRIPTION.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<JobViewModel> GetSearchContent(Hashtable searchParams)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();

                        List<decimal> branchs = new List<decimal>();
                        List<decimal> jobs = new List<decimal>();
                        /*
                        if (searchParams.ContainsKey("province"))
                        {
                            string province_id = (string)searchParams["province"];
                            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(province_id))
                            {
                                var items = context.WZ_BRANCH.Where(b=>b.PROVINCE_ID == province_id).ToList();
                                if (items != null)
                                {
                                    foreach (var b in items)
                                    {
                                        branchs.Add(b.ID);
                                    }
                                }
                            }
                        }

                        if (branchs.Count > 0)
                        {
                            var items = context.WZ_RE_JOB_BRANCH.Where(b => branchs.Contains(b.BRANCH_ID)).ToList();
                            if (items != null)
                            {
                                foreach (var b in items)
                                {
                                    jobs.Add(b.JOB_ID);
                                }
                            }
                        }
                        */
                        if (searchParams.ContainsKey("province"))
                        {
                            string province_id = (string)searchParams["province"];
                            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(province_id))
                            {
                                var items = context.WZ_RE_JOB_PROVINCE.Where(p => p.PROVINCE_ID == province_id).ToList();
                                if (items != null)
                                {
                                    foreach (var b in items)
                                    {
                                        jobs.Add(b.JOB_ID);
                                    }
                                }
                            }
                        }

                        var query = from m in context.WZ_RE_JOB
                                    from mc in context.WZ_RE_JOB_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.POST_DAY descending
                                    select new JobViewModel
                                    {
                                        ID = (int)m.ID,
                                        NUM = (int)m.NUM,
                                        CID = (int)m.CID,
                                        TERM_ID = (int)m.TERM_ID,
                                        PRIORITY = (int)m.PRIORITY,
                                        TITLE = mc.TITLE,
                                        SLUG = mc.SLUG,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CREATED = m.CREATED,
                                        DEADLINE = m.DEADLINE,
                                        MODIFIED = m.MODIFIED,
                                        POST_DAY = m.POST_DAY != null ? (DateTime)m.POST_DAY : new DateTime()
                                    };
                        if (searchParams.ContainsKey("cid"))
                        {
                            int cid = (int)searchParams["cid"];
                            if (cid > 0)
                                query = query.Where(a => a.CID == cid);
                        }

                        if (searchParams.ContainsKey("term_id"))
                        {
                            int term_id = (int)searchParams["term_id"];
                            if (term_id > 0)
                                query = query.Where(a => a.TERM_ID == term_id);
                        }

                        if (searchParams.ContainsKey("deadline"))
                        {
                            int deadline = (int)searchParams["deadline"];
                            if (deadline == 1)
                                query = query.Where(a => a.DEADLINE > DateTime.Now);
                        }

                        if (searchParams.ContainsKey("province"))
                        {
                            string province_id = (string)searchParams["province"];
                            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(province_id))
                            {
                                query = query.Where(a => jobs.Contains(a.ID));
                            }
                        }

                        if (searchParams.ContainsKey("not_ins"))
                        {
                            int[] not_ins = searchParams["not_ins"] as int[];
                            if (not_ins != null && not_ins.Count() > 0)
                                query = query.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("ins"))
                        {
                            int[] ins = searchParams["ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("term"))
                        {
                            int term = (int)searchParams["term"];
                            if (term > 0)
                                query = query.Where(a => a.TERM_ID == term);
                        }

                        if (searchParams.ContainsKey("dateFrom") && !string.IsNullOrEmpty( (string)searchParams["dateFrom"] ) )
                        {
                            string dateFrom = (string)searchParams["dateFrom"] ;
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.POST_DAY >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("dateTo") && !string.IsNullOrEmpty((string)searchParams["dateTo"]))
                        {
                            string dateTo = (string)searchParams["dateTo"];
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("searchText"))
                        {
                            string searchText = (string)searchParams["searchText"];
                            if (!string.IsNullOrEmpty(searchText))
                                query = from a in query
                                        where (a.TITLE.ToLower().Contains(searchText.ToLower()) || a.DESCRIPTION.ToLower().Contains(searchText.ToLower()))
                                        select a;
                        }

                        if (searchParams.ContainsKey("page") && searchParams.ContainsKey("perpage"))
                        {
                            int page = (int)searchParams["page"];
                            int perpage = (int)searchParams["perpage"];

                            if (query.Count() > 0 && page > 0)
                            {
                                query = query.Skip((page - 1) * perpage).Take(perpage);
                            }
                        }
                        //else
                        //{
                        //    return query.ToList();
                        //}
                        var result = query.ToList();
                        if (result != null)
                        {
                            result.ForEach(n => n.Provinces = GetJobProvinces(n.ID));
                        }
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(Hashtable searchParams)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();

                        List<decimal> branchs = new List<decimal>();
                        List<decimal> jobs = new List<decimal>();
                        if (searchParams.ContainsKey("province"))
                        {
                            string province_id = (string)searchParams["province"];
                            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(province_id))
                            {
                                var items = context.WZ_RE_JOB_PROVINCE.Where(p => p.PROVINCE_ID == province_id).ToList();
                                if (items != null)
                                {
                                    foreach (var b in items)
                                    {
                                        jobs.Add(b.JOB_ID);
                                    }
                                }
                            }
                        }

                        var query = from m in context.WZ_RE_JOB
                                    from mc in context.WZ_RE_JOB_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new JobViewModel
                                    {
                                        ID = (int)m.ID,
                                        NUM = (int)m.NUM,
                                        CID = (int)m.CID,
                                        TERM_ID = (int)m.TERM_ID,
                                        PRIORITY = (int)m.PRIORITY,
                                        TITLE = mc.TITLE,
                                        SLUG = mc.SLUG,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CREATED = m.CREATED,
                                        DEADLINE = m.DEADLINE,
                                        MODIFIED = m.MODIFIED
                                    };
                        if (searchParams.ContainsKey("cid"))
                        {
                            int cid = (int)searchParams["cid"];
                            if (cid > 0)
                                query = query.Where(a => a.CID == cid);
                        }

                        if (searchParams.ContainsKey("deadline"))
                        {
                            int deadline = (int)searchParams["deadline"];
                            if (deadline == 1)
                                query = query.Where(a => a.DEADLINE > DateTime.Now);
                        }

                        if (searchParams.ContainsKey("province"))
                        {
                            string province_id = (string)searchParams["province"];
                            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(province_id))
                            {
                                query = query.Where(a => jobs.Contains(a.ID));
                            }
                        }

                        if (searchParams.ContainsKey("not_ins"))
                        {
                            int[] not_ins = searchParams["not_ins"] as int[];
                            if (not_ins != null && not_ins.Count() > 0)
                                query = query.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("ins"))
                        {
                            int[] ins = searchParams["ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("term"))
                        {
                            int term = (int)searchParams["term"];
                            if (term > 0)
                                query = query.Where(a => a.TERM_ID == term);
                        }

                        if (searchParams.ContainsKey("dateFrom") && !string.IsNullOrEmpty((string)searchParams["dateFrom"]))
                        {
                            string dateFrom = (string)searchParams["dateFrom"];
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("dateTo") && !string.IsNullOrEmpty((string)searchParams["dateTo"]))
                        {
                            string dateTo = (string)searchParams["dateTo"];
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("searchText"))
                        {
                            string searchText = (string)searchParams["searchText"];
                            if (!string.IsNullOrEmpty(searchText))
                                query = from a in query
                                        where (a.TITLE.ToLower().Contains(searchText.ToLower()) || a.DESCRIPTION.ToLower().Contains(searchText.ToLower()))
                                        select a;
                        }

                        return query.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            public List<WZ_CT_PROVINCE> GetJobProvinces(int JobId)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var query = from p in context.WZ_CT_PROVINCE
                                    from jp in context.WZ_RE_JOB_PROVINCE
                                    where p.PROVINCE_ID == jp.PROVINCE_ID && jp.JOB_ID == JobId
                                    orderby p.PRIORITY select p;

                        return query.ToList();
                    }
                }
                catch { }
                return null;
            }

            public List<BranchViewModel> GetWorkPlaces(int JobId) 
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var query = from b in context.WZ_BRANCH
                                    from bc in context.WZ_BRANCH_CONTENT
                                    from jb in context.WZ_RE_JOB_BRANCH
                                    where bc.NID == b.ID && b.ID == jb.BRANCH_ID && jb.JOB_ID == JobId
                                    select new BranchViewModel
                                    {
                                        ID = (int)b.ID,
                                        NAME = bc !=null ? bc.NAME : "",
                                        DESCRIPTION = bc !=null ? bc.DESCRIPTION : "",
                                        PROVINCE_ID = b.PROVINCE_ID,
                                        DISTRICT_ID = b.DISTRICT_ID,
                                        CREATED = b.CREATED,
                                        TEL = b.TEL,
                                        FAX = b.FAX,
                                        LAT_X = b.LAT_X,
                                        LAT_Y = b.LAT_Y
                                    };

                        return query.ToList();
                    }
                }
                catch { }
                return null;
            }

            /// <summary>
            /// GET FORM DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public JobViewModel GetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_RE_JOB
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new JobViewModel
                                    {
                                        ID = (int)n.ID,
                                        NUM = (int)n.NUM,
                                        CID = (int)n.CID,
                                        TERM_ID = (int)n.TERM_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        DEADLINE = n.DEADLINE,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS,
                                        POST_DAY = n.POST_DAY != null ? (DateTime)n.POST_DAY : new DateTime()
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                            //node.WorkPlaces = this.GetWorkPlaces((int)node.ID);
                            node.Provinces = this.GetJobProvinces((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public bool InsertWorkPlaces(string BranchIds, int jobId)
            {
                try
                {
                    if (string.IsNullOrEmpty(BranchIds))
                        return false;
                    using (DBContext context = new DBContext())
                    {
                        string[] branchs = BranchIds.Split(',');
                        if (branchs != null && branchs.Count() > 0)
                        {
                            //remove old workplaces
                            context.WZ_RE_JOB_BRANCH.Where(x => x.JOB_ID == jobId).ToList().ForEach(context.WZ_RE_JOB_BRANCH.DeleteObject);
                            context.SaveChanges();

                            foreach (var b in branchs)
                            {
                                WZ_RE_JOB_BRANCH item = new WZ_RE_JOB_BRANCH();
                                item.JOB_ID = jobId;
                                item.BRANCH_ID = StringHelper.IsNumber(b);
                                if (item.BRANCH_ID > 0) {
                                    context.WZ_RE_JOB_BRANCH.AddObject(item);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch { }
                return false;
            }

            public bool InsertJobProvinces(string arProvinces, int jobId)
            {
                try
                {
                    if (string.IsNullOrEmpty(arProvinces))
                        return false;
                    using (DBContext context = new DBContext())
                    {
                        string[] provinces = arProvinces.Split(',');
                        if (provinces != null && provinces.Count() > 0)
                        {
                            //remove old workplaces
                            context.WZ_RE_JOB_PROVINCE.Where(x => x.JOB_ID == jobId).ToList().ForEach(context.WZ_RE_JOB_PROVINCE.DeleteObject);
                            context.SaveChanges();

                            foreach (var p in provinces)
                            {
                                WZ_RE_JOB_PROVINCE item = new WZ_RE_JOB_PROVINCE();
                                item.JOB_ID = jobId;
                                item.PROVINCE_ID = p;
                                if (item.JOB_ID > 0 && !StringHelper.IsNullOrEmptyOrWhiteSpace(item.PROVINCE_ID))
                                {
                                    context.WZ_RE_JOB_PROVINCE.AddObject(item);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch { }
                return false;
            }

            /// <summary>
            /// GET FORM DETAIL
            /// </summary>
            /// <param name="id">ID FORM</param>
            /// <returns></returns>
            public JobViewModel GetContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_RE_JOB
                                    from nc in context.WZ_RE_JOB_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new JobViewModel
                                    {
                                        ID = (int)n.ID,
                                        CID = (int)n.CID,
                                        TERM_ID = (int)n.TERM_ID,
                                        NUM = (int)n.NUM,
                                        TITLE = nc.TITLE,
                                        SLUG = nc.SLUG,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        DEADLINE = n.DEADLINE,
                                        STATUS = (int)n.STATUS,
                                        POST_DAY = n.POST_DAY != null ? (DateTime)n.POST_DAY : new DateTime()
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                            node.Provinces = this.GetJobProvinces((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public JobViewModel AdminGetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = (from n in context.WZ_RE_JOB
                                       where (n.ID == id)
                                       select new JobViewModel
                                       {
                                           ID = (int)n.ID,
                                           CID = (int)n.CID,
                                           TERM_ID = (int)n.TERM_ID,
                                           NUM = (int)n.NUM,
                                           STATUS = (int)n.STATUS,
                                           PRIORITY = (int)n.PRIORITY,
                                           MODIFIED = n.MODIFIED,
                                           CREATED = n.CREATED,
                                           DEADLINE = n.DEADLINE,
                                           POST_DAY = n.POST_DAY != null ? (DateTime)n.POST_DAY : new DateTime()
                                       }).FirstOrDefault();

                        if (article != null)
                        {
                            article.ContentTranslates = this.GetContentTranslates(article.ID);
                            article.Provinces = this.GetJobProvinces(article.ID);
                        }

                        return article;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(JobViewModel term)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_RE_JOB.Where(a => a.ID == term.ID).FirstOrDefault();
                        if (node != null)
                        {
                            node.CID = term.CID;
                            node.NUM = term.NUM;
                            node.TERM_ID = term.TERM_ID;
                            node.STATUS = term.STATUS;
                            node.PRIORITY = term.PRIORITY;
                            node.DEADLINE = term.DEADLINE;
                            node.MODIFIED = DateTime.Now;
                            node.POST_DAY = term.POST_DAY;
                            context.SaveChanges();

                            //update content translates
                            if (term.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, JobContent> item in term.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_RE_JOB_CONTENT.Where(a => a.NID == term.ID && a.LANG == content_translate.LANG).FirstOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.TITLE))
                                    {
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.REQUIRE = content_translate.REQUIRE;
                                        node_content_lang.BENEFIT = content_translate.BENEFIT;
                                        node_content_lang.DOC_REQUIRE = content_translate.DOC_REQUIRE;
                                        node_content_lang.DEADLINE = content_translate.DEADLINE;
                                        node_content_lang.PLACE_CV = content_translate.PLACE_CV;

                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        node_content_lang = new WZ_RE_JOB_CONTENT();
                                        node_content_lang.NID = term.ID;
                                        node_content_lang.LANG = content_translate.LANG;
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.REQUIRE = content_translate.REQUIRE;
                                        node_content_lang.BENEFIT = content_translate.BENEFIT;
                                        node_content_lang.DOC_REQUIRE = content_translate.DOC_REQUIRE;
                                        node_content_lang.DEADLINE = content_translate.DEADLINE;
                                        node_content_lang.PLACE_CV = content_translate.PLACE_CV;
                                        context.WZ_RE_JOB_CONTENT.AddObject(node_content_lang);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_RE_JOB.Where(a => a.ID == id).FirstOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parenNID, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_RE_JOB
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_RE_JOB_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.DESCRIPTION.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list nodes by paging
            public List<JobViewModel> GetByPaging(int parenNID, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<JobViewModel> lstArticlles = new List<JobViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_RE_JOB
                                     join ac in context.WZ_RE_JOB_CONTENT on a.ID equals ac.NID
                                     where ac.LANG == lang
                                     select new JobViewModel {
                                        ID = (int)a.ID,
                                        NUM = (int)a.NUM,
                                        CID = (int)a.CID,
                                        TERM_ID = (int)a.TERM_ID,
                                        CREATED = a.CREATED,
                                        MODIFIED = a.MODIFIED,
                                        DEADLINE = a.DEADLINE,
                                        PRIORITY = (int)a.PRIORITY,
                                        TITLE = ac.TITLE,
                                        SLUG = ac.SLUG,
                                        DESCRIPTION = ac.DESCRIPTION,
                                        STATUS = (int)a.STATUS
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_RE_JOB_CONTENT
                                     where a.TITLE.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(JobViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_RE_JOB node = new WZ_RE_JOB();
                        if (viewModel != null)
                        {
                            node.ID = this._getNexNID();
                            node.NUM = viewModel.NUM;
                            node.CID = viewModel.CID;
                            node.TERM_ID = viewModel.TERM_ID;
                            node.STATUS = viewModel.STATUS;
                            node.PRIORITY = viewModel.PRIORITY;
                            node.DEADLINE = viewModel.DEADLINE;
                            node.CREATED = DateTime.Now;
                            node.MODIFIED = DateTime.Now;
                            node.POST_DAY = viewModel.POST_DAY;

                            context.WZ_RE_JOB.AddObject(node);
                            context.SaveChanges();

                            if (viewModel.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, JobContent> item in viewModel.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_RE_JOB_CONTENT content = new WZ_RE_JOB_CONTENT();
                                    content.NID = node.ID;
                                    content.TITLE = node_content.TITLE;
                                    content.SLUG = node_content.SLUG;
                                    content.DESCRIPTION = node_content.DESCRIPTION;
                                    content.LANG = node_content.LANG;
                                    content.REQUIRE = node_content.REQUIRE;
                                    content.BENEFIT = node_content.BENEFIT;
                                    content.DOC_REQUIRE = node_content.DOC_REQUIRE;
                                    content.PLACE_CV = node_content.PLACE_CV;
                                    content.DEADLINE = node_content.DEADLINE;

                                    context.WZ_RE_JOB_CONTENT.AddObject(content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)node.ID;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNexNID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_JOB" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_JOB";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        var check = context.WZ_RE_JOB.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNexNID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_RE_JOB.Where(a => a.ID == id).FirstOrDefault();

                        if (node != null)
                        {
                            context.WZ_RE_JOB.DeleteObject(node);
                            context.SaveChanges();

                            //
                            context.WZ_RE_JOB_PROVINCE.Where(p => p.JOB_ID == id).ToList().ForEach(context.WZ_RE_JOB_PROVINCE.DeleteObject);

                            //delete article content translate
                            var content_translates = context.WZ_RE_JOB_CONTENT.Where(ac=>ac.NID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_RE_JOB_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lsNID)
            {
                try
                {
                    if (lsNID != null)
                    {
                        foreach (int id in lsNID)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            /// <summary>
            /// Check if article slug already exist following language
            /// </summary>
            /// <param name="slug">Input slug</param>
            /// <param name="lang">Input language</param>
            /// <returns>True is exist</returns>
            public bool CheckSlug(string slug, string lang)
            {
                try
                {
                    lang = string.IsNullOrEmpty(lang) ? "vi" : lang;
                    using (var context = new DBContext())
                    {
                        var node = (from a in context.WZ_RE_JOB
                                    from ac in context.WZ_RE_JOB_CONTENT
                                    where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug
                                    select a).FirstOrDefault();
                        if (node != null) return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            #endregion
        }
    }
}