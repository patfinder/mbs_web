﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Helpers;
using System.Linq.Dynamic;

namespace WizardCMS.Models
{
    #region Admin Account View Model
    public class AdminAccountModel
    {
        public int ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string PERMISSION { get; set; }
        public int CUSTOM_PERMISSION { get; set; }
        public int STATUS { get; set; }
        public int GROUP_ID { get; set; }
        public DateTime CREATED { get; set; }
        public ADMIN_WZ_GROUPS GROUP { get; set; }
    }
    #endregion

    public static class AdminAccountService
    {
        //private LogService logService = new LogServiceImpl();

        public static bool AddAccount(AdminAccountModel account)
        {
            try
            {
                return AdmincAccountDAL.Instance.AddAccount(account);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static AdminAccountModel GetData(string userName)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetData(userName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static AdminAccountModel GetData(string userName, string password)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetData(userName, password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountListAccountByPerpage(string searchText, string dateFrom, string dateTo)
        {
            try
            {
                return AdmincAccountDAL.Instance.CountListAccountByPerpage(searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetListAccountByPerpage(string dateFrom, string dateTo, string searchText, Paging paging)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetListAccountByPerpage(searchText, dateFrom, dateTo, paging);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetListAccountByPerpage(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy, string sortType)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetListAccountByPerpage(dateFrom, dateTo, searchText, paging, sortBy, sortType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountUser()
        {
            try
            {
                return AdmincAccountDAL.Instance.CountUser();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static AdminAccountModel GetAccountById(int userId)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetAccountById(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteAccountByArrayId(List<int> userId)
        {
            try
            {
                return AdmincAccountDAL.Instance.DeleteAccountByArrayId(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckData(string userName)
        {
            try
            {
                return AdmincAccountDAL.Instance.CheckData(userName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetSearchContent(int limit, int page)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetSearchContent(limit, page);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetTotalSearchContent()
        {
            try
            {
                return AdmincAccountDAL.Instance.GetTotalSearchContent();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetAllAccount()
        {
            try
            {
                return AdmincAccountDAL.Instance.GetAllAccount();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AccountGroupModel> GetAllGroup()
        {
            try
            {
                return AdmincAccountDAL.Instance.GetAllGroup();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(List<int> listId, short status)
        {
            try
            {
                return AdmincAccountDAL.Instance.UpdateStatus(listId, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetIdAccount(string userName)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetIdAccount(userName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ChangePassword(int accountId, string password)
        {
            try
            {
                return AdmincAccountDAL.Instance.ChangePassword(accountId, password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetAccountsByGroupId(int groupId)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetAccountsByGroupId(groupId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateUserPermission(int groupId, string perms)
        {
            try
            {
                return AdmincAccountDAL.Instance.UpdateUserPermission(groupId, perms);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Save(AdminAccountModel acc, int idUser)
        {
            try
            {
                return AdmincAccountDAL.Instance.Save(acc, idUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetGroupName(int GROUP_ID)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetGroupName(GROUP_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region interface permission
        public static string GetPermByUserId(int userId)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetPermByUserId(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetPermByGroupId(int GROUP_ID)
        {
            try
            {
                return AdmincAccountDAL.Instance.GetPermByGroupId(GROUP_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ResetPerm(AdminAccountModel account)
        {
            try
            {
                return AdmincAccountDAL.Instance.ResetPerm(account);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class AdmincAccountDAL
        {
            private static AdmincAccountDAL _instance = null;

            #region constructor
            static public AdmincAccountDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new AdmincAccountDAL();
                    return _instance;
                }
            }

            #endregion

            #region Constructor
            public AdmincAccountDAL() : base()
            {
            }
            #endregion

            #region get data function
            public bool AddAccount(AdminAccountModel account)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.ADMIN_USER_SEQ;
                        //    sequence.ADMIN_USER_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ADMIN_USER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ADMIN_USER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        ADMIN_WZ_USERS user = new ADMIN_WZ_USERS();
                        user.ID             = newID;
                        user.USERNAME       = account.USERNAME;
                        user.PASSWORD       = account.PASSWORD;
                        user.PERMISSION     = account.PERMISSION;
                        user.CUSTOM_PERMISSION = account.CUSTOM_PERMISSION;
                        user.GROUP_ID       = account.GROUP_ID;
                        user.STATUS         = account.STATUS;
                        user.CREATED        = DateTime.Now;

                        context.ADMIN_WZ_USERS.AddObject(user);
                        context.SaveChanges();
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

            public int CountListAccountByPerpage(string searchText, string dateFrom, string dateTo)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = from acc in context.ADMIN_WZ_USERS
                                         from gr in context.ADMIN_WZ_GROUPS
                                         where acc.GROUP_ID == gr.ID && acc.ID != 1
                                         select acc;

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstAccount = lstAccount.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstAccount = lstAccount.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstAccount = lstAccount.Where(x => x.USERNAME.ToLower().Contains(searchText.ToLower()));
                        }

                        return lstAccount.Count();
                    }
                }
                catch
                {
                    return 0;
                }
            }

            public List<AdminAccountModel> GetListAccountByPerpage(string dateFrom, string dateTo, string searchText, Paging paging)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = (from acc in context.ADMIN_WZ_USERS
                                          from gr in context.ADMIN_WZ_GROUPS
                                          where acc.GROUP_ID == gr.ID
                                          select new AdminAccountModel { 
                                            ID = (int)acc.ID,
                                            USERNAME = acc.USERNAME,
                                            PASSWORD = acc.PASSWORD,
                                            PERMISSION = acc.PERMISSION,
                                            CUSTOM_PERMISSION = (int)acc.CUSTOM_PERMISSION,
                                            GROUP_ID = (int)acc.GROUP_ID,
                                            GROUP = acc.ADMIN_WZ_GROUPS,
                                            STATUS = (int)acc.STATUS,
                                            CREATED = acc.CREATED
                                          });
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstAccount = lstAccount.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstAccount = lstAccount.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstAccount = lstAccount.Where(x => x.USERNAME.ToLower().Contains(searchText.ToLower()));
                        }

                        if (paging.Perpage > 0)
                        {
                            lstAccount = lstAccount.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return lstAccount.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            public List<AdminAccountModel> GetListAccountByPerpage(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy, string sortType)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = (from acc in context.ADMIN_WZ_USERS
                                          from gr in context.ADMIN_WZ_GROUPS
                                          where acc.GROUP_ID == gr.ID && acc.ID != 1
                                          select new AdminAccountModel
                                          {
                                              ID = (int)acc.ID,
                                              USERNAME = acc.USERNAME,
                                              PASSWORD = acc.PASSWORD,
                                              PERMISSION = acc.PERMISSION,
                                              CUSTOM_PERMISSION = (int)acc.CUSTOM_PERMISSION,
                                              GROUP_ID = (int)acc.GROUP_ID,
                                              GROUP = acc.ADMIN_WZ_GROUPS,
                                              STATUS = (int)acc.STATUS,
                                              CREATED = acc.CREATED
                                          });
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstAccount = lstAccount.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstAccount = lstAccount.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstAccount = lstAccount.Where(x => x.USERNAME.ToLower().Contains(searchText.ToLower()));
                        }

                        if (sortType == "") sortType = Const.Constants.SORT_ASC;
                        lstAccount = lstAccount.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            lstAccount = lstAccount.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return lstAccount.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            public int CountUser()
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        int count = context.ADMIN_WZ_USERS.Count();
                        return count;
                    }
                }
                catch
                {
                    return 0;
                }
            }

            public AdminAccountModel GetAccountById(int userId)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var acc = (from a in context.ADMIN_WZ_USERS
                                   from gr in context.ADMIN_WZ_GROUPS
                                   where a.ID == userId && a.GROUP_ID == gr.ID
                                   select new AdminAccountModel
                                   {
                                       ID = (int)a.ID,
                                       USERNAME = a.USERNAME,
                                       PASSWORD = a.PASSWORD,
                                       PERMISSION = a.PERMISSION,
                                       CUSTOM_PERMISSION = (int)a.CUSTOM_PERMISSION,
                                       GROUP_ID = (int)a.GROUP_ID,
                                       GROUP = a.ADMIN_WZ_GROUPS,
                                       STATUS = (int)a.STATUS,
                                       CREATED = a.CREATED
                                   }).FirstOrDefault();
                        return acc;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public bool DeleteAccountByArrayId(List<int> userId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //List<LogDTO> lstLog = new List<LogDTO>();
                        var accounts = from a in context.ADMIN_WZ_USERS where userId.Contains((int)a.ID) select a;
                        if (accounts != null && accounts.Count() > 0)
                        {
                            foreach (ADMIN_WZ_USERS acc in accounts)
                            {
                                context.ADMIN_WZ_USERS.DeleteObject(acc);
                            }
                            context.SaveChanges();
                        }
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

            public bool CheckData(string userName)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var acc = (from user in context.ADMIN_WZ_USERS
                                   where user.USERNAME == userName
                                   select user.USERNAME).FirstOrDefault();
                        if (acc.Count() > 0)
                            return true;
                        else return false;
                    }
                }
                catch
                {
                    return false;
                }
            }

            public AdminAccountModel GetData(string userName)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var data = (from user in context.ADMIN_WZ_USERS
                                    where user.USERNAME == userName
                                    select new AdminAccountModel
                                    {
                                        ID = (int)user.ID,
                                        USERNAME = user.USERNAME,
                                        PASSWORD = user.PASSWORD,
                                        PERMISSION = user.PERMISSION,
                                        CUSTOM_PERMISSION = (int)user.CUSTOM_PERMISSION,
                                        GROUP_ID = (int)user.GROUP_ID,
                                        GROUP = user.ADMIN_WZ_GROUPS,
                                        STATUS = (int)user.STATUS,
                                        CREATED = user.CREATED
                                    }).FirstOrDefault();
                        return data;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public AdminAccountModel GetData(string userName, string password)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        context.Connection.Open();
                        var user = (from u in context.ADMIN_WZ_USERS
                                    where (u.USERNAME == userName && password == u.PASSWORD)
                                    select new AdminAccountModel
                                    {
                                        ID = (int)u.ID,
                                        USERNAME = u.USERNAME,
                                        PASSWORD = u.PASSWORD,
                                        PERMISSION = u.PERMISSION,
                                        CUSTOM_PERMISSION = (int)u.CUSTOM_PERMISSION,
                                        GROUP_ID = (int)u.GROUP_ID,
                                        GROUP = u.ADMIN_WZ_GROUPS,
                                        STATUS = (int)u.STATUS,
                                        CREATED = u.CREATED
                                    }).FirstOrDefault();
                        return user;
                    }
                }
                catch(Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                    Logs.WriteLogError(ex.Message);
                    Logs.WriteLogError(ex.InnerException.StackTrace);
                    return null;
                }
            }

            public List<AdminAccountModel> GetSearchContent(int limit, int page)
            {
                try
                {
                    return null;
                }
                catch
                {
                    return null;
                }
            }

            public int GetTotalSearchContent()
            {
                try
                {
                    return -1;
                }
                catch
                {
                    return -1;
                }
            }

            public List<AdminAccountModel> GetAllAccount()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = from acc in context.ADMIN_WZ_USERS
                                         from gr in context.ADMIN_WZ_GROUPS
                                         where acc.GROUP_ID == gr.ID
                                         select new AdminAccountModel
                                         {
                                             ID = (int)acc.ID,
                                             USERNAME = acc.USERNAME,
                                             PASSWORD = acc.PASSWORD,
                                             PERMISSION = acc.PERMISSION,
                                             CUSTOM_PERMISSION = (int)acc.CUSTOM_PERMISSION,
                                             GROUP_ID = (int)acc.GROUP_ID,
                                             GROUP = acc.ADMIN_WZ_GROUPS,
                                             STATUS = (int)acc.STATUS,
                                             CREATED = acc.CREATED
                                         };
                        return lstAccount.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            public List<AccountGroupModel> GetAllGroup()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstGroup = (from gr in context.ADMIN_WZ_GROUPS
                                        orderby gr.NAME ascending
                                        select new AccountGroupModel {
                                            ID = (int)gr.ID,
                                            NAME = gr.NAME,
                                            PERMISSION = gr.PERMISSION,
                                            STATUS = (int)gr.STATUS,
                                            CREATED = gr.CREATED
                                        });
                        return lstGroup.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            public bool UpdateStatus(List<int> listId, short status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var accounts = context.ADMIN_WZ_USERS.Where(x => listId.Contains((int)x.ID));
                        if (accounts != null && accounts.Count() > 0)
                        {
                            foreach (ADMIN_WZ_USERS acc in accounts)
                            {
                                acc.STATUS = status;
                                context.SaveChanges();
                            }
                        }
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

            public int GetIdAccount(string userName)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        decimal id = context.ADMIN_WZ_USERS.FirstOrDefault(i => i.USERNAME.Trim() == userName).ID;
                        return (int)id;
                    }
                }
                catch
                {
                    return -1;
                }
            }

            public bool ChangePassword(int accountId, string password)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        ADMIN_WZ_USERS account = context.ADMIN_WZ_USERS.FirstOrDefault(a => a.ID == accountId);
                        if (account == null) return false;
                        account.PASSWORD = password;
                        context.SaveChanges();
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }

            }

            public List<AdminAccountModel> GetAccountsByGroupId(int groupId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = from acc in context.ADMIN_WZ_USERS
                                         from gr in context.ADMIN_WZ_GROUPS
                                         where acc.GROUP_ID == gr.ID && acc.GROUP_ID == groupId
                                         select new AdminAccountModel
                                         {
                                             ID = (int)acc.ID,
                                             USERNAME = acc.USERNAME,
                                             PASSWORD = acc.PASSWORD,
                                             PERMISSION = acc.PERMISSION,
                                             CUSTOM_PERMISSION = (int)acc.CUSTOM_PERMISSION,
                                             GROUP_ID = (int)acc.GROUP_ID,
                                             GROUP = acc.ADMIN_WZ_GROUPS,
                                             STATUS = (int)acc.STATUS,
                                             CREATED = acc.CREATED
                                         };
                        return lstAccount.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            private bool updateUserPerm(int userId, string perm)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        ADMIN_WZ_USERS user = context.ADMIN_WZ_USERS.Where(u => u.ID == userId).FirstOrDefault();
                        if (user != null)
                        {
                            user.PERMISSION = perm;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch { }
                return false;
            }

            public bool UpdateUserPermission(int groupId, string perms)
            {
                try
                {
                    List<AdminAccountModel> lstAccounts = this.GetAccountsByGroupId(groupId);
                    if (lstAccounts != null)
                    {
                        foreach (var account in lstAccounts)
                        {
                            this.updateUserPerm((int)account.ID, perms);
                        }
                        return true;
                    }
                }
                catch { }
                return false;
            }

            //password must not uppercase
            public bool Save(AdminAccountModel acc, int idUser)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (idUser == 0)
                        {
                            ADMIN_WZ_USERS user = new ADMIN_WZ_USERS();
                            user.ID = _getNextID();
                            user.STATUS = acc.STATUS;
                            user.USERNAME = acc.USERNAME;
                            user.PASSWORD = acc.PASSWORD.ToLower();
                            user.GROUP_ID = acc.GROUP_ID;
                            user.CUSTOM_PERMISSION = acc.CUSTOM_PERMISSION;
                            user.PERMISSION = acc.PERMISSION;
                            user.CREATED = DateTime.Now;

                            context.ADMIN_WZ_USERS.AddObject(user);
                            context.SaveChanges();
                        }
                        else
                        {
                            //edit account
                            ADMIN_WZ_USERS account = context.ADMIN_WZ_USERS.FirstOrDefault(i => i.ID == acc.ID);
                            account.STATUS = acc.STATUS;
                            account.USERNAME = acc.USERNAME;
                            account.PASSWORD = acc.PASSWORD.ToLower();
                            account.GROUP_ID = acc.GROUP_ID;
                            account.CUSTOM_PERMISSION = acc.CUSTOM_PERMISSION;
                            account.PERMISSION = acc.PERMISSION;

                            context.SaveChanges();
                        }
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ADMIN_USER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ADMIN_USER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        var check = context.WZ_TERM.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            public string GetGroupName(int GROUP_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string name = context.ADMIN_WZ_GROUPS.FirstOrDefault(i => i.ID == GROUP_ID).NAME;
                        return name;
                    }
                }
                catch
                {
                    return "";
                }
            }
            #endregion

            #region permission
            public string GetPermByUserId(int userId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var perm = (from p in context.ADMIN_WZ_USERS
                                    where p.ID == userId
                                    select p.PERMISSION).FirstOrDefault();
                        return perm;
                    }
                }
                catch
                {
                    return "";
                }
            }

            public string GetPermByGroupId(int groupId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var perm = (from gr in context.ADMIN_WZ_GROUPS
                                    where gr.ID == groupId
                                    select gr.PERMISSION).FirstOrDefault();

                        return perm;
                    }
                }
                catch
                {
                    return "";
                }
            }

            public bool ResetPerm(AdminAccountModel account)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        ADMIN_WZ_USERS acc = context.ADMIN_WZ_USERS.FirstOrDefault(i => i.ID == account.ID);
                        if (acc != null)
                        {
                            acc.PERMISSION = account.PERMISSION;
                            context.SaveChanges();
                            return true;
                        }
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
            #endregion
        }
    }
}