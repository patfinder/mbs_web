﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class UserViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string FULLNAME { get; set; }
        public string CMND { get; set; }
        public string DATE_PLACE_CMND { get; set; }
        public string ADDRESS { get; set; }
        public string ADDRESS_CONTACT { get; set; }
        public string HOME_PHONE { get; set; }
        public string MOBILE_PHONE { get; set; }
        public string HOME_PHONE_CONTACT { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public int GENDER { get; set; }
        public string HEIGHT { get; set; }
        public string WEIGHT { get; set; }
        public int MIRITAL { get; set; }
        public string MIRITAL_OTHER { get; set; }
        public DateTime DOB { get; set; }
        public string POB { get; set; }
        public string IMAGE { get; set; }
        public string RELATE_EXP { get; set; }
        public string CONTACT_LEADER { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string TOKEN { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class UserServices
    {
        #region service interface
        public static UserViewModel Login(string userame, string password)
        {
            try
            {
                return UserDAL.Instance.Login(userame, password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int Active(int uid, string token)
        {
            try
            {
                return UserDAL.Instance.Active(uid, token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string ForgotPassword(string email, out int uid)
        {
            try
            {
                return UserDAL.Instance.ForgotPassword(email, out uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetPassword(int uid, string token)
        {
            try
            {
                return UserDAL.Instance.GetPassword(uid, token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UniqueEmail(string email)
        {
            try
            {
                return UserDAL.Instance.UniqueEmail(email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UniqueUsername(string username)
        {
            try
            {
                return UserDAL.Instance.UniqueUsername(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UniquePhone(string phone)
        {
            try
            {
                return UserDAL.Instance.UniquePhone(phone);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static UserViewModel Get(int Id)
        {
            try
            {
                return UserDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(UserViewModel slideViewModel)
        {
            try
            {
                return UserDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateToken(int uid, string token)
        {
            try
            {
                return UserDAL.Instance.UpdateToken(uid, token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdatePassword(int uid, string password)
        {
            try
            {
                return UserDAL.Instance.UpdatePassword(uid, password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return UserDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(UserViewModel slideViewModel)
        {
            try
            {
                return UserDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return UserDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<UserViewModel> GetAll()
        {
            try
            {
                return UserDAL.Instance.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return UserDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<UserViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return UserDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class UserDAL
        {
            #region static instance
            private static UserDAL _instance;

            static public UserDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new UserDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public bool UniqueEmail(string email)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(u => u.EMAIL == email.ToLower()).FirstOrDefault();
                        if (node != null)
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return true;
            }
            
            public bool UniqueUsername(string username)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(u => u.USERNAME == username.ToLower()).FirstOrDefault();
                        if (node != null)
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return true;
            }
            
            public bool UniquePhone(string phone)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(u => u.MOBILE_PHONE == phone.ToLower()).FirstOrDefault();
                        if (node != null)
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return true;
            }
            
            public UserViewModel Login(string userame, string password)
            {
                try
                {
                    string EncodePassword = StringHelper.EncryptMD5(password);

                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_USER
                                where a.USERNAME == userame && a.PASSWORD == EncodePassword
                                select new UserViewModel
                                {
                                    ID = (int)a.ID,
                                    USERNAME = a.USERNAME,
                                    PASSWORD = a.PASSWORD,
                                    NAME = a.NAME,
                                    FULLNAME = a.FULLNAME,
                                    CMND = a.CMND,
                                    DATE_PLACE_CMND = a.DATE_PLACE_CMND,
                                    ADDRESS = a.ADDRESS,
                                    ADDRESS_CONTACT = a.ADDRESS_CONTACT,
                                    HOME_PHONE = a.HOME_PHONE,
                                    HOME_PHONE_CONTACT = a.HOME_PHONE_CONTACT,
                                    MOBILE_PHONE = a.MOBILE_PHONE,
                                    FAX = a.FAX,
                                    EMAIL = a.EMAIL,
                                    WEIGHT = a.WEIGHT,
                                    HEIGHT = a.HEIGHT,
                                    GENDER = (int)a.GENDER,
                                    MIRITAL = (int)a.MIRITAL,
                                    MIRITAL_OTHER = a.MIRITAL_OTHER,
                                    RELATE_EXP = a.RELATE_EXP,
                                    IMAGE = a.IMAGE,
                                    CONTACT_LEADER = a.CONTACT_LEADER,
                                    DOB = a.DOB,
                                    POB = a.POB,
                                    STATUS = (int)a.STATUS,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public UserViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_USER
                                where a.ID == Id
                                select new UserViewModel
                                {
                                    ID = (int)a.ID,
                                    USERNAME = a.USERNAME,
                                    PASSWORD = a.PASSWORD,
                                    NAME = a.NAME,
                                    FULLNAME = a.FULLNAME,
                                    CMND = a.CMND,
                                    DATE_PLACE_CMND = a.DATE_PLACE_CMND,
                                    ADDRESS = a.ADDRESS,
                                    ADDRESS_CONTACT = a.ADDRESS_CONTACT,
                                    HOME_PHONE = a.HOME_PHONE,
                                    HOME_PHONE_CONTACT = a.HOME_PHONE_CONTACT,
                                    MOBILE_PHONE = a.MOBILE_PHONE,
                                    FAX = a.FAX,
                                    EMAIL = a.EMAIL,
                                    WEIGHT = a.WEIGHT,
                                    HEIGHT = a.HEIGHT,
                                    GENDER = (int)a.GENDER,
                                    MIRITAL = (int)a.MIRITAL,
                                    MIRITAL_OTHER = a.MIRITAL_OTHER,
                                    RELATE_EXP = a.RELATE_EXP,
                                    IMAGE = a.IMAGE,
                                    CONTACT_LEADER = a.CONTACT_LEADER,
                                    DOB = a.DOB,
                                    POB = a.POB,
                                    STATUS = (int)a.STATUS,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(UserViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USERNAME = viewModel.USERNAME;
                        //node.PASSWORD = viewModel.PASSWORD;
                        node.NAME = viewModel.NAME;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CMND = viewModel.CMND;
                        node.DATE_PLACE_CMND = viewModel.DATE_PLACE_CMND;
                        node.ADDRESS = viewModel.ADDRESS;
                        node.ADDRESS_CONTACT = viewModel.ADDRESS_CONTACT;
                        node.HOME_PHONE = viewModel.HOME_PHONE;
                        node.HOME_PHONE_CONTACT = viewModel.HOME_PHONE_CONTACT;
                        node.MOBILE_PHONE = viewModel.MOBILE_PHONE;
                        node.FAX = viewModel.FAX;
                        node.EMAIL = viewModel.EMAIL;
                        node.WEIGHT = viewModel.WEIGHT;
                        node.HEIGHT = viewModel.HEIGHT;
                        node.GENDER = (int)viewModel.GENDER;
                        node.MIRITAL = (int)viewModel.MIRITAL;
                        node.MIRITAL_OTHER = viewModel.MIRITAL_OTHER;
                        node.RELATE_EXP = viewModel.RELATE_EXP;
                        node.IMAGE = viewModel.IMAGE;
                        node.CONTACT_LEADER = viewModel.CONTACT_LEADER;
                        node.DOB = viewModel.DOB;
                        node.POB = viewModel.POB;
                        node.STATUS = (int)viewModel.STATUS;
                        node.MODIFIED = viewModel.MODIFIED;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateToken(int uid, string token)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a => a.ID == uid).FirstOrDefault();
                        if (node == null) return false;

                        node.TOKEN = token;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public string ForgotPassword(string email, out int uid)
            {
                uid = 0;
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a =>a.EMAIL == email).FirstOrDefault();
                        if (node == null) return string.Empty;

                        //string new_pass = StringHelper.RandomString();

                        string newToken = StringHelper.CreateToken();

                        node.TOKEN = newToken;

                        context.SaveChanges();

                        uid = (int)node.ID;
                        return newToken;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return string.Empty;
            }

            public string GetPassword(int uid, string token)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a => a.ID == uid && a.TOKEN == token).FirstOrDefault();
                        if (node == null) return string.Empty;

                        string new_pass = StringHelper.RandomString();

                        node.PASSWORD = StringHelper.EncryptMD5(new_pass);
                        node.TOKEN = string.Empty;

                        context.SaveChanges();

                        return new_pass;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return string.Empty;
            }

            public bool UpdatePassword(int uid, string password)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a => a.ID == uid).FirstOrDefault();
                        if (node == null) return false;

                        node.PASSWORD = password;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }
            /// <summary>
            /// Active user
            /// </summary>
            /// <param name="uid">user id</param>
            /// <param name="token">token</param>
            /// <returns>0: fail, 1: success, 2: already active</returns>
            public int Active(int uid, string token)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER.Where(a => a.ID == uid && a.TOKEN == token).FirstOrDefault();
                        if (node == null) return 0;
                        
                        if (node.STATUS == 0)
                        {
                            node.TOKEN = string.Empty;
                            node.STATUS = 1;
                            context.SaveChanges();

                            return 1;
                        }
                        return 2;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }

            public int Insert(UserViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_USER node = new WZ_RE_USER();
                        node.ID = this._getNextID();
                        node.USERNAME = viewModel.USERNAME;
                        node.PASSWORD = viewModel.PASSWORD;
                        node.TOKEN = viewModel.TOKEN;
                        node.NAME = viewModel.NAME;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CMND = viewModel.CMND;
                        node.DATE_PLACE_CMND = viewModel.DATE_PLACE_CMND;
                        node.ADDRESS = viewModel.ADDRESS;
                        node.ADDRESS_CONTACT = viewModel.ADDRESS_CONTACT;
                        node.HOME_PHONE = viewModel.HOME_PHONE;
                        node.HOME_PHONE_CONTACT = viewModel.HOME_PHONE_CONTACT;
                        node.MOBILE_PHONE = viewModel.MOBILE_PHONE;
                        node.FAX = viewModel.FAX;
                        node.EMAIL = viewModel.EMAIL;
                        node.WEIGHT = viewModel.WEIGHT;
                        node.HEIGHT = viewModel.HEIGHT;
                        node.GENDER = (int)viewModel.GENDER;
                        node.MIRITAL = (int)viewModel.MIRITAL;
                        node.MIRITAL_OTHER = viewModel.MIRITAL_OTHER;
                        node.RELATE_EXP = viewModel.RELATE_EXP;
                        node.IMAGE = viewModel.IMAGE;
                        node.CONTACT_LEADER = viewModel.CONTACT_LEADER;
                        node.DOB = viewModel.DOB;
                        node.POB = viewModel.POB;
                        node.STATUS = (int)viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_USER.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_USER.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_USER.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_USER.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_USER
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.USERNAME.Contains(searchText) || a.NAME.Contains(searchText) || a.FULLNAME.Contains(searchText) || a.EMAIL.Contains(searchText) || a.HOME_PHONE.Contains(searchText) || a.MOBILE_PHONE.Contains(searchText) || a.HOME_PHONE_CONTACT.Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            public List<UserViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<UserViewModel> lsTaxonomyViewModels = new List<UserViewModel>();

                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_USER
                                     select new UserViewModel
                                     {
                                         ID = (int)a.ID,
                                         USERNAME = a.USERNAME,
                                         PASSWORD = a.PASSWORD,
                                         NAME = a.NAME,
                                         FULLNAME = a.FULLNAME,
                                         CMND = a.CMND,
                                         DATE_PLACE_CMND = a.DATE_PLACE_CMND,
                                         ADDRESS = a.ADDRESS,
                                         ADDRESS_CONTACT = a.ADDRESS_CONTACT,
                                         HOME_PHONE = a.HOME_PHONE,
                                         HOME_PHONE_CONTACT = a.HOME_PHONE_CONTACT,
                                         MOBILE_PHONE = a.MOBILE_PHONE,
                                         FAX = a.FAX,
                                         EMAIL = a.EMAIL,
                                         WEIGHT = a.WEIGHT,
                                         HEIGHT = a.HEIGHT,
                                         GENDER = (int)a.GENDER,
                                         MIRITAL = (int)a.MIRITAL,
                                         MIRITAL_OTHER = a.MIRITAL_OTHER,
                                         RELATE_EXP = a.RELATE_EXP,
                                         IMAGE = a.IMAGE,
                                         CONTACT_LEADER = a.CONTACT_LEADER,
                                         DOB = a.DOB,
                                         POB = a.POB,
                                         STATUS = (int)a.STATUS,
                                         MODIFIED = a.MODIFIED,
                                         CREATED = a.CREATED
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.USERNAME.Contains(searchText) || a.NAME.Contains(searchText) || a.FULLNAME.Contains(searchText) || a.EMAIL.Contains(searchText) || a.HOME_PHONE.Contains(searchText) || a.MOBILE_PHONE.Contains(searchText) || a.HOME_PHONE_CONTACT.Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<UserViewModel> GetAll()
            {
                try
                {
                    List<UserViewModel> lsTaxonomyViewModels = new List<UserViewModel>();

                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_USER
                                     select new UserViewModel
                                     {
                                         ID = (int)a.ID,
                                         USERNAME = a.USERNAME,
                                         PASSWORD = a.PASSWORD,
                                         NAME = a.NAME,
                                         FULLNAME = a.FULLNAME,
                                         CMND = a.CMND,
                                         DATE_PLACE_CMND = a.DATE_PLACE_CMND,
                                         ADDRESS = a.ADDRESS,
                                         ADDRESS_CONTACT = a.ADDRESS_CONTACT,
                                         HOME_PHONE = a.HOME_PHONE,
                                         HOME_PHONE_CONTACT = a.HOME_PHONE_CONTACT,
                                         MOBILE_PHONE = a.MOBILE_PHONE,
                                         FAX = a.FAX,
                                         EMAIL = a.EMAIL,
                                         WEIGHT = a.WEIGHT,
                                         HEIGHT = a.HEIGHT,
                                         GENDER = (int)a.GENDER,
                                         MIRITAL = (int)a.MIRITAL,
                                         MIRITAL_OTHER = a.MIRITAL_OTHER,
                                         RELATE_EXP = a.RELATE_EXP,
                                         IMAGE = a.IMAGE,
                                         CONTACT_LEADER = a.CONTACT_LEADER,
                                         DOB = a.DOB,
                                         POB = a.POB,
                                         STATUS = (int)a.STATUS,
                                         MODIFIED = a.MODIFIED,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }
            
            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_USER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_USER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_USER.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }

            #endregion
        }
        #endregion
    }
}