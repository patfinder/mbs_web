﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class EducationViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string LEVEL_NAME { get; set; }
        public string SCHOOL { get; set; }
        public DateTime DATE_FROM { get; set; }
        public DateTime DATE_TO { get; set; }
        public string MAJOR { get; set; }
        public string GRADUATE_POINT { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReEducationServices
    {
        #region service interface
        public static int Insert(EducationViewModel viewModel)
        {
            try
            {
                return EducationDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(EducationViewModel viewModel)
        {
            try
            {
                return EducationDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return EducationDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static EducationViewModel Get(int id)
        {
            try
            {
                return EducationDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EducationViewModel> GetAll(int uid)
        {
            try
            {
                return EducationDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class EducationDAL
        {
            #region static instance
            private static EducationDAL _instance;

            static public EducationDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new EducationDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public EducationViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_EDUCATION
                                where a.ID == Id
                                select new EducationViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    LEVEL_NAME = a.LEVEL_NAME,
                                    SCHOOL = a.SCHOOL,
                                    DATE_FROM = a.DATE_FROM,
                                    MAJOR = a.MAJOR,
                                    GRADUATE_POINT = a.GRADUATE_POINT,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(EducationViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_EDUCATION.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        node.LEVEL_NAME = viewModel.LEVEL_NAME;
                        node.SCHOOL = viewModel.SCHOOL;
                        node.DATE_FROM = viewModel.DATE_FROM;
                        node.MAJOR = viewModel.MAJOR;
                        node.GRADUATE_POINT = viewModel.GRADUATE_POINT;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<EducationViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_EDUCATION
                                     where a.USER_ID == uid
                                     select new EducationViewModel
                                     {
                                         ID = (int)a.ID,
                                         LEVEL_NAME = a.LEVEL_NAME,
                                         SCHOOL = a.SCHOOL,
                                         DATE_FROM = a.DATE_FROM,
                                         MAJOR = a.MAJOR,
                                         GRADUATE_POINT = a.GRADUATE_POINT,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_EDUCATION.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_EDUCATION.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(EducationViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_EDUCATION node = new WZ_RE_EDUCATION();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.LEVEL_NAME = viewModel.LEVEL_NAME;
                        node.SCHOOL = viewModel.SCHOOL;
                        node.DATE_FROM = viewModel.DATE_FROM;
                        node.MAJOR = viewModel.MAJOR;
                        node.GRADUATE_POINT = viewModel.GRADUATE_POINT;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_EDUCATION.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_EDUCATION" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_EDUCATION";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_EDUCATION.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}