﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Models
{
    public class BranchGroupViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string LANG { get; set; }
        public int PRIORITY { get; set; }
        public int STATUS { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class BranchGroupServices
    {
        #region service interface
        public static BranchGroupViewModel Get(int Id)
        {
            try
            {
                return BrandGroupDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(BranchGroupViewModel slideViewModel)
        {
            try
            {
                return BrandGroupDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return BrandGroupDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(BranchGroupViewModel slideViewModel)
        {
            try
            {
                return BrandGroupDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return BrandGroupDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return BrandGroupDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchGroupViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return BrandGroupDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class BrandGroupDAL
        {
            #region static instance
            private static BrandGroupDAL _instance;

            static public BrandGroupDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new BrandGroupDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public BranchGroupViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_BRANCH_GROUP
                                where a.ID == Id
                                select new BranchGroupViewModel
                                {
                                    ID = (int)a.ID,
                                    NAME = a.NAME,
                                    DESCRIPTION = a.DESCRIPTION,
                                    LANG = a.LANG,
                                    STATUS = (int)a.STATUS,
                                    PRIORITY = (int)a.PRIORITY,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(BranchGroupViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_BRANCH_GROUP.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.NAME = viewModel.NAME;
                        node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.LANG = viewModel.LANG;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(BranchGroupViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_BRANCH_GROUP node = new WZ_BRANCH_GROUP();
                        node.ID = this._getNextID();
                        node.NAME = viewModel.NAME;
                        node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.LANG = viewModel.LANG;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_BRANCH_GROUP.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_BRANCH.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.NAME.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<BranchGroupViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<BranchGroupViewModel> lsBranchGroupViewModels = new List<BranchGroupViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_GROUP
                                     select new BranchGroupViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME  = a.NAME,
                                         DESCRIPTION = a.DESCRIPTION,
                                         LANG = a.LANG,
                                         PRIORITY = (int)a.PRIORITY,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.NAME.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "BRANCH_GROUP" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "BRANCH_GROUP";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        var check = context.WZ_BRANCH.Where(a=>a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }

            public List<WZ_CT_PROVINCE> GetProvinces(int region = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_PROVINCE orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_DISTRICT> GetDistricts(string PROVINCE_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_DISTRICT where d.PROVINCE_ID == PROVINCE_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_WARD> GetWards(string DISTRICT_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_WARD where d.DISTRICT_ID == DISTRICT_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }
            #endregion
        }
        #endregion
    }
}