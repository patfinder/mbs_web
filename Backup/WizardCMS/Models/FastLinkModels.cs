﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class FastLinkViewModel
    {
        public int ID { get; set; }
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public int TYPE { get; set; }
        public int TURN_ON { get; set; }

        public string NAME { get; set; }
        public string LINK { get; set; }

        public string ICON { get; set; }
        public string ICON_HOVER { get; set; }
        public string IMAGE { get; set; }

        public DateTime CREATED { get; set; }
        public DateTime MODIFIED { get; set; }

        public Dictionary<string, FastLinkContent> ContentTranslates { get; set; }


        //Constructor
        public FastLinkViewModel()
        {
            this.ContentTranslates = new Dictionary<string, FastLinkContent>();
        }
    }

    public class FastLinkContent
    {
        public int NID { get; set; }
        public string NAME { get; set; }
        public string SUB_TITLE { get; set; }
        public string LINK { get; set; }
        public string DESCRIPTION { get; set; }
        public string ATTACHMENT { get; set; }
        public string LANG { get; set; }

        public FastLinkContent()
        { }
    }

    public static class FastLinkServices
    {
        #region FastLink services interface
        public static FastLinkContent GetFastLinkContentByLang(Dictionary<string, FastLinkContent> lstFastLinkContent, string lang = "vi")
        {
            try
            {
                if (lstFastLinkContent != null && lstFastLinkContent.ContainsKey(lang))
                {
                    return lstFastLinkContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static Dictionary<string, FastLinkContent> GetContentTranslates(int NID)
        {
            try
            {
                return FastLinkDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<FastLinkViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return FastLinkDAL.Instance.GetSearchContent(parrent, page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return FastLinkDAL.Instance.GetCountSearchContent(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<FastLinkViewModel> GetFastLink(int type, int turn_on, string lang = "", List<int> ins = null)
        {
            try
            {
                return FastLinkDAL.Instance.GetFastLink(type, turn_on, lang, ins);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FastLinkViewModel GetById(int id)
        {
            try
            {
                return FastLinkDAL.Instance.GetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FastLinkViewModel GetFastLinkContentById(int id, string lang = "")
        {
            try
            {
                return FastLinkDAL.Instance.GetFastLinkContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FastLinkViewModel AdminGetById(int id)
        {
            try
            {
                return FastLinkDAL.Instance.AdminGetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(FastLinkViewModel article)
        {
            try
            {
                return FastLinkDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return FastLinkDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return FastLinkDAL.Instance.GetCount(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<FastLinkViewModel> GetByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang="vi")
        {
            try
            {
                return FastLinkDAL.Instance.GetByPaging(parentId, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="FastLinkViewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(FastLinkViewModel FastLinkViewModel)
        {
            try
            {
                return FastLinkDAL.Instance.Insert(FastLinkViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return FastLinkDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteFile(int id, string field)
        {
            try
            {
                return FastLinkDAL.Instance.DeleteFile(id, field);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lstId)
        {
            try
            {
                return FastLinkDAL.Instance.DeleteList(lstId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class FastLinkDAL
        {
            private static FastLinkDAL _instance;

            static public FastLinkDAL Instance { 
                get {
                    if (_instance == null)
                        _instance = new FastLinkDAL();

                    return _instance;
                }
            }

            #region constructor
            public FastLinkDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment

            public Dictionary<string, FastLinkContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, FastLinkContent> result = new Dictionary<string, FastLinkContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_FASTLINK_CONTENT
                                    where ct.NID == NID
                                    orderby ct.LANG descending
                                    select new FastLinkContent
                                    {
                                        NID = (int)ct.NID,
                                        NAME = ct.NAME,
                                        LINK = ct.LINK,
                                        LANG = ct.LANG
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            public void GetAll()
            {
                try
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            public List<FastLinkViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText="", string dateFrom="", string dateTo="")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_FASTLINK
                                    from mc in context.WZ_FASTLINK_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new FastLinkViewModel
                                    {
                                        ID = (int)m.ID,
                                        NAME = mc.NAME,
                                        LINK = mc.LINK,
                                        IMAGE = m.IMAGE,
                                        ICON = m.ICON,
                                        ICON_HOVER = m.ICON_HOVER,
                                        CREATED = m.CREATED,
                                    };
                        var result = query.ToList();

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate).ToList();
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate).ToList();
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = (from a in result
                                     where (a.NAME.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText))
                                     select a).ToList();
                        }

                        if (result != null && page > 0)
                        {
                            result = result.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_FASTLINK
                                     where a.STATUS == Constants.SHOW
                                     select a;
                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_FASTLINK_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.NAME.ToLower().Contains(searchText) || ac.LINK.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            /// <summary>
            /// GET NODE DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public FastLinkViewModel GetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_FASTLINK
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new FastLinkViewModel
                                    {
                                        ID = (int)n.ID,
                                        CREATED = n.CREATED,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS,
                                        TYPE = (int)n.TYPE,
                                        TURN_ON = (int)n.TURN_ON,
                                        ICON = n.ICON,
                                        ICON_HOVER = n.ICON_HOVER,
                                        IMAGE = n.IMAGE,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id">ID ARTICLE</param>
            /// <returns></returns>
            public FastLinkViewModel GetFastLinkContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_FASTLINK
                                    from nc in context.WZ_FASTLINK_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new FastLinkViewModel
                                    {
                                        ID = (int)n.ID,
                                        NAME = nc.NAME,
                                        LINK = nc.LINK,
                                        ICON = n.ICON,
                                        ICON_HOVER = n.ICON_HOVER,
                                        IMAGE = n.IMAGE,
                                        TYPE = (int)n.TYPE,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS,
                                        TURN_ON = (int)n.TURN_ON,
                                        CREATED = n.CREATED,
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public FastLinkViewModel AdminGetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (from a in context.WZ_FASTLINK
                                       where (a.ID == id)
                                       select new FastLinkViewModel
                                       {
                                           ID = (int)a.ID,
                                           STATUS = (int)a.STATUS,
                                           TURN_ON = (int)a.TURN_ON,
                                           CREATED = a.CREATED,
                                           PRIORITY = (int)a.PRIORITY,
                                           TYPE = (int)a.TYPE,
                                           ICON = a.ICON,
                                           ICON_HOVER = a.ICON_HOVER,
                                           IMAGE = a.IMAGE

                                       }).FirstOrDefault();

                        if (node != null)
                            node.ContentTranslates = this.GetContentTranslates(node.ID);

                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(FastLinkViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_FASTLINK.Where(a => a.ID == viewModel.ID).SingleOrDefault();
                        if (node != null)
                        {
                            node.STATUS = viewModel.STATUS;
                            node.TURN_ON = viewModel.TURN_ON;
                            node.ICON = viewModel.ICON;
                            node.ICON_HOVER = viewModel.ICON_HOVER;
                            node.IMAGE = viewModel.IMAGE;
                            node.PRIORITY = viewModel.PRIORITY;
                            node.TYPE = viewModel.TYPE;
                            context.SaveChanges();

                            //update content translates
                            if (viewModel.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, FastLinkContent> item in viewModel.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_FASTLINK_CONTENT.Where(a => a.NID == viewModel.ID && a.LANG == content_translate.LANG).SingleOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.NAME))
                                    {
                                        node_content_lang.NAME = content_translate.NAME;
                                        node_content_lang.LINK = content_translate.LINK;

                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        node_content_lang = new WZ_FASTLINK_CONTENT();
                                        node_content_lang.NID = viewModel.ID;
                                        node_content_lang.LANG = content_translate.LANG;
                                        node_content_lang.NAME = content_translate.NAME;
                                        node_content_lang.LINK = content_translate.LINK;

                                        context.WZ_FASTLINK_CONTENT.AddObject(node_content_lang);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_FASTLINK.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_FASTLINK
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_FASTLINK_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.NAME.ToLower().Contains(searchText) || ac.LINK.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list article by paging
            public List<FastLinkViewModel> GetByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<FastLinkViewModel> lstArticlles = new List<FastLinkViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_FASTLINK 
                                     from ac in context.WZ_FASTLINK_CONTENT
                                     where ac.NID == a.ID && ac.LANG == lang
                                     select new FastLinkViewModel {
                                        ID = (int)a.ID,
                                        TYPE = (int)a.TYPE,
                                        CREATED = a.CREATED,
                                        PRIORITY = (int)a.PRIORITY,
                                        TURN_ON = (int)a.TURN_ON,
                                        NAME = ac.NAME,
                                        LINK = ac.LINK,
                                        ICON = a.ICON,
                                        ICON_HOVER = a.ICON_HOVER,
                                        IMAGE = a.IMAGE,
                                        STATUS = (int)a.STATUS
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_FASTLINK_CONTENT
                                     where (a.NAME.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        var temp = result.ToList();

                        var lstArticles = from n in temp
                                          from ac in context.WZ_FASTLINK_CONTENT
                                          where n.ID == ac.NID && ac.LANG == lang
                                          select new FastLinkViewModel {
                            ID = (int)n.ID,
                            TYPE = (int)n.TYPE,
                            CREATED = n.CREATED,
                            MODIFIED = n.MODIFIED,
                            PRIORITY = (int)n.PRIORITY,
                            NAME = ac.NAME,
                            LINK = ac.LINK,
                            ICON = n.ICON,
                            ICON_HOVER = n.ICON_HOVER,
                            IMAGE = n.IMAGE,
                            STATUS = (int)n.STATUS,
                            ContentTranslates = this.GetContentTranslates((int)n.ID)
                        };

                        return lstArticles.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(FastLinkViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_FASTLINK node = new WZ_FASTLINK();
                        if (viewModel != null)
                        {
                            node.ID = this._getNextID();
                            node.TYPE = viewModel.TYPE;
                            node.TURN_ON = viewModel.TURN_ON;
                            node.ICON = viewModel.ICON;
                            node.ICON_HOVER = viewModel.ICON_HOVER;
                            node.IMAGE = viewModel.IMAGE;
                            node.STATUS = viewModel.STATUS;
                            node.PRIORITY = viewModel.PRIORITY;
                            node.CREATED = DateTime.Now;

                            context.WZ_FASTLINK.AddObject(node);
                            context.SaveChanges();

                            if (viewModel.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, FastLinkContent> item in viewModel.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_FASTLINK_CONTENT article_content = new WZ_FASTLINK_CONTENT();
                                    article_content.NID = node.ID;
                                    article_content.NAME = node_content.NAME;
                                    article_content.LINK = node_content.LINK;
                                    article_content.LANG = node_content.LANG;

                                    context.WZ_FASTLINK_CONTENT.AddObject(article_content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)node.ID;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID(bool isTry = false)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_FASTLINK_SEQ;
                        //    sequence.WZ_FASTLINK_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "FASTLINK" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "FASTLINK";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        var check = context.WZ_FASTLINK.Where(a => a.ID == newID).SingleOrDefault();
                        if (check != null && isTry == false)
                            return this._getNextID(true);
                        else if (check != null && isTry == true)
                            return 0;

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_FASTLINK.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            context.WZ_FASTLINK.DeleteObject(article);
                            context.SaveChanges();

                            //delete article content translate
                            var content_translates = context.WZ_FASTLINK_CONTENT.Where(ac=>ac.NID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_FASTLINK_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            public bool DeleteFile(int nid, string field)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_FASTLINK.Where(a => a.ID == nid).FirstOrDefault();
                        if (field == "imageAdmincp")
                        {
                            if (node != null)
                            {
                                node.IMAGE = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "iconAdmincp")
                        {
                            if (node != null)
                            {
                                node.ICON = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                        else if (field == "iconHoverAdmincp")
                        {
                            if (node != null)
                            {
                                node.ICON_HOVER = string.Empty;
                                context.SaveChanges();
                                return true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lstId)
            {
                try
                {
                    if (lstId != null)
                    {
                        foreach (int id in lstId)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            public List<FastLinkViewModel> GetFastLink(int type, int turn_on, string lang, List<int> ins)
            {
                List<FastLinkViewModel> result = new List<FastLinkViewModel>();

                try
                {
                    lang = string.IsNullOrEmpty(lang) ? StringHelper.language() : lang;
                    using (var dbContext = new DBContext())
                    {
                        var query = from t in dbContext.WZ_FASTLINK
                                    from c in dbContext.WZ_FASTLINK_CONTENT
                                    where c.NID == t.ID && t.STATUS == 1 && c.LANG == lang && t.TYPE == type
                                    orderby t.PRIORITY ascending, t.CREATED descending
                                    select new FastLinkViewModel
                                    {
                                        ID = (int)t.ID,
                                        TYPE = (int)t.TYPE,
                                        TURN_ON = (int)t.TURN_ON,
                                        NAME = c.NAME,
                                        LINK = c.LINK,
                                        ICON = t.ICON,
                                        ICON_HOVER = t.ICON_HOVER,
                                        IMAGE = t.IMAGE,
                                        CREATED = t.CREATED,
                                    };
                        if (turn_on >= 0) {
                            query = query.Where(t => t.TURN_ON == turn_on);
                        }
                        if (ins != null && ins.Count > 0) {
                            query = query.Where(t => ins.Contains((int)t.ID));
                        }
                        return query.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }
            #endregion
        }
    }
}