﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using System.Xml;
using WizardCMS.Const;
using CaptchaMvc.HtmlHelpers;
using WizardCMS.domain;
using System.Collections;
using System.Web.Script.Serialization;

namespace WizardCMS.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()
        {
            
        }

        /// <summary>
        /// Thông báo cái gì đó tới người dùng
        /// </summary>
        /// <returns></returns>
        public ActionResult Notice()
        {
            //Code xu ly bla bla
            //End code xu ly

            //Tra ve GUI Content
            return View("../FrontEnd/ThongBao");
        }

        public ActionResult BaoTri()
        {
            return View("../FrontEnd/BaoTri");
        }

        public ActionResult Index(string url = "")
        {
            /*
            if (Session["username"] == null)
            {
                return RedirectToAction("BaoTri");
            }
            */
            string currentLang = WizardCMS.Helpers.StringHelper.language();

            WizardCMS.Helpers.StringHelper.GetTranslateDictionary();

            string demo = StringHelper.lang("contact");

            var node = ArticlesServices.GetArticleByLink(url);

            if (Request != null && Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            if (url == string.Empty)
            {
                //Home page
                Hashtable searchParams = new Hashtable();
                searchParams.Add("parent", Constants.NODE_KHACH_HANG_TC);
                searchParams.Add("page", 1);
                searchParams.Add("perpage", 2);

                Hashtable sortby = new Hashtable();
                sortby.Add("PRIORITY", "ASC");
                searchParams.Add("sortby", sortby);
                var dich_vu = ArticlesServices.GetSearchContent(searchParams);

                var node_dvck_khcn = ArticlesServices.GetArticleContentById(Constants.NODE_DV_CK_CN);
                var node_dv_nhdt_khtc = ArticlesServices.GetArticleContentById(Constants.NODE_DV_NHDT_TC);

                Dictionary<ArticleViewModel, List<ArticleViewModel>> services = new Dictionary<ArticleViewModel, List<ArticleViewModel>>();
                //if (dich_vu != null)
                //{
                //    foreach (var item in dich_vu)
                //    {
                //        var featured = ArticlesServices.GetFeaturedContent(item.ID, 1, 5);
                //        services.Add(item, featured);
                //    }
                //}
                if (node_dvck_khcn != null)
                {
                    var featured = ArticlesServices.GetFeaturedContent(node_dvck_khcn.ID, 1, 5);
                    services.Add(node_dvck_khcn, featured);
                }
                if (node_dv_nhdt_khtc != null)
                {
                    var featured = ArticlesServices.GetFeaturedContent(node_dv_nhdt_khtc.ID, 1, 5);
                    services.Add(node_dv_nhdt_khtc, featured);
                }
                ViewBag.services = services;

                //Fast link under the home banner
                ViewBag.fastLinks = FastLinkServices.GetFastLink(1,-1);
                //ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                ViewBag.fastLinkBottomAll = FastLinkServices.GetFastLink(2, 0);
                ViewBag.personalSupport = ArticlesServices.GetArticleContentById(131);

                //HOT NEW
                searchParams = new Hashtable();
                searchParams.Add("page", 1);
                searchParams.Add("perpage", 5);
                searchParams.Add("parent_ins", new int[] { Constants.NODE_TIN_MBS, Constants.NODE_MBS_BAO_CHI });
                ViewBag.Hotnews = ArticlesServices.GetSearchContent(searchParams);

                //banner
                ViewBag.banners = SlideServices.GetList(currentLang);

                var home_view = StringHelper.language() == "en" ? "Index_en" : "Index";
                return View(home_view);
            }
            else if (node != null)
            {
                var switch_lang = StringHelper.language() == "en" ? "vi" : "en";
                ViewBag.switch_url = ArticlesServices.GetNodeLink(node.ID, switch_lang);

                if (node.TEMPLATE == Constants.TEMPLATE_LIEN_HE)
                {
                    //return Redirect(StringHelper.base_url_lang() + "contact");
                    return this.Contact();
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_GIOI_THIEU)
                {
                    return this.About(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_GOC_TR_THONG)
                {
                    var childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
                    if (childs != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + childs[0].LINK);
                    }
                    else
                    {
                        return Redirect(StringHelper.base_url_lang());
                    }
                }
                else if (node.ID == Constants.NODE_QUAN_HE_CD)
                {
                    var childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
                    if (childs != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + childs[0].LINK);
                    }
                    else {
                        return Redirect(StringHelper.base_url_lang());
                    }
                    //return this.Communications(node);
                }
                else if (node.ID == Constants.NODE_TIN_CO_DONG)
                {
                    return this.QHCD_News(node);
                }
                else if (node.ID == Constants.NODE_CONGBO_THONGTIN)
                {
                    return this.QHCD_News(node);
                }
                else if (node.ID == Constants.NODE_VANHOA_NOIBO)
                {
                    return this.NewsCulture(node);
                }
                else if (node.ID == Constants.NODE_CO_HOI_NN)
                {
                    return Redirect(StringHelper.base_url_lang() + StringHelper.lang("slug_tuyen_dung") + "/" + StringHelper.lang("slug_vitri_tuyendung"));
                }
                else if (node.ID == Constants.NODE_KHACH_HANG_CN)
                {
                    var node_child = ArticlesServices.GetArticleContentById(Constants.NODE_MOI_GIOI_KH_CN);
                    if (node_child != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + node_child.LINK);
                    }

                    var childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
                    if (childs != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + childs[0].LINK);
                    }
                    else
                    {
                        return Redirect(StringHelper.base_url_lang());
                    }
                }
                else if (node.ID == Constants.NODE_KHACH_HANG_TC)
                {
                    var node_child = ArticlesServices.GetArticleContentById(Constants.NODE_TUVAN_COPHIEU_KH_TC);
                    if (node_child != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + node_child.LINK);
                    }

                    var childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
                    if (childs != null)
                    {
                        return Redirect(StringHelper.base_url_lang() + childs[0].LINK);
                    }
                    else
                    {
                        return Redirect(StringHelper.base_url_lang());
                    }
                }
                else if (node.ID == Constants.NODE_DV_DOI_NGU)
                {
                    return this.OurTeam(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_CHAM_SOC_KH)
                {
                    return this.CustomerCare(node);
                }
                else if (node.ID == Constants.NODE_HUONG_DAN)
                {
                    return this.Guide(node);
                }
                else if (node.ID == Constants.NODE_HOI_THAO_DAO_TAO)
                {
                    return this.Workshop(node);
                }
                else if (node.ID == Constants.NODE_CHUONG_TRINH_UU_DAI)
                {
                    return this.Promotion(node);
                }
                else if (node.ID == Constants.NODE_CAU_HOI_THUONG_GAP)
                {
                    return this.FAQ(node);
                }
                else if (node.ID == Constants.NODE_TRUNGTAM_PHANTICH)
                {
                    return this.AnalyzeCenter(node);
                }
                else if (node.ID == Constants.NODE_TINTUC_THITRUONG)
                {
                    return this.MarketNews(node);
                }
                else if (node.ID == Constants.NODE_BAOCAO_PHANTICH)
                {
                    return this.AnalyzeReport(node);
                }
                else if (node.ID == Constants.NODE_PT_DULIEU_TT)
                {
                    return this.DuLieuThiTruong(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_DICH_VU)
                {
                    return this.Service(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_KHACH_HANG)
                {
                    return this.HighlightDeals(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_BAO_CAO)
                {
                    return this.Report(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_TIN_TUC)
                {
                    return this.News(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_VAN_BAN)
                {
                    return this.VBPL(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_TAM_NHIN)
                {
                    return this.Vision(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_GIA_TRI_COT_LOI)
                {
                    return this.CoreValues(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_SO_DO_TO_CHUC)
                {
                    return this.OrganizeChart(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_THANH_TUU)
                {
                    return this.Appcomplish(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_LANH_DAO)
                {
                    return this.Leader(node);
                }
                else if (node.TEMPLATE == Constants.TEMPLATE_MANG_LUOI)
                {
                    return this.Network(node);
                }
                else if (node.ID == Constants.NODE_TL_THAM_KHAO_CKPS
                    || node.PARENT_ID == Constants.NODE_TL_THAM_KHAO_CKPS)
                {
                    return this.DerivativeSecuritiesList(node, null);
                }
                else
                {
                    var Parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);
                    if (Parent != null)
                    {
                        if (Parent.TEMPLATE == Constants.TEMPLATE_DICH_VU)
                            return this.ServiceDetail(node, Parent);
                        if (Parent.TEMPLATE == Constants.TEMPLATE_BAO_CAO)
                            return this.ReportDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_CHUONG_TRINH_UU_DAI)
                            return this.PromotionDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_VANHOA_NOIBO)
                            return this.NewsCultureDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_HOI_THAO_DAO_TAO)
                            return this.WorkshopDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_HUONG_DAN_CHILD)
                            return this.GuideDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_THITRUONG_CK || Parent.ID == Constants.NODE_TIN_VI_MO || Parent.ID == Constants.NODE_TIN_THE_GIOI)
                            return this.GuideDetail(node, Parent);
                        if (Parent.ID == Constants.NODE_BAOCAO_PHANTICH)
                            return this.AnalyzeReportList(node, Parent);
                        if (Parent.PARENT_ID == Constants.NODE_BAOCAO_PHANTICH)
                            return this.AnalyzeReportDetail(node, Parent);
                        if (Parent.PARENT_ID == Constants.NODE_TL_THAM_KHAO_CKPS)
                            return this.DerivativeSecuritiesDetail(node, Parent);
                    }

                    //Default detail page
                    return this.NewsDetail(node, Parent);
                }
            }
            else 
            {
                return this.Page404();
                //return RedirectToAction("Page404");
            }

            
            //return View();
        }

        //Set cookie fastlinks user added
        public string SetFastLink() 
        {
            try
            {
                int id = StringHelper.IsNumber(Request["id"]);
                if (id > 0)
                {
                    var item = FastLinkServices.GetFastLinkContentById(id);

                    if (item != null && Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
                    {
                        var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                        if (deserialized != null)
                        {
                            if (deserialized.Contains(item.ID))
                            {
                                deserialized.Remove(item.ID);
                            }

                            deserialized.Insert(0, item.ID);

                            HttpCookie cookie = new HttpCookie("UserSettings");
                            string serializedObject = new JavaScriptSerializer().Serialize(deserialized);
                            cookie.Value = HttpUtility.UrlEncode(serializedObject);
                            cookie.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Add(cookie);
                        }
                        else
                        {
                            List<int> userSettings = new List<int>();
                            userSettings.Add(item.ID);

                            HttpCookie cookie = new HttpCookie("UserSettings");
                            string serializedObject = new JavaScriptSerializer().Serialize(userSettings);
                            cookie.Value = HttpUtility.UrlEncode(serializedObject);
                            cookie.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Add(cookie);
                        }
                        return "success";
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return "fail";
        }

        #region BAO CAO
        public ActionResult Report(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            int year_report = !string.IsNullOrEmpty(Request["year_report"]) && StringHelper.IsNumeric((Request["year_report"])) ? int.Parse((Request["year_report"])) : 0;
            int archived = !string.IsNullOrEmpty(Request["archived"]) && StringHelper.IsNumeric((Request["archived"])) ? int.Parse((Request["archived"])) : 0;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;

            List<int> in_years = new List<int>();
            List<int> years = new List<int>();
            for (int i = DateTime.Now.Year; i > 2005; i--)
            {
                int count = ArticlesServices.GetCountSearchReport(node.ID, i, null , "", "", "", "");
                if (count > 0)
                {
                    if (years.Count >= 3)
                        in_years.Add(i);
                    else
                        years.Add(i);
                }
            }

            var Childs = new List<ArticleViewModel>();

            if (year_report > 0)
            {
                int totalRows = ArticlesServices.GetCountSearchReport(node.ID, year_report, null, dateFrom, dateTo, "", StringHelper.language());
                AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
                pagination.Perpage = 10;

                ViewBag.pagination = pagination.CreateLinks();
                Childs = ArticlesServices.GetSearchReport(node.ID, year_report, null, page, pagination.Perpage, "", dateFrom, dateTo);
            }
            else if (archived == 1)
            {
                int totalRows = ArticlesServices.GetCountSearchReport(node.ID, 0, in_years, dateFrom, dateTo, "", StringHelper.language());
                AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
                pagination.Perpage = 10;

                ViewBag.pagination = pagination.CreateLinks();
                Childs = ArticlesServices.GetSearchReport(node.ID, 0, in_years, page, pagination.Perpage, "", dateFrom, dateTo);
            }
            else if (years.Count > 0)
            {
                int totalRows = ArticlesServices.GetCountSearchReport(node.ID, years[0], null, dateFrom, dateTo, "", StringHelper.language());
                AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
                pagination.Perpage = 10;

                ViewBag.pagination = pagination.CreateLinks();
                Childs = ArticlesServices.GetSearchReport(node.ID, years[0], null, page, pagination.Perpage, "", dateFrom, dateTo);
            }
            
            if (Request.IsAjaxRequest())
            {
                return View("../Home/Report/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.years = years;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/Report/List", node);
            }
        }

        public ActionResult ReportDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            //ArticlesServices.GetSearchReport(node.ID, years[0], null, page, pagination.Perpage);
            ViewBag.related = ArticlesServices.GetSearchReport(node.PARENT_ID, node.YEAR_REPORT, null, 1, 4, "", "", "", node.ID);

            ViewBag.parent = parent;

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/Report/Detail", node);
        }

        #endregion

        #region TIN TUC
        public ActionResult News(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();

            //System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo);
            //sw.Stop();
            //Console.WriteLine("Thoi gian dem: " + sw.ElapsedMilliseconds);

            if (Request.IsAjaxRequest())
            {
                ViewBag.parent = node;
                return View("../Home/News/AjaxNewsList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/News/NewsList", node);
            }
        }

        public ActionResult VBPL(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.Perpage = 100;
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();

            //System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo);
            //sw.Stop();
            //Console.WriteLine("Thoi gian dem: " + sw.ElapsedMilliseconds);

            if (Request.IsAjaxRequest())
            {
                ViewBag.parent = node;
                return View("../Home/News/AjaxVBPL", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/News/VBPL", node);
            }
        }

        public ActionResult NewsDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.parent = parent;
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/News/Detail", node);
        }

        public ActionResult NewsCulture(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");
            //pagination.Perpage = 4;

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/NewsCulture/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/NewsCulture/List", node);
            }
        }

        public ActionResult NewsCultureDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.parent = parent;
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", node.CREATED.ToString("dd-MM-yyyy"), new int[] { node.ID });

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/NewsCulture/Detail", node);
        }

        [HttpPost]
        public ActionResult AjaxGetNews()
        {
            int parent = !string.IsNullOrEmpty(Request.QueryString["parrent"]) && StringHelper.IsNumeric((Request.QueryString["parrent"])) ? int.Parse((Request.QueryString["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request.QueryString["page"]) && StringHelper.IsNumeric((Request.QueryString["page"])) ? int.Parse((Request.QueryString["page"])) : 1;
            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(parent, "", "", "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(parent, totalRows, currentPath, 1, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(parent, page);
            return View("../Home/News/AjaxNewsList", Childs);
        }
        #endregion

        #region DV CHUNG KHOAN
        public ActionResult Brokerage(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, "", "", "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Brokerage/AjaxNewsList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/Service/List", node);
            }
        }

        public ActionResult Service(ArticleViewModel node)
        {
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

            var nodes = same_levels == null ? new int[] {node.ID} : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            return View("../Home/Service/List", node);
        }

        public ActionResult ServiceDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.parent = parent;
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);

            return View("../Home/Service/Detail", node);
        }

        //
        public ActionResult DichVuDienTuCN()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_DV_DIEN_TU_CN);
            var parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            ViewBag.parent = parent;

            ViewBag.switch_url = StringHelper.lang("link_dvdt_cn", true);
            return View("../Home/Service/DVDT", node);
        }

        public ActionResult DichVuDienTuTC()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            var node = ArticlesServices.GetArticleContentById(Constants.NODE_DV_DIEN_TU);
            var parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            ViewBag.parent = parent;
            ViewBag.switch_url = StringHelper.lang("link_dvdt_tc", true);
            return View("../Home/Service/DVDT", node);
        }

        //THUONG VU DIEN HINH
        public ActionResult HighlightDeals(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, "", "", "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.Perpage = 9;
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();

            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/ThuongVu/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var parentNode = ArticlesServices.GetArticleContentById(node.PARENT_ID);
                var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parentNode.PARENT_ID : node.PARENT_ID);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
                ViewBag.parent = parentNode;
                return View("../Home/ThuongVu/List", node);
            }
        }

        public ActionResult BieuPhi()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_DV_BIEU_PHI_CN);
            var parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            ViewBag.parent = parent;
            ViewBag.switch_url = StringHelper.lang("link_hieu_phi_cn", true);
            return View("../Home/Service/BieuPhi", node);
        }

        public ActionResult BieuPhiTC()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_DV_BIEU_PHI_TC);
            var parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            ViewBag.parent = parent;
            ViewBag.switch_url = StringHelper.lang("link_bieu_phi_tc", true);
            return View("../Home/Service/BieuPhi", node);
        }

        public ActionResult OurTeam(ArticleViewModel node)
        {
            Hashtable searchParams = new Hashtable();
            searchParams.Add("parent", node.ID);

            var Childs = ArticlesServices.GetSearchContent(searchParams);

            var parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);

            ViewBag.childs = Childs;
            ViewBag.parent = parent;

            var same_levels = ArticlesServices.GetArticleByParrent(parent!=null ? parent.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            return View("../Home/Service/OurTeam", node);
        }
        #endregion

        #region MODULE GIOI THIEU
        public ActionResult MenuAbout()
        {
            var result = "";
            return PartialView("../Home/About/menu", result);
        }

        public ActionResult MenuAboutMobile()
        {
            var result = "";
            return PartialView("../Home/About/menu_mobile", result);
        }

        public ActionResult About(ArticleViewModel node)
        {
            ViewBag.NodeLoiThe = ArticlesServices.GetArticleContentById(Constants.NODE_LOI_THE_CT);
            ViewBag.LoiTheCanhTranhs = ArticlesServices.GetArticleByParrent(Constants.NODE_LOI_THE_CT, 1);

            ViewBag.NodeSuKien = ArticlesServices.GetArticleContentById(Constants.NODE_MOC_SUKIEN);
            ViewBag.MocSuKiens = ArticlesServices.GetArticleByParrent(Constants.NODE_MOC_SUKIEN, 2);

            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);

            var switch_lang = StringHelper.language() == "en" ? "vi" : "en";
            ViewBag.switch_url = ArticlesServices.GetNodeLink(node.ID, switch_lang);
            return View("../Home/About/About", node);
        }

        public ActionResult Vision(ArticleViewModel node)
        {
            //get list childs node of core values
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);

            var switch_lang = StringHelper.language() == "en" ? "vi" : "en";
            ViewBag.switch_url = ArticlesServices.GetNodeLink(node.ID, switch_lang);
            return View("../Home/About/Vision", node);
        }
        /// <summary>
        /// Giá trị cốt lõi
        /// </summary>
        /// <returns></returns>
        public ActionResult CoreValues( ArticleViewModel node )
        {
            //get list childs node of core values
            ViewBag.childs = ArticlesServices.GetArticlesByParentId(node.ID,1,10);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/CoreValues", node);
        }

        /// <summary>
        /// Sơ đồ tổ chức
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ActionResult OrganizeChart(ArticleViewModel node)
        {
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/OrganizeChart", node);
        }

        /// <summary>
        /// Thành tựu
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ActionResult Appcomplish(ArticleViewModel node)
        {
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/Appcomplish", node);
        }

        /// <summary>
        /// Lãnh đạo
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ActionResult Leader(ArticleViewModel node)
        {
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/Leader", node);
        }

        /// <summary>
        /// Mạng lưới
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ActionResult Network(ArticleViewModel node)
        {
            /* NEW CODE
            var hoiso = BranchServices.GetBranchsByGroup(Constants.GROUP_HOISO);
            var norths = BranchServices.GetBranchsByRegion(Constants.REGION_BAC);
            var middles = BranchServices.GetBranchsByRegion(Constants.REGION_TRUNG);
            var souths = BranchServices.GetBranchsByRegion(Constants.REGION_NAM);

            Dictionary<string, List<BranchViewModel>> branchs = new Dictionary<string, List<BranchViewModel>>();
            if (norths != null && norths.Count > 0) branchs.Add(StringHelper.lang("text_mien_bac"), norths);
            if (middles != null && middles.Count > 0) branchs.Add(StringHelper.lang("text_mien_trung"), middles);
            if (souths != null && souths.Count > 0) branchs.Add(StringHelper.lang("text_mien_nam"), souths);

            ViewBag.branchs = branchs;
            //if (hoiso != null && hoiso.Count > 0)
                //ViewBag.hoiso = hoiso[0];
            ViewBag.hoiso = hoiso;
             * */
            //ViewBag.transactions = BranchServices.GetBranchsByGroup(Constants.GROUP_DIEM_GD);
            ViewBag.hoisos = BranchServices.GetBranchsByGroup(Constants.GROUP_HOISO);
            ViewBag.branchs = BranchServices.GetBranchsByGroup(Constants.GROUP_HOISO_CN);
            var transactions = BranchServices.GetBranchsByGroup(Constants.GROUP_DIEM_GD);
            ViewBag.transactions = transactions.OrderBy(x => x.REGION).ThenBy(x => x.PRIORITY).ToList();
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/Network", node);
        }
        #endregion

        #region GOC TRUYEN THONG
        /// <summary>
        /// Góc truyền thông
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ActionResult Communications(ArticleViewModel node)
        {
            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 2);
            ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
            return View("../Home/About/Communications", node);
        }
        #endregion

        #region QUAN HE CO DONG
        public ActionResult QHCD_News(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];

            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/QHCD/AjaxNewsList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.menus = ArticlesServices.GetMenuByListId(new int[] { Constants.NODE_GIOI_THIEU, Constants.NODE_QUAN_HE_CD, Constants.NODE_GOC_TT, Constants.NODE_CO_HOI_NN }, 1);
                return View("../Home/QHCD/NewsList", node);
            }
        }
        #endregion

        #region HUONG DAN GIAO DICH
        public ActionResult Guide(ArticleViewModel node)
        {
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            int term = !string.IsNullOrEmpty(Request["term"]) && StringHelper.IsNumeric((Request["term"])) ? int.Parse((Request["term"])) : 0;
            string searchText = !string.IsNullOrEmpty(Request["searchText"]) ? Request["searchText"] : string.Empty;
            string currentPath = Request.Url.OriginalString;

            Hashtable searchParams = new Hashtable();
            searchParams.Add("parent", Constants.NODE_HUONG_DAN_CHILD);
            searchParams.Add("term", term);
            searchParams.Add("page", page);
            searchParams.Add("searchText", searchText);

            int totalRows = ArticlesServices.GetCountSearchContent(searchParams);
            AjaxPagination pagination = new AjaxPagination(Constants.NODE_HUONG_DAN_CHILD, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");
            pagination.Perpage = 100;
            ViewBag.pagination = pagination.CreateLinks();

            searchParams.Add("perpage", pagination.Perpage);
            var Childs = ArticlesServices.GetSearchContent(searchParams);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Guide/AjaxList", Childs);
            }
            else
            {
                ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
                ViewBag.node_huong_dan = ArticlesServices.GetArticleContentById(Constants.NODE_HUONG_DAN_CHILD);
                ViewBag.bieuphi = ArticlesServices.GetArticleContentById(Constants.NODE_HUONG_DAN_BIEU_PHI);
                ViewBag.terms = TermServices.GetTerms(Constants.TAXO_HUONGDAN_GD, StringHelper.language());

                ViewBag.childs = Childs;

                ViewBag.fees = ArticlesServices.GetArticleByParrent(Constants.NODE_DV_BIEU_PHI_TC, 1);
                ViewBag.videos = ArticlesServices.GetFeaturedContent(Constants.NODE_VIDEO_HUONG_DAN, 1, 2);

                var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/Guide/List", node);
            }
        }

        public ActionResult GuideDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.parent = parent;
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.node_huongdan = ArticlesServices.GetArticleContentById(Constants.NODE_HUONG_DAN);
            ViewBag.previous = ArticlesServices.GetPreviousNode(node.ID, node.PARENT_ID);
            ViewBag.next = ArticlesServices.GetNextNode(node.ID, node.PARENT_ID);

            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", "", new int[] { node.ID });

            var parentNode = parent != null ? ArticlesServices.GetArticleContentById(parent.PARENT_ID) : parent;
            var same_levels = ArticlesServices.GetArticleByParrent(parentNode != null ? parentNode.PARENT_ID : node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/Guide/Detail", node);
        }
        #endregion

        private List<ArticleViewModel> GetRepresentativeArticleOfCKPS(int pNumberRow)
        {
            List<ArticleViewModel> objSameLevels = ArticlesServices.GetArticleByParrent(Constants.NODE_TL_THAM_KHAO_CKPS);
            List<ArticleViewModel> objAllArticle = new List<ArticleViewModel>();

            foreach(ArticleViewModel subItem in objSameLevels)
            {
                List<ArticleViewModel> subList = ArticlesServices.GetFeaturedContent(subItem.ID, 1, pNumberRow);
                foreach (var item in subList)
                    objAllArticle.Add(item);
            }

            return objAllArticle;
        }

        public ActionResult CustomerCare(ArticleViewModel node)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            var same_levels = ArticlesServices.GetArticleByParrent(node.ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.guides = ArticlesServices.GetFeaturedContent(Constants.NODE_HUONG_DAN_CHILD, 1, 3);
            
            ViewBag.workshops = ArticlesServices.GetFeaturedContent(Constants.NODE_HOI_THAO_DAO_TAO, 1, 3);
            ViewBag.promotions = ArticlesServices.GetFeaturedContent(Constants.NODE_CHUONG_TRINH_UU_DAI, 1, 3);

            ViewBag.tlckps = GetRepresentativeArticleOfCKPS(2);

            var faq_terms = TermServices.GetTerms(Constants.TAXO_FAQ, StringHelper.language());

            Dictionary<string, List<ArticleViewModel>> faqs = new Dictionary<string, List<ArticleViewModel>>();
            if (faq_terms != null) {
                int count = 0;
                foreach (var term in faq_terms)
                {
                    //Hashtable searchParams = new Hashtable();
                    //searchParams.Add("parent", Constants.NODE_CAU_HOI_THUONG_GAP);
                    //searchParams.Add("term", term.ID);
                    //searchParams.Add("featured", 1);
                    //searchParams.Add("page", 1); searchParams.Add("perpage", 4);

                    var term_faq = ArticlesServices.GetFeaturedContent(Constants.NODE_CAU_HOI_THUONG_GAP, 1, 4, term.ID);
                    faqs.Add(term.NAME, term_faq);

                    if (count == 1) break;
                    count++;
                }
            }
            ViewBag.faqs = faqs;

            return View("../Home/CustomerCare");
        }

        #region HOI THAO DAO TAO
        public ActionResult Workshop(ArticleViewModel node)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            var featured = ArticlesServices.GetFeaturedContent(node.ID, 1, 1);

            int[] not_ins = null;
            if (featured != null && featured.Count > 0)
            {
                not_ins = featured.Select(a => a.ID).ToArray();
                ViewBag.featured = featured[0];
            }

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language(), not_ins);
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo, not_ins);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Workshop/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                Hashtable searchParams = new Hashtable();
                searchParams.Add("parent", node.ID);
                searchParams.Add("deadline", 1);
                //searchParams.Add("dateFrom", dateFrom);
                //searchParams.Add("dateTo", dateTo);
                ViewBag.workshops = ArticlesServices.GetSearchContent(searchParams);

                var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/Workshop/List", node);
            }
        }

        public ActionResult WorkshopDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", "", new int[] { node.ID });

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/Workshop/Detail", node);
        }
        #endregion

        #region CHỨNG KHOÁN PHÁI SINH
        public ActionResult DerivativeSecuritiesList(ArticleViewModel node, ArticleViewModel pParents)
        {
            ViewBag.node_ck_phai_sinh = ArticlesServices.GetArticleContentById(Constants.NODE_TL_THAM_KHAO_CKPS);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            //var featured = ArticlesServices.GetFeaturedContent(node.ID, 1, 1);

            //int[] not_ins = null;
            //if (featured != null && featured.Count > 0)
            //{
            //    not_ins = featured.Select(a => a.ID).ToArray();
            //    ViewBag.featured = featured[0];
            //}

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, "", "", "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            List<ArticleViewModel> Childs;
            List<ArticleViewModel> objSameLevels;
            string sShowStyle = "";

            if (node.ID == Constants.NODE_TL_THAM_KHAO_CKPS)
            {
                objSameLevels = ArticlesServices.GetArticleByParrent(Constants.NODE_TL_THAM_KHAO_CKPS);
                Childs = GetRepresentativeArticleOfCKPS(4);
                sShowStyle = "ALL_CATEGORY";
            }
            else
            {
                objSameLevels = new List<ArticleViewModel>();
                objSameLevels.Add(ArticlesServices.GetArticleContentById(node.ID));

                Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage * 2, "", "", "");
                sShowStyle = "SINGLE_CATEGORY";
            }

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Workshop/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                Hashtable searchParams = new Hashtable();
                searchParams.Add("parent", node.ID);
                searchParams.Add("deadline", 1);
                //searchParams.Add("dateFrom", dateFrom);
                //searchParams.Add("dateTo", dateTo);
                ViewBag.workshops = ArticlesServices.GetSearchContent(searchParams);
                ViewBag.list_category = objSameLevels;
                ViewBag.show_style = sShowStyle;

                var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_CHAM_SOC_KH);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/CKPS/List", node);
            }
        }
        public ActionResult DerivativeSecuritiesDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_ck_phai_sinh = ArticlesServices.GetArticleContentById(Constants.NODE_TL_THAM_KHAO_CKPS);
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", "", new int[] { node.ID });

            var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_CHAM_SOC_KH);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/CKPS/Detail", node);
        }
        #endregion


        #region CHUONG TRINH UU DAI
        public ActionResult Promotion(ArticleViewModel node)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            var featured = ArticlesServices.GetFeaturedContent(node.ID, 1, 1);
            
            int[] not_ins = null;
            if (featured != null)
            {
                not_ins = featured.Select(a => a.ID).ToArray();
                ViewBag.featured = featured[0];
            }

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, "", "", "", StringHelper.language(), not_ins);
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", "", "", not_ins);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Promotion/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/Promotion/List", node);
            }
        }

        public ActionResult PromotionDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", "", new int[] { node.ID });

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/Promotion/Detail", node);
        }
        #endregion

        #region FAQ
        public ActionResult FAQ(ArticleViewModel node)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            int term = !string.IsNullOrEmpty(Request["term"]) && StringHelper.IsNumeric((Request["term"])) ? int.Parse((Request["term"])) : 0;
            string searchText = !string.IsNullOrEmpty(Request["searchText"]) ? Request["searchText"] : string.Empty;

            Hashtable searchParams = new Hashtable();
            searchParams.Add("parent", node.ID);
            searchParams.Add("term", term);
            searchParams.Add("searchText", searchText);

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(searchParams);
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(searchParams);

            if (Request.IsAjaxRequest())
            {
                ViewBag.search_text = searchText;
                if (term > 0) {
                    ViewBag.term = TermServices.GetTermContentById(term);
                    if (ViewBag.term != null && !string.IsNullOrEmpty(ViewBag.term.SERVICES))
                    {
                        string[] services = ViewBag.term.SERVICES.Split(',');
                        int[] ins = Array.ConvertAll(services, int.Parse);

                        Hashtable searchParams2 = new Hashtable();
                        searchParams2.Add("ins", ins);

                        ViewBag.services = ArticlesServices.GetSearchContent(searchParams2);
                    }
                }
                return View("../Home/FAQ/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                ViewBag.terms = TermServices.GetTerms(Constants.TAXO_FAQ, StringHelper.language());
                var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/FAQ/List", node);
            }
        }

        public ActionResult FAQDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_cham_soc = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", "", new int[] { node.ID });

            var same_levels = ArticlesServices.GetArticleByParrent(parent != null ? parent.PARENT_ID : node.PARENT_ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/FAQ/Detail", node);
        }
        #endregion

        #region LICH SU KIEN
        public ActionResult Events()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.node_cham_soc = node;

            var same_levels = ArticlesServices.GetArticleByParrent(node.ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.switch_url = StringHelper.lang("link_lich_sk", true);
            return View("../Home/Events");
        }
        #endregion

        #region LIÊN HỆ
        [HttpPost, ValidateInput(false)]
        public ActionResult Contact()
        {
            if (Request.IsAjaxRequest())
            {
                //post data from contact form
                string fullname     = Request["ct_fullname"];
                string email        = Request["ct_email"];
                string phone        = Request["ct_phone"];
                string address      = Request["ct_address"];
                string title        = Request["ct_title"];
                string content      = Request["ct_content"];
                string CaptchaValue = Request["CaptchaValue"];
                string InvisibleCaptchaValue = Request["InvisibleCaptchaValue"];

                if (StringHelper.IsNullOrEmptyOrWhiteSpace(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_require") });
                }
                if (StringHelper.MaxLength(fullname, 64) || StringHelper.MinLength(fullname, 5))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_length") });
                }
                if (!StringHelper.ValidFullname(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_invalid") });
                }

                bool cv = CaptchaController.IsValidCaptchaValue(CaptchaValue.ToUpper());
                bool icv = InvisibleCaptchaValue == "";

                if (!cv || !icv)
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

                if (ModelState.IsValid)
                {
                    //
                    ContactViewModel contact = new ContactViewModel();
                    contact.FULLNAME = fullname;
                    contact.EMAIL = email;
                    contact.PHONE = phone;
                    contact.ADDRESS = address;
                    contact.TITLE = title;
                    contact.CONTENT = StringHelper.StripTags( content );

                    if (ContactServices.Insert(contact) >= 1)
                    {
                        ModelState.Clear();
                        return Json(new { st = 1, field = "CaptchaInputText", msg = "Gửi thông tin liên hệ thành công." });
                    }
                    else
                    {

                    }
                }
                else
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }
                
            }

            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.node_cham_soc = node;

            var same_levels = ArticlesServices.GetArticleByParrent(node.ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            ViewBag.switch_url = StringHelper.lang("lien-he-link", true);
            return View("../FrontEnd/Contact");
        }
        #endregion

        #region DANG KY HOI THAO
        [HttpPost, ValidateInput(false)]
        public ActionResult RegisterWorkshop()
        {
            if (Request.IsAjaxRequest())
            {
                //post data from contact form
                //int nid = StringHelper.IsNumber(Request["ct_workshop"]);
                string workshops = Request["ct_workshop"];
                string fullname = Request["ct_fullname"];
                string cmnd = Request["ct_cmnd"];
                string cmnd_ngaycap = Request["ct_cmnd_ngaycap"];
                string cmnd_noicap = Request["ct_cmnd_noicap"];
                string email = Request["ct_email"];
                string phone = Request["ct_phone"];
                string bank_card = Request["ct_bank_card"];
                string bank_card_cn = Request["ct_bank_card_cn"];
                string mo_tai = Request["ct_mo_tai"];
                string taikhoan_mbs = Request["ct_taikhoan_mbs"];
                string CaptchaValue = Request["CaptchaValue"];
                string InvisibleCaptchaValue = Request["InvisibleCaptchaValue"];
                //if (nid <= 0)
                //{
                //    return Json(new { st = 0, field = "ct_workshop", msg = StringHelper.lang("workshop_require") });
                //}
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(workshops))
                {
                    return Json(new { st = 0, field = "ct_workshop", msg = StringHelper.lang("workshop_require") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_require") });
                }
                if (StringHelper.MaxLength(fullname, 64) || StringHelper.MinLength(fullname, 5))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_length") });
                }
                if (!StringHelper.ValidFullname(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_invalid") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(cmnd))
                {
                    return Json(new { st = 0, field = "ct_cmnd", msg = StringHelper.lang("cmnd_require") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(phone))
                {
                    return Json(new { st = 0, field = "ct_phone", msg = StringHelper.lang("phone_require") });
                }
                if (!StringHelper.IsNumeric(phone))
                {
                    return Json(new { st = 0, field = "ct_phone", msg = StringHelper.lang("phone_invalid") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(email))
                {
                    return Json(new { st = 0, field = "ct_email", msg = StringHelper.lang("email_require") });
                }

                bool cv = CaptchaController.IsValidCaptchaValue(CaptchaValue.ToUpper());
                bool icv = InvisibleCaptchaValue == "";

                if (!cv || !icv)
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

                if (ModelState.IsValid)
                {
                    //
                    var temp = workshops.Split(',');
                    if (temp != null) {
                        foreach (var ws in temp) {
                            RegWorkshopViewModel viewModel = new RegWorkshopViewModel();
                            viewModel.NID = int.Parse(ws) ;
                            viewModel.FULLNAME = fullname;
                            viewModel.EMAIL = email;
                            viewModel.PHONE = phone;
                            viewModel.CMND = cmnd;
                            viewModel.CMND_NGAYCAP = null;
                            if (!string.IsNullOrEmpty(cmnd_ngaycap))
                                viewModel.CMND_NGAYCAP = DatetimeHelper.getDateTimeWithString(cmnd_ngaycap.Replace('-', '/'));
                            viewModel.CMND_NOICAP = cmnd_noicap;
                            viewModel.BANK_CARD = bank_card;
                            viewModel.BANK_CARD_CN = bank_card_cn;
                            viewModel.MBS_ACCOUNT = taikhoan_mbs;
                            //viewModel.CONTENT = StringHelper.StripTags(content);

                            if (RegWorkshopServices.Insert(viewModel) >= 1)
                            {
                                
                                
                            }
                        }
                        ModelState.Clear();
                        return Json(new { st = 1, field = "CaptchaInputText", msg = "Đăng ký thành công." });
                    }
                    
                    else
                    {

                    }
                }
                else
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

            }
            return Json(new { st = 0, field = "error", msg = "you not have permission to access this page." });
        }
        #endregion

        #region DIEU KHOAN SU DUNG
        public ActionResult TermOfUse()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var nodes = new int[] { Constants.NODE_KHACH_HANG_CN, Constants.NODE_KHACH_HANG_TC, Constants.NODE_TRUNGTAM_PHANTICH, Constants.NODE_CHAM_SOC_KH, Constants.NODE_GIOI_THIEU};
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            if (StringHelper.language() == "en") {
                return View("../FrontEnd/TermOfUseEn");
            }

            ViewBag.switch_url = StringHelper.lang("dieu-khoan-su-dung-slug", true);
            return View("../FrontEnd/TermOfUse");
        }
        #endregion

        #region TRUNG TAM PHAN TICH
        public ActionResult AnalyzeCenter(ArticleViewModel node)
        {
            var childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            if (childs != null)
            {
                return Redirect(StringHelper.base_url_lang() + childs[0].LINK);
            }
            else
            {
                return Redirect(StringHelper.base_url_lang());
            }
        }

        public ActionResult AnalyzeReport(ArticleViewModel node)
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);

            ViewBag.childs = ArticlesServices.GetSearchContent(node.ID, 1, 0, "", "", "");

            var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            return View("../Home/BCPT/Group", node);
        }

        public ActionResult AnalyzeReportList(ArticleViewModel node, ArticleViewModel parentNode = null)
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(node.ID, dateFrom, dateTo, "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(node.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");
            pagination.Perpage = 10;

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = ArticlesServices.GetSearchContent(node.ID, page, pagination.Perpage, "", dateFrom, dateTo);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/BCPT/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_TRUNGTAM_PHANTICH);

                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/BCPT/List", node);
            }
        }

        public ActionResult AnalyzeReportDetail(ArticleViewModel node, ArticleViewModel parent = null)
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", node.CREATED.ToString("dd-MM-yyyy"), new int[] { node.ID });

            var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_TRUNGTAM_PHANTICH);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            Hashtable searchParams = new Hashtable();
            searchParams.Add("page", 1);
            searchParams.Add("perpage", 4);

            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.parent = parent;

            ViewBag.childs = ArticlesServices.GetArticleByParrent(node.ID, 1);
            return View("../Home/BCPT/Detail", node);
        }

        //Tin tuc thi truong
        public ActionResult MarketNews(ArticleViewModel node)
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            ViewBag.tabs = ArticlesServices.GetArticleByParrent(Constants.NODE_TINTUC_THITRUONG);

            int[] categories = Constants.CATEGORIES_THI_TRUONG;
            if (ViewBag.tabs != null && ViewBag.tabs.Count > 0)
            {
                if (ViewBag.tabs[0].ID == Constants.NODE_PT_VI_MO)
                    categories = Constants.CATEGORIES_VI_MO;
                else if (ViewBag.tabs[0].ID == Constants.NODE_PT_THE_GIOI)
                    categories = Constants.CATEGORIES_THE_GIOI;
                else if (ViewBag.tabs[0].ID == Constants.NODE_PT_CKPS)
                    categories = Constants.CATEGORIES_CKPS;

                ViewBag.category = ViewBag.tabs[0];
            }
            else 
            {
                Redirect(StringHelper.base_url());
            }

            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string currentPath = Request.Url.OriginalString;
            int totalRows = Search24Services.CountData(categories, dateFrom, dateTo);
            AjaxPagination pagination = new AjaxPagination(ViewBag.category.ID, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = Search24Services.GetData(categories, page, pagination.Perpage, "", null, dateTo, dateFrom);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/TTPT/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_TRUNGTAM_PHANTICH);
                var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                return View("../Home/TTPT/List", node);
            }
        }

        public ActionResult MarketNewsData()
        {
            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            int parent = !string.IsNullOrEmpty(Request["parrent"]) && StringHelper.IsNumeric((Request["parrent"])) ? int.Parse((Request["parrent"])) : 0;
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            ArticleViewModel objAttestorViewModel = ArticlesServices.GetArticleContentById(parent);
            ViewBag.category = objAttestorViewModel;

            int[] categories = Constants.CATEGORIES_THI_TRUONG;
            if (parent == Constants.NODE_PT_VI_MO)
                categories = Constants.CATEGORIES_VI_MO;
            else if (parent == Constants.NODE_PT_THE_GIOI)
                categories = Constants.CATEGORIES_THE_GIOI;
            else if (parent == Constants.NODE_PT_CKPS)
                categories = Constants.CATEGORIES_CKPS;

            string currentPath = Request.Url.OriginalString;
            var dateFrom = Request["dateFrom"] != null ? Request["dateFrom"] : "";
            var dateTo = Request["dateTo"] != null ? Request["dateTo"] : "";

            int totalRows = Search24Services.CountData(categories, "", dateFrom, dateTo);
            AjaxPagination pagination = new AjaxPagination(parent, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = Search24Services.GetData(categories, page, pagination.Perpage, "", null, dateFrom, dateTo);

            ViewBag.dateFrom = dateFrom;
            ViewBag.dateTo = dateTo;
            if (Request.IsAjaxRequest())
            {
                return View("../Home/TTPT/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_TRUNGTAM_PHANTICH);
                var nodes = same_levels == null ? new int[] { parent } : same_levels.Select(a => a.ID).ToArray();
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                var node = ArticlesServices.GetArticleContentById(parent);
                return View("../Home/TTPT/List", node);
            }
        }

        public ActionResult MarketNewsDetail(string category_slug, int message_id, string slug)
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);

            var category = ArticlesServices.GetArticleBySlug(category_slug, Constants.NODE_TINTUC_THITRUONG);
            if (category == null)
                return Redirect(StringHelper.base_url());

            int[] categories = Constants.CATEGORIES_THI_TRUONG;
            if (category.ID == Constants.NODE_TIN_VI_MO)
                categories = Constants.CATEGORIES_VI_MO;
            if (category.ID == Constants.NODE_TIN_THE_GIOI)
                categories = Constants.CATEGORIES_THE_GIOI;

            var node = Search24Services.Get(message_id);
            if (node == null)
                return Redirect(StringHelper.base_url());

            ViewBag.related = Search24Services.GetData(categories, 1, 4, "" ,new int[] {node.ID}, node.CREATED.ToString("dd-MM-yyyy HH:mm:ss"));
            ViewBag.category = category;

            var same_levels = ArticlesServices.GetArticleByParrent(Constants.NODE_TRUNGTAM_PHANTICH);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            ViewBag.switch_url = StringHelper.lang("link_ttpt_tintuc_tt", true) + "/";
            return View("../Home/TTPT/Detail", node);
        }

        public ActionResult DuLieuThiTruong(ArticleViewModel node, ArticleViewModel parent = null)
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var Parent = ArticlesServices.GetArticleContentById(node.PARENT_ID);
            ViewBag.parent = Parent;
            ViewBag.related = ArticlesServices.GetSearchContent(node.PARENT_ID, 1, 4, "", "", node.CREATED.ToString("dd-MM-yyyy"), new int[] { node.ID });

            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);

            ViewBag.childs = ArticlesServices.GetSearchContent(node.ID, 1, 0, "", "", "");

            var same_levels = ArticlesServices.GetArticleByParrent(node.PARENT_ID);

            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

            return View("../Home/TTPT/DLTT", node);
        }

        #endregion

        #region TUYENDUNG
        public ActionResult RE_Login()
        {
            if (Request.IsAjaxRequest())
            {
                //post data from contact form
                string username = Request["ct_username"];
                string password = Request["ct_password"];
                string CaptchaValue = Request["CaptchaValue"];
                string InvisibleCaptchaValue = Request["InvisibleCaptchaValue"];

                if (StringHelper.IsNullOrEmptyOrWhiteSpace(username))
                {
                    return Json(new { st = 0, field = "ct_username", msg = StringHelper.lang("username_require") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(password))
                {
                    return Json(new { st = 0, field = "ct_password", msg = StringHelper.lang("password_require") });
                }

                bool cv = CaptchaController.IsValidCaptchaValue(CaptchaValue.ToUpper());
                bool icv = InvisibleCaptchaValue == "";

                if (!cv || !icv)
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

                if (ModelState.IsValid)
                {
                    var user = UserServices.Login(username, password);
                    if (user != null)
                    {
                        //set session
                        Session["UID"] = user.ID;
                        Session["USERNAME"] = user.USERNAME;

                        ModelState.Clear();

                        string refer = Session["REFERER"].ToString();
                        refer = string.IsNullOrEmpty(refer) ? StringHelper.base_url_lang() + StringHelper.lang("link_vitri_tuyendung") : refer;

                        Session.Remove("REFERER");
                        return Json(new { st = 1, field = "CaptchaInputText", msg = "Đăng nhập thành công.", refer = refer });
                    }
                    else
                    {
                        return Json(new { st = 0, field = "re_username", msg = StringHelper.lang("username_or_password_incorrect") });
                    }
                }
                else
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

            }

            #region LIEN KET NHANH
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            #endregion

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.node_cham_soc = node;

            var same_levels = ArticlesServices.GetArticleByParrent(node.ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            return View("../Home/Recruitment/Login");
        }

        public ActionResult RE_Register()
        {
            if (Request.IsAjaxRequest())
            {
                //post data from contact form
                string fullname = Request["ct_fullname"];
                string email = Request["ct_email"];
                string username = Request["ct_username"];
                string password = Request["ct_password"];
                string password_confirm = Request["ct_password_confirm"];
                string CaptchaValue = Request["CaptchaValue"];
                string InvisibleCaptchaValue = Request["InvisibleCaptchaValue"];

                if (StringHelper.IsNullOrEmptyOrWhiteSpace(username))
                {
                    return Json(new { st = 0, field = "ct_username", msg = StringHelper.lang("username_require") });
                }
                if (StringHelper.MaxLength(username, 64) || StringHelper.MinLength(username, 5))
                {
                    return Json(new { st = 0, field = "ct_username", msg = StringHelper.lang("username_length") });
                }
                if (StringHelper.ValidateUsername(username)==false)
                {
                    return Json(new { st = 0, field = "ct_username", msg = StringHelper.lang("username_invalid") });
                }
                if (UserServices.UniqueUsername(username) == false)
                {
                    return Json(new { st = 0, field = "ct_username", msg = StringHelper.lang("username_unique") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(password))
                {
                    return Json(new { st = 0, field = "ct_password", msg = StringHelper.lang("password_require") });
                }
                if (StringHelper.MaxLength(password, 64) || StringHelper.MinLength(password, 5))
                {
                    return Json(new { st = 0, field = "ct_password", msg = StringHelper.lang("password_length") });
                }
                if (password_confirm != password)
                {
                    return Json(new { st = 0, field = "ct_password", msg = StringHelper.lang("password_match") });
                }
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_require") });
                }
                if (StringHelper.MaxLength(fullname, 64) || StringHelper.MinLength(fullname, 5))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_length") });
                }
                if (!StringHelper.ValidFullname(fullname))
                {
                    return Json(new { st = 0, field = "ct_fullname", msg = StringHelper.lang("fullname_invalid") });
                }
                if (!StringHelper.IsValidEmail(email))
                {
                    return Json(new { st = 0, field = "ct_email", msg = StringHelper.lang("email_invalid") });
                }
                if (UserServices.UniqueEmail(email) == false)
                {
                    return Json(new { st = 0, field = "ct_email", msg = StringHelper.lang("email_unique") });
                }

                bool cv = CaptchaController.IsValidCaptchaValue(CaptchaValue.ToUpper());
                bool icv = InvisibleCaptchaValue == "";

                if (!cv || !icv)
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

                if (ModelState.IsValid)
                {
                    UserViewModel viewModel = new UserViewModel();
                    viewModel.USERNAME = username;
                    viewModel.PASSWORD = StringHelper.EncryptMD5(password);
                    viewModel.FULLNAME = fullname;
                    viewModel.EMAIL = email;

                    byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                    byte[] key = Guid.NewGuid().ToByteArray();

                    string token = Convert.ToBase64String(time.Concat(key).ToArray());
                    ViewBag.hash = StringHelper.Base64Encode(viewModel.ID + "|" + token);
                    //token = token.Replace("/","");
                    viewModel.TOKEN = token;

                    return Json(new { st = 1, field = "CaptchaInputText", msg = "Đăng ký tài khoản thành công." });

                    if (UserServices.Insert(viewModel) >= 1)
                    {
                        //Send mail activate user account
                        var email_content = StringHelper.PartialView(this, "../Home/Email/Register", viewModel);
                        CommonHelper.SendEmail(email, email_content, StringHelper.lang("text_register_subject"));

                        ModelState.Clear();
                        return Json(new { st = 1, field = "CaptchaInputText", msg = "Đăng ký tài khoản thành công." });
                    }
                    return Json(new { st = 1, field = "CaptchaInputText", msg = "Đăng ký tài khoản thành công." });
                }
                else
                {
                    return Json(new { st = 0, field = "CaptchaInputText", msg = StringHelper.lang("captcha_invalid") });
                }

            }

            #region LIENKETNHANH
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var fastLinkBottoms = new List<FastLinkViewModel>();
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    foreach (var FID in deserialized)
                    {
                        var item = FastLinkServices.GetFastLinkContentById(FID);
                        if (item != null)
                        {
                            fastLinkBottoms.Add(item);
                        }
                    }
                    ViewBag.fastLinkBottoms = fastLinkBottoms;
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            #endregion

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_CHAM_SOC_KH);
            ViewBag.node_cham_soc = node;

            var same_levels = ArticlesServices.GetArticleByParrent(node.ID);
            var nodes = same_levels == null ? new int[] { node.ID } : same_levels.Select(a => a.ID).ToArray();
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            return View("../Home/Recruitment/Register");
        }

        public ActionResult SendEmail()
        {

            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add("nghiaterry@gmail.com"); //recipient
            message.Subject = "Email sending for testing smtp mvc3";
            message.From = new System.Net.Mail.MailAddress("wizardtest@gmail.com"); //from email
            message.IsBodyHtml = true;
            message.Body = "This is the message body";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.sendgrid.com");// you need an smtp server address to send emails

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s, 
                System.Security.Cryptography.X509Certificates.X509Certificate certificate, 
                System.Security.Cryptography.X509Certificates.X509Chain chain, 
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            smtp.Credentials = new System.Net.NetworkCredential("wizardtest", "lmvhXU0TpB"); //missing line from ur code

            smtp.Port = 25;  //missing line from your code

            smtp.EnableSsl = true;   //missing line from your code
            smtp.Send(message);

            return Content("Email Sent!");
        }

        public ActionResult RE_Testmail()
        {
            CommonHelper.SendEmail("nghiaterry@gmail.com", "MBS - KICH HOAT TAI KHOAN CUA BAN", "MBS - KICH HOAT TAI KHOAN CUA BAN");
            return Content("test send mail");
        }

        public ActionResult RE_Forgot()
        {
            #region LIEN KET NHANH
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            #endregion

            if (Request.IsAjaxRequest())
            {
                string email = Request["ct_email"];
                if (StringHelper.IsNullOrEmptyOrWhiteSpace(email))
                {
                    return Json(new { st = 0, field = "email", msg = StringHelper.lang("email_require") });
                }

                int uid = 0;
                string newToken = UserServices.ForgotPassword(email, out uid);
                if (uid > 0 && !StringHelper.IsNullOrEmptyOrWhiteSpace(newToken))
                {
                    string hash = StringHelper.Base64Encode(uid + "|" + newToken);

                    ViewBag.hash = hash;
                    var email_content = StringHelper.PartialView(this, "../Home/Email/Forgot", null);
                    string subject = StringHelper.lang("text_mail_forgot_subject");
                    CommonHelper.SendEmail("nghiaterry@gmail.com", email_content, subject);
                    return Json(new { st = 1, field = "email", msg = StringHelper.lang("text_fotgot_success") });
                }
                return Json(new { st = 0, field = "email", msg = StringHelper.lang("text_fotgot_fail") });
            }
            else
            {
                return View("../Home/Recruitment/Forgot");
            }
        }

        public ActionResult RE_Active(string hash = "")
        {
            if (hash == "")
                Redirect(StringHelper.base_url_lang());

            string message = "";
            hash = StringHelper.Base64Decode(hash);
            var temp = hash.Split('|');
            if (temp != null && temp.Count() >= 2)
            {
                int uid = StringHelper.IsNumber(temp[0]);
                string token = temp[1];

                if (uid > 0 && !StringHelper.IsNullOrEmptyOrWhiteSpace(token))
                {
                    int active = UserServices.Active(uid, token);
                    if (active == 0)
                    {
                        message = StringHelper.lang("text_active_fail");
                    }
                    else if (active == 1)
                    {
                        message = StringHelper.lang("text_active_success");
                    }
                }
            }
            else
            {
                message = StringHelper.lang("text_active_fail");

            }

            ViewBag.message = message;
            return View("../Home/Recruitment/Active");
        }

        public ActionResult RE_Getpass(string hash="")
        {
            if (hash == "")
                Redirect(StringHelper.base_url_lang());

            string message = "";
            hash = StringHelper.Base64Decode(hash);
            var temp = hash.Split('|');
            if (temp != null && temp.Count() >= 2)
            {
                int uid = StringHelper.IsNumber(temp[0]);
                string token = temp[1];

                //check user info
                string new_password = UserServices.GetPassword(uid, token);
                if (string.IsNullOrEmpty(new_password))
                {
                    message = StringHelper.lang("text_get_pass_fail");
                }
                else
                {
                    //success
                    ViewBag.password = new_password;
                    var user = UserServices.Get(uid);
                    var email_content = StringHelper.PartialView(this, "../Home/Email/Password", user);
                    string subject = StringHelper.lang("text_getpassword_subject");
                    CommonHelper.SendEmail("nghiaterry@gmail.com", email_content, subject);
                    message = StringHelper.lang("text_get_pass_success");
                }
            }
            else
            {
                message = StringHelper.lang("text_get_pass_fail");
            }

            ViewBag.message = message;
            return View("../Home/Recruitment/Getpass");
        }

        public ActionResult RE_Resumes(string step = "")
        {
            string step1 = StringHelper.lang("slug_cv_step1");
            string step2 = StringHelper.lang("slug_cv_step2");
            string step3 = StringHelper.lang("slug_cv_step3");
            string step4 = StringHelper.lang("slug_cv_step4");

            int LoggedID = _LoggedUID();

            if (LoggedID == 0)
            {
                Session["REFERER"] = Request.Url.AbsoluteUri;
                return Redirect(StringHelper.base_url_lang() + StringHelper.lang("link_dang_nhap"));
            }

            if (step == step1 || step == "")
                return RE_Resumes_Step1();
            else if (step == step2)
                return RE_Resumes_Step2();
            else if (step == step3)
                return RE_Resumes_Step3();
            else if (step == step4)
                return RE_Resumes_Step4();
            else
                return Redirect(StringHelper.base_url_lang()+StringHelper.lang("slug_tuyen_dung")+"/"+step1);
        }

        /// <summary>
        /// Get Logged user id
        /// </summary>
        /// <returns>0 if not log in</returns>
        private int _LoggedUID()
        {
            if (Session["UID"] != null)
            {
                return StringHelper.IsNumber(Session["UID"].ToString());
            }
            else
            {
                return 0;
            }
        }

        public JsonResult RE_Action(string action)
        {
            if (action == "add-language")
                return _re_add_lang();
            if (action == "add-learning")
                return _re_add_learning();
            //if (action == "add-language")
            //    return _re_add_lang();

            return Json(new {st= 0, msg= ""});
        }

        private JsonResult _re_add_lang()
        {
            int LoggedID = _LoggedUID();
            if (LoggedID == 0)
                return Json(new { st = 2, msg = "login-required" });

            string lang_name = Request["re_lang_name"];
            string write = Request["re_lang_write"];
            string listen = Request["re_lang_listen"];
            string read = Request["re_lang_read"];
            string speak = Request["re_lang_speak"];

            //Validate data

            //End validate data

            LanguageViewModel language = new LanguageViewModel();
            language.USER_ID = LoggedID;
            language.NAME = lang_name;
            language.READ = read;
            language.WRITE = write;
            language.SPEAK = speak;
            language.LISTEN = listen;

            if (ReLanguageServices.Insert(language) > 0)
            {
                return Json(new { st = 1, msg = "success" });
            }
            else
            {
                return Json(new { st = 0, msg = "error" });

            }
        }

        private JsonResult _re_add_learning()
        {
            int LoggedID = _LoggedUID();
            if (LoggedID == 0)
                return Json(new { st = 2, msg = "login-required" });

            string level = Request["re_learning_level"];
            string name_city_country = Request["re_learning_name"];
            DateTime from = DatetimeHelper.StringToDate(Request["re_learning_from"], "dd-MM-yyyy HH:mm");
            DateTime to = DatetimeHelper.StringToDate(Request["re_learning_to"], "dd-MM-yyyy HH:mm");
            string major = Request["re_learning_major"];
            string graduate_point = Request["re_learning_point"];

            //Validate data

            EducationViewModel education = new EducationViewModel();
            education.USER_ID = LoggedID;
            education.LEVEL_NAME = level;
            education.SCHOOL = name_city_country;
            education.MAJOR = major;
            education.DATE_FROM = from;
            education.DATE_TO = to;
            education.GRADUATE_POINT = graduate_point;

            if (ReEducationServices.Insert(education) > 0)
            {
                return Json(new { st = 1, msg = "success" });
            }
            else
            {
                return Json(new { st = 0, msg = "" });
            }
            
        }

        public ActionResult RE_Resumes_Step1()
        {
            int LoggedID = _LoggedUID();
            UserViewModel User = UserServices.Get(LoggedID);

            if (Request.IsAjaxRequest())
            {
                //STEP 1 DATA
                string FULLNAME = Request["re_fullname"];
                string TENCHINH = Request["re_tenchinh"];
                string CMND = Request["re_cmnd"];
                string DATE_PLACE_CMND = Request["re_date_cmnd"];
                string ADDRESS = Request["re_address"];
                string HOME_PHONE = Request["re_home_phone"];
                string MOBILE_PHONE = Request["re_mobile_phone"];
                string ADDRESS_CONTACT = Request["re_address_contact"];
                string HOME_PHONE_CONTACT = Request["re_phone_contact"];
                string FAX = Request["re_fax"];
                string EMAIL = Request["re_email"];
                string GENDER = Request["re_gender"];
                string HEIGHT = Request["re_height"];
                string WEIGHT = Request["re_weight"];
                int MIRITAL = StringHelper.IsNumber(Request["re_mirital"]);
                string MIRITAL_OTHER = Request["re_mirital_other"];
                DateTime DOB = DatetimeHelper.StringToDate(Request["re_dob"], "dd-MM-yyyy");
                string POB = Request["re_pob"];
                string IMAGE = Request["re_image"];
                string RELATE_EXP = Request["re_relate_exp"];
                string CONTACT_LEADER = Request["re_actact_leader"];

                //VALIDATE DATA

                if (UserServices.Update(User))
                {
                    return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                }
                else
                {
                    return Json(new { st = 0, field = "", msg = StringHelper.lang("cv_update_error") });
                }
            }

            return View("../Home/Recruitment/cv_1", User);
        }
        public ActionResult RE_Resumes_Step2()
        {
            int LoggedID = _LoggedUID();
            UserViewModel User = UserServices.Get(LoggedID);
            MajorViewModel major = ReMajorServices.GetByUserId(LoggedID);

            if (Request.IsAjaxRequest())
            {
                string other_major = Request["re_other_major"];
                string social_activities = Request["re_social_activities"];
                string articles = Request["re_articles"];
                string softwares = Request["re_softwares"];
                string talent = Request["re_talent"];

                major = major == null ? new MajorViewModel() : major;
                major.OTHER_MAJOR = other_major;
                major.SOCIAL_ACTIVITIES = social_activities;
                major.ARTICLES = articles;
                major.SOFTWARES = softwares;
                major.TALENT = talent;

                if (major.ID > 0)
                {
                    if (ReMajorServices.Update(major))
                    {
                        return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                    }
                }
                else
                {
                    if (ReMajorServices.Insert(major) > 0)
                    {
                        return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                    }
                }
                return Json(new { st = 0, field = "", msg = StringHelper.lang("cv_update_success") });
            }

            ViewBag.user = User;
            ViewBag.languages = ReEducationServices.GetAll(LoggedID);
            ViewBag.educations = ReEducationServices.GetAll(LoggedID);


            return View("../Home/Recruitment/cv_2", major);
        }
        public ActionResult RE_Resumes_Step3()
        {
            int LoggedID = _LoggedUID();
            UserViewModel User = UserServices.Get(LoggedID);

            if (Request.IsAjaxRequest())
            {
                string relate_exp = Request["re_relate_exp"];
                string contact_leader = Request["re_contact_leader"];

                //Validate data

                User.RELATE_EXP = relate_exp;
                User.CONTACT_LEADER = contact_leader;

                if (UserServices.Update(User))
                {
                    return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                }
                else
                {
                    return Json(new { st = 0, field = "", msg = StringHelper.lang("cv_update_success") });
                }
            }

            ViewBag.works = ReWorkExpServices.GetAll(LoggedID);
            ViewBag.members = ReUserMemberServices.GetAll(LoggedID);

            return View("../Home/Recruitment/cv_3");
        }
        public ActionResult RE_Resumes_Step4()
        {
            int LoggedID = _LoggedUID();
            UserViewModel User = UserServices.Get(LoggedID);
            MajorViewModel major = ReMajorServices.GetByUserId(LoggedID);
            OtherInfoViewModel OtherInfo = ReOtherInfoServices.GetByUserId(LoggedID);

            if (Request.IsAjaxRequest())
            {
                int know_mbs = StringHelper.IsNumber(Request["re_know_mbs"]);
                int know_job = StringHelper.IsNumber(Request["re_know_job"]);
                string mass_media = Request["re_mass_media"];
                string organization = Request["re_organization"];
                string apply_before = Request["re_apply_before"];
                string work_before = Request["re_work_before"];
                int defect = StringHelper.IsNumber(Request["re_defect"]);
                string defect_detail = Request["re_defect_detail"];
                int work_fly = StringHelper.IsNumber(Request["re_work_fly"]);
                int work_oto = StringHelper.IsNumber(Request["re_work_oto"]);
                int demist = StringHelper.IsNumber(Request["re_demist"]);
                string expected_salary = Request["re_expected_salary"];
                string when_start = Request["re_when_start"];
                string strong_weak = Request["re_strong_weak"];
                string reason_apply = Request["re_reason_apply"];
                DateTime cv_created = DatetimeHelper.StringToDate(Request["re_cv_created"], "dd-MM-yyyy");
                
                //Validate data
                //End validate data

                OtherInfo = OtherInfo == null ? new OtherInfoViewModel() : OtherInfo;
                OtherInfo.KNOW_MBS_BY = know_mbs;
                OtherInfo.KNOW_JOB_BY = know_job;
                OtherInfo.MASS_MEDIA = mass_media;
                OtherInfo.ORGANIZATION = organization;
                OtherInfo.APPLY_BEFORE = apply_before;
                OtherInfo.WORK_BEFORE = work_before;
                OtherInfo.WHEN_START = when_start;
                OtherInfo.WORK_FLY = work_fly;
                OtherInfo.WORK_OTO = work_oto;
                OtherInfo.DEFECT = defect;
                OtherInfo.DEFECT_DETAIL = defect_detail;
                OtherInfo.DEMIST = demist;
                OtherInfo.STRONG_WEAK = strong_weak;
                OtherInfo.REASON_APPLY = reason_apply;
                OtherInfo.CV_CREATED = cv_created;

                if (OtherInfo.ID > 0)
                {
                    if (ReOtherInfoServices.Update(OtherInfo))
                    {
                        return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                    }
                }
                else
                {
                    if (ReOtherInfoServices.Insert(OtherInfo) > 0)
                    {
                        return Json(new { st = 1, field = "", msg = StringHelper.lang("cv_update_success") });
                    }
                }

                return Json(new { st = 0, field = "", msg = StringHelper.lang("cv_update_error") });
            }

            return View("../Home/Recruitment/cv_4");
        }

        public ActionResult Forms()
        {
            #region LIEN KET NHANH
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            #endregion

            ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;

            string currentPath = Request.Url.OriginalString;
            int totalRows = FormServices.GetCountSearchContent("", "", "", StringHelper.language());
            AjaxPagination pagination = new AjaxPagination(0, totalRows, currentPath, page, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");
            //pagination.Perpage = 8;

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = FormServices.GetSearchContent(page, pagination.Perpage, "", "", "");

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Recruitment/AjaxForms", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.switch_url = StringHelper.lang("slug_tuyen_dung", true) + "/" + StringHelper.lang("slug_bieu_mau", true);
                return View("../Home/Recruitment/Forms");
            }
            
        }
        
        public ActionResult Recruitment()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            //ViewBag.node_phan_tich = ArticlesServices.GetArticleContentById(Constants.NODE_TRUNGTAM_PHANTICH);
            int page = !string.IsNullOrEmpty(Request["page"]) && StringHelper.IsNumeric((Request["page"])) ? int.Parse((Request["page"])) : 1;
            int category_id = !string.IsNullOrEmpty(Request["cid"]) && StringHelper.IsNumeric((Request["cid"])) ? int.Parse((Request["cid"])) : 0;
            int term_id = !string.IsNullOrEmpty(Request["term"]) && StringHelper.IsNumeric((Request["term"])) ? int.Parse((Request["term"])) : 0;
            int position = !string.IsNullOrEmpty(Request["position"]) && StringHelper.IsNumeric((Request["position"])) ? int.Parse((Request["position"])) : 0;
            string province = !string.IsNullOrEmpty(Request["province"]) ? Request["province"] : "";
            string searchText = !string.IsNullOrEmpty(Request["searchText"]) ? Request["searchText"] : "";
            string dateFrom = Request["dateFrom"];
            string dateTo = Request["dateTo"];
            if (!string.IsNullOrEmpty(dateFrom)) dateFrom = dateFrom.Replace("/", "-");
            if (!string.IsNullOrEmpty(dateTo)) dateTo = dateTo.Replace("/", "-");

            string cslug = Request.QueryString["position"];
            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(cslug))
            {
                var category = TermServices.GetBySlug(cslug, 3);
                if (category != null)
                    position = category.ID;
            }

            Hashtable searchParams = new Hashtable();
            searchParams.Add("cid", position);
            searchParams.Add("term", term_id);
            //searchParams.Add("position", position);
            searchParams.Add("province", province);
            searchParams.Add("dateFrom", dateFrom);
            searchParams.Add("dateTo", dateTo);
            searchParams.Add("searchText", searchText);

            string currentPath = Request.Url.OriginalString;
            int totalRows = JobServices.GetCountSearchContent(searchParams);
            AjaxPagination pagination = new AjaxPagination(0, totalRows, currentPath, page, "SearchContent");
            //pagination.Perpage = 2;
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");

            searchParams.Add("page", page);
            searchParams.Add("perpage", pagination.Perpage);

            ViewBag.pagination = pagination.CreateLinks();
            var Childs = JobServices.GetSearchContent(searchParams);

            ViewBag.categories = TermServices.GetTerms(3, StringHelper.language());

            List<string> provinces = new List<string>();
            using (DBContext context = new DBContext())
            {
                var items = context.WZ_BRANCH.OrderBy(b=>b.PRIORITY).ToList();
                if (items != null)
                {
                    foreach (var b in items)
                    {
                        provinces.Add(b.PROVINCE_ID);
                    }
                }

                if (provinces.Count > 0)
                {
                    ViewBag.provinces = context.WZ_CT_PROVINCE.Where(p => provinces.Contains(p.PROVINCE_ID)).OrderBy(p => p.PRIORITY).ToList();
                }
            }

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Recruitment/AjaxJobs", Childs);
            }
            else
            {
                ViewBag.childs = Childs;
                ViewBag.terms = TermServices.GetTerms(4, StringHelper.language());
                ;

                var switch_lang = StringHelper.language() == "en" ? "vi" : "en";
                ViewBag.switch_url = StringHelper.lang("link_vitri_tuyendung", true);
                return View("../Home/Recruitment/Jobs");
            }
        }

        public ActionResult JobDetail(int id, string slug)
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var switch_lang = StringHelper.language() == "en" ? "vi" : "en";
            var switch_url = StringHelper.lang("link_vitri_tuyendung", true) + "/";

            var node = JobServices.GetContentById(id);
            if (node != null)
            {
                ViewBag.category = TermServices.GetTermContentById(node.CID);
                ViewBag.term = TermServices.GetTermContentById(node.TERM_ID);

                var node_lang = JobServices.GetContentById(id, switch_lang);
                if (node_lang != null)
                    switch_url += node.ID + "/" + node_lang.SLUG;
            }
            ViewBag.switch_url = switch_url;
            return View("../Home/Recruitment/JobsDetail", node);
        }

        public ActionResult ApplyGuide()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var node = ArticlesServices.GetArticleContentById(Constants.NODE_HUONGDAN_NOP_HS);
            if (node == null)
            {
                return Redirect(StringHelper.base_url_lang() + StringHelper.lang("link_vitri_tuyendung"));
            }

            ViewBag.switch_url = StringHelper.lang("slug_tuyen_dung", true) + "/" + StringHelper.lang("slug_huongdan_nophoso", true);
            return View("../Home/Recruitment/ApplyGuide", node);
        }
        #endregion
        
        public ActionResult Page404()
        {
            return View("../FrontEnd/Page404");
        }

        public ActionResult Sitemap()
        {
            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }

            var nodes = new int[] { Constants.NODE_KHACH_HANG_CN, Constants.NODE_KHACH_HANG_TC, Constants.NODE_TRUNGTAM_PHANTICH, Constants.NODE_CHAM_SOC_KH, Constants.NODE_GIOI_THIEU };
            ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);
            ViewBag.switch_url = StringHelper.lang("sitemap-slug", true);
            return View("../FrontEnd/Sitemap");
        }

        public ActionResult Search(string keyword = "", int p = 1)
        {
            if (!StringHelper.IsNullOrEmptyOrWhiteSpace(keyword))
            {
                
            }

            if (Request.Cookies["UserSettings"] != null && !StringHelper.IsNullOrEmptyOrWhiteSpace(Request.Cookies["UserSettings"].Value))
            {
                var deserialized = new JavaScriptSerializer().Deserialize<List<int>>(HttpUtility.UrlDecode(Request.Cookies["UserSettings"].Value));
                if (deserialized != null && deserialized.Count > 0)
                {
                    ViewBag.fastLinkBottoms = FastLinkServices.GetFastLink(2, -1, "", deserialized);
                }
            }

            if (ViewBag.fastLinkBottoms == null)
            {
                var fastLinkBottoms = FastLinkServices.GetFastLink(2, 1);
                if (fastLinkBottoms != null && fastLinkBottoms.Count > 0)
                {
                    var cookie_data = fastLinkBottoms.Select(f => f.ID).ToList();
                    HttpCookie cookie = new HttpCookie("UserSettings");
                    string serializedObject = new JavaScriptSerializer().Serialize(fastLinkBottoms.Select(f => f.ID).ToList());
                    cookie.Value = HttpUtility.UrlEncode(serializedObject);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                ViewBag.fastLinkBottoms = fastLinkBottoms;
            }
            
            int[] not_ins = null;

            Hashtable searchParams = new Hashtable();
            searchParams.Add("page", p);
            searchParams.Add("searchText", keyword);
            searchParams.Add("parent_not_ins", new int[] { Constants.NODE_DV_THUONG_VU_DH });

            string currentPath = Request.Url.OriginalString;
            int totalRows = ArticlesServices.GetCountSearchContent(searchParams);
            AjaxPagination pagination = new AjaxPagination(0, totalRows, currentPath, p, "SearchContent");
            pagination.PrevLink = StringHelper.lang("text_page_first");
            pagination.NextLink = StringHelper.lang("text_page_end");
            pagination.Perpage = 10;

            ViewBag.pagination = pagination.CreateLinks();

            searchParams.Add("perpage", pagination.Perpage);

            var Childs = ArticlesServices.GetSearchContent(searchParams);

            if (Request.IsAjaxRequest())
            {
                return View("../Home/Search/AjaxList", Childs);
            }
            else
            {
                ViewBag.childs = Childs;

                var nodes = new int[] { Constants.NODE_KHACH_HANG_CN, Constants.NODE_KHACH_HANG_TC, Constants.NODE_TRUNGTAM_PHANTICH, Constants.NODE_CHAM_SOC_KH, Constants.NODE_GIOI_THIEU };
                ViewBag.menus = ArticlesServices.GetMenuByListId(nodes, 2);

                ViewBag.switch_url = StringHelper.lang("slug_timkiem", true) + "?keyword=" + keyword;
                return View("../Home/Search/Index");
            }
        }

        #region Miggrate Data
        public ActionResult Miggrate()
        {
            new System.Threading.Thread(() => { this.ThreadImportData(); }).Start();

            return View();
        }

        public ActionResult MiggrateTinTuc()
        {
            new System.Threading.Thread(() => { this.ThreadImportDataTinTuc(); }).Start();

            return View("Miggrate");
        }


        public ActionResult MiggrateGocTT()
        {
            new System.Threading.Thread(() => { this.getDataGocTruyenThong(); }).Start();

            return View("Miggrate");
        }

        public ActionResult MiggrateVanHoa()
        {
            new System.Threading.Thread(() => { this.ThreadImportDataVanHoa(); }).Start();

            return View("Miggrate");
        }

        public bool mrpeo_clean_data()
        {
            int[] categories = new int[] { 
                Constants.NODE_BC_TRIEN_VONG_VN, 
                Constants.NODE_BC_THI_TRUONG_NO, 
                Constants.NODE_BC_NGHIEN_CUU_NGANH,
                Constants.NODE_BC_NGHIEN_CUU_CP,
                Constants.NODE_BC_BAN_TIN_NGAY,
                Constants.NODE_BC_CHIEN_LUOC_TUAN,
                Constants.NODE_BC_PT_KY_THUAT,
                Constants.NODE_BC_PT_NHANH_CP,

                Constants.NODE_TIN_MBS,
                Constants.NODE_TIN_CO_DONG,
                Constants.NODE_BAO_CAO_TC,
                Constants.NODE_CONGBO_THONGTIN,
                Constants.NODE_THONGTIN_QUANTRI,

                Constants.NODE_VANHOA_NOIBO,
            };
            using (DBContext context = new DBContext())
            {
                foreach (int cate in categories)
                {
                    var nodes = context.WZ_ARTICLES.Where(n => n.PARENT_ID == cate).ToList();
                    foreach (var node in nodes)
                    {
                        context.WZ_ARTICLES.DeleteObject(node);
                        context.SaveChanges();
                        context.WZ_ARTICLES_CONTENT.Where(nc => nc.NID == node.ID).ToList().ForEach(context.WZ_ARTICLES_CONTENT.DeleteObject);
                        context.SaveChanges();
                    }
                }
            }
            return true;
        }

        public void ThreadImportData()
        {   
            /* Bao cao phan tich */
            //Trien vong viet nam
            this.getDataFromThanhLong(new int[] { 1087, 1227 }, Constants.NODE_BC_TRIEN_VONG_VN, "vi");
            this.getDataFromThanhLong(new int[] { 1024, 1043 }, Constants.NODE_BC_TRIEN_VONG_VN, "en");

            this.getDataFromThanhLong(new int[] { 1085 }, Constants.NODE_BC_THI_TRUONG_NO, "vi");
            this.getDataFromThanhLong(new int[] { 1154 }, Constants.NODE_BC_THI_TRUONG_NO, "en");

            this.getDataFromThanhLong(new int[] { 1229 }, Constants.NODE_BC_NGHIEN_CUU_NGANH, "vi");
            this.getDataFromThanhLong(new int[] { 1231, 1238 }, Constants.NODE_BC_NGHIEN_CUU_NGANH, "en");

            this.getDataFromThanhLong(new int[] { 1225, 963 }, Constants.NODE_BC_NGHIEN_CUU_CP, "vi");
            this.getDataFromThanhLong(new int[] { 1025, 1044 }, Constants.NODE_BC_NGHIEN_CUU_CP, "en");

            this.getDataFromThanhLong(new int[] { 1083 }, Constants.NODE_BC_BAN_TIN_NGAY, "vi");
            this.getDataFromThanhLong(new int[] { 1074, 1077, 1023, 1042 }, Constants.NODE_BC_BAN_TIN_NGAY, "en");

            this.getDataFromThanhLong(new int[] { 1223 }, Constants.NODE_BC_CHIEN_LUOC_TUAN, "vi");
            this.getDataFromThanhLong(new int[] { 1234, 1241, 1240, 1233 }, Constants.NODE_BC_CHIEN_LUOC_TUAN, "en");

            this.getDataFromThanhLong(new int[] { 1224 }, Constants.NODE_BC_PT_KY_THUAT, "vi");
            this.getDataFromThanhLong(new int[] { 1235, 1242 }, Constants.NODE_BC_PT_KY_THUAT, "en");

            this.getDataFromThanhLong(new int[] { 1225 }, Constants.NODE_BC_PT_NHANH_CP, "vi");
            //this.getDataFromThanhLong(new int[] {  }, Constants.NODE_BC_PT_NHANH_CP, "en");
            
        }

        public void ThreadImportDataTinTuc()
        {
            //Tin tuc MBS
            this.getDataFromThanhLong(new int[] { 873, 911 }, Constants.NODE_TIN_MBS, "vi");

            //Tin co dong
            this.getDataFromThanhLong(new int[] { 947 }, Constants.NODE_TIN_CO_DONG, "vi");

            //Bao cao tai chinh
            this.getDataFromThanhLong(new int[] { 979 }, Constants.NODE_BAO_CAO_TC, "vi");
            this.getDataFromThanhLong(new int[] { 1011 }, Constants.NODE_BAO_CAO_TC, "en");

            //Cong bo thong tin
            this.getDataFromThanhLong(new int[] { 980 }, Constants.NODE_CONGBO_THONGTIN, "vi");
            this.getDataFromThanhLong(new int[] { 1012 }, Constants.NODE_CONGBO_THONGTIN, "en");

            //Quan tri cong ty
            this.getDataFromThanhLong(new int[] { 981 }, Constants.NODE_THONGTIN_QUANTRI, "vi");
            this.getDataFromThanhLong(new int[] { 1013 }, Constants.NODE_THONGTIN_QUANTRI, "en");
        }

        public void ThreadImportDataVanHoa()
        {
            //Văn hóa nội bộ
            this.getDataFromThanhLong(new int[] { 1204 }, Constants.NODE_VANHOA_NOIBO, "vi");
        }

        public bool getDataGocTruyenThong(string LANG = "vi")
        {
            string NODES = @"13956,13955,13926,13918,13911,13910,13904,13901,13886,13839,13818,13804,13785,13784,13777,
      13773,13743,13705,13704,13700,13694,13681,13680,13679,13667,13659,13648,13632,13616,13613,13600,13546,13534,13533,13522,13517,
      7229,6537,6525,6478,6428,6419,6271,6269,6213,5988,5987,5982,5975,5965,5954,5928,5925,5913,5907,5904,5901,5879,5844,5736,5706,
      5702,5698,5686,5685,5684,5625,5622,5621,5620,5614,5584,5433,5423,5420,5419,5371,5290,5196,5155,5123,5119,5118,4994,4993,4966,4965,4964,4931";
            string[] temp = NODES.Split(',');
            int[] ins = Array.ConvertAll(temp, int.Parse);
            try
            {
                DBContext dbContext = new DBContext();
                var CATEGORY = dbContext.WZ_ARTICLES_CONTENT.Where(n => n.NID == 59 && n.LANG == LANG).FirstOrDefault();

                string CATEGORY_LINK = string.Empty;
                if (CATEGORY != null)
                {
                    CATEGORY_LINK = CATEGORY.LINK + "/";
                }
                else
                {
                    Logs.WriteLogDebug("Import data: Khong tim thay danh muc cha voi ngon ngu tuong ung.");
                    return false;
                }

                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where ins.Contains(n.Content_ID) select n).OrderBy(n => n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            Logs.WriteLogDebug(node.Content_Headline);
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_lang = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = 59;
                            article.STATUS = node.Content_Status == 4 ? 1 : 0;

                            article_content_lang.LANG = LANG;
                            article_content_lang.TITLE = node.Content_Headline;
                            article_content_lang.DESCRIPTION = StringHelper.StripTags(node.Content_Teaser);
                            article_content_lang.CONTENT = node.Content_Body;
                            article_content_lang.IMAGE = node.Content_Avatar; //saveImage(node.Content_Avatar);
                            article_content_lang.ATTACHMENT = node.Content_Document;  //saveFile(node.Content_Document);
                            article_content_lang.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_lang.LINK = CATEGORY_LINK + article_content_lang.SLUG;
                            article_content_lang.SUB_TITLE = string.Empty;

                            //Check exist node in database
                            if (ArticlesServices.CheckSlug(article_content_lang.SLUG, LANG, 59))
                            {
                                continue;
                            }

                            article.ID = this._getNextID();

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_lang.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_lang);
                                dbContext.SaveChanges();
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }

                        Logs.WriteLogDebug("Import data: da import duoc " + news.Count + " tin trong muc " + CATEGORY.TITLE);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return true;
        }

        public bool getDataFromThanhLong(int[] ZONE_ID, int PARENT_ID, string LANG = "vi")
        {
            try
            {
                DBContext dbContext = new DBContext();
                var CATEGORY = dbContext.WZ_ARTICLES_CONTENT.Where(n => n.NID == PARENT_ID && n.LANG == LANG).FirstOrDefault();

                string CATEGORY_LINK = string.Empty;
                if (CATEGORY != null)
                {
                    CATEGORY_LINK = CATEGORY.LINK + "/";
                }
                else
                {
                    Logs.WriteLogDebug("Import data: Khong tim thay danh muc cha voi ngon ngu tuong ung.");
                    return false;
                }

                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where ZONE_ID.Contains(n.Content_ZoneID) select n).OrderBy(n => n.Content_CreateDate).ToList();

                    int count = 0;
                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_lang = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = PARENT_ID;
                            article.STATUS = node.Content_Status == 4 ? 1 : 0;

                            article_content_lang.LANG = LANG;
                            article_content_lang.TITLE = node.Content_Headline;
                            article_content_lang.DESCRIPTION = StringHelper.StripTags(node.Content_Teaser);
                            article_content_lang.CONTENT = node.Content_Body;
                            article_content_lang.IMAGE = node.Content_Avatar; //saveImage(node.Content_Avatar);
                            article_content_lang.ATTACHMENT = node.Content_Document; //saveFile(node.Content_Document);
                            article_content_lang.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_lang.LINK = CATEGORY_LINK + article_content_lang.SLUG;
                            article_content_lang.SUB_TITLE = string.Empty;

                            //Check exist node in database
                            if (ArticlesServices.CheckSlug(article_content_lang.SLUG, LANG, PARENT_ID))
                            {
                                continue;
                            }

                            article.ID = this._getNextID();
                            
                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_lang.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_lang);
                                dbContext.SaveChanges();

                                count++;
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }

                        Logs.WriteLogDebug("Import data: da import duoc " + count + " tin trong muc " + CATEGORY.TITLE);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.ToString());
            }
            return false;
        }

        private String GetFileName(String link)
        {
            try
            {
                return System.IO.Path.GetFileName(Uri.UnescapeDataString(link).Replace("/", "\\")).ToLower();
            }
            catch (Exception)
            {
            }
            string extension = "";
            int fileExtPos = link.LastIndexOf(".");
            if (fileExtPos >= 0)
                extension = link.Substring(fileExtPos);

            string fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + extension.ToLower();

            return fileName;
        }

        public string saveImage(string file, string Dir="")
        {
            try
            {
                if (string.IsNullOrEmpty(file))
                    return string.Empty;

                if (!file.Contains("http"))
                    file = "https://mbs.com.vn" + file;

                string filename = GetFileName(file);

                byte[] data;
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    data = client.DownloadData(file);
                }

                /*
                string extension = "";
                int fileExtPos = file.LastIndexOf(".");
                if (fileExtPos >= 0)
                    extension = file.Substring(fileExtPos);

                string fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + extension;
                */
                string directory = Server.MapPath(Url.Content("~") + Constants.DIR_IMAGE + "/v1/");
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);

                    //set permission
                    CommonHelper.SetPermission(directory);
                }
                string path = System.IO.Path.Combine(directory, filename);

                System.IO.File.WriteAllBytes(path, data);

                return "/" + Constants.DIR_IMAGE + "v1/" + filename;
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public string saveFile(string file, string Dir = "")
        {
            try
            {
                if (string.IsNullOrEmpty(file))
                    return string.Empty;

                if (!file.Contains("http"))
                    file = "https://mbs.com.vn" + file;

                string filename = GetFileName(file);

                byte[] data;
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    data = client.DownloadData(file);
                }

                /*
                string extension = "";
                int fileExtPos = file.LastIndexOf(".");
                if (fileExtPos >= 0)
                    extension = file.Substring(fileExtPos);

                string fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + extension;
                */
                string directory = Server.MapPath(Url.Content("~") + Constants.DIR_FILES + "/v1");
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);

                    //set permission
                    CommonHelper.SetPermission(directory);
                }
                string path = System.IO.Path.Combine(directory, filename);

                System.IO.File.WriteAllBytes(path, data);

                return "/" + Constants.DIR_FILES + "v1/" + filename;
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }


        private string _saveImage(string file)
        {
            try
            {
                byte[] data;
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    data = client.DownloadData("http://mbs.com.vn" + file);
                }

                string extension = "";
                int fileExtPos = file.LastIndexOf(".");
                if (fileExtPos >= 0)
                    extension = file.Substring(fileExtPos);

                string fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + extension;
                string directory = Server.MapPath(Url.Content("~") + Constants.DIR_IMAGE);
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);

                    //set permission
                    CommonHelper.SetPermission(directory);
                }
                string path = System.IO.Path.Combine(directory, fileName);

                System.IO.File.WriteAllBytes(path, data);

                return file;
            }
            catch (Exception)
            {
                
            }
            return string.Empty;
        }

        private decimal _getNextID()
        {
            try
            {
                using (var context = new DBContext())
                {
                    var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ARTICLE" select s).FirstOrDefault();
                    decimal newID = 1;
                    if (sequence != null)
                    {
                        newID = sequence.SEQ;
                        sequence.SEQ += 1;
                        context.SaveChanges();
                    }
                    else
                    {
                        sequence = new ADMIN_WZ_SEQUENCE();
                        sequence.TABLE_NAME = "ARTICLE";
                        sequence.SEQ = 2;

                        context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        context.SaveChanges();
                    }

                    var check = context.WZ_ARTICLES.Where(a => a.ID == newID).FirstOrDefault();
                    if (check != null)
                        return this._getNextID();

                    return newID;
                }
            }
            catch { }
            return 0;
        }
        #endregion
    
        public string UpdateLinkImage()
        {
            using (var context = new DBContext())
            {
                var contents = context.WZ_ARTICLES_CONTENT.Where(c => !string.IsNullOrEmpty(c.IMAGE) || !string.IsNullOrEmpty(c.ATTACHMENT)).ToList();
                foreach(var cont in contents)
                {
                    if (!string.IsNullOrEmpty(cont.ATTACHMENT))
                        cont.ATTACHMENT = Constants.DIR_FILES + cont.ATTACHMENT;

                    if (!string.IsNullOrEmpty(cont.IMAGE))
                    {
                        cont.IMAGE = Constants.DIR_ARTICLE + cont.IMAGE;
                    }
                        

                    context.SaveChanges();
                }
            }

            return "DONE";
        }
    }
}
