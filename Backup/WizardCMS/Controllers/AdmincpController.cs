﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.DTO;
using System.Net;
using System.Reflection;
using WizardCMS.Helpers;
using WizardCMS.Models;
using System.Configuration;
using WizardCMS.Const;
using System.Web.Security;
using WizardCMS.domain;
using System.Web.Configuration;

namespace WizardCMS.Controllers
{
    public class AdmincpController : Controller
    {
        public ActionResult Index(string nameModule)
        {
            Object account = AuthorizeActionFilter.UserIsAuthenticated();

            if (account != null)
            {
                nameModule = CommonHelper.GetSegment(2, Request.Url.AbsoluteUri);
                ViewBag.Account = account;

                if (String.IsNullOrWhiteSpace(nameModule))
                {
                    nameModule = Constants.MANAGER_MODULES_DEFAULT;
                    ViewBag.SelectedNameModule = nameModule;
                    return Redirect(Url.Content("~") + "admincp/" + nameModule);
                }
                else
                {
                    ViewBag.SelectedNameModule = nameModule;
                    return View("../BackEnd/Admin/Index");
                }
            }
            else
            {
                return RedirectToAction("login");
            }
        }

        public ActionResult Login()
        {
            return View("~/Views/BackEnd/Authentication/Index.cshtml");
        }

        private string _currentUsername = string.Empty;

        [HttpPost]
        public string login_action(AdminAccountModel inputAccount)
        {
            try
            {
                string userName = inputAccount.USERNAME.Trim();
                string password = inputAccount.PASSWORD.Trim();
                AdminAccountModel account = AdminAccountService.GetData(userName, password);
                if (account != null)
                {
                    if (account.STATUS == 1)
                    {
                        Session.Add(Constants.AUTHORIZE_NAME, account);
                        Session.Add("IsAuthorized", true);

                        // i was serializing the user data and stuffing it in the auth cookie
                        // but I'm simply going to use the Session[] items collection now, so 
                        // just ignore this variable and its inclusion in the cookie below.
                        var userData = "";
                        var ticket = new FormsAuthenticationTicket(1, account.USERNAME, DateTime.UtcNow, DateTime.UtcNow.AddMinutes(30), false, userData, FormsAuthentication.FormsCookiePath);
                        var encryptedTicket = FormsAuthentication.Encrypt(ticket);
                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket) { HttpOnly = true };
                        Response.Cookies.Add(authCookie);
                        return "1"; // Success
                    }
                    else
                    {
                        return "3";// unactive
                    }
                }
                else
                {
                    return "2"; // UnSuccess
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return "2"; // UnSuccess
        }

        public ActionResult Logout()
        {
            if (Session[Constants.AUTHORIZE_NAME] == null)
                //return View("~/Views/BackEnd/Authentication/Index.cshtml");
                return RedirectToAction("login");
            else
            {
                Session[Constants.AUTHORIZE_NAME] = null;
                Response.Cookies.Clear();
            }
            return RedirectToAction("login");
        }

        [AuthorizeActionFilter]
        public bool SaveLog(string nameModule, int id, string field, string type, object oldVal = null, object newVal = null)
        {
            try
            {
                if (string.IsNullOrEmpty(field) == false)
                {
                    LogModel log = new LogModel();
                    log.TYPE        = type;
                    log.OLD_VALUE   = oldVal == null ? "" : oldVal.ToString();
                    log.NEW_VALUE   = newVal == null ? "" : newVal.ToString();
                    log.FUNCTION    = nameModule;
                    log.FUNCTION_ID = id;
                    log.FIELD       = field;
                    log.IP          = this.GetIp();
                    log.ACCOUNT     = this.GetUserLogin();
                    LogServices.SaveLog(log);
                }
                else
                {
                    string[] propertyNames = oldVal.GetType().GetProperties().Select(p => p.Name).ToArray();
                    foreach (var prop in propertyNames)
                    {
                        try
                        {
                            string valOld = oldVal.GetType().GetProperty(prop).GetValue(oldVal, null).ToString();
                            string valNew = newVal.GetType().GetProperty(prop).GetValue(newVal, null).ToString();
                            if (valOld.Trim() != valNew.Trim())
                            {
                                LogModel log = new LogModel();
                                log.TYPE = type;
                                log.OLD_VALUE = valOld;
                                log.NEW_VALUE = valNew;
                                log.FUNCTION = nameModule;
                                log.FUNCTION_ID = id;
                                log.FIELD = prop;
                                log.IP = this.GetIp();
                                log.ACCOUNT = this.GetUserLogin();
                                LogServices.SaveLog(log);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return true;
        }

        private string GetUserLogin()
        {
            //return Session["userinfo"] == null ? "admintest" : Session["userinfo"].ToString();
            try
            {
                Object accountObj = AuthorizeActionFilter.UserIsAuthenticated();
                AdminAccountModel account = (AdminAccountModel)accountObj;
                return account.USERNAME;
            }
            catch 
            { 
            }
            return string.Empty;
        }

        [AuthorizeActionFilter]
        public string GetIp()
        {
            //string hostName = Dns.GetHostName();
            //IPAddress[] addresses = Dns.GetHostEntry(hostName).AddressList;
            //return addresses[2].ToString();
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        [AuthorizeActionFilter]
        public string GetPermission(int idModule, string type)
        {
            Object account = AuthorizeActionFilter.UserIsAuthenticated();
            if (account != null)
            {
                string perm = ((WizardCMS.Models.AdminAccountModel)account).PERMISSION;
                int pos = perm.IndexOf("," + idModule.ToString() + "|");
                if (pos > 0)
                {
                    pos = pos + idModule.ToString().Length;
                    perm = perm.Substring(pos + 2, 3);
                    if (perm.Contains(type) == true)
                    {
                        return type;
                    }
                    else
                    {
                        return Constants.PERMISSION_DENIED;
                    }
                }
                else
                {
                    return Constants.PERMISSION_DENIED;
                }
            }
            return Constants.PERMISSION_DENIED;
        }

        [AuthorizeActionFilter]
        public PartialViewResult MenuAdmin(string nameModule = null)
        {
            List<ModuleModel> lstModule = ModuleServices.GetListModuleActive();
            AdminAccountModel account = (AdminAccountModel)AuthorizeActionFilter.UserIsAuthenticated(); //AuthorizeActionFilter.AccountLogin;
            if (account != null)
            {
                ViewBag.Permission = account.PERMISSION;
            }
            string articleFunctionName = "article";//WebConfigurationManager.AppSettings["ArticleFunctionName"].ToString();
            ViewBag.ArticleFunctionName = articleFunctionName;
            ViewBag.FullWebAppContext = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "/");

            ViewBag.SubArticleMenus = ArticlesServices.GetAdminSubMenu();
            return PartialView("../BackEnd/Admin/MenuAdmin", lstModule);
        }

        [AuthorizeActionFilter]
        public PartialViewResult MenuAdminArticle(string FunctionName)
        {
            ViewBag.FunctionName = FunctionName;
            List<MenuItemDTO> lstSubMenu = ArticlesServices.GetAdminSubMenu();
            //if (lstDto == null)
            //{
            //    lstDto = new List<MenuItemDTO>();
            //}
            return PartialView("../BackEnd/Admin/MenuAdminArticle", lstSubMenu);
        }
    }
}
