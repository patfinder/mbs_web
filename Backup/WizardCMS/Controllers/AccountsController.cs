﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.DTO;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class AccountsController : Controller
    {
        AdmincpController adminControl = new AdmincpController();
        //
        // GET: /Accounts/
        string nameModule = "Accounts";
        static int IdModule = 0;

        public ActionResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            //Check Permission
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = (int)module.ID;
                    if (adminControl.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        return PartialView("../BackEnd/Accounts/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = adminControl.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = adminControl.GetPermission(IdModule, "d");
                    }
                }
            }

            ///
            string nameAction = CommonHelper.GetSegment(3, curentPath);
            if (!string.IsNullOrEmpty(nameAction))
            {
                string idAccount = CommonHelper.GetSegment(4, curentPath);
                if (!string.IsNullOrEmpty(idAccount))
                {
                    //Update
                    int idUser = int.Parse(idAccount);
                    AdminAccountModel account = AdminAccountService.GetAccountById(idUser);
                    List<AccountGroupModel> lstGroup = AdminAccountService.GetAllGroup();
                    SelectGroup selectGroup = new SelectGroup();
                    selectGroup.SelectList = new SelectList(lstGroup, "ID", "NAME", account.GROUP_ID);
                    ViewBag.lstGroup = new SelectList(lstGroup);

                    List<ModuleModel> lstModules = ModuleServices.GetListModuleByGroupId((int)account.GROUP_ID);

                    ViewBag.selectGroup = selectGroup;
                    ViewBag.lstModules = lstModules;
                    return PartialView("../BackEnd/Accounts/Update", account);
                }
                else
                {
                    //add new account
                    AdminAccountModel account = new AdminAccountModel();
                    //Get all group
                    List<AccountGroupModel> lstGroup = AdminAccountService.GetAllGroup();
                    SelectGroup selectGroup = new SelectGroup();
                    selectGroup.SelectList = new SelectList(lstGroup, "ID", "NAME");
                    ViewBag.lstGroup = new SelectList(lstGroup);

                    //Get module by default group
                    List<ModuleModel> lstModules = ModuleServices.GetListModuleByGroupId((int)lstGroup[0].ID);
                    
                    //get list module by default group
                    account.PERMISSION = AdminAccountService.GetPermByGroupId((int)lstGroup[0].ID);
                    account.STATUS = 1;

                    ViewBag.selectGroup = selectGroup;
                    ViewBag.lstModules = lstModules;
                    return PartialView("../BackEnd/Accounts/Update", account);
                }
            }
            else
            {
                //return PartialView("../BackEnd/Accounts/Index", lstAccount);
                using (var context = new DBContext())
                {
                    AdminAccountModel account = (from a in context.ADMIN_WZ_USERS select new AdminAccountModel { 
                        ID             = (int)a.ID,
                        USERNAME        = a.USERNAME,
                        PASSWORD        = a.PASSWORD,
                        PERMISSION      = a.PERMISSION,
                        CUSTOM_PERMISSION   = (int)a.CUSTOM_PERMISSION,
                        GROUP_ID        = (int)a.GROUP_ID,
                        GROUP           = a.ADMIN_WZ_GROUPS,
                        STATUS          = (int)a.STATUS,
                        CREATED         = a.CREATED
                    }).FirstOrDefault();
                }
                
                Paging paging = new Paging();
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
            }
        }

        [HttpPost]
        public string Save()
        {
            string perm = "";
            string pass = "";
            int cus_perm = 0;
            int idStatus = 0;
            string currentPath = Request.Url.ToString();

            string permission = adminControl.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    int grpId = Convert.ToInt32(Request["GROUP_ID"]);
                    AccountGroupModel group = AccountGroupServices.GetGroupById(grpId);
                    string GroupName = group != null ? group.NAME : "";

                    //get permission of default module
                    string cus_perm_group = AdminAccountService.GetPermByGroupId(grpId);
                    List<ModuleModel> lstModules = ModuleServices.GetListModuleByGroupId(grpId);
                    if (lstModules != null)
                    {
                        //Set perm to default module
                        perm = cus_perm_group.Substring(0, 5);
                        foreach (ModuleModel module in lstModules)
                        {
                            if (module != null)
                            {
                                perm += "," + module.ID.ToString() + "|";
                                //If group is admin, set perm in every module
                                if (grpId == 1)
                                {
                                    if (Request["read" + module.ID.ToString() + "Admincp"] == "on")
                                        perm += "r";
                                    else
                                        perm += "-";

                                    if (Request["write" + module.ID.ToString() + "Admincp"] == "on")
                                        perm += "w";
                                    else
                                        perm += "-";

                                    if (Request["delete" + module.ID.ToString() + "Admincp"] == "on")
                                        perm += "d";
                                    else
                                        perm += "-";
                                }
                                //else not set module 1, 2, 3, 14
                                else
                                {
                                    if (Constants.LST_MODULES_INVISBLE.Contains((int)module.ID) == false)
                                    {
                                        if (Request["read" + module.ID.ToString() + "Admincp"] == "on")
                                            perm += "r";
                                        else
                                            perm += "-";

                                        if (Request["write" + module.ID.ToString() + "Admincp"] == "on")
                                            perm += "w";
                                        else
                                            perm += "-";

                                        if (Request["delete" + module.ID.ToString() + "Admincp"] == "on")
                                            perm += "d";
                                        else
                                            perm += "-";
                                    }
                                    //else
                                    //    perm += "---";
                                }
                            }
                            //}
                        }
                    }

                    //permission changed with group default_permission
                    if (!cus_perm_group.Equals(perm))
                        cus_perm = 1;

                    int idUser = Convert.ToInt32(Request["hiddenIdAdmincp"]);
                    string userName = Request["usernameAdmincp"];

                    if (Request["statusAdmincp"] == "on")
                        idStatus = 1;

                    if (idUser == 0)
                    {
                        // Check if user exist
                        if (AdminAccountService.CheckData(userName))
                            return "error-username-exist";

                        AdminAccountModel account = new AdminAccountModel();
                        account.USERNAME = userName;
                        account.PASSWORD = StringHelper.EncryptMD5(Request["passAdmincp"]);
                        account.STATUS = idStatus;
                        account.GROUP_ID = grpId;
                        account.CUSTOM_PERMISSION = cus_perm;
                        account.PERMISSION = perm;

                        //Write to log in database
                        if (AdminAccountService.Save(account, idUser))
                        {
                            //Get id new account
                            int id = AdminAccountService.GetIdAccount(userName);
                            adminControl.SaveLog(nameModule, id, "Add new", "Add new", "", "");
                            return "success";
                        }
                        return "failed";
                    }
                    else
                    {
                        AdminAccountModel account = AdminAccountService.GetAccountById(idUser);

                        if (account.USERNAME != userName)
                        {
                            // Check if user name had existed
                            if (AdminAccountService.CheckData(userName))
                                return "error-username-exist";
                        }

                        if (Request["passAdmincp"] == "")
                            pass = account.PASSWORD;
                        else
                            pass = StringHelper.EncryptMD5(Request["passAdmincp"]);

                        AdminAccountModel new_account = new AdminAccountModel();
                        new_account.ID = account.ID;
                        new_account.USERNAME = userName;
                        new_account.PASSWORD = pass;
                        new_account.STATUS = idStatus;
                        new_account.GROUP_ID = grpId;
                        new_account.CUSTOM_PERMISSION = cus_perm;
                        new_account.PERMISSION = perm;
                        new_account.CREATED = account.CREATED;

                        if (AdminAccountService.Save(new_account, idUser))
                        {
                            adminControl.SaveLog(nameModule, (int)account.ID, "", "Update", account, new_account);
                            return "success";
                        }

                        return "failed";
                    }
                }
                catch (Exception)
                {
                    return "permission-denied";
                }
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteAccount(string listId, string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType)
        {
            bool flag = false;
            object account = AuthorizeActionFilter.AccountLogin;
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    //listId.Add(int.Parse(item));
                    List<int> listAccountId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        listAccountId.Add(int.Parse(item));
                        adminControl.SaveLog(nameModule, int.Parse(item.Trim()), "Delete", "Delete", "", "");
                    }
                    bool result     = AdminAccountService.DeleteAccountByArrayId(listAccountId);
                    int accountId   = (int)((ADMIN_WZ_USERS)account).ID;
                    if (listAccountId.Contains(accountId))
                    {
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            if (flag)
            {
                Session[Constants.AUTHORIZE_NAME] = null;
                Response.Cookies.Clear();
                ViewBag.LogOut = "logout";
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortBy, false);
        }

        public ActionResult Logout()
        {
            Session[Constants.AUTHORIZE_NAME] = null;
            Response.Cookies.Clear();
            return View("~/Views/BackEnd/Authentication/Index.cshtml");
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(string listId, short status, string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType)
        {
            string permission = adminControl.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    //Check Perm of current user
                    string[] arrayId = listId.Split(',');
                    if (arrayId.Count() > 0)
                    {
                        List<int> listAccountId = new List<int>();
                        int oldSatus = (int)(status == 1 ? 0 : 1);
                        foreach (string item in arrayId)
                        {
                            try
                            {
                                AdminAccountModel account = AdminAccountService.GetAccountById(int.Parse(item.Trim()));
                                if (account != null)
                                {
                                    AccountGroupModel accountGroup = AccountGroupServices.GetGroupById((int)account.GROUP_ID);
                                    if (accountGroup != null)
                                    {
                                        if (accountGroup.STATUS == 1)
                                        {
                                            listAccountId.Add(int.Parse(item));
                                            adminControl.SaveLog(nameModule, int.Parse(item.Trim()), "Status", "Update", oldSatus, status);
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        bool result = AdminAccountService.UpdateStatus(listAccountId, status);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout)
        {
            try
            {
                int totalItem = AdminAccountService.CountListAccountByPerpage(fromDate, toDate, searchText);

                paging.TotalItems = totalItem;
                if (paging.Perpage != 0)
                {
                    paging.TotalPage = totalItem / paging.Perpage;
                    if (totalItem % paging.Perpage > 0)
                    {
                        paging.TotalPage++;
                    }
                    if (paging.Page - 1 < 0)
                    {
                        paging.Page = 1;
                    }
                    paging.Offset = (paging.Page - 1) * paging.Perpage;
                    ViewBag.Paging = paging;
                    ViewBag.SortBy = sortBy.ToUpper();
                    ViewBag.SortType = sortType.ToUpper();
                    ViewBag.AjaxUrl = Url.Content("~") + "Accounts/Search/";

                    List<AdminAccountModel> lstAccount = AdminAccountService.GetListAccountByPerpage(fromDate, toDate, searchText, paging, sortBy, sortType);
                    if (hasLayout != null && hasLayout == true)
                    {
                        return PartialView("../BackEnd/Accounts/Index", lstAccount);
                    }
                    else
                    {
                        return PartialView("../BackEnd/Accounts/AjaxContent", lstAccount);
                    }
                }
                List<AdminAccountModel> lstAccounts = AdminAccountService.GetListAccountByPerpage(fromDate, toDate, searchText, paging, sortBy, sortType);
                return PartialView("../BackEnd/Accounts/AjaxContent", lstAccounts);
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
                return null;
            }
        }

        public PartialViewResult EditModule(int grpId)
        {
            string permission = AdminAccountService.GetPermByGroupId(grpId);
            List<ModuleModel> lstModules = ModuleServices.GetListModuleByGroupId(grpId);

            if (!string.IsNullOrEmpty(permission))
            {
                ViewBag.lstModules = lstModules;
                return PartialView("../BackEnd/Accounts/AjaxPermission", permission);
            }
            else
                return PartialView("../BackEnd/Accounts/AjaxPermission", string.Empty);
        }

        public string ResetPerm(int oldGrpId, int userId)
        {
            string permission = adminControl.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string perm = AdminAccountService.GetPermByGroupId(oldGrpId);
                    AdminAccountModel account = AdminAccountService.GetAccountById(userId);
                    string oldValue = account.PERMISSION;
                    account.PERMISSION = perm;

                    if (AdminAccountService.ResetPerm(account))
                    {
                        adminControl.SaveLog(nameModule, userId, "Permission", "Update", oldValue, perm);
                        return "success";
                    }
                    return "failed";
                }
                catch
                {
                    return "failed";
                }
            }
            //ViewBag.MessagePermssion = permission;
            return Constants.PERMISSION_DENIED;
        }
    }
}
