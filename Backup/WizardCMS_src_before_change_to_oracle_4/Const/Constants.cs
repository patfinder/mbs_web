﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WizardCMS.DTO;

namespace WizardCMS.Const
{
    public static class Constants
    {
        public static string LANG_COOKIE_NAME = "_culture";
        public static string AUTHORIZE_NAME = "username";

        public static string LANG_VI = "vi";
        public static string LANG_EN = "en";

        public static int SHOW = 1;
        public static int HIDE = 0;

        public static int MAIN_MENU = 1;
        public static int IS_SUBMENU = 1;

        /* DIRECTORY UPLOADS */
        public static string DIR_PARTNER    = "uploads/partner/";
        public static string DIR_ARTICLE    = "uploads/article/";
        public static string DIR_FILES      = "uploads/files/";
        public static string DIR_SLIDE      = "uploads/slides/";
        public static string DIR_BLOC       = "uploads/bloc/";
        public static string DIR_BANNER     = "Images/uploads/banner/";
        public static string DIR_IMAGE      = "uploads/images/";
        public static string DIR_BACKUP     = "uploads/backup/";
        public static string DIR_BRANCH     = "uploads/branch/";
        /* DIRECTORY UPLOADS */

        /*TEMPLATE BLOC*/
        public static string BLOC_NORMAL    = "0";
        public static string BLOC_PDF       = "PDF";
        public static string BLOC_WORD      = "WORD";
        public static string BLOC_IMAGE     = "IMAGE";
        public static string BLOC_NEWS      = "NEWS";
        public static string BLOC_CONTACT   = "CONTACT";
        public static string BLOC_RECRUITMENT = "RECRUITMENT";

        /*TEMPLATE*/
        public static string NORMAL_PAGE = "0";
        public static string TEMPLATE_TIN_TUC = "NEWS";
        public static string TEMPLATE_VAN_BAN = "VAN_BAN";
        public static string TEMPLATE_DICH_VU = "SERVICE";
        public static string TEMPLATE_LIEN_HE = "CONTACT";
        public static string TEMPLATE_GIOI_THIEU = "ABOUT";
        public static string TEMPLATE_GOC_TR_THONG = "TRUYEN_THONG";
        public static string TEMPLATE_KHACH_HANG = "CUSTOMER";
        public static string TEMPLATE_TAM_NHIN = "TAM_NHIN";
        public static string TEMPLATE_BAO_CAO = "REPORT";
        public static string TEMPLATE_SU_KIEN = "EVENT";
        public static string TEMPLATE_LANH_DAO = "LEADER";
        public static string TEMPLATE_THANH_TUU = "ACCOMPLISH";
        public static string TEMPLATE_GIA_TRI_COT_LOI = "CORE_VALUE";
        public static string TEMPLATE_SO_DO_TO_CHUC = "ORGANIZE_CHART";
        public static string TEMPLATE_MANG_LUOI = "NETWORK";
        public static string TEMPLATE_DV_CHUNG_KHOAN = "BROKERAGE";
        public static string TEMPLATE_CHAM_SOC_KH = "CUSTOMER_SUPPORT";
        public static string TEMPLATE_HOI_THAO_DT = "WORKSHOP";
        public static string TEMPLATE_CHUONG_TRINH_UD = "PROMOTION";
        public static string TEMPLATE_FAQ = "FAQ";
        public static string TEMPLATE_TIM_HIEU = "SEARCH";
        public static string TEMPLATE_SITE_MAP = "SITEMAP";
        public static string TEMPLATE_REFERRAL = "REFERRAL";
        public static string TEMPLATE_HIDDEN = "HIDDEN";

        public static List<TemplateDTO> LIST_TEMPLATE = new List<TemplateDTO>{
            new TemplateDTO { Name = "[ Mặc định ]", Value = NORMAL_PAGE },
            new TemplateDTO { Name = "Tin tức", Value = TEMPLATE_TIN_TUC },
            new TemplateDTO { Name = "Văn bản", Value = TEMPLATE_VAN_BAN },
            new TemplateDTO { Name = "Dịch vụ", Value = TEMPLATE_DICH_VU },
            new TemplateDTO { Name = "Giới thiệu", Value = TEMPLATE_GIOI_THIEU },
            new TemplateDTO { Name = "Góc truyền thông", Value = TEMPLATE_GOC_TR_THONG },
            new TemplateDTO { Name = "Tầm nhìn - sứ mệnh", Value = TEMPLATE_TAM_NHIN },
            new TemplateDTO { Name = "Lãnh đạo", Value = TEMPLATE_LANH_DAO },
            new TemplateDTO { Name = "Giá trị cột lõi", Value = TEMPLATE_GIA_TRI_COT_LOI },
            new TemplateDTO { Name = "Sơ đồ tổ chức", Value = TEMPLATE_SO_DO_TO_CHUC },
            new TemplateDTO { Name = "Thành tựu", Value = TEMPLATE_THANH_TUU },
            new TemplateDTO { Name = "Mạng lưới", Value = TEMPLATE_MANG_LUOI },
            new TemplateDTO { Name = "Dịch vụ chứng khoán", Value = TEMPLATE_DV_CHUNG_KHOAN },
            new TemplateDTO { Name = "Thương vụ", Value = TEMPLATE_KHACH_HANG },
            new TemplateDTO { Name = "Chăm sóc khách hàng", Value = TEMPLATE_CHAM_SOC_KH },
            new TemplateDTO { Name = "Báo cáo", Value = TEMPLATE_BAO_CAO },
            new TemplateDTO { Name = "Lịch sự kiện", Value = TEMPLATE_SU_KIEN },
            new TemplateDTO { Name = "Liên hệ", Value = TEMPLATE_LIEN_HE },
            new TemplateDTO { Name = "Tìm kiếm", Value = TEMPLATE_TIM_HIEU },
            new TemplateDTO { Name = "Sơ đồ trang", Value = TEMPLATE_SITE_MAP },
            new TemplateDTO { Name = "Ẩn danh", Value = TEMPLATE_HIDDEN }
        };

        public static string[] Langs = { "vi", "en" };

        public static int DEFAULT_ITEM = 10;
        public static string MANAGER_MODULES_DEFAULT = "Article";

        public static string SORT_ASC = "ASC";
        public static string SORT_DESC = "DESC";
        public static string SORT_PORPERTY_DEFAULT = "CREATED";
        public static List<int> LST_MODULES_INVISBLE = new List<int>();

        public static List<int> LST_STATiC_PAGES = new List<int>{6};

        public static string BANNER_VI = "banner_vi";
        public static string BANNER_EN = "banner_en";
        public static string BANNER_NO_IMAGE = "no-images.jpg";

        public static int ACTION_FAULT = -1;
        public static int ACTION_EXIST_NAME = 0;
        public static int ACTION_SUCCESS = 1;

        public static int EXIST_NAME_VN = 0;
        public static int EXIST_NAME_EN = -1;

        public static string SORT_PORPERTY_PDF_DEFAULT = "Name";
        public static string SORT_PORPERTY_WORD_DEFAULT = "Name";

        public static string BLOC_MODULE = "cli_bloc";

        public static string SORT_PORPERTY_IMAGE_DEFAULT = "Name";

        public static string TB_ARTICLE = "wz_article";
        public static string TB_BACKUP = "wz_backup";
        public static string TB_BANNER = "wz_banner";
        public static string TB_BLOC = "wz_bloc";
        public static string PERMISSION_DENIED = "permission-denied";
        public static int MAX_LENGTH_IMG = 2097152; // 2MB
        public static string UPLOAD_FILE_IMG_VN = "File image Vi upload không lớn hơn 2MB";
        public static string UPLOAD_FILE_IMG_EN = "File image En upload không lớn hơn 2MB";
        public static string ERROR_MAX_UPLOAD = "ERROR_MAX_UPLOAD_SIZE";
        public static string ERROR_MAX_UPLOAD_MSG = "File upload quá lớn, vui lòng chọn file khác!";

        public static Dictionary<string, string> DC_TRANSLATES = null;

        public static int GROUP_HOISO = 2;
        public static int GROUP_HOISO_CN = 0;
        public static int GROUP_DIEM_GD = 1;

        public static int REGION_BAC = 1;
        public static int REGION_TRUNG = 2;
        public static int REGION_NAM = 3;

        //STATIC NODE ID
        public static int NODE_TRUNGTAM_PHANTICH = 3;
        public static int NODE_TINTUC_THITRUONG = 266;
        public static int NODE_THITRUONG_CK = 277;
        public static int NODE_TIN_VI_MO = 276;
        public static int NODE_TIN_THE_GIOI = 275;
        public static int NODE_BAOCAO_PHANTICH = 265;

        public static int NODE_GIOI_THIEU = 5;
        public static int NODE_GOC_TT = 55;
        public static int NODE_QUAN_HE_CD = 56;
        public static int NODE_CO_HOI_NN = 57;
        public static int NODE_LOI_THE_CT = 43;
        public static int NODE_MOC_SUKIEN = 44;

        public static int NODE_VANHOA_NOIBO = 12;

        public static int NODE_TIN_MBS = 58;
        public static int NODE_MBS_BAO_CHI = 59;
        public static int NODE_TIN_CO_DONG = 63;
        public static int NODE_BAO_CAO_TC = 64;
        public static int NODE_CONGBO_THONGTIN = 65;
        public static int NODE_THONGTIN_QUANTRI = 66;

        public static int NODE_KHACH_HANG_CN = 1;
        public static int NODE_MOI_GIOI_KH_CN = 129;
        public static int NODE_KHACH_HANG_TC = 112;
        public static int NODE_TUVAN_COPHIEU_KH_TC = 124;
        public static int NODE_DV_DIEN_TU = 120;
        public static int NODE_DV_DIEN_TU_CN = 132;
        public static int NODE_DV_BIEU_PHI_CN = 135;
        public static int NODE_DV_BIEU_PHI_TC = 123;
        public static int NODE_DV_DOI_NGU = 212;
        public static int NODE_DV_CK_CN = 113;
        public static int NODE_DV_CK_TC = 114;
        public static int NODE_DV_NHDT_TC = 115;
        public static int NODE_DV_THUONG_VU_DH = 150;
        //public static int NODE_DOI_NGU_CN = 1;
        public static int NODE_DOI_NGU_TC = 212;

        public static int NODE_CHAM_SOC_KH = 4;
        public static int NODE_HUONG_DAN = 161;
        public static int NODE_HUONG_DAN_CHILD = 165;
        public static int NODE_HUONG_DAN_BIEU_PHI = 166;
        public static int NODE_VIDEO_HUONG_DAN = 167;

        public static int NODE_TL_THAM_KHAO_CKPS = 10856;
        public static int NODE_VAN_BAN_CHINH_PHU_CKPS = 10857;
        public static int NODE_VAN_BAN_BO_TC_CKPS = 10858;
        public static int NODE_VAN_BAN_SGD_CKPS = 10859;

        public static int NODE_CAU_HOI_THUONG_GAP = 164;
        public static int NODE_HOI_THAO_DAO_TAO = 162;
        public static int NODE_CHUONG_TRINH_UU_DAI = 163;

        public static int NODE_PT_THI_TRUONG_CK = 277;
        public static int NODE_PT_CKPS = 11074;//10863;
        public static int NODE_PT_VI_MO = 276;
        public static int NODE_PT_THE_GIOI = 275;
        public static int NODE_PT_DULIEU_TT = 262;

        public static int NODE_BC_TRIEN_VONG_VN = 274;
        public static int NODE_BC_NGHIEN_CUU_NGANH = 272;
        public static int NODE_BC_THI_TRUONG_NO = 273;
        public static int NODE_BC_BAN_TIN_NGAY = 270;
        public static int NODE_BC_NGHIEN_CUU_CP = 271;
        public static int NODE_BC_CHIEN_LUOC_TUAN = 269;
        public static int NODE_BC_PT_KY_THUAT = 268;
        public static int NODE_BC_PT_NHANH_CP = 267;
        //END STATIC NODE ID

        //TAXONOMY
        public static int TAXO_HUONGDAN_GD = 2;
        public static int TAXO_FAQ = 1;
        //END TAXONOMY

        //THI TRUONG CHUNG KHOAN
        public static int[] CATEGORIES_THI_TRUONG = { 107, 1, 179, 180, 205, 208, 209, 210, 211, 212, 213, 214 };
        public static int[] CATEGORIES_CKPS = { 243 };
        public static int[] CATEGORIES_VI_MO = { 130, 188, 191, 195, 196, 157 };
        public static int[] CATEGORIES_THE_GIOI = { 200, 182, 201 };
        //END THI TRUONG CHUNG KHOAN

        public static int NODE_HUONGDAN_NOP_HS = 9142;

        //MANG LUOI
        
    }
}