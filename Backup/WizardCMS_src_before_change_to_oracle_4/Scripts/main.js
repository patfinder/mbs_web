function checkInp(val, error) {
    if (val == '') {

    } else {
        //$('input[name="'+error+'"], textarea[name="'+error+'"]').removeClass('error-fr');
        if (error == 'cmnd') {
            if (isNaN(val)) {
                $('#msg_fr').html('CMND không hợp lệ');
                return false;
            } else if ($('#info_cmnd').val().length < 9 || $('#info_cmnd').val().length > 10) {
                $('#msg_fr').html('CMND không hợp lệ');
                return false;
            }
            return true;
        }

        if (error == 'email') {
            var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (!val.match(filter)) {
                $('#msg_fr').html('Ðịa chỉ Email không hợp lệ');
                return false;
            }
            return true;
        }
        if (error == 'password') {
            if (val.length < 6)
                return false;
            return true;
        }

        if (error == 'username') {
            if (val.length < 6)
                return false;
            return true;
        }

        if (error == 'phone') {
            if (isNaN(val)) {
                $('#msg_fr').html('Số điện thoại không hợp lệ');
                return false;
            } else {
                if (val.substr(0, 2) === '01') {
                    if (val.length < 11 || val.length > 11) {
                        $('#msg_fr').html('Số điện thoại không hợp lệ');
                        return false;
                    }
                } else {
                    if (val.length < 10 || val.length > 10) {
                        $('#msg_fr').html('Số điện thoại không hợp lệ');
                        return false;
                    }
                }
            }
            return true;
        }

        return true;
    }
}