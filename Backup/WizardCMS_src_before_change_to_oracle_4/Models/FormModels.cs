﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class FormViewModel
    {
        public int ID { get; set; }
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public string TITLE { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }
        public string FILE_DOWNLOAD { get; set; }

        public DateTime CREATED { get; set; }
        public DateTime MODIFIED { get; set; }

        //public List<FormContent> ContentTranslates { get; set; }
        public Dictionary<string, FormContent> ContentTranslates { get; set; }

        public List<FormViewModel> Childs { get; set; }

        //Constructor
        public FormViewModel()
        {
            this.ContentTranslates = new Dictionary<string, FormContent>();
        }
    }

    public class FormContent
    {
        public int NID { get; set; }
        public string TITLE { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }
        public string FILE_DOWNLOAD { get; set; }
        public string LANG { get; set; }

        public FormContent()
        { }
    }

    public static class FormServices
    {
        #region services interface
        public static FormContent GetFormContentByLang(Dictionary<string, FormContent> lstFormContent, string lang = "vi")
        {
            try
            {
                if (lstFormContent != null && lstFormContent.ContainsKey(lang))
                {
                    return lstFormContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static Dictionary<string, FormContent> GetContentTranslates(int NID)
        {
            try
            {
                return FormDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<FormViewModel> GetSearchContent(int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return FormDAL.Instance.GetSearchContent(page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return FormDAL.Instance.GetCountSearchContent(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FormViewModel GetById(int id)
        {
            try
            {
                return FormDAL.Instance.GetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FormViewModel GetFormContentById(int id, string lang = "")
        {
            try
            {
                return FormDAL.Instance.GetFormContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FormViewModel AdminGetById(int id)
        {
            try
            {
                return FormDAL.Instance.AdminGetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(FormViewModel article)
        {
            try
            {
                return FormDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return FormDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parenNID, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return FormDAL.Instance.GetCount(parenNID, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<FormViewModel> GetByPaging(int parenNID, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return FormDAL.Instance.GetByPaging(parenNID, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="viewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(FormViewModel viewModel)
        {
            try
            {
                return FormDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return FormDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lsNID)
        {
            try
            {
                return FormDAL.Instance.DeleteList(lsNID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckSlug(string slug, string lang)
        {
            try
            {
                return FormDAL.Instance.CheckSlug(slug, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public class FormDAL
        {
            private static FormDAL _instance;

            static public FormDAL Instance { 
                get {
                    if (_instance == null)
                        _instance = new FormDAL();

                    return _instance;
                }
            }

            #region constructor
            public FormDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
           
            public Dictionary<string, FormContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, FormContent> result = new Dictionary<string, FormContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_RE_FORM_CONTENT
                                    where ct.NID == NID
                                    orderby ct.LANG descending
                                    select new FormContent
                                    {
                                        NID = (int)ct.NID,
                                        TITLE = ct.TITLE,
                                        DESCRIPTION = ct.DESCRIPTION,
                                        SLUG = ct.SLUG,
                                        LANG = ct.LANG,
                                        FILE_DOWNLOAD = ct.FILE_DOWNLOAD
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            public List<FormViewModel> GetSearchContent(int page = 1, int perpage = 10, string searchText="", string dateFrom="", string dateTo="")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_RE_FORM
                                    from mc in context.WZ_RE_FORM_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new FormViewModel
                                    {
                                        ID = (int)m.ID,
                                        TITLE = mc.TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        SLUG = mc.SLUG,
                                        FILE_DOWNLOAD = mc.FILE_DOWNLOAD,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = from a in query
                                    where SqlMethods.Like(a.TITLE.ToLower(), "%" + searchText + "%")
                                    select a;
                        }

                        if (query.Count() > 0 && page > 0)
                        {
                            return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var result = from m in context.WZ_RE_FORM
                                     from mc in context.WZ_RE_FORM_CONTENT
                                     where mc.NID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                     select m;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_RE_FORM_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && SqlMethods.Like(ac.TITLE.ToLower(), "%" + searchText + "%")
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            /// <summary>
            /// GET FORM DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public FormViewModel GetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_RE_FORM
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new FormViewModel
                                    {
                                        ID = (int)n.ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// GET FORM DETAIL
            /// </summary>
            /// <param name="id">ID FORM</param>
            /// <returns></returns>
            public FormViewModel GetFormContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_RE_FORM
                                    from nc in context.WZ_RE_FORM_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new FormViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        SLUG = nc.SLUG,
                                        FILE_DOWNLOAD = nc.FILE_DOWNLOAD,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public FormViewModel AdminGetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = (from a in context.WZ_RE_FORM
                                       where (a.ID == id)
                                       select new FormViewModel
                                       {
                                           ID = (int)a.ID,
                                           STATUS = (int)a.STATUS,
                                           MODIFIED = a.MODIFIED,
                                           CREATED = a.CREATED,
                                           PRIORITY = (int)a.PRIORITY,
                                       }).FirstOrDefault();

                        if (article != null)
                            article.ContentTranslates = this.GetContentTranslates(article.ID);

                        return article;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(FormViewModel term)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_RE_FORM.Where(a => a.ID == term.ID).FirstOrDefault();
                        if (node != null)
                        {
                            node.STATUS = term.STATUS;
                            node.PRIORITY = term.PRIORITY;
                            node.MODIFIED = DateTime.Now;
                            context.SaveChanges();

                            //update content translates
                            if (term.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, FormContent> item in term.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_RE_FORM_CONTENT.Where(a => a.NID == term.ID && a.LANG == content_translate.LANG).FirstOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.TITLE))
                                    {
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.FILE_DOWNLOAD = content_translate.FILE_DOWNLOAD;

                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        node_content_lang = new WZ_RE_FORM_CONTENT();
                                        node_content_lang.NID = term.ID;
                                        node_content_lang.LANG = content_translate.LANG;
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.FILE_DOWNLOAD = content_translate.FILE_DOWNLOAD;
                                        context.WZ_RE_FORM_CONTENT.AddObject(node_content_lang);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_RE_FORM.Where(a => a.ID == id).FirstOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parenNID, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_RE_FORM
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_RE_FORM_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list nodes by paging
            public List<FormViewModel> GetByPaging(int parenNID, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<FormViewModel> lstArticlles = new List<FormViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_RE_FORM
                                     from ac in context.WZ_RE_FORM_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang
                                     select new FormViewModel {
                                        ID = (int)a.ID,
                                        CREATED = a.CREATED,
                                        MODIFIED = a.MODIFIED,
                                        PRIORITY = (int)a.PRIORITY,
                                        FILE_DOWNLOAD = ac.FILE_DOWNLOAD,
                                        TITLE = ac.TITLE,
                                        SLUG = ac.SLUG,
                                        DESCRIPTION = ac.DESCRIPTION,
                                        STATUS = (int)a.STATUS
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_RE_FORM_CONTENT
                                     where a.TITLE.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(FormViewModel termView)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_RE_FORM term = new WZ_RE_FORM();
                        if (termView != null)
                        {
                            term.ID = this._getNexNID();
                            term.STATUS = termView.STATUS;
                            term.PRIORITY = termView.PRIORITY;
                            term.CREATED = DateTime.Now;
                            term.MODIFIED = DateTime.Now;

                            context.WZ_RE_FORM.AddObject(term);
                            context.SaveChanges();

                            if (termView.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, FormContent> item in termView.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_RE_FORM_CONTENT content = new WZ_RE_FORM_CONTENT();
                                    content.NID = term.ID;
                                    content.TITLE = node_content.TITLE;
                                    content.SLUG = node_content.SLUG;
                                    content.DESCRIPTION = node_content.DESCRIPTION;
                                    content.LANG = node_content.LANG;
                                    content.FILE_DOWNLOAD = node_content.FILE_DOWNLOAD;

                                    context.WZ_RE_FORM_CONTENT.AddObject(content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)term.ID;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNexNID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_FORM" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_FORM";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        var check = context.WZ_RE_FORM.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNexNID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_RE_FORM.Where(a => a.ID == id).FirstOrDefault();

                        if (node != null)
                        {
                            context.WZ_RE_FORM.DeleteObject(node);
                            context.SaveChanges();

                            //delete article content translate
                            var content_translates = context.WZ_RE_FORM_CONTENT.Where(ac=>ac.NID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_RE_FORM_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lsNID)
            {
                try
                {
                    if (lsNID != null)
                    {
                        foreach (int id in lsNID)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            /// <summary>
            /// Check if article slug already exist following language
            /// </summary>
            /// <param name="slug">Input slug</param>
            /// <param name="lang">Input language</param>
            /// <returns>True is exist</returns>
            public bool CheckSlug(string slug, string lang)
            {
                try
                {
                    lang = string.IsNullOrEmpty(lang) ? "vi" : lang;
                    using (var context = new DBContext())
                    {
                        var node = (from a in context.WZ_RE_FORM
                                    from ac in context.WZ_RE_FORM_CONTENT
                                    where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug
                                    select a).FirstOrDefault();
                        if (node != null) return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            #endregion
        }
    }
}