﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class ServiceViewModel
    {
        public int ID { get; set; }
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public int TYPE { get; set; }

        public string NAME { get; set; }
        public string LINK { get; set; }
        public string ICON { get; set; }
        public string ICON_HOVER { get; set; }

        public DateTime CREATED { get; set; }
        public DateTime MODIFIED { get; set; }

        public Dictionary<string, ServiceContent> ContentTranslates { get; set; }


        //Constructor
        public ServiceViewModel()
        {
            this.ContentTranslates = new Dictionary<string, ServiceContent>();
        }
    }

    public class ServiceContent
    {
        public int NID { get; set; }
        public string TITLE { get; set; }
        public string SUB_TITLE { get; set; }
        public string SLUG { get; set; }
        public string LINK { get; set; }
        public string DESCRIPTION { get; set; }
        public string CONTENT { get; set; }
        public string IMAGE { get; set; }
        public string ATTACHMENT { get; set; }
        public string LANG { get; set; }

        public ServiceContent()
        { }
    }

    public static class ArticlesServices
    {
        #region Articles services interface
        public static ArticleContent GetArticleContentByLang(Dictionary<string, ArticleContent> lstArticleContent, string lang = "vi")
        {
            try
            {
                if (lstArticleContent != null && lstArticleContent.ContainsKey(lang))
                {
                    return lstArticleContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static List<MenuItemDTO> GetMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Dictionary<string, ArticleContent> GetContentTranslates(int NID)
        {
            try
            {
                return ArticlesDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetAdminSubMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetAdminSubMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetMenuByListId(int[] arrArticleIDs, int depth)
        {
            try
            {
                return ArticlesDAL.Instance.GetMenuByListId(arrArticleIDs, depth);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetFooterMenu()
        {
            try
            {
                return ArticlesDAL.Instance.GetFooterMenu();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetAll()
        {
            try
            {
                ArticlesDAL.Instance.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleByLink(string link)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByLink(link);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleBySlug(string slug)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleBySlug(slug);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MenuItemDTO> GetSiteMap()
        {
            try
            {
                return ArticlesDAL.Instance.GetSiteMap();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetTopParent(ArticleViewModel article)
        {
            try
            {
                return ArticlesDAL.Instance.GetTopParent(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetArticlesByParentId(int parentId, int page, int perpage)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticlesByParentId(parentId, page, perpage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// LẤY DANH SÁCH BÀI VIẾT
        /// </summary>
        /// <param name="parrent">ID NODE CHA</param>
        /// <param name="depth">Độ sâu muốn lấy bài viết con</param>
        /// <param name="current_level"></param>
        /// <returns></returns>
        public static List<ArticleViewModel> GetArticleByParrent(int parrent, int depth = 1, int current_level = 1)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByParrent(parrent, depth, current_level);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetSearchContent(parrent, page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return ArticlesDAL.Instance.GetCountSearchContent(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetSearchReport(int parrent, int year_report, List<int> not_in_year, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetSearchReport(parrent, year_report, not_in_year, page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchReport(int parentId, int year_report, List<int> not_in_year, string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return ArticlesDAL.Instance.GetCountSearchReport(parentId, year_report, not_in_year, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountArticlesByParentId(int parentId)
        {
            try
            {
                return ArticlesDAL.Instance.CountArticlesByParentId(parentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleById(int id)
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel GetArticleContentById(int id, string lang = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<int> GetListArticleIdByParentId(int parentId, string lang)
        {
            try
            {
                return ArticlesDAL.Instance.GetListArticleIdByParentId(parentId, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ArticleViewModel AdminGetArticalById(int id)
        {
            try
            {
                return ArticlesDAL.Instance.AdminGetArticalById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(ArticleViewModel article)
        {
            try
            {
                return ArticlesDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return ArticlesDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return ArticlesDAL.Instance.GetCount(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ArticleViewModel> GetArticleByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang="vi")
        {
            try
            {
                return ArticlesDAL.Instance.GetArticleByPaging(parentId, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="articleViewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(ArticleViewModel articleViewModel)
        {
            try
            {
                return ArticlesDAL.Instance.Insert(articleViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return ArticlesDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lstId)
        {
            try
            {
                return ArticlesDAL.Instance.DeleteList(lstId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSelectList(int selectedId, int? parentId, int currentId = 0, string space = "")
        {
            try
            {
                return ArticlesDAL.Instance.GetSelectList(selectedId, parentId, currentId, space = "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckSlug(string slug, string lang, int parent = 0)
        {
            try
            {
                return ArticlesDAL.Instance.CheckSlug(slug, lang, parent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateLinkCustom(int parentId)
        {
            try
            {
                ArticlesDAL.Instance.UpdateLinkCustom(parentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateMenuLink(int parentId, string parentLink = "", string lang = "vi")
        {
            try
            {
                ArticlesDAL.Instance.UpdateMenuLink(parentId, parentLink = "", lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class ArticlesDAL
        {
            private static ArticlesDAL _instance;

            static public ArticlesDAL Instance { 
                get {
                    if (_instance == null)
                        _instance = new ArticlesDAL();

                    return _instance;
                }
            }

            #region constructor
            public ArticlesDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
            /// <summary>
            /// Get All Menu Item
            /// </summary>
            public List<MenuItemDTO> GetMenu()
            {
                List<MenuItemDTO> result = new List<MenuItemDTO>();
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = context.WZ_ARTICLES.Where(a => a.MAIN_MENU == 1 && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).ToList();
                        if (menus != null)
                        {
                            foreach (var item in menus)
                            {
                                MenuItemDTO menu = new MenuItemDTO();
                                menu.Id = (int)item.ID;
                                menu.ParrentId = (int)item.PARENT_ID;

                                menu.ContentTranslates = this.GetContentTranslates((int)item.ID);

                                //get sub menu
                                var submenu = context.WZ_ARTICLES.Where(a => a.PARENT_ID == item.ID && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).Select(a=>new MenuItemDTO
                                {
                                    Id = (int)a.ID,
                                    ParrentId = (int)a.PARENT_ID,
                                    ContentTranslates = this.GetContentTranslates((int)a.ID)
                                });

                                if (submenu != null) menu.lstSubMenu = submenu.ToList();

                                if (menu.ContentTranslates != null) result.Add(menu);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public Dictionary<string, ArticleContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, ArticleContent> result = new Dictionary<string, ArticleContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_ARTICLES_CONTENT
                                    where ct.NID == NID
                                    orderby ct.LANG descending
                                    select new ArticleContent
                                    {
                                        NID = (int)ct.NID,
                                        TITLE = ct.TITLE,
                                        SUB_TITLE = ct.SUB_TITLE,
                                        DESCRIPTION = ct.DESCRIPTION,
                                        CONTENT = ct.CONTENT,
                                        SLUG = ct.SLUG,
                                        LINK = ct.LINK,
                                        IMAGE = ct.IMAGE,
                                        ATTACHMENT = ct.ATTACHMENT,
                                        LANG = ct.LANG
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            /// <summary>
            /// ADMIN CP GET SUB MENU OF ARTICLES
            /// </summary>
            /// <returns>List MenuItemDTO</returns>
            public List<MenuItemDTO> GetAdminSubMenu()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == Constants.LANG_VI && m.ADMIN_SUBMENU == Constants.IS_SUBMENU && m.PARENT_ID == 0
                                    orderby m.PRIORITY ascending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG
                                    };
                        var temp = menus.ToList();

                        if (temp != null)
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetAdminSubMenu2(item.Id);
                            }
                        }

                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }


            public List<MenuItemDTO> GetAdminSubMenu2(int parrent, int level = 0, int current_level = 1)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == Constants.LANG_VI && m.PARENT_ID == parrent
                                    orderby m.PRIORITY ascending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null && (level == 0 || level > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetAdminSubMenu2(item.Id, level, current_level+1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<MenuItemDTO> GetMenuByListId(int[] arrArticleIDs, int depth = 1)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == Constants.LANG_VI && arrArticleIDs.Contains((int)m.ID) && m.TEMPLATE != Constants.TEMPLATE_HIDDEN
                                    orderby m.PRIORITY ascending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null)
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetSubMenu2(item.Id, depth, 1);
                            }

                            return temp;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<MenuItemDTO> GetSubMenu2(int parrent, int level = 0, int current_level = 1)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == Constants.LANG_VI && m.PARENT_ID == parrent && m.TEMPLATE != Constants.TEMPLATE_HIDDEN
                                    orderby m.PRIORITY ascending
                                    select new MenuItemDTO
                                    {
                                        Id = (int)m.ID,
                                        ParrentId = (int)m.PARENT_ID,
                                        Title = mc.TITLE,
                                        Slug = mc.SLUG,
                                        Link = mc.LINK
                                    };
                        var temp = menus.ToList();

                        if (temp != null && (level == 0 || level > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.lstSubMenu = this.GetAdminSubMenu2(item.Id, level, current_level + 1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get Footer Menu
            /// </summary>
            public List<MenuItemDTO> GetFooterMenu()
            {
                List<MenuItemDTO> result = new List<MenuItemDTO>();
                try
                {
                    using (var context = new DBContext())
                    {
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public void GetAll()
            {
                try
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            /// <summary>
            /// Get An Article By Id
            /// </summary>
            /// <param name="link">input link</param>
            /// <returns>result</returns>
            public ArticleViewModel GetArticleByLink(string link)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                   from nc in context.WZ_ARTICLES_CONTENT
                                   where nc.NID == n.ID && nc.LINK == link && nc.LANG == current_lang && n.STATUS == Constants.SHOW
                                   select new ArticleViewModel {
                                      ID = (int)n.ID,
                                      TITLE = nc.TITLE,
                                      SUB_TITLE = nc.SUB_TITLE,
                                      LINK = nc.LINK,
                                      CONTENT = nc.CONTENT,
                                      DESCRIPTION = nc.DESCRIPTION,
                                      PARENT_ID = (int)n.PARENT_ID,
                                      CREATED = n.CREATED,
                                      MODIFIED = n.MODIFIED,
                                      TEMPLATE = n.TEMPLATE,
                                      PRIORITY = (int)n.PRIORITY,
                                      FEATURED = (int)n.FEATURED,
                                      MAIN_MENU = (int)n.MAIN_MENU,
                                      ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                      STATUS = (int)n.STATUS,
                                   }).FirstOrDefault();
                        if (node != null)
                            node.ContentTranslates = this.GetContentTranslates(node.ID);
                        else
                            node = null;
                        return node;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get An Article By Slug
            /// </summary>
            /// <param name="slug">input slug</param>
            /// <returns>result</returns>
            public ArticleViewModel GetArticleBySlug(string slug)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where nc.NID == n.ID && nc.SLUG == slug && nc.LANG == current_lang && n.STATUS == Constants.SHOW
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS,
                                        ContentTranslates = this.GetContentTranslates((int)n.ID)
                                    }).FirstOrDefault();
                        return node;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Set the site map
            /// </summary>
            /// <returns></returns>
            public List<MenuItemDTO> GetSiteMap()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        List<MenuItemDTO> result = new List<MenuItemDTO>();
                        var menus = context.WZ_ARTICLES.Where(a => a.MAIN_MENU == Constants.MAIN_MENU && a.PARENT_ID == 0 && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a=>a.CREATED).ToList();
                        if (menus != null)
                        {
                            foreach (var item in menus)
                            {
                                MenuItemDTO menu = new MenuItemDTO();
                                menu.Id = (int)item.ID;
                                menu.ParrentId = (int)item.PARENT_ID;
                                menu.ContentTranslates = this.GetContentTranslates((int)item.ID);

                                //get sub menu
                                var submenu = context.WZ_ARTICLES.Where(a => a.PARENT_ID == item.ID && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).Select(a => new MenuItemDTO
                                {
                                    Id = (int)a.ID,
                                    ParrentId = (int)a.PARENT_ID,
                                    ContentTranslates = this.GetContentTranslates((int)a.ID)
                                });

                                if (submenu != null) menu.lstSubMenu = submenu.ToList();

                                if (menu.ContentTranslates != null) result.Add(menu);
                            }
                        }

                        if (result.Count > 0)
                        {
                            return result;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the root parent of an node
            /// </summary>
            /// <param name="article">Current node</param>
            /// <returns></returns>
            public ArticleViewModel GetTopParent(ArticleViewModel article)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (article.PARENT_ID > 0)
                        {
                            var ar = context.WZ_ARTICLES.Where(a => a.ID == (int)article.PARENT_ID && a.STATUS == Constants.SHOW).FirstOrDefault();
                            if (ar != null)
                            {
                                article.ID = (int)ar.ID;
                                article.TEMPLATE = ar.TEMPLATE;
                                article.PARENT_ID = (int)ar.PARENT_ID;
                                article.STATUS = (int)ar.STATUS;
                                article.PRIORITY = (int)ar.PRIORITY;
                                article.FEATURED = (int)ar.FEATURED;

                                while (article.PARENT_ID > 0)
                                {
                                    return this.GetTopParent(article);
                                }

                                return article;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return article;
            }

            public List<ArticleViewModel> GetArticlesByParentId(int parentId, int page, int perpage)
            {
                List<ArticleViewModel> result = new List<ArticleViewModel>();
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticles = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId && a.STATUS == Constants.SHOW).OrderBy(a => a.PRIORITY).ThenByDescending(a => a.CREATED).ThenByDescending(a => a.ID).ToList();
                        if (lstArticles != null)
                        {
                            lstArticles = lstArticles.Skip((page - 1) * perpage).Take(perpage).ToList();
                            foreach (var item in lstArticles)
                            {
                                ArticleViewModel article    = new ArticleViewModel();
                                article.ID                  = (int)item.ID;
                                article.PARENT_ID           = (int)item.PARENT_ID;
                                article.MAIN_MENU           = (int)item.MAIN_MENU;
                                article.STATUS              = (int)item.STATUS;
                                article.CREATED             = item.CREATED;
                                article.PRIORITY            = (int)item.PRIORITY;
                                article.FEATURED            = (int)item.FEATURED;
                                article.VIEWS               = (int)item.VIEWS;
                                article.ADMIN_SUBMENU       = (int)item.ADMIN_SUBMENU;
                                article.ContentTranslates   = this.GetContentTranslates((int)item.ID);

                                result.Add(article);
                            }
                            return result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return result;
            }

            public List<ArticleViewModel> GetArticleByParrent(int parrent, int depth = 1, int current_level = 1, int page = 1, int perpage = 10)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var menus = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == Constants.LANG_VI && m.PARENT_ID == parrent
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE
                                    };
                        var temp = menus.ToList();

                        if (temp != null && page > 0)
                        {
                            temp = temp.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        if (temp != null && (depth == 0 || depth > current_level))
                        {
                            foreach (var item in temp)
                            {
                                item.Childs = this.GetArticleByParrent(item.ID, depth, current_level + 1);
                            }
                        }
                        return temp;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public List<ArticleViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText="", string dateFrom="", string dateTo="")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED
                                    };
                        var result = query.ToList();

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate).ToList();
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate).ToList();
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = (from a in result
                                     where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText))
                                     select a).ToList();
                        }

                        if (result != null && page > 0)
                        {
                            result = result.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES
                                     where a.STATUS == Constants.SHOW
                                     select a;
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId);
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<ArticleViewModel> GetSearchReport(int parrent, int year_report, List<int> not_in_year, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
            {
                try
                {
                    if (not_in_year != null && not_in_year.Count == 0)
                        return null;

                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_ARTICLES
                                    from mc in context.WZ_ARTICLES_CONTENT
                                    where mc.NID == m.ID && mc.LANG == currentLang && m.PARENT_ID == parrent && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new ArticleViewModel
                                    {
                                        ID = (int)m.ID,
                                        PARENT_ID = (int)m.PARENT_ID,
                                        TITLE = mc.TITLE,
                                        SUB_TITLE = mc.SUB_TITLE,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        CONTENT = mc.CONTENT,
                                        SLUG = mc.SLUG,
                                        LINK = mc.LINK,
                                        THUMBNAIL = mc.IMAGE,
                                        ATTACHMENT = mc.ATTACHMENT,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED,
                                        YEAR_REPORT = (int)m.YEAR_REPORT
                                    };
                        //var result = query.ToList();

                        if (year_report > 0) {
                            query = (from m in query where m.YEAR_REPORT == year_report select m);
                        }
                        else if (not_in_year != null && not_in_year.Count() > 0)
                        {
                            query = (from m in query where !not_in_year.Contains((int)m.YEAR_REPORT) select m);
                        }

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = (from a in query
                                      where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText))
                                      select a);
                        }

                        if (query != null && page > 0)
                        {
                            query = query.Skip((page - 1) * perpage).Take(perpage);
                        }
                        var result = query.ToList();
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchReport(int parentId, int year_report, List<int> not_in_year, string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    if (not_in_year != null && not_in_year.Count == 0)
                        return 0;

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES
                                     where a.STATUS == Constants.SHOW
                                     select a;
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId);
                        }

                        if (year_report > 0)
                        {
                            result = result.Where(m=>m.YEAR_REPORT == year_report);
                        }
                        else if (not_in_year != null && not_in_year.Count() > 0)
                        {
                            result = from m in result where !not_in_year.Contains( (int)m.YEAR_REPORT ) select m;
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //Count list article for pagination
            public int CountArticlesByParentId(int parentId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticle = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId && a.STATUS == Constants.SHOW).OrderByDescending(a => a.ID).ToList();
                        if (lstArticle != null)
                        {
                            return lstArticle.Count;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return 0;
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public ArticleViewModel GetArticleById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id">ID ARTICLE</param>
            /// <returns></returns>
            public ArticleViewModel GetArticleContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_ARTICLES
                                    from nc in context.WZ_ARTICLES_CONTENT
                                    where n.ID == id && nc.NID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new ArticleViewModel
                                    {
                                        ID = (int)n.ID,
                                        TITLE = nc.TITLE,
                                        SUB_TITLE = nc.SUB_TITLE,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        THUMBNAIL = nc.IMAGE,
                                        CONTENT = nc.CONTENT,
                                        SLUG = nc.SLUG,
                                        LINK = nc.LINK,
                                        PARENT_ID = (int)n.PARENT_ID,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        TEMPLATE = n.TEMPLATE,
                                        PRIORITY = (int)n.PRIORITY,
                                        FEATURED = (int)n.FEATURED,
                                        MAIN_MENU = (int)n.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public List<int> GetListArticleIdByParentId(int parentId, string lang)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstArticle = from article in context.WZ_ARTICLES
                                         where article.PARENT_ID == parentId
                                         //orderby article.datepost descending
                                         orderby article.CREATED descending
                                         select (int)article.ID;

                        return lstArticle.ToList();
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public ArticleViewModel AdminGetArticalById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = (from a in context.WZ_ARTICLES
                                       where (a.ID == id)
                                       select new ArticleViewModel
                                       {
                                           ID = (int)a.ID,
                                           PARENT_ID = (int)a.PARENT_ID,
                                           TEMPLATE = a.TEMPLATE,
                                           STATUS = (int)a.STATUS,
                                           MODIFIED = a.MODIFIED,
                                           CREATED = a.CREATED,
                                           PRIORITY = (int)a.PRIORITY,
                                           FEATURED = (int)a.FEATURED,
                                           MAIN_MENU = (int)a.MAIN_MENU,
                                           ADMIN_SUBMENU = (int)a.ADMIN_SUBMENU,
                                           YEAR_REPORT = (int)a.YEAR_REPORT,
                                           VIEWS = (int)a.VIEWS
                                       }).FirstOrDefault();

                        if (article != null)
                            article.ContentTranslates = this.GetContentTranslates(article.ID);

                        return article;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(ArticleViewModel article)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_ARTICLES.Where(a => a.ID == article.ID).SingleOrDefault();
                        if (node != null)
                        {
                            node.STATUS = article.STATUS;
                            node.PARENT_ID = article.PARENT_ID;
                            node.TEMPLATE = article.TEMPLATE;
                            node.YEAR_REPORT = article.YEAR_REPORT;
                            node.MAIN_MENU = article.MAIN_MENU;
                            node.ADMIN_SUBMENU = article.ADMIN_SUBMENU;
                            node.PRIORITY = article.PRIORITY;
                            node.FEATURED = article.FEATURED;
                            node.MODIFIED = DateTime.Now;
                            context.SaveChanges();

                            //update content translates
                            if (article.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, ArticleContent> item in article.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == article.ID && a.LANG == content_translate.LANG).SingleOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.TITLE))
                                    {
                                        node_content_lang.TITLE = content_translate.TITLE;
                                        node_content_lang.SUB_TITLE = content_translate.SUB_TITLE;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        node_content_lang.CONTENT = content_translate.CONTENT;
                                        node_content_lang.LINK = content_translate.LINK;
                                        node_content_lang.ATTACHMENT = content_translate.ATTACHMENT;
                                        if ( !string.IsNullOrEmpty(content_translate.IMAGE))
                                        {
                                            node_content_lang.IMAGE = content_translate.IMAGE;
                                        }

                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_ARTICLES.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES
                                     select a;
                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId || a.ID == parentId);
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.TITLE.ToLower().Contains(searchText) || ac.CONTENT.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list article by paging
            public List<ArticleViewModel> GetArticleByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<ArticleViewModel> lstArticlles = new List<ArticleViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_ARTICLES 
                                     from ac in context.WZ_ARTICLES_CONTENT
                                     where ac.NID == a.ID && ac.LANG == lang
                                     select new ArticleViewModel {
                                        ID = (int)a.ID,
                                        PARENT_ID = (int)a.PARENT_ID,
                                        CREATED = a.CREATED,
                                        MODIFIED = a.MODIFIED,
                                        TEMPLATE = a.TEMPLATE,
                                        PRIORITY = (int)a.PRIORITY,
                                        FEATURED = (int)a.FEATURED,
                                        YEAR_REPORT = (int)a.YEAR_REPORT,
                                        TITLE = ac.TITLE,
                                        LINK = ac.LINK,
                                        CONTENT = ac.CONTENT,
                                        THUMBNAIL = ac.IMAGE,
                                        MAIN_MENU = (int)a.MAIN_MENU,
                                        ADMIN_SUBMENU = (int)a.ADMIN_SUBMENU,
                                        STATUS = (int)a.STATUS
                                    };

                        if (parentId >= 0)
                        {
                            result = result.Where(a => a.PARENT_ID == parentId || a.ID == parentId);
                        }
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_ARTICLES_CONTENT
                                     where (a.TITLE.ToLower().Contains(searchText) || a.CONTENT.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        var temp = result.ToList();

                        var lstArticles = from n in temp
                                          from ac in context.WZ_ARTICLES_CONTENT
                                          where n.ID == ac.NID && ac.LANG == lang
                                          select new ArticleViewModel {
                            ID = (int)n.ID,
                            PARENT_ID = (int)n.PARENT_ID,
                            CREATED = n.CREATED,
                            MODIFIED = n.MODIFIED,
                            TEMPLATE = n.TEMPLATE,
                            PRIORITY = (int)n.PRIORITY,
                            FEATURED = (int)n.FEATURED,
                            TITLE = ac.TITLE,
                            THUMBNAIL = ac.IMAGE,
                            MAIN_MENU = (int)n.MAIN_MENU,
                            ADMIN_SUBMENU = (int)n.ADMIN_SUBMENU,
                            STATUS = (int)n.STATUS,
                            ContentTranslates = this.GetContentTranslates((int)n.ID)
                        };

                        return lstArticles.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(ArticleViewModel articleViewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_ARTICLES article = new WZ_ARTICLES();
                        if (articleViewModel != null)
                        {
                            article.ID = this._getNextID();
                            article.PARENT_ID = articleViewModel.PARENT_ID;
                            article.TEMPLATE = articleViewModel.TEMPLATE;
                            article.YEAR_REPORT = articleViewModel.YEAR_REPORT;
                            article.MAIN_MENU = articleViewModel.MAIN_MENU;
                            article.STATUS = articleViewModel.STATUS;
                            article.PRIORITY = articleViewModel.PRIORITY;
                            article.FEATURED = articleViewModel.FEATURED;
                            article.CREATED = DateTime.Now;
                            article.MODIFIED = DateTime.Now;

                            context.WZ_ARTICLES.AddObject(article);
                            context.SaveChanges();

                            if (articleViewModel.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, ArticleContent> item in articleViewModel.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_ARTICLES_CONTENT article_content = new WZ_ARTICLES_CONTENT();
                                    article_content.NID = article.ID;
                                    article_content.TITLE = node_content.TITLE;
                                    article_content.SUB_TITLE = node_content.SUB_TITLE;
                                    article_content.SLUG = node_content.SLUG;
                                    article_content.LINK = node_content.LINK;
                                    article_content.DESCRIPTION = node_content.DESCRIPTION;
                                    article_content.CONTENT = node_content.CONTENT;
                                    article_content.IMAGE = node_content.IMAGE;
                                    article_content.ATTACHMENT = node_content.ATTACHMENT;
                                    article_content.LANG = node_content.LANG;

                                    context.WZ_ARTICLES_CONTENT.AddObject(article_content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)article.ID;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_ARTICLES_SEQ;
                        //    sequence.WZ_ARTICLES_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ARTICLE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ARTICLE";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        var check = context.WZ_ARTICLES.Where(a => a.ID == newID).SingleOrDefault();
                        if (check != null)
                            return this._getNextID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_ARTICLES.Where(a => a.ID == id).SingleOrDefault();

                        if (article != null)
                        {
                            context.WZ_ARTICLES.DeleteObject(article);
                            context.SaveChanges();

                            //delete article content translate
                            var content_translates = context.WZ_ARTICLES_CONTENT.Where(ac=>ac.NID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_ARTICLES_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lstId)
            {
                try
                {
                    if (lstId != null)
                    {
                        foreach (int id in lstId)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //Get list menu option
            public string GetSelectList(int selectedId, int? parentId, int currentId = 0, string space = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string output = string.Empty;
                        string selected = string.Empty;

                        if (parentId == null) parentId = 0;

                        var articles = from a in context.WZ_ARTICLES
                                       from ac in context.WZ_ARTICLES_CONTENT
                                       where ac.NID == a.ID && a.PARENT_ID == parentId && ac.LANG == Constants.LANG_VI && !string.IsNullOrEmpty(ac.TITLE)
                                       orderby a.PRIORITY ascending
                                       orderby a.CREATED descending
                                       select new ArticleViewModel{ 
                                            ID = (int)a.ID,
                                            PARENT_ID = (int)a.PARENT_ID,
                                            TEMPLATE = a.TEMPLATE,
                                            TITLE = ac.TITLE,
                                            LINK = ac.LINK
                                       };
                        if (articles != null)
                        {
                            foreach (var item in articles)
                            {
                                selected = item.ID == selectedId ? "selected" : string.Empty;
                                var sub = item.LINK.Split('/');
                                int countsub = sub.Count();

                                switch (countsub)
                                {
                                    case 1:
                                        space = "";
                                        break;
                                    case 2:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ ";
                                        break;
                                    case 3:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ";
                                        break;
                                    case 4:
                                        space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* ";
                                        break;
                                }

                                //De qui lai option list
                                string title = item.TITLE;
                                if (currentId == item.ID)
                                    output += "<option value='" + item.ID + "' " + selected + " disabled='disabled' >" + space + title + "</option>";
                                else
                                    output += "<option value='" + item.ID + "' " + selected + " >" + space + title + "</option>";

                                output += this.GetSelectList(selectedId, item.ID, currentId, space);

                                

                                //output += this.GetSelectList(selectedId, item.id, space);
                            }
                        }
                        return output;
                    }
                }
                catch (Exception ex)
                {

                }
                return string.Empty;
            }

            /// <summary>
            /// Check if article slug already exist following language
            /// </summary>
            /// <param name="slug">Input slug</param>
            /// <param name="lang">Input language</param>
            /// <returns>True is exist</returns>
            public bool CheckSlug(string slug, string lang, int parent = 0)
            {
                try
                {
                    lang = string.IsNullOrEmpty(lang) ? "vi" : lang;
                    using (var context = new DBContext())
                    {
                        if (parent > 0)
                        {
                            var node = (from a in context.WZ_ARTICLES
                                        from ac in context.WZ_ARTICLES_CONTENT
                                        where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug && ac.NID == parent
                                        select a).SingleOrDefault();
                            if (node != null) return true;
                        }
                        else
                        {
                            var node = (from a in context.WZ_ARTICLES
                                        from ac in context.WZ_ARTICLES_CONTENT
                                        where ac.NID == a.ID && ac.LANG == lang && ac.SLUG == slug
                                        select a).SingleOrDefault();
                            if (node != null) return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            public void UpdateLinkCustom(int parentId)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var parent = this.GetArticleById(parentId);
                        if (parent.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in parent.ContentTranslates)
                            {
                                var parent_content = item.Value;
                                this.UpdateMenuLink(parentId, parent_content.LINK, parent_content.LANG);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }

            //update menu link
            public void UpdateMenuLink(int parentId, string parentLink = "", string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var articles = context.WZ_ARTICLES.Where(a => a.PARENT_ID == parentId).ToList();
                        if (articles != null)
                        {
                            foreach (var item in articles)
                            {
                                int currentId = (int)item.ID;

                                var nodeContent = (from nc in context.WZ_ARTICLES_CONTENT where nc.NID == item.ID && nc.LANG == lang select nc).SingleOrDefault();

                                if (nodeContent == null) continue;

                                string newLink = nodeContent.SLUG;

                                if (parentLink != string.Empty)
                                {
                                    newLink = parentLink + "/" + newLink;
                                }

                                //Neu thay doi link thi cap nhat lai link cua tat ca cac node con
                                if (newLink != nodeContent.LINK)
                                {
                                    this.UpdateLink(newLink, lang, currentId);
                                    this.UpdateMenuLink(currentId, newLink, lang);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { }
            }

            private void UpdateLink(string link, string lang, int nid)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var ar = context.WZ_ARTICLES_CONTENT.Where(a => a.NID == nid && a.LANG == lang).SingleOrDefault();
                        if (ar != null)
                        {
                            ar.LINK = link;
                            //using (TransactionScope scrope = new TransactionScope())
                            //{
                                context.SaveChanges();
                                return;
                            //}
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            #endregion
        }
    }
}