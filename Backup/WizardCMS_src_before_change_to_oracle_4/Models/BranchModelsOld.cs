﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Models
{
    public class BranchViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string TEL { get; set; }
        public string FAX { get; set; }
        public string LAT_X { get; set; }
        public string LAT_Y { get; set; }
        public string LANG { get; set; }
        public string IMAGE { get; set; }
        public string PROVINCE_ID { get; set; }
        public string PROVINCE_NAME { get; set; }
        public string DISTRICT_ID { get; set; }
        public string DISTRICT_NAME { get; set; }
        public int REGION { get; set; }
        public int PRIORITY { get; set; }
        public int FEATURED { get; set; }
        public int STATUS { get; set; }
        public int GROUP_ID { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class BranchServices
    {
        #region service interface
        public static BranchViewModel Get(int Id)
        {
            try
            {
                return BrandDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(BranchViewModel slideViewModel)
        {
            try
            {
                return BrandDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return BrandDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(BranchViewModel slideViewModel)
        {
            try
            {
                return BrandDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return BrandDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return BrandDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchViewModel> GetList()
        {
            try
            {
                return BrandDAL.Instance.GetList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<BranchViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return BrandDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchViewModel> GetBranchsByGroup(int groupID)
        {
            try
            {
                return BrandDAL.Instance.GetBranchsByGroup(groupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_PROVINCE> GetProvinces()
        {
            try
            {
                return BrandDAL.Instance.GetProvinces();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_DISTRICT> GetDistricts(string PROVINCE_ID)
        {
            try
            {
                return BrandDAL.Instance.GetDistricts(PROVINCE_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_WARD> GetWards(string DISTRICT_ID)
        {
            try
            {
                return BrandDAL.Instance.GetWards(DISTRICT_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class BrandDAL
        {
            #region static instance
            private static BrandDAL _instance;

            static public BrandDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new BrandDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public BranchViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_BRANCH
                                from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                where a.ID == Id
                                select new BranchViewModel
                                {
                                    ID = (int)a.ID,
                                    NAME = a.NAME,
                                    DESCRIPTION = a.DESCRIPTION,
                                    TEL = a.TEL,
                                    FAX = a.FAX,
                                    LAT_X = a.LAT_X,
                                    LAT_Y = a.LAT_Y,
                                    LANG = a.LANG,
                                    IMAGE = a.IMAGE,
                                    PROVINCE_ID = a.PROVINCE_ID,
                                    PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                    DISTRICT_ID = a.DISTRICT_ID,
                                    DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                    STATUS = (int)a.STATUS,
                                    PRIORITY = (int)a.PRIORITY,
                                    REGION = (int)a.REGION,
                                    FEATURED = (int)a.FEATURED,
                                    GROUP_ID = (int)a.GROUP_ID,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(BranchViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_BRANCH.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.NAME = viewModel.NAME;
                        node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.TEL = viewModel.TEL;
                        node.FAX = viewModel.FAX;
                        node.LAT_X = viewModel.LAT_X;
                        node.LAT_Y = viewModel.LAT_Y;
                        node.LANG = viewModel.LANG;
                        node.PROVINCE_ID = viewModel.PROVINCE_ID;
                        node.DISTRICT_ID = viewModel.DISTRICT_ID;
                        node.GROUP_ID = viewModel.GROUP_ID;
                        node.IMAGE = viewModel.IMAGE;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(BranchViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_BRANCH node = new WZ_BRANCH();
                        node.ID = this._getNextID();
                        node.NAME = viewModel.NAME;
                        node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.TEL = viewModel.TEL;
                        node.FAX = viewModel.FAX;
                        node.LAT_X = viewModel.LAT_X;
                        node.LAT_Y = viewModel.LAT_Y;
                        node.LANG = viewModel.LANG;
                        node.IMAGE = viewModel.IMAGE;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.PROVINCE_ID = viewModel.PROVINCE_ID;
                        node.DISTRICT_ID = viewModel.DISTRICT_ID;
                        node.GROUP_ID = viewModel.GROUP_ID;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;
                        node.CREATED = DateTime.Now;

                        context.WZ_BRANCH.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_BRANCH.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.NAME.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            public List<BranchViewModel> GetList()
            {
                try
                {
                    List<BranchViewModel> lsBranchViewModels = new List<BranchViewModel>();
                    string lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     //where a.LANG == lang
                                     orderby a.LANG descending, a.NAME
                                     select new BranchViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME = a.NAME,
                                         DESCRIPTION = a.DESCRIPTION,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         IMAGE = a.IMAGE,
                                         LANG = a.LANG,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //get list article by paging
            public List<BranchViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<BranchViewModel> lsBranchViewModels = new List<BranchViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     select new BranchViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME  = a.NAME,
                                         DESCRIPTION = a.DESCRIPTION,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         IMAGE = a.IMAGE,
                                         LANG = a.LANG,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.NAME.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<BranchViewModel> GetBranchsByGroup(int groupID)
            {
                try
                {
                    List<BranchViewModel> lsBranchViewModels = new List<BranchViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     where a.GROUP_ID == groupID
                                     orderby a.PRIORITY ascending, a.CREATED descending
                                     select new BranchViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME = a.NAME,
                                         DESCRIPTION = a.DESCRIPTION,
                                         TEL = a.TEL,
                                         FAX = a.FAX,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         IMAGE = a.IMAGE,
                                         LANG = a.LANG,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }
            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "BRANCH" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "BRANCH";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        var check = context.WZ_BRANCH.Where(a=>a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }

            public List<WZ_CT_PROVINCE> GetProvinces(int region = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_PROVINCE orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_DISTRICT> GetDistricts(string PROVINCE_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_DISTRICT where d.PROVINCE_ID == PROVINCE_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_WARD> GetWards(string DISTRICT_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_WARD where d.DISTRICT_ID == DISTRICT_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }
            #endregion
        }
        #endregion
    }
}