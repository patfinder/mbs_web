﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class BranchFooterViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string LINK { get; set; }
        public string TEL { get; set; }
        public string FAX { get; set; }
        public string LAT_X { get; set; }
        public string LAT_Y { get; set; }
        public string LANG { get; set; }
        public string IMAGE { get; set; }
        public string PROVINCE_ID { get; set; }
        public string PROVINCE_NAME { get; set; }
        public string DISTRICT_ID { get; set; }
        public string DISTRICT_NAME { get; set; }
        public int REGION { get; set; }
        public int PRIORITY { get; set; }
        public int FEATURED { get; set; }
        public int STATUS { get; set; }
        public int GROUP_ID { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }

        public Dictionary<string, BranchFooterContent> ContentTranslates { get; set; }

        //Constructor
        public BranchFooterViewModel()
        {
            this.ContentTranslates = new Dictionary<string, BranchFooterContent>();
        }
    }

    public class BranchFooterContent
    {
        public int NID { get; set; }
        public string LANG { get; set; }
        public string NAME { get; set; }
        public string SLUG { get; set; }
        public string LINK { get; set; }
        public string DESCRIPTION { get; set; }

        public BranchFooterContent()
        { }
    }

    public static class BranchFooterServices
    {
        #region service interface
        public static BranchFooterViewModel Get(int Id)
        {
            try
            {
                return BranchFooterDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(BranchFooterViewModel slideViewModel)
        {
            try
            {
                return BranchFooterDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return BranchFooterDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(BranchFooterViewModel slideViewModel)
        {
            try
            {
                return BranchFooterDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return BranchFooterDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return BranchFooterDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchFooterViewModel> GetList()
        {
            try
            {
                return BranchFooterDAL.Instance.GetList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<BranchFooterViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return BranchFooterDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchFooterViewModel> GetBranchsByGroup(int groupID, int status = -1)
        {
            try
            {
                return BranchFooterDAL.Instance.GetBranchsByGroup(groupID, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BranchFooterViewModel> GetBranchsByRegion(int regionID, int status = -1)
        {
            try
            {
                return BranchFooterDAL.Instance.GetBranchsByRegion(regionID, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_PROVINCE> GetProvinces()
        {
            try
            {
                return BranchFooterDAL.Instance.GetProvinces();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_DISTRICT> GetDistricts(string PROVINCE_ID)
        {
            try
            {
                return BranchFooterDAL.Instance.GetDistricts(PROVINCE_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WZ_CT_WARD> GetWards(string DISTRICT_ID)
        {
            try
            {
                return BranchFooterDAL.Instance.GetWards(DISTRICT_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static BranchFooterContent GetContentByLang(Dictionary<string, BranchFooterContent> lstFormContent, string lang = "vi")
        {
            try
            {
                if (lstFormContent != null && lstFormContent.ContainsKey(lang))
                {
                    return lstFormContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        public static Dictionary<string, BranchFooterContent> GetContentTranslates(int NID)
        {
            try
            {
                return BranchFooterDAL.Instance.GetContentTranslates(NID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class BranchFooterDAL
        {
            #region static instance
            private static BranchFooterDAL _instance;

            static public BranchFooterDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new BranchFooterDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public Dictionary<string, BranchFooterContent> GetContentTranslates(int NID)
            {
                try
                {
                    Dictionary<string, BranchFooterContent> result = new Dictionary<string, BranchFooterContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_BRANCH_CONTENT_FOOTER
                                             where ct.NID == NID
                                             orderby ct.LANG descending
                                             select new BranchFooterContent
                                             {
                                                 NID = (int)ct.NID,
                                                 NAME = ct.NAME,
                                                 LINK = ct.LINK,
                                                 DESCRIPTION = ct.DESCRIPTION,
                                                 SLUG = ct.SLUG,
                                                 LANG = ct.LANG,
                                             }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            public BranchFooterViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new DBContext())
                    {
                        var result = (from a in context.WZ_BRANCH_FOOTER
                                    from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                    from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                    from ac in context.WZ_BRANCH_CONTENT_FOOTER.Where(ac=>a.ID == ac.NID).DefaultIfEmpty()
                                    where a.ID == Id
                                    select new BranchFooterViewModel
                                    {
                                        ID = (int)a.ID,
                                        LINK = ac != null ? ac.LINK : string.Empty,
                                        NAME = ac != null ? ac.NAME : string.Empty,
                                        DESCRIPTION = ac != null ? ac.DESCRIPTION : string.Empty,
                                        TEL = a.TEL,
                                        FAX = a.FAX,
                                        LAT_X = a.LAT_X,
                                        LAT_Y = a.LAT_Y,
                                        LANG = ac.LANG,
                                        PROVINCE_ID = a.PROVINCE_ID,
                                        PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                        DISTRICT_ID = a.DISTRICT_ID,
                                        DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                        STATUS = (int)a.STATUS,
                                        PRIORITY = (int)a.PRIORITY,
                                        REGION = (int)a.REGION,
                                        FEATURED = (int)a.FEATURED,
                                        GROUP_ID = (int)a.GROUP_ID,
                                        MODIFIED = a.MODIFIED,
                                        CREATED = a.CREATED
                                    }).FirstOrDefault();

                        if (result != null) {
                            result.ContentTranslates = this.GetContentTranslates(result.ID);
                        }
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(BranchFooterViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_BRANCH_FOOTER.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        //node.NAME = viewModel.NAME;
                        //node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.TEL = viewModel.TEL;
                        node.FAX = viewModel.FAX;
                        node.LAT_X = viewModel.LAT_X;
                        node.LAT_Y = viewModel.LAT_Y;
                        //node.LANG = viewModel.LANG;
                        node.PROVINCE_ID = viewModel.PROVINCE_ID;
                        node.DISTRICT_ID = viewModel.DISTRICT_ID;
                        node.GROUP_ID = viewModel.GROUP_ID;
                        node.REGION = viewModel.REGION;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;

                        context.SaveChanges();

                        if (viewModel.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, BranchFooterContent> item in viewModel.ContentTranslates)
                            {
                                var content_translate = item.Value;
                                var node_content_lang = context.WZ_BRANCH_CONTENT_FOOTER.Where(a => a.NID == viewModel.ID && a.LANG == content_translate.LANG).FirstOrDefault();
                                if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.NAME))
                                {
                                    node_content_lang.NAME = content_translate.NAME;
                                    node_content_lang.SLUG = content_translate.SLUG;
                                    node_content_lang.LINK = content_translate.LINK;
                                    node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;

                                    context.SaveChanges();
                                }
                                else
                                {
                                    node_content_lang = new WZ_BRANCH_CONTENT_FOOTER();
                                    node_content_lang.NID = viewModel.ID;
                                    node_content_lang.LANG = content_translate.LANG;
                                    node_content_lang.NAME = content_translate.NAME;
                                    node_content_lang.SLUG = content_translate.SLUG;
                                    node_content_lang.LINK = content_translate.LINK;
                                    node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                    context.WZ_BRANCH_CONTENT_FOOTER.AddObject(node_content_lang);
                                    context.SaveChanges();
                                }
                            }
                        }

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(BranchFooterViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_BRANCH_FOOTER node = new WZ_BRANCH_FOOTER();
                        node.ID = this._getNextID();
                        //node.NAME = viewModel.NAME;
                        //node.DESCRIPTION = viewModel.DESCRIPTION;
                        node.TEL = viewModel.TEL;
                        node.FAX = viewModel.FAX;
                        node.LAT_X = viewModel.LAT_X;
                        node.LAT_Y = viewModel.LAT_Y;
                        //node.LANG = viewModel.LANG;
                        node.PRIORITY = viewModel.PRIORITY;
                        node.PROVINCE_ID = viewModel.PROVINCE_ID;
                        node.DISTRICT_ID = viewModel.DISTRICT_ID;
                        node.GROUP_ID = viewModel.GROUP_ID;
                        node.REGION = viewModel.REGION;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;
                        node.CREATED = DateTime.Now;

                        context.WZ_BRANCH_FOOTER.AddObject(node);
                        context.SaveChanges();

                        if (viewModel.ContentTranslates.Count > 0)
                        {
                            foreach (KeyValuePair<string, BranchFooterContent> item in viewModel.ContentTranslates)
                            {
                                var node_content = item.Value;
                                WZ_BRANCH_CONTENT_FOOTER content = new WZ_BRANCH_CONTENT_FOOTER();
                                content.NID = node.ID;
                                content.LINK = node_content.LINK;
                                content.NAME = node_content.NAME;
                                content.SLUG = node_content.SLUG;
                                content.DESCRIPTION = node_content.DESCRIPTION;
                                content.LANG = node_content.LANG;
                                context.WZ_BRANCH_CONTENT_FOOTER.AddObject(content);
                                context.SaveChanges();
                            }
                        }

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH_FOOTER.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_BRANCH_FOOTER.DeleteObject(node);
                        context.SaveChanges();

                        var content_translates = context.WZ_BRANCH_CONTENT_FOOTER.Where(ac => ac.NID == ID).ToList();
                        if (content_translates != null)
                        {
                            foreach (var node_translate in content_translates)
                            {
                                context.WZ_BRANCH_CONTENT_FOOTER.DeleteObject(node_translate);
                                context.SaveChanges();
                            }
                        }

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_BRANCH_FOOTER.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_FOOTER
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_BRANCH_CONTENT_FOOTER
                                     where a.ID == ac.NID && ac.LANG == lang && (ac.NAME.ToLower().Contains(searchText) || ac.NAME.ToLower().Contains(searchText))
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            public List<BranchFooterViewModel> GetList()
            {
                try
                {
                    List<BranchFooterViewModel> lsBranchViewModels = new List<BranchFooterViewModel>();
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_FOOTER
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     from ac in context.WZ_BRANCH_CONTENT_FOOTER.Where(ac => a.ID == ac.NID).DefaultIfEmpty()
                                     orderby a.PRIORITY ascending, a.CREATED descending
                                     select new BranchFooterViewModel
                                     {
                                         ID = (int)a.ID,
                                         LINK = ac != null ? ac.LINK : string.Empty,
                                         NAME = ac != null ? ac.NAME : string.Empty,
                                         DESCRIPTION = ac != null ? ac.DESCRIPTION : string.Empty,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         LANG = ac.LANG,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //get list article by paging
            public List<BranchFooterViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<BranchFooterViewModel> lsBranchViewModels = new List<BranchFooterViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_FOOTER
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     from ac in context.WZ_BRANCH_CONTENT_FOOTER.Where(ac=>a.ID==ac.NID && ac.LANG == lang).DefaultIfEmpty()
                                     select new BranchFooterViewModel
                                     {
                                         ID = (int)a.ID,
                                         LINK = ac != null ? ac.LINK : string.Empty,
                                         NAME = ac != null ? ac.NAME : string.Empty,
                                         DESCRIPTION = ac != null ? ac.DESCRIPTION : string.Empty,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         LANG = ac != null ? ac.LANG : string.Empty,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.NAME.ToLower().Contains(searchText) || a.DESCRIPTION.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<BranchFooterViewModel> GetBranchsByGroup(int groupID, int status = -1)
            {
                try
                {
                    List<BranchFooterViewModel> lsBranchViewModels = new List<BranchFooterViewModel>();
                    string lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_FOOTER
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     from ac in context.WZ_BRANCH_CONTENT_FOOTER.Where(ac => a.ID == ac.NID && ac.LANG == lang).DefaultIfEmpty()
                                     where a.GROUP_ID == groupID
                                     orderby a.PRIORITY ascending, a.CREATED descending
                                     select new BranchFooterViewModel
                                     {
                                         ID = (int)a.ID,
                                         LINK = ac != null ? ac.LINK : string.Empty,
                                         NAME = ac != null ? ac.NAME : string.Empty,
                                         DESCRIPTION = ac != null ? ac.DESCRIPTION : string.Empty,
                                         TEL = a.TEL,
                                         FAX = a.FAX,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         LANG = ac != null ? ac.LANG : string.Empty,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (status >= 0) 
                        {
                            result = result.Where(a => a.STATUS == status);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<BranchFooterViewModel> GetBranchsByRegion(int regionID, int status = -1)
            {
                try
                {
                    List<BranchFooterViewModel> lsBranchViewModels = new List<BranchFooterViewModel>();
                    string lang = StringHelper.language();
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_BRANCH_FOOTER
                                     from p in context.WZ_CT_PROVINCE.Where(p => p.PROVINCE_ID == a.PROVINCE_ID).DefaultIfEmpty()
                                     from d in context.WZ_CT_DISTRICT.Where(d => d.DISTRICT_ID == a.DISTRICT_ID).DefaultIfEmpty()
                                     from ac in context.WZ_BRANCH_CONTENT_FOOTER.Where(ac => a.ID == ac.NID && ac.LANG == lang).DefaultIfEmpty()
                                     where a.REGION == regionID && a.GROUP_ID != Constants.GROUP_HOISO
                                     orderby a.PRIORITY ascending, a.CREATED descending
                                     select new BranchFooterViewModel
                                     {
                                         ID = (int)a.ID,
                                         LINK = ac != null ? ac.LINK : string.Empty,
                                         NAME = ac != null ? ac.NAME : string.Empty,
                                         DESCRIPTION = ac != null ? ac.DESCRIPTION : string.Empty,
                                         TEL = a.TEL,
                                         FAX = a.FAX,
                                         LAT_X = a.LAT_X,
                                         LAT_Y = a.LAT_Y,
                                         LANG = ac != null ? ac.LANG : string.Empty,
                                         PROVINCE_ID = a.PROVINCE_ID,
                                         PROVINCE_NAME = p != null ? p.PROVINCE_NAME : string.Empty,
                                         DISTRICT_ID = a.DISTRICT_ID,
                                         DISTRICT_NAME = d != null ? d.DISTRICT_NAME : string.Empty,
                                         PRIORITY = (int)a.PRIORITY,
                                         GROUP_ID = (int)a.GROUP_ID,
                                         REGION = (int)a.REGION,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (status >= 0)
                        {
                            result = result.Where(a => a.STATUS == status);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "BRANCH_FOOTER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "BRANCH_FOOTER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        var check = context.WZ_BRANCH_FOOTER.Where(a=>a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }

            public List<WZ_CT_PROVINCE> GetProvinces(int region = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_PROVINCE orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_DISTRICT> GetDistricts(string PROVINCE_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_DISTRICT where d.PROVINCE_ID == PROVINCE_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public List<WZ_CT_WARD> GetWards(string DISTRICT_ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from d in context.WZ_CT_WARD where d.DISTRICT_ID == DISTRICT_ID orderby d.PRIORITY descending orderby d.SLUG ascending select d).ToList();
                        return result;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }
            #endregion
        }
        #endregion
    }
}