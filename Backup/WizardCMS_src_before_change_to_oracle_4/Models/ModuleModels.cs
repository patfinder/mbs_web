﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Models
{
    #region Module View Model
    public class ModuleModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string NAME_FUNCTION { get; set; }
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public DateTime CREATED { get; set; }
        public bool IsShow { get; set; }
    }
    #endregion

    public static class ModuleServices
    {
        #region Modules services interface
        public static List<ModuleModel> GetListModuleActive()
        {
            try
            {
                return ModuleModels.Instance.GetListModuleActive();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateInstallModule(ModuleModel module)
        {
            try
            {
                return ModuleModels.Instance.UpdateInstallModule(module);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ModuleModel> GetAllModules()
        {
            try
            {
                return ModuleModels.Instance.GetAllListModule();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ModuleModel GetModuleById(int id)
        {
            try
            {
                return ModuleModels.Instance.GetModuleById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ModuleModel GetModuleActiveById(int id)
        {
            try
            {
                return ModuleModels.Instance.GetModuleActiveById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ModuleModel GetModuleByFunction(string functionName)
        {
            try
            {
                return ModuleModels.Instance.GetModuleByFunction(functionName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount()
        {
            try
            {
                return ModuleModels.Instance.GetCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ModuleModel> GetListModuleByPaging(Models.Paging paging, string sortBy, string sortType)
        {
            try
            {
                return ModuleModels.Instance.GetListModuleByPaging(paging, sortBy, sortType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ModuleModel> GetListModuleByGroupId(int grpId)
        {
            try
            {
                return ModuleModels.Instance.GetListModuleByGroupId(grpId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ModuleModel> GetListModuleByAccountId(int USER_ID, int GROUP_ID)
        {
            try
            {
                return ModuleModels.Instance.GetListModuleByAccountId(USER_ID, GROUP_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckNameBeforeInsert(string name)
        {
            try
            {
                return ModuleModels.Instance.CheckNameBeforeInsert(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckNameBeforeUpdate(ModuleModel module)
        {
            try
            {
                return ModuleModels.Instance.CheckNameBeforeUpdate(module);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class ModuleModels
        {
            private static ModuleModels _instance = null;

            #region constructor
            static public ModuleModels Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new ModuleModels();
                    return _instance;
                }
            }

            #endregion

            #region Constructor
            public ModuleModels()
                : base()
            {
            }
            #endregion

            #region get data method
            public List<ModuleModel> GetListModuleActive()
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = from m in context.ADMIN_WZ_MODULES
                                     where m.STATUS == Constants.SHOW
                                     orderby m.PRIORITY ascending, m.NAME ascending
                                     select new ModuleModel { 
                                        ID = (int)m.ID,
                                        NAME = m.NAME,
                                        NAME_FUNCTION = m.NAME_FUNCTION,
                                        STATUS = (int)m.STATUS,
                                        PRIORITY = (int)m.PRIORITY,
                                        CREATED = m.CREATED
                                     };
                        return result.ToList();
                    }

                }
                catch (Exception)
                {
                }
                return null;
            }

            public int UpdateInstallModule(ModuleModel moduleModel)
            {
                try
                {
                    if (!this.CheckNameBeforeUpdate(moduleModel))
                    {
                        return Constants.ACTION_EXIST_NAME;
                    }
                    using (DBContext context = new DBContext())
                    {
                        ADMIN_WZ_MODULES module = context.ADMIN_WZ_MODULES.FirstOrDefault(m => m.ID == moduleModel.ID);
                        if (string.IsNullOrEmpty(moduleModel.NAME) == false)
                        {
                            module.NAME = moduleModel.NAME;
                        }
                        module.STATUS = moduleModel.STATUS;
                        module.PRIORITY = moduleModel.PRIORITY;
                        context.SaveChanges();
                        return Constants.ACTION_SUCCESS;
                    }
                }
                catch (Exception)
                {
                }
                return Constants.ACTION_FAULT;
            }

            public List<ModuleModel> GetAllListModule()
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = from m in context.ADMIN_WZ_MODULES
                                     orderby m.PRIORITY ascending, m.NAME ascending
                                     select new ModuleModel
                                     {
                                         ID = (int)m.ID,
                                         NAME = m.NAME,
                                         NAME_FUNCTION = m.NAME_FUNCTION,
                                         STATUS = (int)m.STATUS,
                                         PRIORITY = (int)m.PRIORITY,
                                         CREATED = m.CREATED
                                     };
                        return result.ToList();
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            public ModuleModel GetModuleById(int id)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = (from m in context.ADMIN_WZ_MODULES
                                      where m.ID == id
                                      select new ModuleModel
                                      {
                                          ID = (int)m.ID,
                                          NAME = m.NAME,
                                          NAME_FUNCTION = m.NAME_FUNCTION,
                                          STATUS = (int)m.STATUS,
                                          PRIORITY = (int)m.PRIORITY,
                                          CREATED = m.CREATED
                                      }).FirstOrDefault();
                        return result;
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            public ModuleModel GetModuleActiveById(int id)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = (from m in context.ADMIN_WZ_MODULES
                                      where m.ID == id
                                      && m.STATUS == Constants.SHOW
                                      select new ModuleModel
                                      {
                                          ID = (int)m.ID,
                                          NAME = m.NAME,
                                          NAME_FUNCTION = m.NAME_FUNCTION,
                                          STATUS = (int)m.STATUS,
                                          PRIORITY = (int)m.PRIORITY,
                                          CREATED = m.CREATED
                                      }).FirstOrDefault();
                        return result;
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            public ModuleModel GetModuleByFunction(string functionName)
            {
                try
                {
                    functionName = functionName.ToLower();
                    using (DBContext context = new DBContext())
                    {
                        var result = (from m in context.ADMIN_WZ_MODULES
                                      where m.NAME_FUNCTION.Trim().Equals(functionName.Trim())
                                      select new ModuleModel
                                      {
                                          ID = (int)m.ID,
                                          NAME = m.NAME,
                                          NAME_FUNCTION = m.NAME_FUNCTION,
                                          STATUS = (int)m.STATUS,
                                          PRIORITY = (int)m.PRIORITY,
                                          CREATED = m.CREATED
                                      }).FirstOrDefault();
                        return result;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }

            public int GetCount()
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = from m in context.ADMIN_WZ_MODULES
                                     where Constants.LST_MODULES_INVISBLE.Contains((int)m.ID) == false
                                     select m;
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<ModuleModel> GetListModuleByPaging(Models.Paging paging, string sortBy, string sortType)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = (from m in context.ADMIN_WZ_MODULES
                                      where Constants.LST_MODULES_INVISBLE.Contains((int)m.ID) == false
                                      orderby m.NAME ascending
                                      select new ModuleModel
                                      {
                                          ID = (int)m.ID,
                                          NAME = m.NAME,
                                          NAME_FUNCTION = m.NAME_FUNCTION,
                                          STATUS = (int)m.STATUS,
                                          PRIORITY = (int)m.PRIORITY,
                                          CREATED = m.CREATED,
                                          IsShow = true
                                      });
                        if (string.IsNullOrEmpty(sortType)) sortType = Constants.SORT_ASC;
                        result = result.OrderBy(sortBy + " " + sortType);

                        result = result.Skip(paging.Offset).Take(paging.Perpage);
                        return result.ToList();
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            /// <summary>
            /// Get list module by group id
            /// not containts default module
            /// </summary>
            /// <param name="grpId"></param>
            /// <returns></returns>
            public List<ModuleModel> GetListModuleByGroupId(int grpId)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        List<ModuleModel> lst = new List<ModuleModel>();
                        string perm = context.ADMIN_WZ_GROUPS.Where(g => g.ID == grpId).Select(m => m.PERMISSION).FirstOrDefault();
                        if (perm != string.Empty)
                        {
                            string[] lstModule = perm.Split(',');
                            for (int i = 0; i < lstModule.Count(); i++)
                            {
                                int pos = lstModule[i].IndexOf("|");
                                if (pos > 0)
                                {
                                    int idModule = Convert.ToInt32(lstModule[i].Substring(0, pos));
                                    if (grpId == 1)
                                    {
                                        if (idModule != 0)
                                        {
                                            ModuleModel mDto = GetModuleActiveById(idModule);
                                            if (mDto != null)
                                                lst.Add(mDto);
                                        }
                                    }
                                    else
                                    {
                                        if (Constants.LST_MODULES_INVISBLE.Contains(idModule) == false)
                                        {
                                            if (idModule != 0)
                                            {
                                                ModuleModel mDto = GetModuleActiveById(idModule);
                                                lst.Add(mDto);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return lst;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public List<ModuleModel> GetListModuleByAccountId(int USER_ID, int GROUP_ID)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        List<ModuleModel> lst = new List<ModuleModel>();
                        //get permission by acc login
                        string perm = context.ADMIN_WZ_USERS.Where(a => a.ID == USER_ID).Select(m => m.PERMISSION).FirstOrDefault();
                        if (perm != string.Empty)
                        {
                            string[] lstModule = perm.Split(',');
                            for (int i = 0; i < lstModule.Count(); i++)
                            {
                                int pos = lstModule[i].IndexOf("|");
                                if (pos > 0)
                                {
                                    int idModule = Convert.ToInt32(lstModule[i].Substring(0, pos));
                                    //If group is admin, display module account, account_group, module and logs
                                    if (GROUP_ID == 1)
                                    {
                                        if (idModule != 0)
                                        {
                                            ModuleModel mDto = GetModuleActiveById(idModule);
                                            if (mDto != null)
                                                lst.Add(mDto);
                                        }
                                    }
                                    //else not display module account, account_group, module and log
                                    else
                                    {
                                        if (Constants.LST_MODULES_INVISBLE.Contains(idModule) == false)
                                        {
                                            if (idModule != 0)
                                            {
                                                ModuleModel mDto = GetModuleActiveById(idModule);
                                                lst.Add(mDto);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        return lst;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public bool CheckNameBeforeInsert(string name)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = from m in context.ADMIN_WZ_MODULES
                                     where m.NAME == name
                                     select m;
                        if (result.Count() > 0)
                            return false;
                        return true;
                    }
                }
                catch (Exception)
                {
                }
                return true;
            }

            public bool CheckNameBeforeUpdate(ModuleModel module)
            {
                try
                {
                    using (DBContext context = new DBContext())
                    {
                        var result = from m in context.ADMIN_WZ_MODULES
                                     where m.NAME == module.NAME && m.ID != module.ID
                                     select m;
                        if (result.Count() > 0)
                            return false;
                        return true;
                    }
                }
                catch (Exception)
                {
                }
                return true;
            }
            #endregion
        }
    }
}