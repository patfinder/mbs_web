﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Helpers;
using System.Reflection;
using System.Linq.Dynamic;

namespace WizardCMS.Models
{
    public class AccountGroupModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string PERMISSION { get; set; }
        public int STATUS { get; set; }
        public DateTime CREATED { get; set; }
    }
    public static class AccountGroupServices
    {
        #region Account Group View Model
        #endregion
        #region Account group services interface
        public static AccountGroupModel GetGroupById(int id)
        {
            try
            {
                return AccountGroupModels.Instance.GetGroupById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateAccountGroup(AccountGroupModel group) 
        {
            try
            {
                return AccountGroupModels.Instance.UpdateAccountGroup(group);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int AddNewAccountGroup(AccountGroupModel group) 
        {
            try
            {
                return AccountGroupModels.Instance.AddNewAccountGroup(group);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteAccountGroup(AccountGroupModel group)
        {
            try
            {
                return AccountGroupModels.Instance.DeleteAccountGroup(group);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountListAccountGroupByPerpage(string dateFrom, string dateTo, string searchText)
        {
            try
            {
                return AccountGroupModels.Instance.CountListAccountGroupByPerpage(dateFrom, dateTo, searchText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AccountGroupModel> GetListAccountGroupByCondition(string dateFrom, string dateTo, string searchText, Models.Paging paging)
        {
            try
            {
                return AccountGroupModels.Instance.GetListAccountGroupByCondition(dateFrom, dateTo, searchText, paging);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AccountGroupModel> GetSortListAccountGroupByCondition(string dateFrom, string dateTo, string searchText, Models.Paging paging, string sortBy, string typeSort)
        {
            try
            {
                return AccountGroupModels.Instance.GetSortListAccountGroupByCondition(dateFrom, dateTo, searchText, paging, sortBy, typeSort);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckNameBeforeInsert(string name)
        {
            try
            {
                return AccountGroupModels.Instance.CheckNameBeforeInsert(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckNameBeforeUpdate(AccountGroupModel node)
        {
            try
            {
                return AccountGroupModels.Instance.CheckNameBeforeUpdate(node);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AdminAccountModel> GetAccountsByIdGroup(int idGroup)
        {
            try
            {
                return AccountGroupModels.Instance.GetAccountsByIdGroup(idGroup);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateStatustByArrayId(List<int> listId, int status)
        {
            try
            {
                AccountGroupModels.Instance.UpdateStatustByArrayId(listId, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteGroupByArrayId(List<int> listId)
        {
            try
            {
                AccountGroupModels.Instance.DeleteGroupByArrayId(listId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class AccountGroupModels
        {
            private static AccountGroupModels _instance = null;

            #region constructor
            static public AccountGroupModels Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new AccountGroupModels();
                    return _instance;
                }
            }

            #endregion

            #region Constructor
            public AccountGroupModels()
                : base()
            {
            }
            #endregion

            #region get data function
            public AccountGroupModel GetGroupById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = (from node in context.ADMIN_WZ_GROUPS
                                      where node.ID == id
                                      select new AccountGroupModel { 
                                        ID          = (int)node.ID,
                                        NAME        = node.NAME,
                                        PERMISSION  = node.PERMISSION,
                                        STATUS      = (int)node.STATUS,
                                        CREATED     = node.CREATED
                                      }).FirstOrDefault();
                        return result;
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            public int UpdateAccountGroup(AccountGroupModel group)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (this.CheckNameBeforeUpdate(group) == true)
                        {
                            return Const.Constants.ACTION_EXIST_NAME;
                        }
                        var acc = context.ADMIN_WZ_GROUPS.FirstOrDefault(a => a.ID == group.ID);
                        acc.NAME        = group.NAME;
                        acc.PERMISSION  = group.PERMISSION;
                        acc.STATUS      = group.STATUS;

                        context.SaveChanges();
                        return Const.Constants.ACTION_SUCCESS;
                    }
                }
                catch (Exception)
                {
                }
                return Const.Constants.ACTION_FAULT;
            }

            public int AddNewAccountGroup(AccountGroupModel group)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        if (this.CheckNameBeforeInsert(group.NAME))
                        {
                            return Const.Constants.ACTION_EXIST_NAME;
                        }
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ADMIN_GROUP" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ADMIN_GROUP";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.ADMIN_GROUP_SEQ;
                        //    sequence.ADMIN_GROUP_SEQ += 1;
                        //    context.SaveChanges();
                        //}

                        ADMIN_WZ_GROUPS new_group = new ADMIN_WZ_GROUPS();
                        new_group.ID        = newID;
                        new_group.NAME      = group.NAME;
                        new_group.PERMISSION = group.PERMISSION;
                        new_group.STATUS    = group.STATUS;
                        new_group.CREATED    = group.CREATED;

                        context.ADMIN_WZ_GROUPS.AddObject(new_group);
                        context.SaveChanges();

                        return (int)newID;
                    }
                }
                catch (Exception)
                {
                }
                return Const.Constants.ACTION_FAULT;
            }

            public bool DeleteAccountGroup(int group_id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var group = context.ADMIN_WZ_GROUPS.Where(g=>g.ID == (decimal)group_id).FirstOrDefault();
                        if (group != null)
                        {
                            context.ADMIN_WZ_GROUPS.DeleteObject(group);
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception)
                {
                }
                return false;
            }

            public bool DeleteAccountGroup(AccountGroupModel accountGroup)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var group_delete = (from g in context.ADMIN_WZ_GROUPS where g.ID == accountGroup.ID select g).FirstOrDefault();
                        if (group_delete != null) 
                        {
                            context.ADMIN_WZ_GROUPS.DeleteObject(group_delete);
                            context.SaveChanges();
                            return true;
                        }
                        
                    }
                }
                catch (Exception)
                {
                }
                return false;
            }

            public int CountListAccountGroupByPerpage(string dateFrom, string dateTo, string searchText)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = (from node in context.ADMIN_WZ_GROUPS
                                          select node);

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstAccount = lstAccount.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstAccount = lstAccount.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstAccount = lstAccount.Where(x => x.NAME.ToLower().Contains(searchText.ToLower()));
                        }

                        return lstAccount.Count();
                    }
                }
                catch
                {
                }
                return 0;
            }

            public List<AccountGroupModel> GetListAccountGroupByCondition(string dateFrom, string dateTo, string searchText, Models.Paging paging)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = from node in context.ADMIN_WZ_GROUPS
                                         select new AccountGroupModel
                                         {
                                             ID = (int)node.ID,
                                             NAME = node.NAME,
                                             PERMISSION = node.PERMISSION,
                                             STATUS = (int)node.STATUS,
                                             CREATED = node.CREATED
                                         };
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstAccount = lstAccount.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstAccount = lstAccount.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstAccount = lstAccount.Where(x => x.NAME.ToLower().Contains(searchText.ToLower()));
                        }

                        if (paging.Perpage > 0)
                        {
                            lstAccount = lstAccount.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return lstAccount.ToList();
                    }
                }
                catch
                {
                }
                return null;
            }

            public List<AccountGroupModel> GetListAccountGroupByCondition(string dateFrom, string dateTo, string searchText, Models.Paging paging, string sortBy, string sortType)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var query = from node in context.ADMIN_WZ_GROUPS
                                    select new AccountGroupModel
                                    {
                                        ID = (int)node.ID,
                                        NAME = node.NAME,
                                        PERMISSION = node.PERMISSION,
                                        STATUS = (int)node.STATUS,
                                        CREATED = node.CREATED
                                    };
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            query = query.Where(x => x.NAME.ToLower().Contains(searchText.ToLower()));
                        }

                        if (sortType == "") sortType = Const.Constants.SORT_ASC;

                        query = query.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            query = query.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return query.ToList();
                    }
                }
                catch(Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public bool CheckNameBeforeInsert(string name)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from node in context.ADMIN_WZ_GROUPS
                                     where node.NAME == name
                                     select node;
                        if (result.Count() > 0)
                            return true;
                        return false;
                    }
                }
                catch (Exception)
                {
                }
                return true;
            }

            public bool CheckNameBeforeUpdate(AccountGroupModel node)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from acc in context.ADMIN_WZ_GROUPS
                                     where acc.NAME == node.NAME && acc.ID != node.ID
                                     select acc;
                        if (result.Count() > 0)
                            return true;
                        return false;
                    }
                }
                catch (Exception)
                {
                }
                return true;
            }

            public List<AdminAccountModel> GetAccountsByIdGroup(int idGroup)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstAccount = from a in context.ADMIN_WZ_USERS
                                         where a.GROUP_ID == idGroup
                                         select new AdminAccountModel
                                         {
                                             ID = (int)a.ID,
                                             USERNAME = a.USERNAME,
                                             PASSWORD = a.PASSWORD,
                                             PERMISSION = a.PERMISSION,
                                             CUSTOM_PERMISSION = (int)a.CUSTOM_PERMISSION,
                                             GROUP_ID = (int)a.GROUP_ID,
                                             GROUP = a.ADMIN_WZ_GROUPS,
                                             STATUS = (int)a.STATUS,
                                             CREATED = a.CREATED
                                         };
                        return lstAccount.ToList();
                    }
                }
                catch
                {
                }
                return null;
            }

            public List<AccountGroupModel> GetSortListAccountGroupByCondition(string dateFrom, string dateTo, string searchText, Models.Paging paging, string sortBy, string typeSort)
            {
                try
                {
                    return this.GetListAccountGroupByCondition(dateFrom, dateTo, searchText, paging, sortBy, typeSort);
                    //List<ADMIN_WZ_GROUPS> lst = this.GetListAccountGroupByCondition(dateFrom, dateTo, searchText, paging);
                    //return DataHelper.SortListByProperty(lst, sortBy, typeSort);
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public void UpdateStatustByArrayId(List<int> listId, int status)
            {
                foreach (var group_id in listId)
                {
                    try
                    {
                        AccountGroupModel group = this.GetGroupById(group_id);
                        group.STATUS = status;
                        this.UpdateAccountGroup(group);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            public void DeleteGroupByArrayId(List<int> listId)
            {
                foreach (var group_id in listId)
                {
                    try
                    {
                        this.DeleteAccountGroup(group_id);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            #endregion
        }
    }
}