﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Models
{
    public class TranslateViewModel
    {
        public int ID { get; set; }
        public string SLUG { get; set; }
        public string TEXT_VI { get; set; }
        public string TEXT_EN { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class TranslateServices
    {
        #region service interface
        public static TranslateViewModel Get(int Id)
        {
            try
            {
                return TranslateDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(TranslateViewModel slideViewModel)
        {
            try
            {
                return TranslateDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return TranslateDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(TranslateViewModel slideViewModel)
        {
            try
            {
                return TranslateDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return TranslateDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return TranslateDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TranslateViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return TranslateDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class TranslateDAL
        {
            #region static instance
            private static TranslateDAL _instance;

            static public TranslateDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new TranslateDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public TranslateViewModel Get(int Id)
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        return (from a in context.WZ_TRANSLATES
                                where a.ID == Id
                                select new TranslateViewModel
                                {
                                    ID = (int)a.ID,
                                    SLUG = a.SLUG,
                                    TEXT_VI = a.TEXT_VI,
                                    TEXT_EN = a.TEXT_EN,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(TranslateViewModel viewModel)
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        var node = context.WZ_TRANSLATES.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.SLUG = viewModel.SLUG;
                        node.TEXT_VI = viewModel.TEXT_VI;
                        node.TEXT_EN = viewModel.TEXT_EN;
                        node.STATUS = viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(TranslateViewModel viewModel)
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        WZ_TRANSLATES node = new WZ_TRANSLATES();
                        node.ID = this._getNextID();
                        node.SLUG = viewModel.SLUG;
                        node.TEXT_VI = viewModel.TEXT_VI;
                        node.TEXT_EN = viewModel.TEXT_EN;
                        node.STATUS = viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_TRANSLATES.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        var slide = (context.WZ_TRANSLATES.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (slide == null) return false;

                        context.WZ_TRANSLATES.DeleteObject(slide);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        var node = (context.WZ_TRANSLATES.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new TranslateContext())
                    {
                        var result = from a in context.WZ_TRANSLATES
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.SLUG.ToLower().Contains(searchText) || a.TEXT_VI.ToLower().Contains(searchText) || a.TEXT_EN.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<TranslateViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<TranslateViewModel> lsSlideViewModels = new List<TranslateViewModel>();

                    using (var context = new TranslateContext())
                    {
                        var result = from a in context.WZ_TRANSLATES
                                     select new TranslateViewModel
                                     {
                                         ID = (int)a.ID,
                                         SLUG = a.SLUG,
                                         TEXT_VI = a.TEXT_VI,
                                         TEXT_EN = a.TEXT_EN,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.SLUG.ToLower().Contains(searchText) || a.TEXT_VI.ToLower().Contains(searchText) || a.TEXT_EN.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "TRANSLATE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "TRANSLATE";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        using (var context2 = new TranslateContext())
                        {
                            var check = context2.WZ_TRANSLATES.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                        }
                        
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
        #endregion
    }
}