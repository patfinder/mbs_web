﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.DTO;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;

namespace WizardCMS.Models
{
    public class LogModel
    {
        public int ID { get; set; }
        public string FUNCTION { get; set; }
        public int FUNCTION_ID { get; set; }
        public string FIELD { get; set; }
        public string TYPE { get; set; }
        public string ACCOUNT { get; set; }
        public string IP { get; set; }
        public DateTime CREATED { get; set; }
        public object OLD_VALUE { get; set; }
        public object NEW_VALUE { get; set; }
    }
    public static class LogServices
    {
        #region Log services interface
        public static bool SaveLog(List<LogModel> lstLogDto)
        {
            try
            {
                return LogModels.Instance.SaveLog(lstLogDto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveLog(LogModel logDto)
        {
            try
            {
                return LogModels.Instance.SaveLog(logDto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountListLogByPerpage(string dateFrom, string dateTo, string searchText)
        {
            try
            {
                return LogModels.Instance.CountListLogByPerpage(dateFrom, dateTo, searchText);
            }
            catch (Exception ex)
            {
                throw ex;
            }   
        }

        public static List<LogModel> GetListLogByPerpage(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy, string sortType)
        {
            try
            {
                return LogModels.Instance.GetListLogByPerpage(dateFrom, dateTo, searchText, paging, sortBy, sortType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Log Impliment method
        public class LogModels
        {
            private static LogModels _instance = null;

            static public LogModels Instance {
                get
                {
                    if (_instance == null)
                        _instance = new LogModels();
                    return _instance;
                }
            }

            #region Constructor
            public LogModels()
                : base()
            {
            }
            #endregion

            public bool SaveLog(List<LogModel> lstLogDto)
            {
                try
                {
                    if (lstLogDto != null)
                    {
                        foreach (var logDto in lstLogDto)
                        {
                            if (logDto.FIELD == "")
                            {
                                //Use for function Update
                                if (logDto.OLD_VALUE != null && logDto.OLD_VALUE != "" && logDto.NEW_VALUE != null && logDto.NEW_VALUE != "")
                                {
                                    Object oldType = logDto.OLD_VALUE;
                                    Object newType = logDto.NEW_VALUE;

                                    if (oldType == null && newType == null)
                                    {
                                        return true;
                                    }
                                    if (oldType == null || newType == null)
                                    {
                                        return false;
                                    }
                                    Type firstType = oldType.GetType();
                                    Type secondeType = newType.GetType();
                                    if (secondeType != firstType)
                                    {
                                        return false; // Or throw an exception
                                    }

                                    foreach (System.Reflection.PropertyInfo propertyInfo in firstType.GetProperties())
                                    {
                                        if (propertyInfo.CanRead)
                                        {
                                            object firstValue = propertyInfo.GetValue(oldType, null);
                                            object secondValue = propertyInfo.GetValue(newType, null);
                                            if (firstValue is String || firstValue is DateTime)
                                            {
                                                firstValue = firstValue.ToString().Trim();
                                            }
                                            if (secondValue is String || secondValue is DateTime)
                                            {
                                                secondValue = secondValue.ToString().Trim();
                                            }
                                            if (!object.Equals(firstValue, secondValue))
                                            {
                                                if (firstValue == null)
                                                    firstValue = "";
                                                if (secondValue == null)
                                                    secondValue = "";

                                                ADMIN_WZ_LOGS log = new ADMIN_WZ_LOGS();
                                                log.ID = this._getNextID();
                                                log.FUNCTION = logDto.FUNCTION;
                                                log.FUNCTION_ID = logDto.FUNCTION_ID;
                                                log.FIELD = propertyInfo.ToString();
                                                log.TYPE = logDto.TYPE;
                                                log.OLD_VALUE = firstValue.ToString();
                                                log.NEW_VALUE = secondValue.ToString();
                                                log.IP = logDto.IP;
                                                log.ACCOUNT = logDto.ACCOUNT;
                                                log.CREATED = DateTime.Now;

                                                using (var context = new DBContext())
                                                {
                                                    context.ADMIN_WZ_LOGS.AddObject(log);
                                                    context.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Use for function Delete, has field = ""
                                    ADMIN_WZ_LOGS log = new ADMIN_WZ_LOGS();
                                    log.ID = this._getNextID();
                                    log.FUNCTION = logDto.FUNCTION;
                                    log.FUNCTION_ID = logDto.FUNCTION_ID;
                                    log.FIELD = logDto.FIELD;
                                    log.TYPE = logDto.TYPE;
                                    log.OLD_VALUE = logDto.OLD_VALUE.ToString();
                                    log.NEW_VALUE = logDto.NEW_VALUE.ToString();
                                    log.IP = logDto.IP;
                                    log.ACCOUNT = logDto.ACCOUNT;
                                    log.CREATED = DateTime.Now;
                                    using (var context = new DBContext())
                                    {
                                        context.ADMIN_WZ_LOGS.AddObject(log);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                //Use for function Update Status, Add New have field != ""
                                ADMIN_WZ_LOGS log = new ADMIN_WZ_LOGS();
                                log.ID = this._getNextID();
                                log.FUNCTION = logDto.FUNCTION;
                                log.FUNCTION_ID = logDto.FUNCTION_ID;
                                log.FIELD = logDto.FIELD;
                                log.TYPE = logDto.TYPE;
                                log.OLD_VALUE = logDto.OLD_VALUE.ToString();
                                log.NEW_VALUE = logDto.NEW_VALUE.ToString();
                                log.IP = logDto.IP;
                                log.ACCOUNT = logDto.ACCOUNT;
                                log.CREATED = DateTime.Now;

                                using (var context = new DBContext())
                                {
                                    context.ADMIN_WZ_LOGS.AddObject(log);
                                    context.SaveChanges();
                                }
                            }
                            //return true;
                        }
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            private decimal _getNextID() {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ADMIN_LOG" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ADMIN_LOG";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            public bool SaveLog(LogModel logDto)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        ADMIN_WZ_LOGS log = new ADMIN_WZ_LOGS();
                        log.ID = this._getNextID();
                        log.FUNCTION = logDto.FUNCTION;
                        log.FUNCTION_ID = logDto.FUNCTION_ID;
                        log.FIELD = logDto.FIELD;
                        log.TYPE = logDto.TYPE;
                        log.OLD_VALUE = logDto.OLD_VALUE.ToString();
                        log.NEW_VALUE = logDto.NEW_VALUE.ToString();
                        log.IP = logDto.IP;
                        log.ACCOUNT = logDto.ACCOUNT;
                        log.CREATED = DateTime.Now;
                        context.ADMIN_WZ_LOGS.AddObject(log);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return true;
            }

            public int CountListLogByPerpage(string dateFrom, string dateTo, string searchText)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var lstLog = (from log in context.ADMIN_WZ_LOGS
                                      select log);

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                lstLog = lstLog.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                lstLog = lstLog.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            lstLog = lstLog.Where(x =>
                                    x.FIELD.ToLower().Contains(searchText.ToLower())
                                    || x.FUNCTION.ToLower().Contains(searchText.ToLower())
                                    || x.TYPE.ToLower().Contains(searchText.ToString()));
                        }

                        return lstLog.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                    return 0;
                }
            }

            public List<LogModel> GetListLogByPerpage(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy, string sortType)
            {
                try
                {
                    try
                    {
                        using (var context = new DBContext())
                        {
                            var lstLog = from log in context.ADMIN_WZ_LOGS select new LogModel { 
                                ID = (int)log.ID,
                                FUNCTION = log.FUNCTION,
                                FUNCTION_ID = (int)log.FUNCTION_ID,
                                FIELD = log.FIELD,
                                TYPE = log.TYPE,
                                ACCOUNT = log.ACCOUNT,
                                IP = log.IP,
                                CREATED = log.CREATED,
                                OLD_VALUE = log.OLD_VALUE,
                                NEW_VALUE = log.NEW_VALUE
                            };

                            if (String.IsNullOrWhiteSpace(dateFrom) == false)
                            {
                                try
                                {
                                    DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                    fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                    lstLog = lstLog.Where(x => x.CREATED >= fromDate);
                                }
                                catch (Exception)
                                {
                                }
                            }
                            if (String.IsNullOrWhiteSpace(dateTo) == false)
                            {
                                try
                                {
                                    DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                    toDate = DatetimeHelper.GetEndOfDay(toDate);
                                    lstLog = lstLog.Where(x => x.CREATED <= toDate);
                                }
                                catch (Exception)
                                {
                                }
                            }
                            if (String.IsNullOrWhiteSpace(searchText) == false)
                            {
                                lstLog = lstLog.Where(x =>
                                    x.FIELD.ToLower().Contains(searchText.ToLower())
                                    || x.FUNCTION.ToLower().Contains(searchText.ToLower())
                                    || x.TYPE.ToLower().Contains(searchText.ToString()));
                            }

                            if (string.IsNullOrEmpty(sortType)) sortType = Constants.SORT_ASC;
                            if (string.IsNullOrEmpty(sortBy)) sortBy = Constants.SORT_PORPERTY_DEFAULT;

                            lstLog = lstLog.OrderBy(sortBy + " " + sortType);

                            if (paging.Perpage > 0)
                            {
                                lstLog = lstLog.Skip(paging.Offset).Take(paging.Perpage);
                            }

                            return lstLog.ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.WriteLogError(ex.StackTrace);
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                    return null;
                }
            }
        }
        #endregion
    }
}