﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class WorkExpViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string OFFICE_NAME_ADDRESS { get; set; }
        public string OFFICE_PHONE { get; set; }
        public string WORK_FROM { get; set; }
        public string WORK_TO { get; set; }
        public string POSITION { get; set; }
        public string SALARY { get; set; }
        public string LEADER { get; set; }
        public string SUBORDINATES { get; set; }
        public string OUT_REASON { get; set; }
        public string WORK_DESCRIPTION { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReWorkExpServices
    {
        #region service interface
        public static int Insert(WorkExpViewModel viewModel)
        {
            try
            {
                return MajorDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(WorkExpViewModel viewModel)
        {
            try
            {
                return MajorDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return MajorDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static WorkExpViewModel Get(int id)
        {
            try
            {
                return MajorDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<WorkExpViewModel> GetAll(int uid)
        {
            try
            {
                return MajorDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class MajorDAL
        {
            #region static instance
            private static MajorDAL _instance;

            static public MajorDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new MajorDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public WorkExpViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_WORK_EXP
                                where a.ID == Id
                                select new WorkExpViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    OFFICE_NAME_ADDRESS = a.OFFICE_NAME_ADDRESS,
                                    OFFICE_PHONE = a.OFFICE_PHONE,
                                    WORK_FROM = a.WORK_FROM,
                                    WORK_TO = a.WORK_TO,
                                    POSITION = a.POSITION,
                                    SALARY = a.SALARY,
                                    LEADER = a.LEADER,
                                    SUBORDINATES = a.SUBORDINATES,
                                    OUT_REASON = a.OUT_REASON,
                                    WORK_DESCRIPTION = a.WORK_DESCRIPTION,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(WorkExpViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_WORK_EXP.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        node.OFFICE_NAME_ADDRESS = viewModel.OFFICE_NAME_ADDRESS;
                        node.OFFICE_PHONE = viewModel.OFFICE_PHONE;
                        node.WORK_FROM = viewModel.WORK_FROM;
                        node.WORK_TO = viewModel.WORK_TO;
                        node.POSITION = viewModel.POSITION;
                        node.SALARY = viewModel.SALARY;
                        node.LEADER = viewModel.LEADER;
                        node.SUBORDINATES = viewModel.SUBORDINATES;
                        node.OUT_REASON = viewModel.OUT_REASON;
                        node.WORK_DESCRIPTION = viewModel.WORK_DESCRIPTION;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<WorkExpViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_WORK_EXP
                                     where a.USER_ID == uid
                                     select new WorkExpViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         OFFICE_NAME_ADDRESS = a.OFFICE_NAME_ADDRESS,
                                         OFFICE_PHONE = a.OFFICE_PHONE,
                                         WORK_FROM = a.WORK_FROM,
                                         WORK_TO = a.WORK_TO,
                                         POSITION = a.POSITION,
                                         SALARY = a.SALARY,
                                         LEADER = a.LEADER,
                                         SUBORDINATES = a.SUBORDINATES,
                                         OUT_REASON = a.OUT_REASON,
                                         WORK_DESCRIPTION = a.WORK_DESCRIPTION,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_WORK_EXP.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_WORK_EXP.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(WorkExpViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_WORK_EXP node = new WZ_RE_WORK_EXP();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.OFFICE_NAME_ADDRESS = viewModel.OFFICE_NAME_ADDRESS;
                        node.OFFICE_PHONE = viewModel.OFFICE_PHONE;
                        node.WORK_FROM = viewModel.WORK_FROM;
                        node.WORK_TO = viewModel.WORK_TO;
                        node.POSITION = viewModel.POSITION;
                        node.SALARY = viewModel.SALARY;
                        node.LEADER = viewModel.LEADER;
                        node.SUBORDINATES = viewModel.SUBORDINATES;
                        node.OUT_REASON = viewModel.OUT_REASON;
                        node.WORK_DESCRIPTION = viewModel.WORK_DESCRIPTION;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_WORK_EXP.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_WORK_EXP" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_WORK_EXP";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_WORK_EXP.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}