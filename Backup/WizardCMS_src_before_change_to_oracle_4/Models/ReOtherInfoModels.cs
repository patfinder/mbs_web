﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class OtherInfoViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }

        public int KNOW_MBS_BY { get; set; } //Biet den mbs tu nguon nao
        public int KNOW_JOB_BY { get; set; } //Biet thong tin tu
        public string MASS_MEDIA { get; set; }  //Biet thong tin tuyen dung qua phuong tien thong tin
        public string ORGANIZATION { get; set; } //Ten, moi lien he, va ten to chuc
        
        public string JOB_MASS_MEDIA { get; set; }
        public string JOB_ORGANIZATION { get; set; }

        public string APPLY_BEFORE { get; set; }
        public string WORK_BEFORE { get; set; }
        public int DEFECT { get; set; } //Khuyet tat
        public string DEFECT_DETAIL { get; set; }//Chi tiet khuyet tat
        public int WORK_FLY { get; set; }//San sang cong tac = may bay
        public int WORK_OTO { get; set; }//San sang cong tac = oto
        public int DEMIST { get; set; }//Da tung bi sa thai chua
        public string DEMIST_DETAIL { get; set; }//     
        public string EXPECTED_SALARY { get; set; }//Muc luong mong muon
        public string WHEN_START { get; set; }
        public string STRONG_WEAK { get; set; }//Diem manh, diem yeu
        public string REASON_APPLY { get; set; }
        public DateTime CV_CREATED { get; set; }//Ngay lam ho so
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReOtherInfoServices
    {
        #region service interface
        public static int Insert(OtherInfoViewModel viewModel)
        {
            try
            {
                return OtherInfoDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(OtherInfoViewModel viewModel)
        {
            try
            {
                return OtherInfoDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return OtherInfoDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OtherInfoViewModel Get(int id)
        {
            try
            {
                return OtherInfoDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OtherInfoViewModel GetByUserId(int id)
        {
            try
            {
                return OtherInfoDAL.Instance.GetByUserId(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<OtherInfoViewModel> GetAll(int uid)
        {
            try
            {
                return OtherInfoDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class OtherInfoDAL
        {
            #region static instance
            private static OtherInfoDAL _instance;

            static public OtherInfoDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new OtherInfoDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public OtherInfoViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_OTHER_INFO
                                where a.ID == Id
                                select new OtherInfoViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    KNOW_MBS_BY = (int)a.KNOW_MBS_BY,
                                    MASS_MEDIA = a.MASS_MEDIA,
                                    ORGANIZATION = a.ORGANIZATION,
                                    KNOW_JOB_BY = (int)a.KNOW_JOB_BY,
                                    JOB_MASS_MEDIA = a.JOB_MASS_MEDIA,
                                    JOB_ORGANIZATION = a.JOB_ORGANIZATION,
                                    APPLY_BEFORE = a.APPLY_BEFORE,
                                    WORK_BEFORE = a.WORK_BEFORE,
                                    DEFECT = (int)a.DEFECT,
                                    DEFECT_DETAIL = a.DEFECT_DETAIL,
                                    WORK_FLY = (int)a.WORK_FLY,
                                    WORK_OTO = (int)a.WORK_OTO,
                                    DEMIST = (int)a.DEMIST,
                                    DEMIST_DETAIL = a.DEMIST_DETAIL,
                                    EXPECTED_SALARY = a.EXPECTED_SALARY,
                                    WHEN_START = a.WHEN_START,
                                    STRONG_WEAK = a.STRONG_WEAK,
                                    REASON_APPLY = a.REASON_APPLY,
                                    CV_CREATED = a.CV_CREATED,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public OtherInfoViewModel GetByUserId(int Id)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_OTHER_INFO
                                where a.USER_ID == Id
                                select new OtherInfoViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    KNOW_MBS_BY = (int)a.KNOW_MBS_BY,
                                    MASS_MEDIA = a.MASS_MEDIA,
                                    ORGANIZATION = a.ORGANIZATION,
                                    KNOW_JOB_BY = (int)a.KNOW_JOB_BY,
                                    JOB_MASS_MEDIA = a.JOB_MASS_MEDIA,
                                    JOB_ORGANIZATION = a.JOB_ORGANIZATION,
                                    APPLY_BEFORE = a.APPLY_BEFORE,
                                    WORK_BEFORE = a.WORK_BEFORE,
                                    DEFECT = (int)a.DEFECT,
                                    DEFECT_DETAIL = a.DEFECT_DETAIL,
                                    WORK_FLY = (int)a.WORK_FLY,
                                    WORK_OTO = (int)a.WORK_OTO,
                                    DEMIST = (int)a.DEMIST,
                                    DEMIST_DETAIL = a.DEMIST_DETAIL,
                                    EXPECTED_SALARY = a.EXPECTED_SALARY,
                                    WHEN_START = a.WHEN_START,
                                    STRONG_WEAK = a.STRONG_WEAK,
                                    REASON_APPLY = a.REASON_APPLY,
                                    CV_CREATED = a.CV_CREATED,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(OtherInfoViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_OTHER_INFO.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        node.KNOW_MBS_BY = viewModel.KNOW_MBS_BY;
                        node.MASS_MEDIA = viewModel.MASS_MEDIA;
                        node.ORGANIZATION = viewModel.ORGANIZATION;
                        node.KNOW_JOB_BY = viewModel.KNOW_JOB_BY;
                        node.JOB_MASS_MEDIA = viewModel.JOB_MASS_MEDIA;
                        node.JOB_ORGANIZATION = viewModel.JOB_ORGANIZATION;
                        node.APPLY_BEFORE = viewModel.APPLY_BEFORE;
                        node.WORK_BEFORE = viewModel.WORK_BEFORE;
                        node.DEFECT = viewModel.DEFECT;
                        node.DEFECT_DETAIL = viewModel.DEFECT_DETAIL;
                        node.WORK_FLY = viewModel.WORK_FLY;
                        node.WORK_OTO = viewModel.WORK_OTO;
                        node.DEMIST = viewModel.DEMIST;
                        node.DEMIST_DETAIL = viewModel.DEMIST_DETAIL;
                        node.EXPECTED_SALARY = viewModel.EXPECTED_SALARY;
                        node.WHEN_START = viewModel.WHEN_START;
                        node.STRONG_WEAK = viewModel.STRONG_WEAK;
                        node.REASON_APPLY = viewModel.REASON_APPLY;
                        node.CV_CREATED = viewModel.CV_CREATED;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<OtherInfoViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_OTHER_INFO
                                     where a.USER_ID == uid
                                     select new OtherInfoViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         KNOW_MBS_BY = (int)a.KNOW_MBS_BY,
                                         MASS_MEDIA = a.MASS_MEDIA,
                                         ORGANIZATION = a.ORGANIZATION,
                                         KNOW_JOB_BY = (int)a.KNOW_JOB_BY,
                                         JOB_MASS_MEDIA = a.JOB_MASS_MEDIA,
                                         JOB_ORGANIZATION = a.JOB_ORGANIZATION,
                                         APPLY_BEFORE = a.APPLY_BEFORE,
                                         WORK_BEFORE = a.WORK_BEFORE,
                                         DEFECT = (int)a.DEFECT,
                                         DEFECT_DETAIL = a.DEFECT_DETAIL,
                                         WORK_FLY = (int)a.WORK_FLY,
                                         WORK_OTO = (int)a.WORK_OTO,
                                         DEMIST = (int)a.DEMIST,
                                         DEMIST_DETAIL = a.DEMIST_DETAIL,
                                         EXPECTED_SALARY = a.EXPECTED_SALARY,
                                         WHEN_START = a.WHEN_START,
                                         STRONG_WEAK = a.STRONG_WEAK,
                                         REASON_APPLY = a.REASON_APPLY,
                                         CV_CREATED = a.CV_CREATED,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_OTHER_INFO.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_OTHER_INFO.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(OtherInfoViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_OTHER_INFO node = new WZ_RE_OTHER_INFO();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.KNOW_MBS_BY = viewModel.KNOW_MBS_BY;
                        node.MASS_MEDIA = viewModel.MASS_MEDIA;
                        node.ORGANIZATION = viewModel.ORGANIZATION;
                        node.KNOW_JOB_BY = viewModel.KNOW_JOB_BY;
                        node.JOB_MASS_MEDIA = viewModel.JOB_MASS_MEDIA;
                        node.JOB_ORGANIZATION = viewModel.JOB_ORGANIZATION;
                        node.APPLY_BEFORE = viewModel.APPLY_BEFORE;
                        node.WORK_BEFORE = viewModel.WORK_BEFORE;
                        node.DEFECT = viewModel.DEFECT;
                        node.DEFECT_DETAIL = viewModel.DEFECT_DETAIL;
                        node.WORK_FLY = viewModel.WORK_FLY;
                        node.WORK_OTO = viewModel.WORK_OTO;
                        node.DEMIST = viewModel.DEMIST;
                        node.DEMIST_DETAIL = viewModel.DEMIST_DETAIL;
                        node.EXPECTED_SALARY = viewModel.EXPECTED_SALARY;
                        node.WHEN_START = viewModel.WHEN_START;
                        node.STRONG_WEAK = viewModel.STRONG_WEAK;
                        node.REASON_APPLY = viewModel.REASON_APPLY;
                        node.CV_CREATED = viewModel.CV_CREATED;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_OTHER_INFO.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_OTHER_INFO" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_OTHER_INFO";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_OTHER_INFO.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
        #endregion
    }
}