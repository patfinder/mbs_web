﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.Models
{
    public class Paging
    {
        public int TotalItems { get; set; }
        public int Offset { get; set; }
        public int Perpage { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
        public Paging()
        {
            Offset = 0;
            Page = 1;
            Perpage = WizardCMS.Const.Constants.DEFAULT_ITEM;
        }
    }
}