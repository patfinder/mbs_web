﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WizardCMS.domain;
using Oracle.DataAccess.Client;
using System.Data;
using System.Configuration;
using System.Xml;
using WizardCMS.Const;
using WizardCMS.Helpers;

namespace WizardCMS.Models
{
    public class HomeModels
    {
        DBContext dbContext = new DBContext();

        string ConnectionString = ConfigurationManager.ConnectionStrings["strConnectionString"].ToString();

        public List<WZ_ARTICLES> getHotNews()
        {
            List<WZ_ARTICLES> result = new List<WZ_ARTICLES>();

            using (DBContext dbContext = new DBContext())
            {
                var query = from a in dbContext.WZ_ARTICLES where a.STATUS == 1 select a;
                if (query != null)
                    result = query.ToList();
            }

            return result;
        }

        public int createNewUser()
        {
            int insert_id = 0;
            try
            {
                string sql = @"INSERT INTO ADMIN_WZ_USERS(USERNAME, PASSWORD, PERMISSION, GROUP_ID, STATUS, CREATED, ID) 
                VALUES(:1,:2,:3,:4,:5,:6, admin_user_seq.nexval)";

                OracleParameter[] prm = new OracleParameter[6];

                // Create OracleParameter objects through OracleParameterCollection
                OracleCommand cmd = new OracleCommand();

                prm[0] = cmd.Parameters.Add("paramUSERNAME", OracleDbType.Varchar2, "mrpeo", ParameterDirection.Input);
                prm[1] = cmd.Parameters.Add("paramPASSWORD", OracleDbType.Varchar2, "e10adc3949ba59abbe56e057f20f883e", ParameterDirection.Input);
                prm[2] = cmd.Parameters.Add("paramPERMISSION", OracleDbType.Varchar2, "0|rwd,1|rwd,2|rwd,3|rwd,14|rwd,5|rwd,6|rwd,7|rwd,8|rwd,12|rwd,13|rwd,10|rwd,11|rwd,16|rwd,18|rw-", ParameterDirection.Input);
                prm[3] = cmd.Parameters.Add("paramGROUP_ID", OracleDbType.Int16, 1, ParameterDirection.Input);
                prm[4] = cmd.Parameters.Add("paramSTATUS", OracleDbType.Int16, 1, ParameterDirection.Input);
                prm[5] = cmd.Parameters.Add("paramCREATED", OracleDbType.TimeStamp, DateTime.Now, ParameterDirection.Input);
                cmd.CommandText = sql;

                int result = this.ExecNonQuery(cmd);

                //List<object> parameterList = new List<object>();
                //parameterList.Add(new OracleParameter("@P0","mrpeo"));
                //parameterList.Add(new OracleParameter("@P1", "e10adc3949ba59abbe56e057f20f883e"));
                //parameterList.Add(new OracleParameter("@P2", "0|rwd,1|rwd,2|rwd,3|rwd,14|rwd,5|rwd,6|rwd,7|rwd,8|rwd,12|rwd,13|rwd,10|rwd,11|rwd,16|rwd,18|rw-"));
                //parameterList.Add(new OracleParameter("@P3", 1));
                //parameterList.Add(new OracleParameter("@P4", 1));
                //parameterList.Add(new OracleParameter("@P5", DateTime.Now));
                //object[] parameters = parameterList.ToArray();
                //int result = dbContext.ExecuteStoreCommand(sql, parameters);

                insert_id = result;
            }
            catch(Exception ex) {
                Console.Write(ex.StackTrace);
            }
            return insert_id;
        }

        #region TestConnectionString

        public bool TestConnectionString()
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    string strSql = @"SELECT null from dual";
                    OracleCommand command = new OracleCommand(strSql, conn);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Log.WriteLogs(ex);
                return false;
            }
        }

        #endregion

        #region ExecNonQuery
        public int ExecNonQuery(OracleCommand cmdUpdate)
        {
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                OracleTransaction tracsaction = conn.BeginTransaction();
                try
                {
                    cmdUpdate.Connection = conn;
                    return cmdUpdate.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    tracsaction.Rollback();
                    tracsaction.Dispose();
                    //Log.WriteLogs(ex);
                    throw ex;
                }
            }
        }
        #endregion

        #region migrate data from MBS
        public bool migrateDataFromMBS()
        {
            try
            {
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == 913 select n).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
 
                        }
                    }
                }
            }
            catch { }
            return false;
        }

        public bool getTinMBS()
        {
            try
            {
                string CategoryLinkVi = "goc-truyen-thong/tin-mbs/";
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == 873 select n).OrderBy(n=>n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_vi = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = Constants.NODE_TIN_MBS;
                            article.STATUS = node.Content_Status;

                            article_content_vi.LANG = "vi";
                            article_content_vi.TITLE = node.Content_Headline;
                            article_content_vi.DESCRIPTION = node.Content_Teaser;
                            article_content_vi.CONTENT = node.Content_Body;
                            article_content_vi.IMAGE = node.Content_Avatar;
                            article_content_vi.ATTACHMENT = node.Content_Document;
                            article_content_vi.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_vi.LINK = CategoryLinkVi + article_content_vi.SLUG;
                            article_content_vi.SUB_TITLE = node.Content_HeadlineMapping;

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_vi.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_vi);
                            }
                            catch (Exception)
                            {
                                continue;
                            }


                        }
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public bool getTinChung()
        {
            try
            {
                string CategoryLinkVi = "goc-truyen-thong/tin-mbs/";
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == 911 select n).OrderBy(n => n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_vi = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = Constants.NODE_TIN_CO_DONG;
                            article.STATUS = node.Content_Status;

                            article_content_vi.LANG = "vi";
                            article_content_vi.TITLE = node.Content_Headline;
                            article_content_vi.DESCRIPTION = node.Content_Teaser;
                            article_content_vi.CONTENT = node.Content_Body;
                            article_content_vi.IMAGE = node.Content_Avatar;
                            article_content_vi.ATTACHMENT = node.Content_Document;
                            article_content_vi.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_vi.LINK = CategoryLinkVi + article_content_vi.SLUG;
                            article_content_vi.SUB_TITLE = node.Content_HeadlineMapping;

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_vi.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_vi);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public bool getTinCoDong()
        {
            try
            {
                string CategoryLinkVi = "quan-he-co-dong/tin-co-dong/";
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == 947 select n).OrderBy(n => n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_vi = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = Constants.NODE_TIN_CO_DONG;
                            article.STATUS = node.Content_Status;

                            article_content_vi.LANG = "vi";
                            article_content_vi.TITLE = node.Content_Headline;
                            article_content_vi.DESCRIPTION = node.Content_Teaser;
                            article_content_vi.CONTENT = node.Content_Body;
                            article_content_vi.IMAGE = node.Content_Avatar;
                            article_content_vi.ATTACHMENT = node.Content_Document;
                            article_content_vi.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_vi.LINK = CategoryLinkVi + article_content_vi.SLUG;
                            article_content_vi.SUB_TITLE = node.Content_HeadlineMapping;

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_vi.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_vi);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public bool getBaoCaoTaiChinh()
        {
            try
            {
                string CategoryLinkVi = "quan-he-co-dong/tin-co-dong/";
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == 979 select n).OrderBy(n => n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_vi = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = Constants.NODE_BAO_CAO_TC;
                            article.STATUS = node.Content_Status;

                            article_content_vi.LANG = "vi";
                            article_content_vi.TITLE = node.Content_Headline;
                            article_content_vi.DESCRIPTION = node.Content_Teaser;
                            article_content_vi.CONTENT = node.Content_Body;
                            article_content_vi.IMAGE = node.Content_Avatar;
                            article_content_vi.ATTACHMENT = node.Content_Document;
                            article_content_vi.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_vi.LINK = CategoryLinkVi + article_content_vi.SLUG;
                            article_content_vi.SUB_TITLE = node.Content_HeadlineMapping;

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_vi.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_vi);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public bool getDataFromThanhLong(int ZONE_ID, int PARENT_ID, string PARENT_LINK, string LANG = "vi")
        {
            try
            {
                using (var context = new ThanhLongMBSDataContext())
                {
                    //Tin tuc
                    var news = (from n in context.CMS_Contents where n.Content_ZoneID == ZONE_ID select n).OrderBy(n => n.Content_CreateDate).ToList();

                    if (news != null)
                    {
                        foreach (var node in news)
                        {
                            WZ_ARTICLES article = new WZ_ARTICLES();
                            WZ_ARTICLES_CONTENT article_content_lang = new WZ_ARTICLES_CONTENT();
                            article.CREATED = node.Content_CreateDate;
                            article.MODIFIED = node.Content_ModifiedDate;
                            article.PARENT_ID = PARENT_ID;
                            article.STATUS = node.Content_Status == 4 ? 1 : 0;

                            article_content_lang.LANG = LANG;
                            article_content_lang.TITLE = node.Content_Headline;
                            article_content_lang.DESCRIPTION = node.Content_Teaser;
                            article_content_lang.CONTENT = node.Content_Body;
                            article_content_lang.IMAGE = node.Content_Avatar;
                            article_content_lang.ATTACHMENT = node.Content_Document;
                            article_content_lang.SLUG = StringHelper.CreateSlug(node.Content_Headline);
                            article_content_lang.LINK = PARENT_LINK + article_content_lang.SLUG;
                            article_content_lang.SUB_TITLE = node.Content_HeadlineMapping;

                            //INSERT ARTICLE
                            try
                            {
                                dbContext.WZ_ARTICLES.AddObject(article);
                                dbContext.SaveChanges();

                                article_content_lang.NID = article.ID;
                                dbContext.WZ_ARTICLES_CONTENT.AddObject(article_content_lang);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public bool migrateDataFromSQL()
        {
            bool result = false;
            using (SqlDBContextDataContext sqlContext = new SqlDBContextDataContext()) 
            {
                /*
                var query = from a in sqlContext.admin_nqt_users select a;
                if (query != null) {
                    List<admin_nqt_user> lstNodes = query.ToList();
                    foreach (admin_nqt_user n in lstNodes)
                    {
                        var sequence = (from s in dbContext.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.ADMIN_USER_SEQ;
                            sequence.ADMIN_USER_SEQ += 1;
                            dbContext.SaveChanges();
                        }

                        ADMIN_WZ_USERS node = new ADMIN_WZ_USERS();
                        node.ID         = newID;
                        node.USERNAME   = n.username;
                        node.PASSWORD   = n.password;
                        node.GROUP_ID   = n.group_id;
                        node.PERMISSION = n.permission;
                        node.STATUS     = n.status;
                        node.CREATED    = n.created;
                        node.CUSTOM_PERMISSION = n.custom_permission;

                        dbContext.ADMIN_WZ_USERS.AddObject(node);
                        dbContext.SaveChanges();
                    }
                }
                 * */

                int[] arModuleGet = { 1,2,3,5,9,14};
                
                var modules = (from m in sqlContext.admin_nqt_modules where arModuleGet.Contains((int)m.id) select m).ToList();
                if (modules != null)
                {
                    foreach (var module in modules)
                    {
                        var sequence = (from s in dbContext.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "ADMIN_MODULE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "ADMIN_MODULE";
                            sequence.SEQ = 2;

                            dbContext.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        ADMIN_WZ_MODULES new_module = new ADMIN_WZ_MODULES();

                        new_module.ID               = newID;
                        new_module.NAME             = module.name;
                        new_module.NAME_FUNCTION    = module.name_function;
                        new_module.STATUS           = module.status;
                        new_module.CREATED          = DateTime.Now;

                        dbContext.ADMIN_WZ_MODULES.AddObject(new_module);
                        dbContext.SaveChanges();
                    }
                }
                return true;
            }
            return result;
        }

        /// <summary>
        /// IMPORT DATA - DISTRICT
        /// </summary>
        public void importDistrict()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "Assets/cli_district_new.xml");
                XmlNodeList elemList = doc.GetElementsByTagName("table");
                for (int i = 0; i < elemList.Count; i++)
                {
                    XmlNodeList childNodes = elemList[i].ChildNodes;
                    WZ_CT_DISTRICT district = new WZ_CT_DISTRICT();
                    if (childNodes.Count > 0)
                    {
                        foreach (XmlNode node in childNodes)
                        {
                            if (node.Attributes["name"].Value == "district_id")
                            {
                                district.DISTRICT_ID = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "district_name")
                            {
                                district.DISTRICT_NAME = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "slug")
                            {
                                district.SLUG = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "type")
                            {
                                district.TYPE = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "province_id")
                            {
                                district.PROVINCE_ID = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "order")
                            {
                                //district.PRIORITY = node.InnerText;
                                decimal priority = 0;
                                var c = decimal.TryParse(node.InnerText, out priority);
                                district.PRIORITY = priority;
                            }
                        }

                        if (district.DISTRICT_ID != string.Empty)
                        {
                            using (var context = new DBContext())
                            {
                                context.WZ_CT_DISTRICT.AddObject(district);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }catch(Exception ex){
            }
        }

        /// <summary>
        /// IMPORT DATA - PROVINCE
        /// </summary>
        public void importProvince()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "Assets/cli_province_new.xml");
                XmlNodeList elemList = doc.GetElementsByTagName("table");
                for (int i = 0; i < elemList.Count; i++)
                {
                    XmlNodeList childNodes = elemList[i].ChildNodes;
                    WZ_CT_PROVINCE province = new WZ_CT_PROVINCE();
                    if (childNodes.Count > 0)
                    {
                        foreach (XmlNode node in childNodes)
                        {
                            if (node.Attributes["name"].Value == "province_id")
                            {
                                province.PROVINCE_ID = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "province_name")
                            {
                                province.PROVINCE_NAME = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "slug")
                            {
                                province.SLUG = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "type")
                            {
                                province.TYPE = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "order")
                            {
                                //district.PRIORITY = node.InnerText;
                                decimal priority = 0;
                                var c = decimal.TryParse(node.InnerText, out priority);
                                province.PRIORITY = priority;
                            }
                        }

                        if (province.PROVINCE_ID != string.Empty)
                        {
                            using (var context = new DBContext())
                            {
                                context.WZ_CT_PROVINCE.AddObject(province);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// IMPORT DATA - WARDS
        /// </summary>
        public void importWard()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "Assets/cli_ward_new.xml");
                XmlNodeList elemList = doc.GetElementsByTagName("table");
                for (int i = 0; i < elemList.Count; i++)
                {
                    XmlNodeList childNodes = elemList[i].ChildNodes;
                    WZ_CT_WARD ward = new WZ_CT_WARD();
                    if (childNodes.Count > 0)
                    {
                        foreach (XmlNode node in childNodes)
                        {
                            if (node.Attributes["name"].Value == "ward_id")
                            {
                                ward.WARD_ID = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "ward_name")
                            {
                                ward.WARD_NAME = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "slug")
                            {
                                ward.SLUG = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "type")
                            {
                                ward.TYPE = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "district_id")
                            {
                                ward.DISTRICT_ID = node.InnerText;
                            }
                            else if (node.Attributes["name"].Value == "order")
                            {
                                //district.PRIORITY = node.InnerText;
                                decimal priority = 0;
                                var c = decimal.TryParse(node.InnerText, out priority);
                                ward.PRIORITY = priority;
                            }
                        }

                        if (ward.WARD_ID != string.Empty)
                        {
                            using (var context = new DBContext())
                            {
                                context.WZ_CT_WARD.AddObject(ward);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}