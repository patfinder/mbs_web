﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class LanguageViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string NAME { get; set; }
        public string READ { get; set; }
        public string WRITE { get; set; }
        public string SPEAK { get; set; }
        public string LISTEN { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReLanguageServices
    {
        #region service interface
        public static int Insert(LanguageViewModel viewModel)
        {
            try
            {
                return LanguageDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(LanguageViewModel viewModel)
        {
            try
            {
                return LanguageDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return LanguageDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static LanguageViewModel Get(int id)
        {
            try
            {
                return LanguageDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<LanguageViewModel> GetAll(int uid)
        {
            try
            {
                return LanguageDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class LanguageDAL
        {
            #region static instance
            private static LanguageDAL _instance;

            static public LanguageDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new LanguageDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public LanguageViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_LANGUAGE
                                where a.ID == Id
                                select new LanguageViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    NAME = a.NAME,
                                    READ = a.READ,
                                    WRITE = a.WRITE,
                                    SPEAK = a.SPEAK,
                                    LISTEN = a.LISTEN,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(LanguageViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_LANGUAGE.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        node.NAME = viewModel.NAME;
                        node.READ = viewModel.READ;
                        node.WRITE = viewModel.WRITE;
                        node.SPEAK = viewModel.SPEAK;
                        node.LISTEN = viewModel.LISTEN;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<LanguageViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_LANGUAGE
                                     where a.USER_ID == uid
                                     select new LanguageViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         NAME = a.NAME,
                                         READ = a.READ,
                                         WRITE = a.WRITE,
                                         SPEAK = a.SPEAK,
                                         LISTEN = a.LISTEN,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_LANGUAGE.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_LANGUAGE.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(LanguageViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_LANGUAGE node = new WZ_RE_LANGUAGE();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.NAME = viewModel.NAME;
                        node.READ = viewModel.READ;
                        node.WRITE = viewModel.WRITE;
                        node.SPEAK = viewModel.SPEAK;
                        node.LISTEN = viewModel.LISTEN;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_LANGUAGE.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_LANGUAGE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_LANGUAGE";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_LANGUAGE.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}