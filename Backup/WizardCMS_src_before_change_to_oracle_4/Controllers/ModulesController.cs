﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Helpers;
using WizardCMS.Models;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class ModulesController : Controller
    {
        AdmincpController admin = new AdmincpController();
        static int IdModule = 0;
        public PartialViewResult Index()
        {
            string url = Request.Url.AbsoluteUri;
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        return PartialView("../BackEnd/Module/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;
            string nameAction = CommonHelper.GetSegment(3, curentPath);
            if (!string.IsNullOrEmpty(nameAction))
            {
                string idModule = CommonHelper.GetSegment(4, curentPath);
                if (!string.IsNullOrEmpty(idModule))
                {
                    try
                    {
                        int id = int.Parse(idModule);
                        ModuleModel module = ModuleServices.GetModuleById(id);
                        return PartialView("../BackEnd/Module/Update", module);
                    }
                    catch (Exception)
                    {
                        return Search(paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
                    }
                }
                else
                {
                    // bao loi
                    return Search(paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
                }
            }
            else
            {
                return Search(paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
            }
        }

        [HttpPost]
        public string Save(string hiddenIdAdmincp, string statusAdmincp, string moduleNameAdmincp)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    ModuleModel module = new ModuleModel();
                    module.ID = int.Parse(hiddenIdAdmincp);
                    module.NAME = moduleNameAdmincp;
                    module.PRIORITY = StringHelper.IsNumber(Request["priority"]);

                    if (statusAdmincp == "on")
                    {
                        module.STATUS = 1;
                    }

                    int updateResult = ModuleServices.UpdateInstallModule(module);

                    if (updateResult == Constants.ACTION_SUCCESS)
                    {
                        return "success";
                    }
                    else if (updateResult == Constants.ACTION_EXIST_NAME)
                    {
                        return "name-module-exist";
                    }
                    return "fault";
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                string curentPath = Request.Url.AbsoluteUri;
                ModuleModel module = ModuleServices.GetModuleById(id);
                if (module != null)
                {
                    module.STATUS = (short)(module.STATUS == 1 ? 0 : 1);
                    ModuleServices.UpdateInstallModule(module);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search(paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult Search(Paging paging, string sortBy, string sortType, bool? hasLayout)
        {
            int totalItem = ModuleServices.GetCount();
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Modules/Search/";

            List<ModuleModel> lstModule = ModuleServices.GetListModuleByPaging(paging, sortBy, sortType);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Module/Index", lstModule);
            }
            else
            {
                return PartialView("../BackEnd/Module/AjaxContent", lstModule);
            }
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    ModuleModel node = ModuleServices.GetModuleById(id);
                    if (order == 0)
                    {
                        node.PRIORITY -= 1;
                        if (node.PRIORITY < 0)
                        {
                            node.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (node.PRIORITY + 1 < int.MaxValue)
                        {
                            node.PRIORITY += 1;
                        }
                    }
                    ModuleServices.UpdateInstallModule(node);
                    return node.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
