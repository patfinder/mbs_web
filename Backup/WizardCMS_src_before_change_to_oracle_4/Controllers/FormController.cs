﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using WizardCMS.Const;
using System.Web.Configuration;
using System.IO;

namespace WizardCMS.Controllers
{
    public class FormController : Controller
    {
        string nameModule = "form";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Form/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            ViewBag.module = "Form";

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);
                    ViewBag.categories = TermServices.GetTerms(3);
                    ViewBag.terms = TermServices.GetTerms(4);
                    ViewBag.Taxonomies = TaxonomyServices.GetAll();
                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            FormViewModel node = FormServices.GetById(id);

                            return PartialView("../BackEnd/Form/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Form/Update", null);
                    }
                }
                else
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    FormViewModel viewModel = new FormViewModel();

                    viewModel.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    viewModel.PRIORITY = StringHelper.IsNumber(Request["priority"]);
                    viewModel.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    foreach (var lang in Constants.Langs)
                    {
                        var name = Request["name_" + lang];
                        var file_download = Request["file_download_" + lang];
                        var description = Request["description_" + lang];
                        var slug = StringHelper.CreateSlug(name);

                        //Validate data input
                        if (name == string.Empty) continue;

                        FormContent content = new FormContent();
                        content.TITLE = name;
                        content.SLUG = slug;
                        content.FILE_DOWNLOAD = file_download;
                        content.DESCRIPTION = CommonHelper.CutText(description, 250);
                        content.LANG = lang;

                        viewModel.ContentTranslates.Add(lang, content);
                    }

                    if (viewModel.ID > 0)
                    {
                        //update
                        FormViewModel viewModelOld = FormServices.AdminGetById(viewModel.ID);
                        if (viewModelOld.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, FormContent> item in viewModelOld.ContentTranslates)
                            {
                                FormContent old_content = item.Value;

                                if (viewModel.ContentTranslates != null)
                                {
                                    if (viewModel.ContentTranslates.ContainsKey(old_content.LANG))
                                    {
                                        if (viewModel.ContentTranslates[old_content.LANG].SLUG != old_content.SLUG)
                                        {
                                            if (FormServices.CheckSlug(viewModel.ContentTranslates[old_content.LANG].SLUG, old_content.LANG))
                                                return "Tên đã có trong hệ thống.";
                                        }
                                    }
                                    else
                                    {
                                        viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                    }
                                }
                                else
                                {
                                    viewModel.ContentTranslates = new Dictionary<string, FormContent>();
                                    viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                }
                            }
                        }

                        if (FormServices.Update(viewModel)) return "success";
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                    else
                    {

                        if (viewModel.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, FormContent> item in viewModel.ContentTranslates)
                            {
                                FormContent content = item.Value;
                                if (FormServices.CheckSlug(content.SLUG, content.LANG))
                                    return "Tên đã có trong hệ thống.";
                            }
                        }
                        else
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = FormServices.Insert(viewModel);
                        if (newID >= Constants.ACTION_SUCCESS)
                            return "success";
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (FormServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        FormServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    FormViewModel node = FormServices.GetById(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        FormServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = FormServices.GetById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    FormServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "term";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Form/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = FormServices.GetCount(0, fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Form/Search/";

            List<FormViewModel> terms = FormServices.GetByPaging(0, fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Form/Index", terms);
            }
            else
            {
                return PartialView("../BackEnd/Form/AjaxContent", terms);
            }
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    FormViewModel node = FormServices.GetById(id);
                    if (order == 0)
                    {
                        node.PRIORITY -= 1;
                        if (node.PRIORITY < 0)
                        {
                            node.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (node.PRIORITY + 1 < int.MaxValue)
                        {
                            node.PRIORITY += 1;
                        }
                    }
                    FormServices.Update(node);
                    return node.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
