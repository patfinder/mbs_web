﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.Models;
using System.Web.UI;
using System.IO;
using ClosedXML.Excel;

namespace WizardCMS.Controllers
{
    public class ContactController : Controller
    {
        string nameModule = "contact";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Contact/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            ContactViewModel node = ContactServices.Get(id);

                            //Update viewed contact
                            ContactServices.UpdateStatus(id, 1);

                            return PartialView("../BackEnd/Contact/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Contact/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                //not allow edit
                return "success";
                /*
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    ContactViewModel node = new ContactViewModel();

                    node.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    node.FULLNAME = Request["fullnameAdmincp"];
                    node.EMAIL = Request["emailAdmincp"];
                    node.PHONE = Request["phoneAdmincp"];
                    node.ADDRESS = Request["addressAdmincp"];
                    node.TITLE = Request["titleAdmincp"];
                    node.CONTENT = Request["contentAdmincp"];
                    node.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    if (node.ID > 0)
                    {
                        if (ContactServices.Update(node))
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {
                        //Not allow to add new contact from admin
                        return Constants.PERMISSION_DENIED;
                    }
                }
                */
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (ContactServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, int parentId = 0)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        ContactServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    ContactViewModel node = ContactServices.Get(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        ArticlesServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = SlideServices.Get(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    SlideServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Slide";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Contact/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = ContactServices.GetCount(fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Contact/Search/";

            List<ContactViewModel> lstSlide = ContactServices.GetList(fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Contact/Index", lstSlide);
            }
            else
            {
                return PartialView("../BackEnd/Contact/AjaxContent", lstSlide);
            }
        }

        [HttpGet]
        public ActionResult ExportData(string from = "", string to = "", string lang = "vi")
        {
            Paging paging = new Paging();
            int totalItem = ContactServices.GetCount(from, to, "", lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = (int)Math.Ceiling( (double) (totalItem / paging.Perpage) );
            paging.Perpage = 0; //get all data
            paging.Offset = (paging.Page - 1) * paging.Perpage;

            var data = ContactServices.GetList(from, to, "", paging);

            string sheetName = "Export";
            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add(sheetName);
            var rngTitle = ws.Range("A1:I1");
            rngTitle.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rngTitle.Style.Font.Bold = true;

            //ws.Cell(1, 1).InsertTable(data);
            //HEADER
            ws.Cell(1, 1).Value = "STT";
            ws.Cell(1, 2).Value = "FULLNAME";
            ws.Cell(1, 3).Value = "EMAIL";
            ws.Cell(1, 4).Value = "TITLE";
            ws.Cell(1, 5).Value = "CONTENT";
            ws.Cell(1, 6).Value = "STATUS";
            ws.Cell(1, 7).Value = "CREATED";

            int cell = 1; int row = 2;
            if (data != null)
            {
                foreach (var item in data)
                {
                    cell = 1;
                    ws.Cell(row, cell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    ws.Cell(row, cell).Value = row - 1; cell++;
                    ws.Cell(row, cell).Value = item.FULLNAME; cell++;
                    ws.Cell(row, cell).Value = item.EMAIL; cell++;
                    ws.Cell(row, cell).Value = item.TITLE; cell++;
                    ws.Cell(row, cell).Value = item.CONTENT; cell++;
                    ws.Cell(row, cell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    ws.Cell(row, cell).Value = item.STATUS == 1 ? "Read" : "UnRead"; cell++;
                    ws.Cell(row, cell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    ws.Cell(row, cell).Value = "'"+item.CREATED.ToString("hh:mm:ss dd/MM/yyyy"); cell++;
                    row++;
                }
            }
            ws.Columns().AdjustToContents();

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", sheetName.Replace(" ", "_")));

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.Flush();
            Response.End();

            //StringHelper.ExportToExcel(data, "Export");

            //var export_content = StringHelper.PartialView(this, "../BackEnd/Contact/Export", data);

            //Response.ClearContent();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment; filename=export.xls");
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.Charset = "";

            //Response.Output.Write(export_content);
            //Response.Flush();
            //Response.End();

            return RedirectToAction("Index");
        }

        
        #region FRONTEND CODE
        
        #endregion
    }
}
