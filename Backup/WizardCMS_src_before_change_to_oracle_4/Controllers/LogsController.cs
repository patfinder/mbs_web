﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Helpers;
using WizardCMS.Models;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    public class LogsController : Controller
    {

        //
        // GET: /Logs/

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string nameAction = CommonHelper.GetSegment(3, curentPath);
            if (!string.IsNullOrEmpty(nameAction))
            {
                string idBloc = CommonHelper.GetSegment(4, curentPath);
                if (!string.IsNullOrEmpty(idBloc))
                {
                    int id = int.Parse(idBloc);

                    //BlocDTO bDto = blocService.AdminGetBlocById(id);
                    //ViewBag.OptionList = articleService.GetSelectList(bDto.ParrentId, null);
                    //int redirect = 0;
                    //ViewBag.OptionListRedirect = articleService.GetSelectList(redirect, null);
                    //return PartialView("../BackEnd/Bloc/Update", bDto);
                    return PartialView();
                }
                else
                {
                    // bao loi
                    return PartialView("../BackEnd/Logs/Update", null);
                }
            }
            else
            {
                Paging paging = new Paging();
                paging.Perpage = Constants.DEFAULT_ITEM;
                paging.Page = 1;
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
            }
            //return View();
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout)
        {
            try
            {
                int totalItem = LogServices.CountListLogByPerpage(fromDate, toDate, searchText);
                paging.TotalItems = totalItem;
                paging.TotalPage = totalItem / paging.Perpage;
                if (totalItem % paging.Perpage > 0)
                {
                    paging.TotalPage++;
                }
                if (paging.Page - 1 < 0)
                {
                    paging.Page = 1;
                }
                paging.Offset = (paging.Page - 1) * paging.Perpage;
                ViewBag.Paging = paging;
                ViewBag.SortBy = sortBy.ToUpper();
                ViewBag.SortType = sortType.ToUpper();
                ViewBag.AjaxUrl = Url.Content("~") + "Logs/Search/";

                List<LogModel> lstLogs = LogServices.GetListLogByPerpage(fromDate, toDate, searchText, paging, sortBy, sortType);

                if (hasLayout != null && hasLayout == true)
                {
                    return PartialView("../BackEnd/Logs/Index", lstLogs);
                }
                else
                {
                    return PartialView("../BackEnd/Logs/AjaxContent", lstLogs);
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
                return null;
            }
        }
    }
}
