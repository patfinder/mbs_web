﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.domain;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class AccountGroupsController : Controller
    {
        AdmincpController admin = new AdmincpController();

        string nameModule = "AccountGroups";
        static int IdModule = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = (int)module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        return PartialView("../BackEnd/Account_Groups/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;
            string nameAction = CommonHelper.GetSegment(3, curentPath);
            //SearchDTO searchDto = new SearchDTO();
            if (!string.IsNullOrEmpty(nameAction))
            {
                List<ModuleModel> lstModuleCommon = ModuleServices.GetAllModules();
                ViewBag.ListModule = lstModuleCommon;
                ViewBag.ListModuleLimit = Constants.LST_MODULES_INVISBLE;
                string idModule = CommonHelper.GetSegment(4, curentPath);
                if (!string.IsNullOrEmpty(idModule))
                {
                    try
                    {
                        int id = int.Parse(idModule);
                        AccountGroupModel group = AccountGroupServices.GetGroupById(id);
                        return PartialView("../BackEnd/Account_Groups/Update", group);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
                    }
                }
                else
                {
                    // them moi account group
                    AccountGroupModel group = new AccountGroupModel();
                    group.STATUS = 1;
                    group.PERMISSION = "";
                    return PartialView("../BackEnd/Account_Groups/Update", group);
                }
            }
            else
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, true);
            }
        }
        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout)
        {
            int totalItem = AccountGroupServices.CountListAccountGroupByPerpage(fromDate, toDate, searchText);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "AccountGroups/Search/";
            List<AccountGroupModel> lstAccGroup = AccountGroupServices.GetSortListAccountGroupByCondition(fromDate, toDate, searchText, paging, sortBy, sortType);
            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Account_Groups/Index", lstAccGroup);
            }
            else
            {
                return PartialView("../BackEnd/Account_Groups/AjaxLoadContent", lstAccGroup);
            }
        }

        [HttpPost]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string permDefault = "0|";
                    string perm = "";
                    List<ModuleModel> lstModule = ModuleServices.GetAllModules();
                    List<int> lst = Constants.LST_MODULES_INVISBLE;
                    lstModule = (from m in lstModule
                                 where lst.Contains((int)m.ID) == false
                                 select m).ToList();
                    if (Request["read0Admincp"] == "on")
                    {
                        permDefault += "r";
                    }
                    else
                    {
                        permDefault += "-";
                    }
                    if (Request["write0Admincp"] == "on")
                    {
                        permDefault += "w";
                    }
                    else
                    {
                        permDefault += "-";
                    }
                    if (Request["delete0Admincp"] == "on")
                    {
                        permDefault += "d";
                    }
                    else
                    {
                        permDefault += "-";
                    }

                    int idAccGroup = Convert.ToInt32(Request["hiddenIdAdmincp"]);
                    string nameGroup = Request["nameAdmincp"];
                    foreach (var m in lstModule)
                    {
                        perm += "," + m.ID.ToString() + "|";
                        if (Request["read" + m.ID.ToString() + "Admincp"] == "on")
                        {
                            perm += "r";
                        }
                        else
                        {
                            perm += "-";
                        }
                        if (Request["write" + m.ID.ToString() + "Admincp"] == "on")
                        {
                            perm += "w";
                        }
                        else
                        {
                            perm += "-";
                        }
                        if (Request["delete" + m.ID.ToString() + "Admincp"] == "on")
                        {
                            perm += "d";
                        }
                        else
                        {
                            perm += "-";
                        }
                    }
                    if (idAccGroup > 0)
                    {
                        AccountGroupModel group_old   = AccountGroupServices.GetGroupById(idAccGroup);
                        AccountGroupModel group = new AccountGroupModel();
                        group.ID            = idAccGroup;
                        group.NAME          = nameGroup;
                        group.PERMISSION    = permDefault + perm;
                        group.STATUS        = Request["statusAdmincp"] == "on" ? 1 : 0;
                        group.CREATED       = group_old.CREATED;

                        int result = AccountGroupServices.UpdateAccountGroup(group);

                        if (result == Constants.ACTION_SUCCESS)
                        {
                            //update permission of users inside this group
                            AdminAccountService.UpdateUserPermission(idAccGroup,permDefault + perm);

                            admin.SaveLog(nameModule, idAccGroup, "", "Update", group_old, group);
                            return "success";
                        }
                        else if (result == Constants.ACTION_EXIST_NAME)
                            return "name-group-exist";
                        return "fault";
                    }
                    else
                    {
                        // them moi 
                        AccountGroupModel group = new AccountGroupModel();
                        group.ID = idAccGroup;
                        group.NAME = nameGroup;
                        group.PERMISSION = permDefault + perm;
                        group.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;
                        group.CREATED = DateTime.Now;

                        int result = AccountGroupServices.AddNewAccountGroup(group);
                        if (result == Constants.ACTION_FAULT)
                            return "fault";
                        else if (result == Constants.ACTION_EXIST_NAME)
                            return "name-group-exist";
                        else
                        {
                            admin.SaveLog(nameModule, result, "Add new", "Add new");
                            return "success";
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteAccount(string listId, string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType)
        {
            //Check Perm of current user
            string permission = admin.GetPermission(IdModule, "d");
            if (permission == "d")
            {
                try
                {
                    string[] arrayId = listId.Split(',');
                    if (arrayId.Count() > 0)
                    {
                        List<int> lstId = new List<int>();
                        foreach (string item in arrayId)
                        {
                            lstId.Add(int.Parse(item.Trim()));
                            admin.SaveLog(nameModule, int.Parse(item.Trim()), "Delete", "Delete");
                        }
                        AccountGroupServices.DeleteGroupByArrayId(lstId);

                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(string listId, short status, string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType)
        {
            //Check Perm of current user
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string[] arrayId = listId.Split(',');
                    if (arrayId.Count() > 0)
                    {
                        List<int> lstId = new List<int>();
                        int oldSatus = (int)(status == 1 ? 0 : 1);
                        foreach (string item in arrayId)
                        {
                            lstId.Add(int.Parse(item.Trim()));
                            List<AdminAccountModel> lstAccounts = AccountGroupServices.GetAccountsByIdGroup(int.Parse(item.Trim()));
                            if (lstAccounts.Count > 0)
                            {
                                List<int> listAccountId = lstAccounts.Select(x => (int)x.ID).ToList();
                                if (listAccountId.Count > 0)
                                {
                                    foreach (int uid in listAccountId)
                                    {
                                        admin.SaveLog("Accounts", uid, "Status", "Update", oldSatus, status);
                                    }
                                    AdminAccountService.UpdateStatus(listAccountId, status);
                                }
                            }
                            admin.SaveLog(nameModule, int.Parse(item.Trim()), "Status", "Update", oldSatus, status);
                        }
                        AccountGroupServices.UpdateStatustByArrayId(lstId, status);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public string CheckDeleteGroup(string listId)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> lstId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        lstId.Add(int.Parse(item.Trim()));
                        List<AdminAccountModel> lstAccounts = AccountGroupServices.GetAccountsByIdGroup(int.Parse(item.Trim()));
                        if (lstAccounts.Count > 0)
                        {
                            return "fault";
                        }
                    }
                    return "success";
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return "fault";
        }
    }
}
