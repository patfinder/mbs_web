﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using WizardCMS.Const;
using System.Web.Configuration;
using System.IO;

namespace WizardCMS.Controllers
{
    public class TranslateController : Controller
    {
        string nameModule = "Translate";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            ViewBag.module = nameModule;

            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Translate/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            TranslateViewModel node = TranslateServices.Get(id);

                            return PartialView("../BackEnd/Translate/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Translate/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    TranslateViewModel node = new TranslateViewModel();

                    node.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    node.SLUG = Request["slugAdmincp"];
                    node.TEXT_VI = Request["textViAdmincp"];
                    node.TEXT_EN = Request["textEnAdmincp"];
                    node.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;
                    var image = string.Empty;

                    if (string.IsNullOrWhiteSpace(node.SLUG))
                    {
                        return "Chưa nhập key.";
                    }

                    if (string.IsNullOrWhiteSpace(node.TEXT_VI))
                    {
                        return "Chưa nhập nội dung tiếng Việt.";
                    }

                    if (string.IsNullOrWhiteSpace(node.TEXT_EN))
                    {
                        return "Chưa nhập nội dung tiếng Anh.";
                    }

                    if (node.ID > 0)
                    {
                        //update an article
                        TranslateViewModel nodeOld = TranslateServices.Get(node.ID);

                        if (TranslateServices.Update(node))
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {
                        int newID = TranslateServices.Insert(node);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (TranslateServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        TranslateServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    TranslateViewModel node = TranslateServices.Get(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        ArticlesServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = TranslateServices.Get(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    TranslateServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Slide";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Translate/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = TranslateServices.GetCount(fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Translate/Search/";

            List<TranslateViewModel> lstSlide = TranslateServices.GetList(fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Translate/Index", lstSlide);
            }
            else
            {
                return PartialView("../BackEnd/Translate/AjaxContent", lstSlide);
            }
        }

        //function upload image
        private string Upload(HttpPostedFileBase namefile, string dirUpload)
        {
            try
            {

                //if (!int.TryParse(maxAllowConfig, out maxAllow))
                //{
                //    maxAllow = 3;//default is 3MB
                //}

                //if (maxAllow <= 0)
                //{
                //    maxAllow = Constants.MAX_LENGTH_IMG;
                //}

                var supportedTypes = new[] { "jpg", "png" };
                //if (namefile.ContentLength > maxAllow)
                //{
                //    return Constants.ERROR_MAX_UPLOAD;
                //}

                if (namefile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(namefile.FileName);
                    var fileExt = System.IO.Path.GetExtension(namefile.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt.ToLower()) == true)
                    {
                        fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + "." + fileExt;
                        string monthUp = System.DateTime.Now.Month.ToString();
                        string yearUp = System.DateTime.Now.Year.ToString();
                        string directory = Server.MapPath(Url.Content("~") + dirUpload + yearUp + "/" + monthUp + "/");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);

                            //set permission
                            CommonHelper.SetPermission(directory);
                        }
                        string path = Path.Combine(directory, fileName);
                        namefile.SaveAs(path);
                        return yearUp + "/" + monthUp + "/" + fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }
    }
}
