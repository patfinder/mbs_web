﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Helpers;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    public class BaseController : Controller
    {
        protected override void ExecuteCore()
        {
            string cultureName = null;

            var langSession = Session["_culture"];
            if (langSession != null)
            {
                cultureName = langSession.ToString();
            }
            else
            {
                cultureName = "vi-VN"; //CultureHelper.GetDefaultCulture(); //Default lang is Vietnamese
                Session["_culture"] = cultureName;
            }

            Languages.currentLang = CultureHelper.SetCurrentCulture(cultureName);

            base.ExecuteCore();
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}