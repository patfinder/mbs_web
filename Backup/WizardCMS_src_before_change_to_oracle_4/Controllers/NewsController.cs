﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WizardCMS.Models;
using System.Collections;
using WizardCMS.Helpers;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Configuration;
using System.Web.Configuration;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class NewsController : Controller
    {
        string nameModule = "news";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);

            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/News/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string idArticle = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(idArticle) )
                    {
                        try
                        {
                            int id = int.Parse(idArticle);
                            MessageViewModel viewModel = new MessageViewModel();

                            return PartialView("../BackEnd/News/Update", viewModel);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/News/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        Session["back"] = curentPath;
                        string idArticle = CommonHelper.GetSegment(4, curentPath);
                        string category = Request.QueryString["category"];

                        int parentId = 0;
                        ViewBag.parentId = parentId;
                        Session["parentId"] = nameAction + "/" + parentId;
                        ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        if (Session["back"] != null)
                        {
                            string url = Session["back"] as string;
                            string idArticle = CommonHelper.GetSegment(4, url);
                            int parentId = int.Parse(idArticle);
                            ViewBag.parentId = parentId;
                            Session["parentId"] = nameAction + "/" + parentId;
                            ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                        return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                Session["parentId"] = null; //remove session
                string category = Request.QueryString["category"];
                int parentId = -1;
                if (category == "menu")
                {
                    parentId = 0;
                }
                else if (category == "all")
                {
                    parentId = -1;
                }
                else if (!string.IsNullOrEmpty(category))
                {
                    bool tmp = int.TryParse(category, out parentId);
                }
                return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    MessageViewModel viewModel = new MessageViewModel();

                    viewModel.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    if (viewModel.ID > 0)
                    {
                        //update an article
                        MessageViewModel oldModel = Search24Services.Get(viewModel.ID);

                        if (Search24Services.Update(viewModel))
                        {
                            //admin.SaveLog(nameModule, article.ID, "", "Update", articleOld, article);
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {
                        return Constants.PERMISSION_DENIED;

                        if (StringHelper.IsNullOrEmptyOrWhiteSpace(viewModel.TITLE))
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = Search24Services.Insert(viewModel);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            admin.SaveLog(nameModule, newID, "Add new", "Add new", "", "");
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                //KHÔNG CHO XÓA CONTENT TRONG DB SEARCH24
                return Constants.PERMISSION_DENIED;

                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        //MessageViewModel viewModel = Search24Services.Get(id);
                        if (Search24Services.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        listArtId.Add(id);

                        //MessageViewModel viewModel = Search24Services.Get(id);
                        //adminControl.SaveLog(nameModule, int.Parse(item.Trim()), "Delete", "Delete", "", "");

                    }
                    bool result = ArticlesServices.DeleteList(listArtId);

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, parentId, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    MessageViewModel viewModel = Search24Services.Get(id);
                    if (viewModel != null)
                    {
                        viewModel.STATUS = (int)(viewModel.STATUS == 2 ? 1 : 2);
                        Search24Services.UpdateStatus(id, viewModel.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = Search24Services.Get(id);
                if (ar != null)
                {
                    status = status == 2 ? 1 : 2;
                    ar.STATUS = status;
                    Search24Services.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "News";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/News/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, int? parentId, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            int totalItem = Search24Services.GetCount(fromDate, toDate, searchText);
            sw.Stop();
            Console.WriteLine("Thoi gian dem: " + sw.ElapsedMilliseconds);

            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.parentId = parentId;
            ViewBag.AjaxUrl = Url.Content("~") + "News/Search/";

            sw = System.Diagnostics.Stopwatch.StartNew();
            List<MessageViewModel> messages = Search24Services.GetList(fromDate, toDate, searchText, paging, sortBy, sortType);
            sw.Stop();
            Console.WriteLine("Thoi gian lay data: "+sw.ElapsedMilliseconds);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/News/Index", messages);
            }
            else
            {
                return PartialView("../BackEnd/News/AjaxContent", messages);
            }
        }

        //function upload image
        private string Upload(HttpPostedFileBase namefile, string dirUpload)
        {
            try
            {

                //if (!int.TryParse(maxAllowConfig, out maxAllow))
                //{
                //    maxAllow = 3;//default is 3MB
                //}

                //if (maxAllow <= 0)
                //{
                //    maxAllow = Constants.MAX_LENGTH_IMG;
                //}

                var supportedTypes = new[] { "jpg", "png", "pdf", "doc", "docx" };
                maxAllow = Constants.MAX_LENGTH_IMG * 10 ;
                if (namefile.ContentLength > maxAllow)
                {
                    return Constants.ERROR_MAX_UPLOAD;
                }

                if (namefile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(namefile.FileName);
                    var fileExt = System.IO.Path.GetExtension(namefile.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt.ToLower()) == true)
                    {
                        fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + "." + fileExt;
                        string monthUp = System.DateTime.Now.Month.ToString();
                        string yearUp = System.DateTime.Now.Year.ToString();
                        string directory = Server.MapPath(Url.Content("~") + dirUpload + yearUp + "/" + monthUp + "/");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);

                            //set permission
                            CommonHelper.SetPermission(directory);
                        }
                        string path = Path.Combine(directory, fileName);
                        namefile.SaveAs(path);
                        return yearUp + "/" + monthUp + "/" + fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        { 
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                    if (order == 0)
                    {
                        article.PRIORITY -= 1;
                        if (article.PRIORITY < 0)
                        {
                            article.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (article.PRIORITY + 1 < int.MaxValue)
                        {
                            article.PRIORITY += 1;
                        }
                    }
                    ArticlesServices.Update(article);
                    return article.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
