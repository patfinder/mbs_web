﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WizardCMS.Models;
using System.Collections;
using WizardCMS.Helpers;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Configuration;
using System.Web.Configuration;
using WizardCMS.Const;
using System.Xml;
using System.Xml.Linq;
using WizardCMS.domain;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class ArticleController : Controller
    {
        string nameModule = "article";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        string username_logged = string.Empty;

        public ArticleController() {
            Object account_logged = AuthorizeActionFilter.UserIsAuthenticated();
            username_logged = account_logged != null ? ((WizardCMS.Models.AdminAccountModel)account_logged).USERNAME : "";
        }

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);

            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Article/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string idArticle = CommonHelper.GetSegment(4, curentPath);
                    ViewBag.terms = TermServices.GetTerms();

                    if (!string.IsNullOrEmpty(idArticle) )
                    {
                        try
                        {
                            int id = int.Parse(idArticle);
                            ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);

                            ViewBag.OptionList = ArticlesServices.GetSelectList(article.PARENT_ID, null, id);
                            return PartialView("../BackEnd/Article/Update", article);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        ViewBag.OptionList = ArticlesServices.GetSelectList(0, null);
                        return PartialView("../BackEnd/Article/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        Session["back"] = curentPath;
                        string idArticle = CommonHelper.GetSegment(4, curentPath);
                        string category = Request.QueryString["category"];

                        int parentId = -1;
                        if (category == "menu")
                        {
                            parentId = 0;
                        }
                        else if (category == "all")
                        {
                            parentId = -1;
                        }
                        else if (!string.IsNullOrEmpty(category))
                        {
                            bool tmp = int.TryParse(category, out parentId);
                        }
                        ViewBag.parentId = parentId;
                        Session["parentId"] = nameAction + "/" + parentId;
                        ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        if (Session["back"] != null)
                        {
                            string url = Session["back"] as string;
                            string idArticle = CommonHelper.GetSegment(4, url);
                            int parentId = int.Parse(idArticle);
                            ViewBag.parentId = parentId;
                            Session["parentId"] = nameAction + "/" + parentId;
                            ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                        return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                Session["parentId"] = null; //remove session
                string category = Request.QueryString["category"];
                int parentId = -1;
                if (category == "menu")
                {
                    parentId = 0;
                }
                else if (category == "all")
                {
                    parentId = -1;
                }
                else if (!string.IsNullOrEmpty(category))
                {
                    bool tmp = int.TryParse(category, out parentId);
                }
                return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    ArticleViewModel article = new ArticleViewModel();

                    article.ID = StringHelper.IsNumber(Request["hiddenIdAdmincp"]);
                    article.FEATURED = Request["featured"] != null && Request["featured"] != string.Empty ? 1 : 0;
                    article.PARENT_ID = StringHelper.IsNumber(Request["parent_id"]);
                    article.TEMPLATE = StringHelper.IsNullOrEmptyOrWhiteSpace(Request["template"]) ? Constants.NORMAL_PAGE : Request["template"];
                    article.YEAR_REPORT = StringHelper.IsNumber(Request["year_report"]);
                    article.MAIN_MENU = Request["main_menu"] == "on" ? 1 : 0; //Request["main_menu"] == string.Empty ? 0 : int.Parse(Request["main_menu"]);
                    article.ADMIN_SUBMENU = Request["admin_submenu"] == "on" ? 1 : 0; //Request["admin_submenu"] == string.Empty ? 0 : int.Parse(Request["admin_submenu"]);
                    article.PRIORITY = StringHelper.IsNumber(Request["priority"]);
                    article.CID = StringHelper.IsNumber(Request["term"]);
                    article.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;
                    article.IS_DELETE = Request["deleteAdmincp"] == "on" ? 1 : 0;
                    article.IS_CHANGE_SLUG = Request["changeSlugAdmincp"] == "on" ? 1 : 0;
                    article.ICON = Request["iconAdmincp"];
                    article.LOCATION = Request["location"];

                    article.DEAD_LINE = DatetimeHelper.StringToDate(Request["dead_line"], "dd-MM-yyyy HH:mm");
                    article.CREATED = DatetimeHelper.StringToDate(Request["created"], "dd-MM-yyyy HH:mm");
                    article.OPEN_DAY = DatetimeHelper.StringToDate(Request["open_day"], "dd-MM-yyyy HH:mm");

                    foreach (var lang in Constants.Langs)
                    {
                        var title = Server.HtmlEncode(Request["title_" + lang]);
                        var sub_title = Server.HtmlEncode(Request["sub_title_" + lang]);
                        var description = Request["description_" + lang];
                        var content = Request["content_" + lang];
                        var slug = StringHelper.CreateSlug(Request["title_" + lang]);

                        var image = Request["image_" + lang];
                        var image_tablet = Request["image_tablet_" + lang];
                        var image_mobile = Request["image_mobile_" + lang];
                        var file = Request["file_" + lang];

                        /*
                        if (Request.Files != null && Request.Files["image_" + lang] != null)
                        {
                            HttpPostedFileBase namefile = Request.Files["image_" + lang];
                            image = this.Upload(namefile, Constants.DIR_ARTICLE);
                            if (image.Equals(Constants.ERROR_MAX_UPLOAD))
                            {
                                string language = lang == "vi" ? "Tiếng Việt" : "English";
                                return "File hình ảnh (" + language + ") vượt quá dung lượng cho phép.";//Constants.ERROR_MAX_UPLOAD;
                            }
                        }

                        if (Request.Files != null && Request.Files["header_image_" + lang] != null)
                        {
                            HttpPostedFileBase namefile = Request.Files["header_image_" + lang];
                            image = this.Upload(namefile, Constants.DIR_ARTICLE);
                            if (image.Equals(Constants.ERROR_MAX_UPLOAD))
                            {
                                string language = lang == "vi" ? "Tiếng Việt" : "English";
                                return "File hình header (" + language + ") vượt quá dung lượng cho phép.";//Constants.ERROR_MAX_UPLOAD;
                            }
                        }

                        if (Request.Files != null && Request.Files["file_" + lang] != null)
                        {
                            HttpPostedFileBase namefile = Request.Files["file_" + lang];
                            file = this.Upload(namefile, Constants.DIR_FILES);
                            if (file.Equals(Constants.ERROR_MAX_UPLOAD))
                            {
                                string language = lang == "vi" ? "Tiếng Việt" : "English";
                                return "File đính kèm (" + language + ") vượt quá dung lượng cho phép.";//Constants.ERROR_MAX_UPLOAD;
                            }
                        }
                        **/

                        //

                        //Validate data input
                        if (title == string.Empty) continue;

                        ArticleContent article_content = new ArticleContent();
                        article_content.NID = article.ID;
                        article_content.TITLE = title;
                        article_content.SUB_TITLE = sub_title;
                        article_content.SLUG = slug;
                        article_content.LINK = slug;
                        article_content.CONTENT = content;
                        article_content.DESCRIPTION = CommonHelper.CutText(description,490);
                        article_content.IMAGE = image;
                        article_content.IMAGE_TABLET = image_tablet;
                        article_content.IMAGE_MOBILE = image_mobile;
                        article_content.LANG = lang;
                        article_content.ATTACHMENT = file;

                        article.ContentTranslates.Add(lang, article_content);
                    }

                    if (article.ContentTranslates == null || article.ContentTranslates.Count == 0)
                    {
                        return "Chưa nhập nội dung bài viết.";
                    }

                    if (article.ID > 0)
                    {
                        if (article.PARENT_ID == article.ID)
                        {
                            return "Không thể chọn bài viết này làm danh mục cha được.";
                        }

                        //update an article
                        ArticleViewModel articleOld = ArticlesServices.AdminGetArticalById(article.ID);
                        #region check data update
                        if (articleOld.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in articleOld.ContentTranslates)
                            {
                                ArticleContent old_content = item.Value;

                                if (article.ContentTranslates != null)
                                {
                                    if (article.ContentTranslates.ContainsKey(old_content.LANG))
                                    {
                                        if (username_logged != "root" && articleOld.IS_CHANGE_SLUG == 1)
                                        {
                                            article.ContentTranslates[old_content.LANG].SLUG = old_content.SLUG;
                                            article.ContentTranslates[old_content.LANG].LINK = old_content.LINK;
                                            article.PARENT_ID = articleOld.PARENT_ID;
                                        }
                                        
                                        if (article.ContentTranslates[old_content.LANG].SLUG != old_content.SLUG)
                                        {
                                            if (ArticlesServices.CheckSlug(article.ContentTranslates[old_content.LANG].SLUG, old_content.LANG, article.PARENT_ID))
                                                 return "Trùng tiêu đề bài viết.";
                                        }

                                        /* Xử lý file cũ
                                        if (article.ContentTranslates[old_content.LANG].IMAGE == string.Empty)
                                            article.ContentTranslates[old_content.LANG].IMAGE = old_content.IMAGE;
                                        else
                                        {
                                            //remove old image
                                            string FileToDelete = Server.MapPath("~/" + Constants.DIR_ARTICLE + old_content.IMAGE);
                                            if (System.IO.File.Exists(FileToDelete)) 
                                            {
                                                System.IO.File.Delete(FileToDelete);
                                            }
                                        }

                                        if (article.ContentTranslates[old_content.LANG].ATTACHMENT == string.Empty)
                                            article.ContentTranslates[old_content.LANG].ATTACHMENT = old_content.ATTACHMENT;
                                        else
                                        {
                                            //remove old file
                                            string FileToDelete = Server.MapPath("~/" + Constants.DIR_FILES + old_content.ATTACHMENT);
                                            if (System.IO.File.Exists(FileToDelete))
                                            {
                                                System.IO.File.Delete(FileToDelete);
                                            }
                                        }
                                        */
                                    }
                                    else
                                    {
                                        ArticlesServices.DeleteContent(old_content.NID, old_content.LANG);
                                        //article.ContentTranslates.Add(old_content.LANG, old_content);
                                    }
                                }
                                else
                                {
                                    article.ContentTranslates = new Dictionary<string, ArticleContent>();
                                    article.ContentTranslates.Add(old_content.LANG, old_content);
                                }
                            }
                        }
                        #endregion

                        if (ArticlesServices.Update(article))
                        {
                            //admin.SaveLog(nameModule, article.ID, "", "Update", articleOld, article);

                            //Log update content
                            /*
                            if (articleOld.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, ArticleContent> item in articleOld.ContentTranslates)
                                {
                                    ArticleContent old_content = item.Value;

                                    if (article.ContentTranslates != null)
                                    {
                                        if (article.ContentTranslates.ContainsKey(old_content.LANG))
                                        {
                                            admin.SaveLog(nameModule, article.ID, "", "Update", articleOld.ContentTranslates[old_content.LANG], article.ContentTranslates[old_content.LANG]);
                                        }
                                    }
                                }
                            }
                             * */
                            //end log update content

                            if (article.PARENT_ID > 0)
                            {
                                ArticlesServices.UpdateLinkCustom(article.PARENT_ID);
                            }
                            else
                            {
                                ArticlesServices.UpdateLinkCustom(article.ID);
                            }
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {

                        if (article.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in article.ContentTranslates)
                            {
                                ArticleContent article_content = item.Value;
                                if (ArticlesServices.CheckSlug(article_content.SLUG, article_content.LANG, article.PARENT_ID))
                                    return "Trùng tiêu đề bài viết.";
                            }
                        }
                        else
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = ArticlesServices.Insert(article);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            admin.SaveLog(nameModule, newID, "Add new", "Add new", "", "");

                            if (article.PARENT_ID > 0)
                            {
                                ArticlesServices.UpdateLinkCustom(article.PARENT_ID);
                            }
                            else
                            {
                                ArticlesServices.UpdateLinkCustom(newID);
                            }
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);

                        if (article.IS_DELETE == 0 && ArticlesServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string delete_file(int nid, string field)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (nid > 0)
                    {
                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(nid);

                        if (ArticlesServices.DeleteFile(nid, field))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        listArtId.Add(id);

                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                        //adminControl.SaveLog(nameModule, int.Parse(item.Trim()), "Delete", "Delete", "", "");

                    }
                    bool result = ArticlesServices.DeleteList(listArtId);

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, parentId, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    ArticleViewModel article = ArticlesServices.GetArticleById(id);
                    if (article != null)
                    {
                        article.STATUS = (int)(article.STATUS == 1 ? 0 : 1);
                        ArticlesServices.UpdateStatus(id, article.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = ArticlesServices.AdminGetArticalById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    ArticlesServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Article";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Article/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            int totalItem = ArticlesServices.GetCount(parentId, fromDate, toDate, searchText, lang);
            sw.Stop();
            Console.WriteLine("Thoi gian dem: " + sw.ElapsedMilliseconds);

            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.parentId = parentId;
            ViewBag.AjaxUrl = Url.Content("~") + "Article/Search/";

            sw = System.Diagnostics.Stopwatch.StartNew();
            List<ArticleViewModel> lstArticle = ArticlesServices.GetArticleByPaging(parentId, fromDate, toDate, searchText, paging, sortBy, sortType, lang);
            sw.Stop();
            Console.WriteLine("Thoi gian lay data: "+sw.ElapsedMilliseconds);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Article/Index", lstArticle);
            }
            else
            {
                return PartialView("../BackEnd/Article/AjaxContent", lstArticle);
            }
        }

        //function upload image
        private string Upload(HttpPostedFileBase namefile, string dirUpload)
        {
            try
            {

                //if (!int.TryParse(maxAllowConfig, out maxAllow))
                //{
                //    maxAllow = 3;//default is 3MB
                //}

                //if (maxAllow <= 0)
                //{
                //    maxAllow = Constants.MAX_LENGTH_IMG;
                //}

                var supportedTypes = new[] { "jpg", "png", "pdf", "doc", "docx" };
                maxAllow = Constants.MAX_LENGTH_IMG * 10 ;
                if (namefile.ContentLength > maxAllow)
                {
                    return Constants.ERROR_MAX_UPLOAD;
                }

                if (namefile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(namefile.FileName);
                    var fileExt = System.IO.Path.GetExtension(namefile.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt.ToLower()) == true)
                    {
                        fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + "." + fileExt;
                        string monthUp = System.DateTime.Now.Month.ToString();
                        string yearUp = System.DateTime.Now.Year.ToString();
                        string directory = Server.MapPath(Url.Content("~") + dirUpload + yearUp + "/" + monthUp + "/");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);

                            //set permission
                            CommonHelper.SetPermission(directory);
                        }
                        string path = Path.Combine(directory, fileName);
                        namefile.SaveAs(path);
                        return yearUp + "/" + monthUp + "/" + fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        { 
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                    if (order == 0)
                    {
                        article.PRIORITY -= 1;
                        if (article.PRIORITY < 0)
                        {
                            article.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (article.PRIORITY + 1 < int.MaxValue)
                        {
                            article.PRIORITY += 1;
                        }
                    }
                    ArticlesServices.Update(article);
                    return article.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }

        public string ExportArticle()
        {
            XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles.xml"), System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("language");

            int count = 0;

            using (var context = new WizardCMS.domain.DBContext())
            {
                var result = context.WZ_ARTICLES.ToList();
                if (result != null)
                {
                    foreach (var node in result)
                    {
                        if (node != null) 
                        {
                            var dead_line = node.DEAD_LINE != null ? node.DEAD_LINE.Value.ToString("dd-MM-yyyy HH:mm") : "";
                            var date_post = node.DATE_POST != null ? node.DATE_POST.Value.ToString("dd-MM-yyyy HH:mm") : "";
                            var open_date = node.OPEN_DAY != null ? node.OPEN_DAY.Value.ToString("dd-MM-yyyy HH:mm") : "";
                            writer.WriteStartElement("item");
                            writer.WriteStartElement("id"); writer.WriteString(node.ID.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("status"); writer.WriteString(node.STATUS.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("created"); writer.WriteString(node.CREATED.ToString("dd-MM-yyyy HH:mm")); writer.WriteEndElement();
                            writer.WriteStartElement("modified"); writer.WriteString(node.MODIFIED.ToString("dd-MM-yyyy HH:mm")); writer.WriteEndElement();
                            writer.WriteStartElement("parent_id"); writer.WriteString(node.PARENT_ID.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("priority"); writer.WriteString(node.PRIORITY.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("template"); writer.WriteString(node.TEMPLATE); writer.WriteEndElement();
                            writer.WriteStartElement("main_menu"); writer.WriteString(node.MAIN_MENU.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("admin_submenu"); writer.WriteString(node.ADMIN_SUBMENU.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("views"); writer.WriteString(node.VIEWS.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("featured"); writer.WriteString(node.FEATURED.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("year_report"); writer.WriteString(node.YEAR_REPORT.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("icon"); writer.WriteString(node.ICON); writer.WriteEndElement();
                            writer.WriteStartElement("cid"); writer.WriteString(node.CID.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("type"); writer.WriteString(node.TYPE.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("is_delete"); writer.WriteString(node.IS_DELETE.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("is_change_slug"); writer.WriteString(node.IS_CHANGE_SLUG.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("dead_line"); writer.WriteString(dead_line); writer.WriteEndElement();
                            writer.WriteStartElement("date_post"); writer.WriteString(date_post); writer.WriteEndElement();
                            writer.WriteStartElement("open_date"); writer.WriteString(open_date); writer.WriteEndElement();
                            writer.WriteStartElement("location"); writer.WriteString(node.LOCATION); writer.WriteEndElement();
                            writer.WriteEndElement();

                            count++;
                        }
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            return "DONE - TOTAL " + count;
        }
        public string ImportArticle()
        {
            try
            {
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles.xml"));
                var node_contents = xml.Elements("language").Elements("item").ToList();

                if (node_contents != null)
                {
                    foreach (var temp in node_contents)
                    {
                        WZ_ARTICLES node = new WZ_ARTICLES();
                        node.ID = StringHelper.IsNumber(temp.Element("id").Value.ToString());
                        node.STATUS = StringHelper.IsNumber(temp.Element("status").Value.ToString());
                        node.PARENT_ID = StringHelper.IsNumber(temp.Element("parent_id").Value.ToString());
                        node.PRIORITY = StringHelper.IsNumber(temp.Element("priority").Value.ToString());
                        node.VIEWS = StringHelper.IsNumber(temp.Element("views").Value.ToString());
                        node.CID = StringHelper.IsNumber(temp.Element("cid").Value.ToString());
                        node.TYPE = StringHelper.IsNumber(temp.Element("type").Value.ToString());
                        node.MAIN_MENU = StringHelper.IsNumber(temp.Element("main_menu").Value.ToString());
                        node.ADMIN_SUBMENU = StringHelper.IsNumber(temp.Element("admin_submenu").Value.ToString());
                        node.FEATURED = StringHelper.IsNumber(temp.Element("featured").Value.ToString());
                        node.YEAR_REPORT = StringHelper.IsNumber(temp.Element("year_report").Value.ToString());
                        node.IS_DELETE = StringHelper.IsNumber(temp.Element("is_delete").Value.ToString());
                        node.IS_CHANGE_SLUG = StringHelper.IsNumber(temp.Element("is_change_slug").Value.ToString());
                        node.CREATED = temp.Element("created").Value != null ? DatetimeHelper.StringToDate(temp.Element("created").Value.ToString(), DatetimeHelper.DD_MM_YYYY_HH_MM) : DateTime.Now;
                        node.MODIFIED = temp.Element("modified").Value != null ? DatetimeHelper.StringToDate(temp.Element("modified").Value.ToString(), DatetimeHelper.DD_MM_YYYY_HH_MM) : DateTime.Now;
                        node.TEMPLATE = temp.Element("template").Value.ToString();
                        node.ICON = temp.Element("icon").Value.ToString();
                        node.DEAD_LINE = temp.Element("dead_line").Value != null ? DatetimeHelper.StringToDate(temp.Element("dead_line").Value.ToString(), DatetimeHelper.DD_MM_YYYY_HH_MM) : DateTime.Now;
                        node.DATE_POST = temp.Element("date_post").Value != null ? DatetimeHelper.StringToDate(temp.Element("date_post").Value.ToString(), DatetimeHelper.DD_MM_YYYY_HH_MM) : DateTime.Now;
                        node.OPEN_DAY = temp.Element("open_date").Value != null ? DatetimeHelper.StringToDate(temp.Element("open_date").Value.ToString(), DatetimeHelper.DD_MM_YYYY_HH_MM) : DateTime.Now;
                        node.LOCATION = temp.Element("location").Value.ToString();

                        using (DBContext context = new DBContext())
                        {
                            var node_check = context.WZ_ARTICLES.Where(n => n.ID == node.ID).FirstOrDefault();
                            if (node_check != null)
                            {
                                node_check.STATUS = node.STATUS;
                                node_check.PARENT_ID = node.PARENT_ID;
                                node_check.PRIORITY = node.PRIORITY;
                                node_check.VIEWS = node.VIEWS;
                                node_check.CID = node.CID;
                                node_check.TYPE = node.TYPE;
                                node_check.MAIN_MENU = node.MAIN_MENU;
                                node_check.ADMIN_SUBMENU = node.ADMIN_SUBMENU;
                                node_check.FEATURED = node.FEATURED;
                                node_check.YEAR_REPORT = node.YEAR_REPORT;
                                node_check.IS_DELETE = node.IS_DELETE;
                                node_check.IS_CHANGE_SLUG = node.IS_CHANGE_SLUG;
                                node_check.CREATED = node.CREATED;
                                node_check.MODIFIED = node.MODIFIED;
                                node_check.TEMPLATE = node.TEMPLATE;
                                node_check.ICON = node.ICON;
                                node_check.DEAD_LINE = node.DEAD_LINE;
                                node_check.DATE_POST = node.DATE_POST;
                                node_check.OPEN_DAY = node.OPEN_DAY;
                                node_check.LOCATION = node.LOCATION;

                                //context.SaveChanges();
                            }
                            else
                            {
                                context.WZ_ARTICLES.AddObject(node);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    return "";
                }
                return "DONE";
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.Message);
                Logs.WriteLogError(ex.StackTrace);
            }
            return "EXCEPTIONS - SEE LOGS FOR DETAIL.";
        }

        public string ExportEnglish()
        {
            XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles_english.xml"), System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("language");

            using (var context = new WizardCMS.domain.DBContext())
            {
                var result = context.WZ_ARTICLES.ToList();
                if (result != null)
                {
                    foreach (var node in result) {
                        var node_content = context.WZ_ARTICLES_CONTENT.Where(n => n.NID == node.ID && n.LANG == Constants.LANG_EN).FirstOrDefault();
                        if (node_content != null) {
                            writer.WriteStartElement("item");
                                writer.WriteStartElement("nid"); writer.WriteString(node_content.NID.ToString()); writer.WriteEndElement();
                                writer.WriteStartElement("title"); writer.WriteString(node_content.TITLE); writer.WriteEndElement();
                                writer.WriteStartElement("slug"); writer.WriteString(node_content.SLUG); writer.WriteEndElement();
                                writer.WriteStartElement("description"); writer.WriteString(node_content.DESCRIPTION); writer.WriteEndElement();
                                writer.WriteStartElement("lang"); writer.WriteString(node_content.LANG); writer.WriteEndElement();
                                writer.WriteStartElement("image"); writer.WriteString(node_content.IMAGE); writer.WriteEndElement();
                                writer.WriteStartElement("link"); writer.WriteString(node_content.LINK); writer.WriteEndElement();
                                writer.WriteStartElement("content"); writer.WriteString(node_content.CONTENT); writer.WriteEndElement();
                                writer.WriteStartElement("sub_title"); writer.WriteString(node_content.SUB_TITLE); writer.WriteEndElement();
                                writer.WriteStartElement("attachment"); writer.WriteString(node_content.ATTACHMENT); writer.WriteEndElement();
                                writer.WriteStartElement("image_table"); writer.WriteString(node_content.IMAGE_TABLET); writer.WriteEndElement();
                                writer.WriteStartElement("image_mobile"); writer.WriteString(node_content.IMAGE_MOBILE); writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            return "DONE";
        }

        public string ImportEnglish()
        {
            try
            {
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles_english.xml"));
                var node_contents = xml.Elements("language").Elements("item").ToList();

                if (node_contents != null)
                {
                    foreach (var temp in node_contents)
                    {
                        WZ_ARTICLES_CONTENT node_content = new WZ_ARTICLES_CONTENT();
                        node_content.NID = StringHelper.IsNumber(temp.Element("nid").Value.ToString());
                        node_content.LANG = temp.Element("lang").Value.ToString();
                        node_content.TITLE = temp.Element("title").Value.ToString();
                        node_content.SLUG = temp.Element("slug").Value.ToString();
                        node_content.DESCRIPTION = temp.Element("description").Value.ToString();
                        node_content.IMAGE = temp.Element("image").Value.ToString();
                        node_content.LINK = temp.Element("link").Value.ToString();
                        node_content.CONTENT = temp.Element("content").Value.ToString();
                        node_content.SUB_TITLE = temp.Element("sub_title").Value.ToString();
                        node_content.ATTACHMENT = temp.Element("attachment").Value.ToString();

                        using (DBContext context = new DBContext())
                        {
                            var check_parent = context.WZ_ARTICLES.Where(n => n.ID == node_content.NID).FirstOrDefault();
                            if (check_parent == null) continue; //BỎ QUA NẾU KO CÓ NODE CHA

                            var node_check = context.WZ_ARTICLES_CONTENT.Where(nc => nc.LANG == node_content.LANG && nc.NID == node_content.NID).FirstOrDefault();
                            if (node_check != null)
                            {
                                node_check.TITLE = temp.Element("title").Value.ToString();
                                node_check.SLUG = temp.Element("slug").Value.ToString();
                                node_check.DESCRIPTION = temp.Element("description").Value.ToString();
                                node_check.IMAGE = temp.Element("image").Value.ToString();
                                node_check.LINK = temp.Element("link").Value.ToString();
                                node_check.CONTENT = temp.Element("content").Value.ToString();
                                node_check.SUB_TITLE = temp.Element("sub_title").Value.ToString();
                                node_check.ATTACHMENT = temp.Element("attachment").Value.ToString();

                                context.SaveChanges();
                            }
                            else
                            {
                                context.WZ_ARTICLES_CONTENT.AddObject(node_content);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    return "";
                }
                return "DONE";
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.Message);
                Logs.WriteLogError(ex.StackTrace);
            }
            return "EXCEPTIONS - SEE LOGS FOR DETAIL.";

        }

        public string ExportVietnam()
        {
            XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles_vietnam.xml"), System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("language");

            using (var context = new WizardCMS.domain.DBContext())
            {
                var result = context.WZ_ARTICLES.ToList();
                if (result != null)
                {
                    foreach (var node in result)
                    {
                        var node_content = context.WZ_ARTICLES_CONTENT.Where(n => n.NID == node.ID && n.LANG == Constants.LANG_VI).FirstOrDefault();
                        if (node_content != null)
                        {
                            writer.WriteStartElement("item");
                            writer.WriteStartElement("nid"); writer.WriteString(node_content.NID.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("title"); writer.WriteString(node_content.TITLE); writer.WriteEndElement();
                            writer.WriteStartElement("slug"); writer.WriteString(node_content.SLUG); writer.WriteEndElement();
                            writer.WriteStartElement("description"); writer.WriteString(node_content.DESCRIPTION); writer.WriteEndElement();
                            writer.WriteStartElement("lang"); writer.WriteString(node_content.LANG); writer.WriteEndElement();
                            writer.WriteStartElement("image"); writer.WriteString(node_content.IMAGE); writer.WriteEndElement();
                            writer.WriteStartElement("link"); writer.WriteString(node_content.LINK); writer.WriteEndElement();
                            writer.WriteStartElement("content"); writer.WriteString(node_content.CONTENT); writer.WriteEndElement();
                            writer.WriteStartElement("sub_title"); writer.WriteString(node_content.SUB_TITLE); writer.WriteEndElement();
                            writer.WriteStartElement("attachment"); writer.WriteString(node_content.ATTACHMENT); writer.WriteEndElement();
                            writer.WriteStartElement("image_table"); writer.WriteString(node_content.IMAGE_TABLET); writer.WriteEndElement();
                            writer.WriteStartElement("image_mobile"); writer.WriteString(node_content.IMAGE_MOBILE); writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            return "DONE";
        }

        public string UpdateVietnam()
        {
            try
            {
                string log = "";
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/articles_vietnam.xml"));
                var node_contents = xml.Elements("language").Elements("item").ToList();

                if (node_contents != null)
                {
                    foreach (var temp in node_contents)
                    {
                        WZ_ARTICLES_CONTENT node_content = new WZ_ARTICLES_CONTENT();
                        node_content.NID = StringHelper.IsNumber(temp.Element("nid").Value.ToString());
                        node_content.LANG = _get_field_xnode(temp, "lang");
                        node_content.TITLE = _get_field_xnode(temp, "title");
                        node_content.SLUG = _get_field_xnode(temp, "slug");
                        node_content.DESCRIPTION = _get_field_xnode(temp, "description");
                        node_content.IMAGE = _get_field_xnode(temp, "image");
                        node_content.LINK = _get_field_xnode(temp, "link");
                        node_content.CONTENT = _get_field_xnode(temp, "content");
                        node_content.SUB_TITLE = _get_field_xnode(temp, "sub_title");
                        node_content.ATTACHMENT = _get_field_xnode(temp, "attachment");

                        using (DBContext context = new DBContext())
                        {
                            var node_check = context.WZ_ARTICLES_CONTENT.Where(nc => nc.LANG == node_content.LANG && nc.NID == node_content.NID).FirstOrDefault();
                            if (node_check != null)
                            {
                                //ko update, chi bo sung content thieu, do qua tring import cua navicat
                            }
                            else
                            {
                                log += node_content.TITLE + "\r\n";
                                context.WZ_ARTICLES_CONTENT.AddObject(node_content);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    return "NO CONTENT FOUND";
                }

                return log; // "DONE";
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.Message);
                Logs.WriteLogError(ex.StackTrace);
            }
            return "EXCEPTIONS - SEE LOGS FOR DETAIL.";
        }

        private string _get_field_xnode(XElement node, string field)
        {
            try
            {
                return node.Element(field).Value.ToString();
            }
            catch
            { 
            }
            return string.Empty;
        }

        public bool ExportTerm(string lang="en")
        {
            try
            {
                XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/term_" + lang + ".xml"), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("language");

                using (var context = new WizardCMS.domain.DBContext())
                {
                    var result = context.WZ_TERM.ToList();
                    if (result != null)
                    {
                        foreach (var node in result)
                        {
                            var node_content = context.WZ_TERM_CONTENT.Where(n => n.TID == node.ID && n.LANG == lang).FirstOrDefault();
                            if (node_content != null)
                            {
                                writer.WriteStartElement("item");
                                writer.WriteStartElement("tid"); writer.WriteString(node_content.TID.ToString()); writer.WriteEndElement();
                                writer.WriteStartElement("name"); writer.WriteString(node_content.NAME); writer.WriteEndElement();
                                writer.WriteStartElement("slug"); writer.WriteString(node_content.SLUG); writer.WriteEndElement();
                                writer.WriteStartElement("description"); writer.WriteString(node_content.DESCRIPTION); writer.WriteEndElement();
                                writer.WriteStartElement("lang"); writer.WriteString(node_content.LANG); writer.WriteEndElement();
                                writer.WriteEndElement();
                            }
                        }
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        public bool ImportTerm(string lang = "en")
        {
            try
            {
                string log = "";
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/term_"+lang+".xml"));
                var node_contents = xml.Elements("language").Elements("item").ToList();

                if (node_contents != null)
                {
                    foreach (var temp in node_contents)
                    {
                        WZ_TERM_CONTENT node_content = new WZ_TERM_CONTENT();
                        node_content.TID = StringHelper.IsNumber(temp.Element("tid").Value.ToString());
                        node_content.LANG = temp.Element("lang").Value.ToString();
                        node_content.NAME = temp.Element("name").Value.ToString();
                        node_content.SLUG = temp.Element("slug").Value.ToString();
                        node_content.DESCRIPTION = temp.Element("description").Value.ToString();

                        using (DBContext context = new DBContext())
                        {
                            var check_parent = context.WZ_TERM.Where(n => n.ID == node_content.TID).FirstOrDefault();
                            if (check_parent == null) continue; //BỎ QUA NẾU KO CÓ NODE CHA

                            var node_check = context.WZ_TERM_CONTENT.Where(nc => nc.LANG == node_content.LANG && nc.TID == node_content.TID).FirstOrDefault();
                            if (node_check != null)
                            {
                                node_check.NAME = node_content.NAME;
                                node_check.SLUG = node_content.SLUG;
                                node_check.DESCRIPTION = node_content.DESCRIPTION;
                                context.SaveChanges();
                            }
                            else
                            {
                                log += node_content.NAME + "\r\n";
                                context.WZ_TERM_CONTENT.AddObject(node_content);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        //
        public bool ExportBranch(string lang = "en")
        {
            try
            {
                XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/branch_" + lang + ".xml"), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("language");

                using (var context = new WizardCMS.domain.DBContext())
                {
                    var result = context.WZ_BRANCH.ToList();
                    if (result != null)
                    {
                        foreach (var node in result)
                        {
                            var node_content = context.WZ_TERM_CONTENT.Where(n => n.TID == node.ID && n.LANG == lang).FirstOrDefault();
                            if (node_content != null)
                            {
                                writer.WriteStartElement("item");
                                writer.WriteStartElement("tid"); writer.WriteString(node_content.TID.ToString()); writer.WriteEndElement();
                                writer.WriteStartElement("name"); writer.WriteString(node_content.NAME); writer.WriteEndElement();
                                writer.WriteStartElement("slug"); writer.WriteString(node_content.SLUG); writer.WriteEndElement();
                                writer.WriteStartElement("description"); writer.WriteString(node_content.DESCRIPTION); writer.WriteEndElement();
                                writer.WriteStartElement("lang"); writer.WriteString(node_content.LANG); writer.WriteEndElement();
                                writer.WriteEndElement();
                            }
                        }
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        public bool ImportBranch(string lang = "en")
        {
            try
            {
                string log = "";
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/branch_" + lang + ".xml"));
                var node_contents = xml.Elements("language").Elements("item").ToList();

                if (node_contents != null)
                {
                    foreach (var temp in node_contents)
                    {
                        WZ_TERM_CONTENT node_content = new WZ_TERM_CONTENT();
                        node_content.TID = StringHelper.IsNumber(temp.Element("tid").Value.ToString());
                        node_content.LANG = temp.Element("lang").Value.ToString();
                        node_content.NAME = temp.Element("name").Value.ToString();
                        node_content.SLUG = temp.Element("slug").Value.ToString();
                        node_content.DESCRIPTION = temp.Element("description").Value.ToString();

                        using (DBContext context = new DBContext())
                        {
                            var node_check = context.WZ_TERM_CONTENT.Where(nc => nc.LANG == node_content.LANG && nc.TID == node_content.TID).FirstOrDefault();
                            if (node_check != null)
                            {
                                node_check.NAME = node_content.NAME;
                                node_check.SLUG = node_content.SLUG;
                                node_check.DESCRIPTION = node_content.DESCRIPTION;
                                context.SaveChanges();
                            }
                            else
                            {
                                log += node_content.NAME + "\r\n";
                                context.WZ_TERM_CONTENT.AddObject(node_content);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {

                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return false;
        }

        public bool CopyImage() 
        {
            using (var context = new WizardCMS.domain.DBContext())
            {
                string appPath = Request.PhysicalApplicationPath;
                var result = context.WZ_ARTICLES.ToList();
                if (result != null)
                {
                    foreach (var node in result)
                    {
                        var node_content = context.WZ_ARTICLES_CONTENT.Where(n => n.NID == node.ID && n.LANG == Constants.LANG_EN).FirstOrDefault();
                        if (node_content != null)
                        {
                            if (!string.IsNullOrEmpty(node_content.IMAGE))
                            {
                                string filePath = string.Empty;
                                filePath = appPath + node_content.IMAGE.Replace('/', '\\');
                                filePath = filePath.Replace("\\\\","\\");
                                if (!System.IO.File.Exists(filePath))
                                {
                                    continue;
                                }

                                FileInfo fileInfo = new FileInfo(filePath);
                                string dir = fileInfo.DirectoryName;
                                dir = dir.Replace("uploads", "App_Data\\uploads");
                                if (!System.IO.Directory.Exists(dir))
                                {
                                    System.IO.Directory.CreateDirectory(dir);
                                }
                                var destPath = "";
                                if (node_content.IMAGE.StartsWith("/"))
                                {
                                    destPath = "/App_Data" + node_content.IMAGE;
                                }
                                else
                                {
                                    destPath = "/App_Data/" + node_content.IMAGE;
                                }

                                var destFilePath = appPath + destPath.Replace('/', '\\');
                                destFilePath = destFilePath.Replace("\\\\", "\\");
                                if (System.IO.File.Exists(destFilePath))
                                {
                                    continue;
                                }

                                try {
                                    System.IO.File.Copy(filePath, destFilePath, true);
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
