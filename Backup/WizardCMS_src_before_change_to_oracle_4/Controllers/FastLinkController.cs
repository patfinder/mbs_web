﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WizardCMS.Models;
using System.Collections;
using WizardCMS.Helpers;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Configuration;
using System.Web.Configuration;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class FastLinkController : Controller
    {
        string nameModule = "fastlink";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            ViewBag.module = "fastlink";

            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/FastLink/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nid = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(nid) )
                    {
                        try
                        {
                            int id = int.Parse(nid);
                            FastLinkViewModel article = FastLinkServices.AdminGetById(id);

                            return PartialView("../BackEnd/FastLink/Update", article);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/FastLink/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        Session["back"] = curentPath;
                        string nid = CommonHelper.GetSegment(4, curentPath);
                        string category = Request.QueryString["category"];

                        return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage content
            {
                string category = Request.QueryString["category"];
                return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    FastLinkViewModel viewModel = new FastLinkViewModel();

                    viewModel.ID = StringHelper.IsNumber(Request["hiddenIdAdmincp"]);
                    viewModel.TYPE = StringHelper.IsNumber(Request["typeAdmincp"]);
                    viewModel.ICON = Request["iconAdmincp"];
                    viewModel.ICON_HOVER = Request["iconHoverAdmincp"];
                    viewModel.IMAGE = Request["imageAdmincp"];
                    viewModel.PRIORITY = Request["priority"] == string.Empty ? 0 : int.Parse(Request["priority"]);
                    viewModel.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;
                    viewModel.TURN_ON = Request["turnOnAdmincp"] == "on" ? 1 : 0;

                    foreach (var lang in Constants.Langs)
                    {
                        var title = Request["title_"+lang];
                        var link = Request["link_" + lang];

                        if (title == string.Empty) continue;

                        FastLinkContent node_content = new FastLinkContent();
                        node_content.NAME = title;
                        node_content.LINK = link;
                        node_content.LANG = lang;

                        viewModel.ContentTranslates.Add(lang, node_content);
                    }

                    if (viewModel.ID > 0)
                    {
                        //update an node
                        if (FastLinkServices.Update(viewModel))
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {

                        if (viewModel.ContentTranslates == null)
                        {
                            return "Vui lòng nhập nội dung liên kết.";
                        }

                        int newID = FastLinkServices.Insert(viewModel);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        FastLinkViewModel node = FastLinkServices.AdminGetById(id);
                        
                        if (FastLinkServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }
        [HttpPost]
        public string delete_file(int nid, string field)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (nid > 0)
                    {
                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(nid);

                        if (ArticlesServices.DeleteFile(nid, field))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }
        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        listArtId.Add(id);
                    }
                    bool result = FastLinkServices.DeleteList(listArtId);

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, parentId, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    FastLinkViewModel node = FastLinkServices.GetById(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        FastLinkServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = FastLinkServices.GetById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    FastLinkServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "fastlink";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/FastLink/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = FastLinkServices.GetCount(parentId, fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.parentId = parentId;
            ViewBag.AjaxUrl = Url.Content("~") + "FastLink/Search/";

            List<FastLinkViewModel> lstArticle = FastLinkServices.GetByPaging(parentId, fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/FastLink/Index", lstArticle);
            }
            else
            {
                return PartialView("../BackEnd/FastLink/AjaxContent", lstArticle);
            }
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        { 
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    FastLinkViewModel article = FastLinkServices.AdminGetById(id);
                    if (order == 0)
                    {
                        article.PRIORITY -= 1;
                        if (article.PRIORITY < 0)
                        {
                            article.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (article.PRIORITY + 1 < int.MaxValue)
                        {
                            article.PRIORITY += 1;
                        }
                    }
                    FastLinkServices.Update(article);
                    return article.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
