﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WizardCMS.Models;
using System.Collections;
using WizardCMS.Helpers;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Configuration;
using System.Web.Configuration;
using WizardCMS.Const;

namespace WizardCMS.Controllers
{
    [AuthorizeActionFilter]
    public class ServiceController : Controller
    {
        string nameModule = "service";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);

            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Service/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string idArticle = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(idArticle) )
                    {
                        try
                        {
                            int id = int.Parse(idArticle);
                            ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);

                            ViewBag.OptionList = ArticlesServices.GetSelectList(article.PARENT_ID, null, id);
                            return PartialView("../BackEnd/Service/Update", article);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        ViewBag.OptionList = ArticlesServices.GetSelectList(0, null);
                        return PartialView("../BackEnd/Service/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        Session["back"] = curentPath;
                        string idArticle = CommonHelper.GetSegment(4, curentPath);
                        string category = Request.QueryString["category"];

                        int parentId = -1;
                        if (category == "menu")
                        {
                            parentId = 0;
                        }
                        else if (category == "all")
                        {
                            parentId = -1;
                        }
                        else if (!string.IsNullOrEmpty(category))
                        {
                            bool tmp = int.TryParse(category, out parentId);
                        }
                        ViewBag.parentId = parentId;
                        Session["parentId"] = nameAction + "/" + parentId;
                        ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        if (Session["back"] != null)
                        {
                            string url = Session["back"] as string;
                            string idArticle = CommonHelper.GetSegment(4, url);
                            int parentId = int.Parse(idArticle);
                            ViewBag.parentId = parentId;
                            Session["parentId"] = nameAction + "/" + parentId;
                            ViewBag.CurrentSub = CommonHelper.GetSegment(4, curentPath);
                            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                        return Search("", "", "", 0, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                Session["parentId"] = null; //remove session
                string category = Request.QueryString["category"];
                int parentId = -1;
                if (category == "menu")
                {
                    parentId = 0;
                }
                else if (category == "all")
                {
                    parentId = -1;
                }
                else if (!string.IsNullOrEmpty(category))
                {
                    bool tmp = int.TryParse(category, out parentId);
                }
                return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    ArticleViewModel article = new ArticleViewModel();

                    article.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    article.FEATURED = Request["featured"] != null && Request["featured"] != string.Empty ? 1 : 0;
                    article.PARENT_ID = Request["parent_id"] == string.Empty ? 0 : int.Parse(Request["parent_id"]);
                    article.TEMPLATE = Request["template"];
                    article.YEAR_REPORT = Request["year_report"] == string.Empty ? 0 : int.Parse(Request["year_report"]);
                    article.MAIN_MENU = Request["main_menu"] == "on" ? 1 : 0; //Request["main_menu"] == string.Empty ? 0 : int.Parse(Request["main_menu"]);
                    article.ADMIN_SUBMENU = Request["admin_submenu"] == "on" ? 1 : 0; //Request["admin_submenu"] == string.Empty ? 0 : int.Parse(Request["admin_submenu"]);
                    article.PRIORITY = Request["priority"] == string.Empty ? 0 : int.Parse(Request["priority"]);
                    article.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    foreach (var lang in Constants.Langs)
                    {
                        var title = Request["title_"+lang];
                        var sub_title = Request["sub_title_" + lang];
                        var description = Request["description_" + lang];
                        var content = Request["content_" + lang];
                        var slug = StringHelper.CreateSlug(title);

                        var image = string.Empty; //Request["image_" + lang];
                        var file = string.Empty;
                        if (Request.Files != null && Request.Files["image_" + lang] != null)
                        {
                            HttpPostedFileBase namefile = Request.Files["image_" + lang];
                            image = this.Upload(namefile, Constants.DIR_ARTICLE);
                            if (image.Equals(Constants.ERROR_MAX_UPLOAD))
                            {
                                string language = lang == "vi" ? "Tiếng Việt" : "English";
                                return "File hình ảnh (" + language + ") vượt quá dung lượng cho phép.";//Constants.ERROR_MAX_UPLOAD;
                            }
                        }

                        if (Request.Files != null && Request.Files["file_" + lang] != null)
                        {
                            HttpPostedFileBase namefile = Request.Files["file_" + lang];
                            file = this.Upload(namefile, Constants.DIR_FILES);
                            if (file.Equals(Constants.ERROR_MAX_UPLOAD))
                            {
                                string language = lang == "vi" ? "Tiếng Việt" : "English";
                                return "File đính kèm (" + language + ") vượt quá dung lượng cho phép.";//Constants.ERROR_MAX_UPLOAD;
                            }
                        }

                        //Validate data input
                        if (title == string.Empty) continue;

                        ArticleContent article_content = new ArticleContent();
                        article_content.TITLE = title;
                        article_content.SUB_TITLE = sub_title;
                        article_content.SLUG = slug;
                        article_content.LINK = slug;
                        article_content.CONTENT = content;
                        article_content.DESCRIPTION = CommonHelper.CutText(description,490);
                        article_content.IMAGE = image;
                        article_content.LANG = lang;
                        article_content.ATTACHMENT = file;

                        article.ContentTranslates.Add(lang, article_content);
                    }

                    if (article.ID > 0)
                    {
                        if (article.PARENT_ID == article.ID)
                        {
                            return "Không thể chọn bài viết này làm danh mục cha được.";
                        }

                        //update an article
                        ArticleViewModel articleOld = ArticlesServices.AdminGetArticalById(article.ID);
                        if (articleOld.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in articleOld.ContentTranslates)
                            {
                                ArticleContent old_content = item.Value;

                                if (article.ContentTranslates != null)
                                {
                                    if (article.ContentTranslates.ContainsKey(old_content.LANG))
                                    {
                                        if (article.ContentTranslates[old_content.LANG].SLUG != old_content.SLUG)
                                        {
                                            if (ArticlesServices.CheckSlug(article.ContentTranslates[old_content.LANG].SLUG, old_content.LANG, article.PARENT_ID))
                                                 return "Trùng tiêu đề bài viết.";
                                        }

                                        if (article.ContentTranslates[old_content.LANG].IMAGE == string.Empty)
                                            article.ContentTranslates[old_content.LANG].IMAGE = old_content.IMAGE;
                                        else
                                        {
                                            //remove old image
                                            string FileToDelete = Server.MapPath("~/" + Constants.DIR_ARTICLE + old_content.IMAGE);
                                            if (System.IO.File.Exists(FileToDelete)) 
                                            {
                                                System.IO.File.Delete(FileToDelete);
                                            }
                                        }

                                        if (article.ContentTranslates[old_content.LANG].ATTACHMENT == string.Empty)
                                            article.ContentTranslates[old_content.LANG].ATTACHMENT = old_content.ATTACHMENT;
                                        else
                                        {
                                            //remove old file
                                            string FileToDelete = Server.MapPath("~/" + Constants.DIR_FILES + old_content.ATTACHMENT);
                                            if (System.IO.File.Exists(FileToDelete))
                                            {
                                                System.IO.File.Delete(FileToDelete);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        article.ContentTranslates.Add(old_content.LANG, old_content);
                                    }
                                }
                                else
                                {
                                    article.ContentTranslates = new Dictionary<string, ArticleContent>();
                                    article.ContentTranslates.Add(old_content.LANG, old_content);
                                }
                            }
                        }

                        if (ArticlesServices.Update(article))
                        {
                            if (article.PARENT_ID > 0)
                            {
                                ArticlesServices.UpdateLinkCustom(article.PARENT_ID);
                            }
                            else
                            {
                                ArticlesServices.UpdateLinkCustom(article.ID);
                            }
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {

                        if (article.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, ArticleContent> item in article.ContentTranslates)
                            {
                                ArticleContent article_content = item.Value;
                                if (ArticlesServices.CheckSlug(article_content.SLUG, article_content.LANG, article.PARENT_ID))
                                    return "Trùng tiêu đề bài viết.";
                            }
                        }
                        else
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = ArticlesServices.Insert(article);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            if (article.PARENT_ID > 0)
                            {
                                ArticlesServices.UpdateLinkCustom(article.PARENT_ID);
                            }
                            else
                            {
                                ArticlesServices.UpdateLinkCustom(newID);
                            }
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                        
                        if (ArticlesServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        listArtId.Add(id);

                        ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                        //adminControl.SaveLog(nameModule, int.Parse(item.Trim()), "Delete", "Delete", "", "");

                    }
                    bool result = ArticlesServices.DeleteList(listArtId);

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, parentId, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    ArticleViewModel article = ArticlesServices.GetArticleById(id);
                    if (article != null)
                    {
                        article.STATUS = (int)(article.STATUS == 1 ? 0 : 1);
                        ArticlesServices.UpdateStatus(id, article.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", parentId, paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = ArticlesServices.GetArticleById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    ArticlesServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Article";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Service/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = ArticlesServices.GetCount(parentId, fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.parentId = parentId;
            ViewBag.AjaxUrl = Url.Content("~") + "Service/Search/";

            List<ArticleViewModel> lstArticle = ArticlesServices.GetArticleByPaging(parentId, fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Service/Index", lstArticle);
            }
            else
            {
                return PartialView("../BackEnd/Service/AjaxContent", lstArticle);
            }
        }

        //function upload image
        private string Upload(HttpPostedFileBase namefile, string dirUpload)
        {
            try
            {

                //if (!int.TryParse(maxAllowConfig, out maxAllow))
                //{
                //    maxAllow = 3;//default is 3MB
                //}

                //if (maxAllow <= 0)
                //{
                //    maxAllow = Constants.MAX_LENGTH_IMG;
                //}

                var supportedTypes = new[] { "jpg", "png", "pdf", "doc", "docx" };
                maxAllow = Constants.MAX_LENGTH_IMG * 10 ;
                if (namefile.ContentLength > maxAllow)
                {
                    return Constants.ERROR_MAX_UPLOAD;
                }

                if (namefile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(namefile.FileName);
                    var fileExt = System.IO.Path.GetExtension(namefile.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt.ToLower()) == true)
                    {
                        fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + "." + fileExt;
                        string monthUp = System.DateTime.Now.Month.ToString();
                        string yearUp = System.DateTime.Now.Year.ToString();
                        string directory = Server.MapPath(Url.Content("~") + dirUpload + yearUp + "/" + monthUp + "/");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);

                            //set permission
                            CommonHelper.SetPermission(directory);
                        }
                        string path = Path.Combine(directory, fileName);
                        namefile.SaveAs(path);
                        return yearUp + "/" + monthUp + "/" + fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        { 
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    ArticleViewModel article = ArticlesServices.AdminGetArticalById(id);
                    if (order == 0)
                    {
                        article.PRIORITY -= 1;
                        if (article.PRIORITY < 0)
                        {
                            article.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (article.PRIORITY + 1 < int.MaxValue)
                        {
                            article.PRIORITY += 1;
                        }
                    }
                    ArticlesServices.Update(article);
                    return article.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
