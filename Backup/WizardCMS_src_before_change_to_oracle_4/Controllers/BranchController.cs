﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using WizardCMS.Const;
using System.Web.Configuration;
using System.IO;
using WizardCMS.domain;

namespace WizardCMS.Controllers
{
    public class BranchController : Controller
    {
        string nameModule = "branch";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Branch/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            ViewBag.module = "branch";

            ViewBag.Provinces = BranchServices.GetProvinces();

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);

                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            BranchViewModel node = BranchServices.Get(id);

                            return PartialView("../BackEnd/Branch/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Branch/Update", null);
                    }
                }
                else//go to sub menu page of article
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    BranchViewModel node = new BranchViewModel();

                    node.ID = StringHelper.IsNumber(Request["hiddenIdAdmincp"]);
                    node.NAME = Request["nameAdmincp"];
                    node.GROUP_ID = StringHelper.IsNumber(Request["groupAdmincp"]);
                    node.REGION = StringHelper.IsNumber(Request["regionAdmincp"]);
                    node.LANG = Request["langAdmincp"];
                    node.TEL = Request["telAdmincp"];
                    node.FAX = Request["faxAdmincp"];
                    node.PROVINCE_ID = Request["provinceAdmincp"];
                    node.DISTRICT_ID = Request["districtAdmincp"];
                    node.LAT_X = string.Empty;
                    node.LAT_Y = string.Empty;
                    node.DESCRIPTION = Request["description"];
                    node.PRIORITY = StringHelper.IsNumber(Request["priority"]);
                    node.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;
                    node.IMAGE = "";// Request["image"];

                    foreach (var lang in Constants.Langs)
                    {
                        var name = Request["name_" + lang];
                        var description = Request["description_" + lang];
                        var slug = StringHelper.CreateSlug(name);

                        //Validate data input
                        if (name == string.Empty) continue;

                        BranchContent content = new BranchContent();
                        content.NAME = name;
                        content.SLUG = slug;
                        content.DESCRIPTION = description;
                        content.LANG = lang;

                        node.ContentTranslates.Add(lang, content);
                    }

                    if (node.ContentTranslates == null || node.ContentTranslates.Count == 0)
                    {
                        return "Chưa nhập tên.";
                    }

                    if (node.ID > 0)
                    {
                        if (BranchServices.Update(node))
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                    else
                    {
                        int newID = BranchServices.Insert(node);
                        if (newID >= Constants.ACTION_SUCCESS)
                        {
                            return "success";
                        }
                        else
                        {
                            return Constants.PERMISSION_DENIED;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        public bool Miggrate()
        {
            using (DBContext context = new DBContext())
            {
                var nodes = context.WZ_BRANCH.ToList();
                foreach (var item in nodes)
                {
                    WZ_BRANCH_CONTENT content = new WZ_BRANCH_CONTENT();
                    content.NAME = item.NAME;
                    content.SLUG = StringHelper.CreateSlug(item.NAME);
                    content.DESCRIPTION = item.DESCRIPTION;
                    content.LANG = item.LANG;
                    content.NID = (int)item.ID;

                    context.WZ_BRANCH_CONTENT.AddObject(content);
                    context.SaveChanges();
                }
            }
            return true;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (BranchServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        BranchServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    BranchViewModel node = BranchServices.Get(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        ArticlesServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = BranchServices.Get(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    BranchServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Branch";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Branch/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            int totalItem = BranchServices.GetCount(fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Branch/Search/";

            List<BranchViewModel> lstSlide = BranchServices.GetList(fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Branch/Index", lstSlide);
            }
            else
            {
                return PartialView("../BackEnd/Branch/AjaxContent", lstSlide);
            }
        }

        //function upload image
        private string Upload(HttpPostedFileBase namefile, string dirUpload)
        {
            try
            {

                //if (!int.TryParse(maxAllowConfig, out maxAllow))
                //{
                //    maxAllow = 3;//default is 3MB
                //}

                //if (maxAllow <= 0)
                //{
                //    maxAllow = Constants.MAX_LENGTH_IMG;
                //}

                var supportedTypes = new[] { "jpg", "png" };
                //if (namefile.ContentLength > maxAllow)
                //{
                //    return Constants.ERROR_MAX_UPLOAD;
                //}

                if (namefile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(namefile.FileName);
                    var fileExt = System.IO.Path.GetExtension(namefile.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt.ToLower()) == true)
                    {
                        fileName = DateTime.Now.ToString("ddMMyyyy_mmHHss_tt") + 1.ToString() + "." + fileExt;
                        string monthUp = System.DateTime.Now.Month.ToString();
                        string yearUp = System.DateTime.Now.Year.ToString();
                        string directory = Server.MapPath(Url.Content("~") + dirUpload + yearUp + "/" + monthUp + "/");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);

                            //set permission
                            CommonHelper.SetPermission(directory);
                        }
                        string path = Path.Combine(directory, fileName);
                        namefile.SaveAs(path);
                        return yearUp + "/" + monthUp + "/" + fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    BranchViewModel node = BranchServices.Get(id);
                    if (order == 0)
                    {
                        node.PRIORITY -= 1;
                        if (node.PRIORITY < 0)
                        {
                            node.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (node.PRIORITY + 1 < int.MaxValue)
                        {
                            node.PRIORITY += 1;
                        }
                    }
                    BranchServices.Update(node);
                    return node.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }

        #region PROVINCE - DISTRICT - WRAD
        [HttpPost]
        public ActionResult changeProvince()
        {
            string PROVINCE_ID = Request["PROVINCE_ID"];
            string DISTRICT_ID = Request["DISTRICT_ID"];

            var Districts = BranchServices.GetDistricts(PROVINCE_ID);
            ViewBag.DISTRICT_ID = DISTRICT_ID;

            return View("../FrontEnd/ListDistricts", Districts);
        }

        [HttpPost]
        public ActionResult changeDistrict()
        {
            string DISTRICT_ID = Request["DISTRICT_ID"];
            string WARD_ID = Request["WARD_ID"];

            var Wards = BranchServices.GetWards(DISTRICT_ID);
            ViewBag.WARD_ID = WARD_ID;

            return View("../FrontEnd/ListWards", Wards);
        }
        #endregion
    }
}
