﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Security.Cryptography;
using System.Xml.Serialization;
using System.Collections;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using System.Xml.Linq;

namespace WizardCMS.Helpers
{
    public static class StringHelper
    {
        public const string CarriageReturnLineFeed = "\r\n";
        public const string Empty = "";
        public const char CarriageReturn = '\r';
        public const char LineFeed = '\n';
        public const char Tab = '\t';

        public const string SECRET_CODE = "";

        public static string CreateToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();

            string token = Convert.ToBase64String(time.Concat(key).ToArray());

            return EncryptMD5(token);
        }

        public static string RandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string PartialView(System.Web.Mvc.Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new System.Web.Mvc.ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        public static void ExportToExcel(IEnumerable<dynamic> data, string sheetName)
        {
            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add(sheetName);
            ws.Cell(2, 1).InsertTable(data);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", sheetName.Replace(" ", "_")));

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                memoryStream.Close();
            }

            HttpContext.Current.Response.End();
        }

        public static string EncryptMD5(string sinput)
        {
            sinput = SECRET_CODE + sinput;
            // Convert string into array int
            byte[] bytes = Encoding.UTF8.GetBytes(sinput);
            // Create class MD5 of .NET
            MD5 md5 = new MD5CryptoServiceProvider();
            // Excute encrypto => hash array
            byte[] hash = md5.ComputeHash(bytes);
            // Convert into string
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.Append(string.Format("{0:X2}", b));
            }

            return sb.ToString();
        }

        //public static string FormatWith(this string format, params object[] args)
        //{
        //  return FormatWith(format, null, args);
        //}

        public static string FormatWith(this string format, IFormatProvider provider, params object[] args)
        {


            return string.Format(provider, format, args);
        }

        /// <summary>
        /// Determines whether the string contains white space.
        /// </summary>
        /// <param name="s">The string to test for white space.</param>
        /// <returns>
        ///   <c>true</c> if the string contains white space; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsWhiteSpace(string s)
        {
            if (s == null)
                throw new ArgumentNullException("s");

            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsWhiteSpace(s[i]))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the string is all white space. Empty string will return false.
        /// </summary>
        /// <param name="s">The string to test whether it is all white space.</param>
        /// <returns>
        ///   <c>true</c> if the string is all white space; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsWhiteSpace(string s)
        {
            if (s == null)
                throw new ArgumentNullException("s");

            if (s.Length == 0)
                return false;

            for (int i = 0; i < s.Length; i++)
            {
                if (!char.IsWhiteSpace(s[i]))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Ensures the target string ends with the specified string.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">The value.</param>
        /// <returns>The target string with the value string at the end.</returns>
        public static string EnsureEndsWith(string target, string value)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            if (value == null)
                throw new ArgumentNullException("value");

            if (target.Length >= value.Length)
            {
                if (string.Compare(target, target.Length - value.Length, value, 0, value.Length, StringComparison.OrdinalIgnoreCase) ==
                                0)
                    return target;

                string trimmedString = target.TrimEnd(null);

                if (string.Compare(trimmedString, trimmedString.Length - value.Length, value, 0, value.Length,
                                StringComparison.OrdinalIgnoreCase) == 0)
                    return target;
            }

            return target + value;
        }

        public static bool IsNullOrEmptyOrWhiteSpace(string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;
            else if (IsWhiteSpace(s))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Perform an action if the string is not null or empty.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="action">The action to perform.</param>
        public static void IfNotNullOrEmpty(string value, Action<string> action)
        {
            IfNotNullOrEmpty(value, action, null);
        }

        private static void IfNotNullOrEmpty(string value, Action<string> trueAction, Action<string> falseAction)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (trueAction != null)
                    trueAction(value);
            }
            else
            {
                if (falseAction != null)
                    falseAction(value);
            }
        }

        /// <summary>
        /// Indents the specified string.
        /// </summary>
        /// <param name="s">The string to indent.</param>
        /// <param name="indentation">The number of characters to indent by.</param>
        /// <returns></returns>
        public static string Indent(string s, int indentation)
        {
            return Indent(s, indentation, ' ');
        }

        /// <summary>
        /// Indents the specified string.
        /// </summary>
        /// <param name="s">The string to indent.</param>
        /// <param name="indentation">The number of characters to indent by.</param>
        /// <param name="indentChar">The indent character.</param>
        /// <returns></returns>
        public static string Indent(string s, int indentation, char indentChar)
        {
            if (s == null)
                throw new ArgumentNullException("s");

            if (indentation <= 0)
                throw new ArgumentException("Must be greater than zero.", "indentation");

            StringReader sr = new StringReader(s);
            StringWriter sw = new StringWriter(CultureInfo.InvariantCulture);

            ActionTextReaderLine(sr, sw, delegate(TextWriter tw, string line)
            {
                tw.Write(new string(indentChar, indentation));
                tw.Write(line);
            });

            return sw.ToString();
        }

        private delegate void ActionLine(TextWriter textWriter, string line);

        private static void ActionTextReaderLine(TextReader textReader, TextWriter textWriter, ActionLine lineAction)
        {
            string line;
            bool firstLine = true;
            while ((line = textReader.ReadLine()) != null)
            {
                if (!firstLine)
                    textWriter.WriteLine();
                else
                    firstLine = false;

                lineAction(textWriter, line);
            }
        }

        /// <summary>
        /// Numbers the lines.
        /// </summary>
        /// <param name="s">The string to number.</param>
        /// <returns></returns>
        public static string NumberLines(string s)
        {
            if (s == null)
                throw new ArgumentNullException("s");

            StringReader sr = new StringReader(s);
            StringWriter sw = new StringWriter(CultureInfo.InvariantCulture);

            int lineNumber = 1;

            ActionTextReaderLine(sr, sw, delegate(TextWriter tw, string line)
            {
                tw.Write(lineNumber.ToString(CultureInfo.InvariantCulture).PadLeft(4));
                tw.Write(". ");
                tw.Write(line);

                lineNumber++;
            });

            return sw.ToString();
        }

        /// <summary>
        /// Nulls an empty string.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns>Null if the string was null, otherwise the string unchanged.</returns>
        public static string NullEmptyString(string s)
        {
            return (string.IsNullOrEmpty(s)) ? null : s;
        }

        public static string ReplaceNewLines(string s, string replacement)
        {
            StringReader sr = new StringReader(s);
            StringBuilder sb = new StringBuilder();

            bool first = true;

            string line;
            while ((line = sr.ReadLine()) != null)
            {
                if (first)
                    first = false;
                else
                    sb.Append(replacement);

                sb.Append(line);
            }

            return sb.ToString();
        }

        public static string Truncate(string s, int maximumLength)
        {
            return Truncate(s, maximumLength, "...");
        }

        public static string Truncate(string s, int maximumLength, string suffix)
        {
            if (suffix == null)
                throw new ArgumentNullException("suffix");

            if (maximumLength <= 0)
                throw new ArgumentException("Maximum length must be greater than zero.", "maximumLength");

            int subStringLength = maximumLength - suffix.Length;

            if (subStringLength <= 0)
                throw new ArgumentException("Length of suffix string is greater or equal to maximumLength");

            if (s != null && s.Length > maximumLength)
            {
                string truncatedString = s.Substring(0, subStringLength);
                // incase the last character is a space
                truncatedString = truncatedString.Trim();
                truncatedString += suffix;

                return truncatedString;
            }
            else
            {
                return s;
            }
        }

        public static StringWriter CreateStringWriter(int capacity)
        {
            StringBuilder sb = new StringBuilder(capacity);
            StringWriter sw = new StringWriter(sb, CultureInfo.InvariantCulture);

            return sw;
        }

        public static int? GetLength(string value)
        {
            if (value == null)
                return null;
            else
                return value.Length;
        }

        public static char IntToHex(int n)
        {
            if (n <= 9)
            {
                return (char)(n + 48);
            }
            return (char)((n - 10) + 97);
        }

        public static string ToCharAsUnicode(char c)
        {
            char h1 = IntToHex((c >> 12) & '\x000f');
            char h2 = IntToHex((c >> 8) & '\x000f');
            char h3 = IntToHex((c >> 4) & '\x000f');
            char h4 = IntToHex(c & '\x000f');

            return new string(new[] { '\\', 'u', h1, h2, h3, h4 });
        }

        public static void WriteCharAsUnicode(TextWriter writer, char c)
        {


            char h1 = IntToHex((c >> 12) & '\x000f');
            char h2 = IntToHex((c >> 8) & '\x000f');
            char h3 = IntToHex((c >> 4) & '\x000f');
            char h4 = IntToHex(c & '\x000f');

            writer.Write('\\');
            writer.Write('u');
            writer.Write(h1);
            writer.Write(h2);
            writer.Write(h3);
            writer.Write(h4);
        }

        public static TSource ForgivingCaseSensitiveFind<TSource>(this IEnumerable<TSource> source, Func<TSource, string> valueSelector, string testValue)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            if (valueSelector == null)
                throw new ArgumentNullException("valueSelector");

            var caseInsensitiveResults = source.Where(s => string.Compare(valueSelector(s), testValue, StringComparison.OrdinalIgnoreCase) == 0);
            if (caseInsensitiveResults.Count() <= 1)
            {
                return caseInsensitiveResults.FirstOrDefault();
            }
            else
            {
                // multiple results returned. now filter using case sensitivity
                var caseSensitiveResults = source.Where(s => string.Compare(valueSelector(s), testValue, StringComparison.Ordinal) == 0);
                return caseSensitiveResults.FirstOrDefault();
            }
        }

        /// <summary>
        /// Serialize an object to string
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="toSerialize">Input Object</param>
        /// <returns>Serialized string</returns>
        public static string SerializeObject<T>(object obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }

        /// <summary>
        /// Deserialize string to an object
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="fromSerialize">Input string</param>
        /// <returns></returns>
        public static T DeSerializeFromString<T>(string fromSerialize)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (StringReader reader = new StringReader(fromSerialize))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        //function create slug from title
        public static string CreateSlug(string input)
        {
            try
            {
                if (input != string.Empty)
                {
                    Hashtable htbUnicode = new Hashtable();
                    htbUnicode.Add("a", "á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ");
                    htbUnicode.Add("A", "Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ");
                    htbUnicode.Add("d", "đ");
                    htbUnicode.Add("D", "Đ");
                    htbUnicode.Add("e", "é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ");
                    htbUnicode.Add("E", "É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ");
                    htbUnicode.Add("i", "í|ì|ỉ|ĩ|ị");
                    htbUnicode.Add("I", "Í|Ì|Ỉ|Ĩ|Ị");
                    htbUnicode.Add("n", "ñ");
                    htbUnicode.Add("N", "Ñ");
                    htbUnicode.Add("o", "ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ");
                    htbUnicode.Add("O", "Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ");
                    htbUnicode.Add("u", "ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự");
                    htbUnicode.Add("U", "Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự");
                    htbUnicode.Add("y", "ý|ỳ|ỷ|ỹ|ỵ");
                    htbUnicode.Add("Y", "Ý|Ỳ|Ỷ|Ỹ|Ỵ");
                    htbUnicode.Add("(*)", " - |–|“|”|?|!|,|.|'|\"");
                    htbUnicode.Add("-", " | - |--");

                    List<string> code_entities_match = new List<string> { 
                        " ","--","&quot;","!","@","#","$","%","^","&","*","(",")","_","+","{","}","|",":","\"","<",">","?","[","]","\\",";","'",",",".","/","*","+","~","`","=" 
                    };

                    List<string> code_entities_replace = new List<string> { 
                        "-","-","","","","","","","","","","","","","","","","-","","-","","","-","-","-","-","","","","","-","","-","-","","-"
                    };

                    for (int i = 0; i < code_entities_replace.Count; i++)
                    {
                        input = input.Replace(code_entities_match[i], code_entities_replace[i]);
                    }

                    foreach (var key in htbUnicode.Keys)
                    {
                        string codau = htbUnicode[key] as string;
                        string[] arr = codau.Split('|');
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            input = input.Replace(arr[i], key.ToString());
                        }
                    }

                    input = input.Replace("(*)", "");
                    input = input.Replace("--", "-");
                    return input.ToLower();
                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        public static string v2e(string input)
        {
            try
            {
                if (input != string.Empty)
                {
                    Hashtable htbUnicode = new Hashtable();
                    htbUnicode.Add("a", "á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ");
                    htbUnicode.Add("A", "Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ");
                    htbUnicode.Add("d", "đ");
                    htbUnicode.Add("D", "Đ");
                    htbUnicode.Add("e", "é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ");
                    htbUnicode.Add("E", "É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ");
                    htbUnicode.Add("i", "í|ì|ỉ|ĩ|ị");
                    htbUnicode.Add("I", "Í|Ì|Ỉ|Ĩ|Ị");
                    htbUnicode.Add("n", "ñ");
                    htbUnicode.Add("N", "Ñ");
                    htbUnicode.Add("o", "ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ");
                    htbUnicode.Add("O", "Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ");
                    htbUnicode.Add("u", "ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự");
                    htbUnicode.Add("U", "Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự");
                    htbUnicode.Add("y", "ý|ỳ|ỷ|ỹ|ỵ");
                    htbUnicode.Add("Y", "Ý|Ỳ|Ỷ|Ỹ|Ỵ");
                    //htbUnicode.Add("(*)", " - |–|“|”|?|!|,|.|'|\"");
                    //htbUnicode.Add("-", " | - |--");

                    List<string> code_entities_match = new List<string> { 
                        " ","--","&quot;","!","@","#","$","%","^","&","*","(",")","_","+","{","}","|",":","\"","<",">","?","[","]","\\",";","'",",",".","/","*","+","~","`","=" 
                    };

                    List<string> code_entities_replace = new List<string> { 
                        "-","-","","","","","","","","","","","","","","","","-","","-","","","-","-","-","-","","","","","-","","-","-","","-"
                    };

                    /*
                    for (int i = 0; i < code_entities_replace.Count; i++)
                    {
                        input = input.Replace(code_entities_match[i], code_entities_replace[i]);
                    }*/

                    foreach (var key in htbUnicode.Keys)
                    {
                        string codau = htbUnicode[key] as string;
                        string[] arr = codau.Split('|');
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            input = input.Replace(arr[i], key.ToString());
                        }
                    }

                    input = input.Replace("(*)", "");
                    input = input.Replace("--", "-");
                    return input;
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogDebug(ex.StackTrace);
            }
            return string.Empty;
        }

        public static string GenerateSlug(this string phrase)
        {
            try
            {
                string str = phrase.RemoveAccent().ToLower();
                // invalid chars           
                str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
                // convert multiple spaces into one space   
                str = Regex.Replace(str, @"\s+", " ").Trim();
                // cut and trim 
                str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
                str = Regex.Replace(str, @"\s", "-"); // hyphens   
                return str;
            }
            catch (Exception)
            {
            }
            return string.Empty;
        }

        public static string RemoveAccent(this string txt)
        {
            try
            {
                byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
                return System.Text.Encoding.ASCII.GetString(bytes);
            }
            catch (Exception)
            {
            }
            return string.Empty;
        }

        public static string GetUriString(int index)
        {
            try
            {
                string str = "";
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                //Logs.WriteLogDebug("AbsoluteUri: " + url);
                if (url.Split("/".ToCharArray()).Count() - 1 >= index)
                {
                    str = url.Split("/".ToCharArray())[index];
                }
                //Logs.WriteLogDebug("AbsoluteUri 2: " + url);
                return str;
            }
            catch { }
            return string.Empty;
        }

        public static string field(string data_vi, string data_en, bool reverse = false)
        {
            string data;
            if (reverse == true)
            {
                if (language() == "vi")
                {
                    data = data_en;
                }
                else
                {
                    data = data_vi;
                }
            }
            else
            {
                if (language() == "vi")
                {
                    data = data_vi;
                }
                else
                {
                    data = data_en;
                }
            }
            return data;
        }

        public static string field_content(Dictionary<string, WizardCMS.Models.ArticleContent> ContentTranslates, string field, bool reverse = false)
        {
            try
            {
                string data = string.Empty;
                if (reverse == true)
                {
                    if (language() == "vi")
                    {
                        if (ContentTranslates.ContainsKey("en"))
                            data = ContentTranslates["en"].GetType().GetProperty(field).GetValue(ContentTranslates["en"], null).ToString();
                    }
                    else
                    {
                        if (ContentTranslates.ContainsKey("vi"))
                            data = ContentTranslates["vi"].GetType().GetProperty(field).GetValue(ContentTranslates["vi"], null).ToString();
                    }
                }
                else
                {
                    if (language() == "vi")
                    {
                        if (ContentTranslates.ContainsKey("vi"))
                            data = ContentTranslates["vi"].GetType().GetProperty(field).GetValue(ContentTranslates["vi"], null).ToString();
                    }
                    else
                    {
                        if (ContentTranslates.ContainsKey("en"))
                            data = ContentTranslates["en"].GetType().GetProperty(field).GetValue(ContentTranslates["en"], null).ToString();
                    }
                }
                return data;
            }
            catch(Exception ex){
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        public static string language()
        {
            try
            {
                string lang = CommonHelper.GetSegment(1);
                if (string.IsNullOrEmpty(lang))
                    lang = "vi";

                if (lang != "vi" && lang != "en") return "vi";

                return lang;
            }
            catch { }
            return "vi";
        }

        public static string language2()
        {
            try
            {
                return CultureHelper.GetCurrentNeutralCulture();
            }
            catch { }
            return "vi";
        }

        public static string base_url()
        {
            Uri contextUri = HttpContext.Current.Request.Url;

            return string.Format("{0}://{1}{2}/", contextUri.Scheme,
               contextUri.Host, contextUri.Port == 80 || contextUri.Port == 443 ? string.Empty : ":" + contextUri.Port);

            /*
            string URL = "http://" + GetUriString(2) + "/";
            if (URL.Contains("localhost"))
                return URL;

            URL = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + ":8899/";
            return URL;
            */
        }

        public static string base_url_lang(bool reverse = false)
        {
            string data;
            if (reverse == true)
            {
                if (language() == "vi")
                {
                    data = "en";
                }
                else
                {
                    data = "vi";
                }
            }
            else
            {
                if (language() == "vi")
                {
                    data = "vi";
                }
                else
                {
                    data = "en";
                }
            }

            return base_url() + data + "/";
        }

        public static string content_link() {
            try
            {
                var base_url = base_url_lang();

                string fullpath = HttpContext.Current.Request.Url.AbsoluteUri;

                //Logs.WriteLogDebug(fullpath);
                //Logs.WriteLogDebug(base_url);

                if (fullpath.Length < base_url.Length)
                    return string.Empty;

                return fullpath.Replace(base_url, "");
            }
            catch (Exception)
            {
                
            }
            return string.Empty;
        }

        public static string lang(string key, bool reverse = false)
        {
            try
            {
                string lang;
                if (reverse == true)
                {
                    if (language() == "vi")
                    {
                        lang = "en";
                    }
                    else
                    {
                        lang = "vi";
                    }
                }
                else
                {
                    if (language() == "vi")
                    {
                        lang = "vi";
                    }
                    else
                    {
                        lang = "en";
                    }
                }

                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/lang.xml"));
                var query = (from c in xml.Elements("root").Elements("node")
                             where c.Attribute("key").Value == key && c.Attribute("lang").Value == lang
                             select c).FirstOrDefault();

                //watch.Stop();
                //Logs.WriteLogDebug(watch.Elapsed.Milliseconds.ToString() + text);

                if (query != null)
                {
                    return query.Value.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        public static string lang(string text, string lang)
        {
            try
            {
                XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/languages.xml"));
                var query = (from c in xml.Elements("language").Elements("item")
                             where c.Element("slug").Value == text
                             select c).FirstOrDefault();

                if (query != null)
                {
                    if (lang == "vi")
                    {
                        return query.Element("text_vi").Value.ToString();

                    }
                    else
                    {
                        return query.Element("text_en").Value.ToString();
                    }
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        public static string lang_old(string key)
        {
            try
            {
                if (Const.Constants.DC_TRANSLATES == null)
                {
                    StringHelper.GetTranslateDictionary();
                }
                if (Const.Constants.DC_TRANSLATES != null && Const.Constants.DC_TRANSLATES.ContainsKey(key))
                    return Const.Constants.DC_TRANSLATES[key];
            }
            catch(Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        public static void GetTranslateDictionary()
        {
            try {
                Const.Constants.DC_TRANSLATES = null;
                if (Const.Constants.DC_TRANSLATES == null)
                {
                    Const.Constants.DC_TRANSLATES = new Dictionary<string, string>();

                    string currentLang = CultureHelper.GetCurrentNeutralCulture();

                    XmlDocument doc = new XmlDocument();
                    doc.Load(AppDomain.CurrentDomain.BaseDirectory + "Assets/lang.xml");
                    XmlNodeList elemList = doc.GetElementsByTagName("node");
                    for (int i = 0; i < elemList.Count; i++)
                    {
                        if (elemList[i].Attributes["lang"].Value == currentLang)
                        {
                            try
                            {
                                Const.Constants.DC_TRANSLATES.Add(elemList[i].Attributes["key"].Value, elemList[i].InnerText);
                            }
                            catch (Exception)
                            {
                                
                                throw;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public static HtmlString post_title(string title_vi, string title_en, int length = 0)
        {
            try
            {
                var title = language() == "en" ? title_en : title_vi;
                title = HttpContext.Current.Server.HtmlDecode(title);
                if (length > 0)
                    title = CommonHelper.CutText(title, length);
                return new HtmlString(title);
            }
            catch (Exception)
            {
                
            }
            return new HtmlString(string.Empty);
        }

        public static HtmlString post_title(string title, int length = 0)
        {
            try
            {
                title = HttpContext.Current.Server.HtmlDecode(title);
                if (length > 0)
                    title = CommonHelper.CutText(title, length);
                return new HtmlString(title);
            }
            catch (Exception)
            {
            }
            return new HtmlString(title);
        }

        public static string NumberToText(int num)
        {
            switch (num)
            {
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
            }
            return string.Empty;
        }

        public static bool MaxLength(string input, int max_leng)
        {
            return input.Length > max_leng;
        }

        public static bool MinLength(string input, int min_leng)
        {
            return input.Length < min_leng;
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidateUsername(string username)
		{
            Regex regex = new Regex(@"^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$");
            return regex.IsMatch(username);
		}

        public static bool ValidFullname(string input)
        {
            string paterm = @"^([a-zA-Z ]|â|ấ|ầ|ẩ|ẫ|ậ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|á|à|ả|ã|ạ|Á|À|Ả|Ã|Ạ|ă|ắ|ằ|ẳ|ẵ|ặ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|ê|ế|ề|ể|ễ|ệ|Ê|Ế|Ề|Ể|Ễ|Ệ|é|è|ẻ|ẽ|ẹ|É|È|Ẻ|Ẽ|Ẹ|í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị|ô|ố|ồ|ổ|ỗ|ộ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ|ư|ứ|ừ|ử|ữ|ự|Ư|Ứ|Ừ|Ử|Ữ|Ự|ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ|Đ|đ|ó|ò|ỏ|õ|ọ|Ó|Ò|Ỏ|Õ|Ọ|ú|ù|ủ|ũ|ụ|Ú|Ù|Ủ|Ũ|Ụ)+$";
            Regex regex = new Regex(paterm);
            Match match = regex.Match(input);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ValidPhone(string input)
        {
            return true;
        }

        public static bool IsNumeric(string input)
        {
            int n;
            return int.TryParse(input, out n);
        }

        public static int IsNumber(string Number)
        {
            var result = 0;
            if (Number != null && Regex.IsMatch(Number, @"\d"))
            {
                result = Convert.ToInt32(Number);
            }
            return result;
        }

        public static string StripTags(string input)
        {
            try
            {
                string noHTML = Regex.Replace(input, @"<[^>]+>|&nbsp;", "").Trim();
                string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
                return noHTMLNormalised;
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return string.Empty;
        }

        public static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

        public static string GetRegion(int region)
        {
            if (region == 1)
                return lang("text_mien_bac");
            else if (region == 2)
                return lang("text_mien_trung");
            else if (region == 3)
                return lang("text_mien_nam");

            return string.Empty;
        }
    }
}
