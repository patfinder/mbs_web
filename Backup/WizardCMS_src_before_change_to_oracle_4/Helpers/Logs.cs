﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Configuration;

namespace WizardCMS.Helpers
{
    public class Logs
    {
        public Logs() { }

        public bool StopThread { set; get; }
        public static void WriteLogError(string content)
        {
            try
            {
                string filename = DateTime.Now.ToString("ddMMyyyy") + "_LogError.txt";
                WriteToFile(filename, content);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public static void WriteLogDebug(string content)
        {
            try
            {
                string filename = DateTime.Now.ToString("ddMMyyyy") + "_LogDebug.txt";
                WriteToFile(filename, content);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static void WriteToFileCSV(string fileName, string content)
        {
            try
            {
                string lastError = string.Empty;
                string pathFolder = ConfigurationSettings.AppSettings["LogFile"];
                pathFolder = AppDomain.CurrentDomain.BaseDirectory + pathFolder;

                if (!Directory.Exists(pathFolder))
                    Directory.CreateDirectory(pathFolder);

                pathFolder = pathFolder + fileName;
                if (File.Exists(pathFolder))
                {
                    StreamReader f = File.OpenText(pathFolder);
                    lastError = f.ReadToEnd();
                    f.Close();
                }

                StreamWriter LogF = File.CreateText(pathFolder);
                LogF.WriteLine(content);

                if (lastError != string.Empty)
                    LogF.Write(lastError.Substring(0));
                LogF.Close();
            }
            catch (Exception)
            {

            }
        }

        private static void WriteToFile(string fileName, string content)
        {
            try
            {
                string lastError = string.Empty;
                string pathFolder = ConfigurationSettings.AppSettings["LogFile"];
                pathFolder = AppDomain.CurrentDomain.BaseDirectory + pathFolder;

                if (!Directory.Exists(pathFolder))
                    Directory.CreateDirectory(pathFolder);

                pathFolder = pathFolder + fileName;
                if (File.Exists(pathFolder))
                {
                    StreamReader f = File.OpenText(pathFolder);
                    lastError = f.ReadToEnd();
                    f.Close();
                }

                StreamWriter LogF = File.CreateText(pathFolder);
                LogF.WriteLine("[Date {0}]", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:ff"));
                LogF.WriteLine(content);
                LogF.WriteLine("-----------------------------------------------");

                if (lastError != string.Empty)
                    LogF.Write(lastError.Substring(0));
                LogF.Close();
            }
            catch (Exception)
            {

            }
        }
    }
}