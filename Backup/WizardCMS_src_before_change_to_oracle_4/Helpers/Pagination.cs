﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.Helpers
{
    public class Pagination
    {
        public string BaseUrl { get; set; }

        public int Perpage { get; set; }
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int Numlinks { get; set; }

        public string FirstLink { get; set; }
        public string LastLink { get; set; }
        public string NextLink { get; set; }
        public string PrevLink { get; set; }

        public Pagination()
        {
            FirstLink = string.Empty;
            LastLink = string.Empty;
            NextLink = Resources.Resources.NEXT_LINK;
            PrevLink = Resources.Resources.PREV_LINK;
            Perpage = 3;

            Numlinks = 4;
        }

        public Pagination(int total, string baseUrl, int page) : this()
        {
            TotalRows = total;
            BaseUrl = baseUrl;
            CurrentPage = page;
        }

        public string CreateLinks()
        {
            if (TotalRows == 0 || Perpage == 0)
            {
                return string.Empty;
            }

            int num_page = (int)Math.Ceiling(TotalRows / (double)Perpage);
            if (num_page == 1)
            {
                return string.Empty;
            }

            if (CurrentPage > num_page)
            {
                CurrentPage = num_page;
            }
            else
            {
 
            }

            int start = (CurrentPage - num_page > 0) ? CurrentPage - num_page : 1;
            //int end = num_page;

            string output = string.Empty;
            //previous link
            if(this.PrevLink != string.Empty && this.CurrentPage != 1)
            {
                output += "<a href='" + BaseUrl + "?p=" + (CurrentPage - 1) + "'>" + this.PrevLink + "</a>";
            }

            //calculate start, end
            int begin = CurrentPage - Numlinks > 0 ? CurrentPage - Numlinks : 1;
            int end = CurrentPage + Numlinks < num_page ? CurrentPage + Numlinks : num_page;

            if (begin > 1)
            {
                output += "<span >...</span>";
            }

            //loop page number
            for (int i = begin; i <= end; i++)
            {
                if (i == CurrentPage)
                {
                    output += "<span >" + i + "</span>";
                }
                else
                {
                    output += "<a href='" + BaseUrl + "?p=" + i + "'>" + i + "</a>";
                }

            }

            if (end < num_page)
            {
                output += "<span >...</span>";
            }

            //next link
            if (this.NextLink != string.Empty && this.CurrentPage < num_page)
            {
                output += "<a class='pag_normal' href='" + BaseUrl + "?p=" + (CurrentPage + 1) + "'>" + this.NextLink + "</a>";
            }

            return output;
        }


    }
}