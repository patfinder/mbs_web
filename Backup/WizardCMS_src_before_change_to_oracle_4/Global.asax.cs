﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CaptchaMvc.Infrastructure;

namespace WizardCMS
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{*allfiles}", new { allfiles = @".*\.(css|js|gif|jpg|png|swf|htc|map)(/.)?" });

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Admincp_index", // Route name
                "Admincp/Login/{action}/{id}", // URL with parameters
                new { controller = "Admincp", action = "Login", id = UrlParameter.Optional } // Parameter defaults
            );

            for (int i = 0; i < 10; i++ )
            {
                routes.MapRoute(
                    "Admincp_index" + i, // Route name
                    "Admincp/Login"+i+"/{action}/{id}", // URL with parameters
                    new { controller = "Admincp", action = "Login", id = UrlParameter.Optional } // Parameter defaults
                );
            }

            routes.MapRoute(
                "Admincp_login", // Route name
                "Admincp/login_action/{action}/{id}", // URL with parameters
                new { controller = "Admincp", action = "login_action", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Admincp_logout", // Route name
                "Admincp/logout/{action}/{id}", // URL with parameters
                new { controller = "Admincp", action = "logout", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Admincp_menu", // Route name
                "Admincp/MenuAdminArticle/{action}/{id}", // URL with parameters
                new { controller = "Admincp", action = "MenuAdminArticle", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Export contact", // Route name
                "Admincp/Contact/ExportData", // URL with parameters
                new { controller = "Contact", action = "ExportData", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Export Workshop", // Route name
                "Admincp/Workshop/ExportData", // URL with parameters
                new { controller = "Workshop", action = "ExportData", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute("Admincp",
               "Admincp/{*nameModule}",
               new
               {
                   controller = "Admincp",
                   action = "Index",
                   nameModule = UrlParameter.Optional
               }
            );

            routes.MapRoute(
                "Search VI", //Route name
                "vi/tim-kiem", //URL with parameters
                new { lang = "vi", controller = "Home", action = "Search" }
            );
            routes.MapRoute(
                "Search EN", //Route name
                "en/search", //URL with parameters
                new { lang = "en", controller = "Home", action = "Search" }
            );

            routes.MapRoute(
                "Thong bao VI", //Route name
                "vi/thong-bao", //URL with parameters
                new { lang = "vi", controller = "Home", action = "Notice" }
            );
            routes.MapRoute(
                "Thong bao EN", //Route name
                "en/notice", //URL with parameters
                new { lang = "en", controller = "Home", action = "Notice" }
            );

            routes.MapRoute(
                "contact", //Route name
                "{lang}/contact", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Contact" }
            );
            routes.MapRoute(
                "register_workshop", //Route name
                "{lang}/register_workshop", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RegisterWorkshop" }
            );

            routes.MapRoute(
                "change province", //Route name
                "{lang}/change-province", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Branch", action = "changeProvince" }
            );

            routes.MapRoute(
                "Sitemap vi", //Route name
                "vi/so-do-trang", //URL with parameters
                new { lang = "vi", controller = "Home", action = "Sitemap" }
            );
            routes.MapRoute(
                "Sitemap en", //Route name
                "en/sitemap", //URL with parameters
                new { lang = "en", controller = "Home", action = "Sitemap" }
            );

            routes.MapRoute(
                "change ward", //Route name
                "{lang}/change-ward", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Branch", action = "changeWard" }
            );

            routes.MapRoute(
                "ajax load content", //Route name
                "{lang}/get-data", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "AjaxGetNews" }
            );

            /* CAC TRANG TINH DE DEMO */
            routes.MapRoute(
                "khtc - dich vu dien tu", //Route name
                "vi/khach-hang-to-chuc/dich-vu-chung-khoan/dich-vu-dien-tu", //URL with parameters
                new { controller = "Home", action = "DichVuDienTuTC" }
            );
            routes.MapRoute(
                "khtc - dich vu dien tu en", //Route name
                "en/institutional-clients/securities-services/online-securities-trading", //URL with parameters
                new { controller = "Home", action = "DichVuDienTuTC" }
            );
            routes.MapRoute(
                "khtc - bieu phi", //Route name
                "vi/khach-hang-to-chuc/dich-vu-chung-khoan/bieu-phi", //URL with parameters
                new { controller = "Home", action = "BieuPhiTC" }
            );
            routes.MapRoute(
                "khtc - bieu phi en", //Route name
                "en/institutional-clients/securities-services/fees", //URL with parameters
                new { controller = "Home", action = "BieuPhiTC" }
            );
            routes.MapRoute(
                "khcn - dich vu dien tu", //Route name
                "vi/khach-hang-ca-nhan/dich-vu-chung-khoan/dich-vu-dien-tu", //URL with parameters
                new { controller = "Home", action = "DichVuDienTuCN" }
            );
            routes.MapRoute(
                "khcn - dich vu dien tu en", //Route name
                "en/individual-clients/securities-services/online-securities-trading", //URL with parameters
                new { controller = "Home", action = "DichVuDienTuCN" }
            );
            routes.MapRoute(
                "khcn - bieu phi", //Route name
                "vi/khach-hang-ca-nhan/dich-vu-chung-khoan/bieu-phi", //URL with parameters
                new { controller = "Home", action = "BieuPhi" }
            );
            routes.MapRoute(
                "khcn - bieu phi en", //Route name
                "en/individual-clients/securities-services/fees", //URL with parameters
                new { controller = "Home", action = "BieuPhi" }
            );
            routes.MapRoute(
                "dieu khoan su dung", //Route name
                "vi/dieu-khoan-su-dung", //URL with parameters
                new { controller = "Home", action = "TermOfUse" }
            );
            routes.MapRoute(
                "dieu khoan su dung en", //Route name
                "en/terms-of-use", //URL with parameters
                new { controller = "Home", action = "TermOfUse" }
            );
            routes.MapRoute(
                "lich su kien", //Route name
                "vi/cham-soc-khach-hang/lich-su-kien", //URL with parameters
                new { controller = "Home", action = "Events" }
            );
            routes.MapRoute(
                "lich su kien en", //Route name
                "en/help-center/right-exercise-and-transfer-to-trading-account", //URL with parameters
                new { controller = "Home", action = "Events" }
            );
            routes.MapRoute(
                "set fastlink vi", //Route name
                "vi/set-fastlink", //URL with parameters
                new { controller = "Home", action = "SetFastLink" }
            );
            routes.MapRoute(
                "set fastlink en", //Route name
                "vi/set-fastlink", //URL with parameters
                new { controller = "Home", action = "SetFastLink" }
            );
            /*
            routes.MapRoute(
                "khcn - dich vu dau tu - thuong vu dien hinh", //Route name
                "vi/khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh", //URL with parameters
                new { controller = "Home", action = "ThuongVu" }
            );
            */
            /* END CAC TRANG TINH DE DEMO */

            routes.MapRoute(
                "ajax data ttck", //Route name
                "{lang}/get-data-ttck", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "MarketNewsData" }
            );

            routes.MapRoute(
                "detail tin ttck - vi", //Route name
                "vi/trung-tam-nghien-cuu/tin-tuc-thi-truong/{category_slug}/{message_id}/{slug}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "MarketNewsDetail" }
            );

            routes.MapRoute(
                "detail tin ttck - en", //Route name
                "en/research-center/market-overview/{category_slug}/{message_id}/{slug}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "MarketNewsDetail" }
            );
            /* TUYEN DUNG */
            routes.MapRoute(
                "tuyen dung | active - vi", //Route name
                "vi/tuyen-dung/active/{hash}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Active", hash = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | active - en", //Route name
                "en/recruitment/active/{hash}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Active", hash = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | get pass - vi", //Route name
                "vi/tuyen-dung/get-pass/{hash}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Getpass", hash = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | get pass - en", //Route name
                "en/recruitment/get-pass/{hash}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Getpass", hash = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | quen mat khau - vi", //Route name
                "vi/tuyen-dung/quen-mat-khau", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Forgot" }
            );
            routes.MapRoute(
                "tuyen dung | fotget password - en", //Route name
                "en/recruitment/forgot-password", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Forgot" }
            );
            routes.MapRoute(
                "tuyen dung | dang ky - vi", //Route name
                "vi/tuyen-dung/dang-ky", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Register" }
            );
            routes.MapRoute(
                "tuyen dung | dang ky - en", //Route name
                "en/recruitment/register", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Register" }
            );
            routes.MapRoute(
                "tuyen dung | dang nhap - vi", //Route name
                "vi/tuyen-dung/dang-nhap", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Login" }
            );
            routes.MapRoute(
                "tuyen dung | dang nhap - en", //Route name
                "en/recruitment/login", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Login" }
            );
            routes.MapRoute(
                "tuyen dung | thong tin ung vien - vi", //Route name
                "vi/tuyen-dung/thong-tin-ung-vien", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Resumes", step = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | thong tin ung vien - en", //Route name
                "en/recruitment/resumes", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Resumes", step = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | thong tin ung vien step - vi", //Route name
                "vi/tuyen-dung/thong-tin-ung-vien/{step}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Resumes", step = UrlParameter.Optional }
            );
            routes.MapRoute(
                "tuyen dung | thong tin ung vien step - en", //Route name
                "en/recruitment/resumes/{slug}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "RE_Resumes", step = UrlParameter.Optional }
            );

            routes.MapRoute(
                "tuyen dung | testing - vi", //Route name
                "vi/tuyen-dung/kiem-tra", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Re_Test" }
            );
            routes.MapRoute(
                "tuyen dung | testing - en", //Route name
                "en/recruitment/testing", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Re_Test" }
            );

            routes.MapRoute(
                "tuyen dung | bieu mau - vi", //Route name
                "vi/tuyen-dung/bieu-mau", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Forms" }
            );
            routes.MapRoute(
                "tuyen dung | bieu mau - en", //Route name
                "en/recruitment/form", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Forms" }
            );
            routes.MapRoute(
                "tuyen dung | huong dan nop ho so - vi", //Route name
                "vi/tuyen-dung/huong-dan-nop-ho-so", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "ApplyGuide" }
            );
            routes.MapRoute(
                "tuyen dung | huong dan nop ho so - en", //Route name
                "en/recruitment/application-instruction", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "ApplyGuide" }
            );
            routes.MapRoute(
                "tuyen dung | vi tri - vi", //Route name
                "vi/tuyen-dung/vi-tri-tuyen-dung", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Recruitment" }
            );
            routes.MapRoute(
                "tuyen dung | vi tri - en", //Route name
                "en/recruitment/opportunities", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Recruitment" }
            );
            routes.MapRoute(
                "tuyen dung | co hoi - vi", //Route name
                "vi/tuyen-dung/co-hoi-nghe-nghiep", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Recruitment" }
            );
            routes.MapRoute(
                "tuyen dung | co hoi - en", //Route name
                "en/recruitment/opportunities", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "Recruitment" }
            );
            routes.MapRoute(
                "tuyen dung | vi tri | detail - vi", //Route name
                "vi/tuyen-dung/vi-tri-tuyen-dung/{id}/{slug}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "JobDetail" }
            );
            routes.MapRoute(
                "tuyen dung | vi tri | detail - en", //Route name
                "en/recruitment/opportunities/{id}/{slug}", //URL with parameters
                new { lang = "[a-z]{2}(-[A-Z]{2})?", controller = "Home", action = "JobDetail" }
            );
            /* END TUYEN DUNG */

            routes.MapRoute(
                "TiengViet", //Route name
                "vi/{*url}", //URL with parameters
                new { controller = "Home", action = "Index", url = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "English", //Route name
                "en/{*url}", //URL with parameters
                new { controller = "Home", action = "Index", url = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            //captcha config
            CaptchaUtils.CaptchaManager.StorageProvider = new CookieStorageProvider();
            var imageGenerator = CaptchaUtils.ImageGenerator;
            imageGenerator.Width = 109;
            imageGenerator.Height = 28;
            //end captcha config

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}