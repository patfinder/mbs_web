﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;

namespace WizardCMS.Models
{
    public class ContactViewModel
    {
        public int ID { get; set; }
        public string FULLNAME { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string ADDRESS { get; set; }
        public string TITLE { get; set; }
        public string CONTENT { get; set; }
        public int STATUS { get; set; }
        public DateTime CREATED { get; set; }
    }

    public class ContactExcelModel
    {
        public int NO { get; set; }
        public string FULLNAME { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string ADDRESS { get; set; }
        public string TITLE { get; set; }
        public string MESSAGE { get; set; }
        public string STATUS { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ContactServices
    {
        #region service interface
        public static ContactViewModel Get(int Id)
        {
            try
            {
                return ContactDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(ContactViewModel viewModel)
        {
            try
            {
                return ContactDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return ContactDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(ContactViewModel viewModel)
        {
            try
            {
                return ContactDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return ContactDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return ContactDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ContactViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return ContactDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region service implement
        public class ContactDAL
        {
            #region the DAL Instance
            private static ContactDAL _instance;

            static public ContactDAL Instance 
            {
                get
                {
                    if (_instance == null)
                        _instance = new ContactDAL();
                    return _instance;
                }
            }
            #endregion

            #region DAL
            public ContactViewModel Get(int Id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_CONTACT
                                where a.ID == Id
                                select new ContactViewModel
                                {
                                    ID = (int)a.ID,
                                    FULLNAME = a.FULLNAME,
                                    EMAIL = a.EMAIL,
                                    PHONE = a.PHONE,
                                    ADDRESS = a.ADDRESS,
                                    TITLE = a.TITLE,
                                    CONTENT = a.CONTENT,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(ContactViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_CONTACT.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.FULLNAME = viewModel.FULLNAME;
                        node.EMAIL = viewModel.EMAIL;
                        node.PHONE = viewModel.PHONE;
                        node.ADDRESS = viewModel.ADDRESS;
                        node.TITLE = viewModel.TITLE;
                        node.CONTENT = viewModel.CONTENT;
                        node.STATUS = viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(ContactViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_CONTACT node = new WZ_CONTACT();
                        node.ID = this._getNextID();
                        node.FULLNAME = viewModel.FULLNAME;
                        node.EMAIL = viewModel.EMAIL;
                        node.PHONE = viewModel.PHONE;
                        node.ADDRESS = viewModel.ADDRESS;
                        node.TITLE = viewModel.TITLE;
                        node.CONTENT = viewModel.CONTENT;
                        node.STATUS = viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_CONTACT.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var slide = (context.WZ_CONTACT.Where(a => a.ID == ID)).FirstOrDefault();

                        if (slide == null) return false;

                        context.WZ_CONTACT.DeleteObject(slide);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_CONTACT.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_CONTACT
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.FULLNAME.ToLower().Contains(searchText) || a.EMAIL.ToLower().Contains(searchText) || a.PHONE.ToLower().Contains(searchText) || a.ADDRESS.ToLower().Contains(searchText) || a.TITLE.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<ContactViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_CONTACT
                                     select new ContactViewModel
                                     {
                                         ID = (int)a.ID,
                                         FULLNAME = a.FULLNAME,
                                         EMAIL = a.EMAIL,
                                         PHONE = a.PHONE,
                                         ADDRESS = a.ADDRESS,
                                         TITLE = a.TITLE,
                                         CONTENT = a.CONTENT,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.FULLNAME.ToLower().Contains(searchText) || a.EMAIL.ToLower().Contains(searchText) || a.PHONE.ToLower().Contains(searchText) || a.ADDRESS.ToLower().Contains(searchText) || a.TITLE.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_CONTACT_SEQ;
                        //    sequence.WZ_CONTACT_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "CONTACT" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "CONTACT";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        var check = context.WZ_CONTACT.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
        #endregion
    }
    
}