﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class TermViewModel
    {
        public int ID { get; set; }
        public int TAXONOMY_ID { get; set; }
        public int REFERANCE_ID { get; set; }
        public int STATUS { get; set; }
        public int PRIORITY { get; set; }
        public string NAME { get; set; }
        public string TAXONOMY_NAME { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }
        public string THUMBNAIL { get; set; }
        public string SERVICES { get; set; }

        public DateTime CREATED { get; set; }
        public DateTime MODIFIED { get; set; }

        public int VIEWS { get; set; }

        //public List<TermContent> ContentTranslates { get; set; }
        public Dictionary<string, TermContent> ContentTranslates { get; set; }

        public List<TermViewModel> Childs { get; set; }

        //Constructor
        public TermViewModel()
        {
            this.ContentTranslates = new Dictionary<string, TermContent>();
        }
    }

    public class TermContent
    {
        public int TID { get; set; }
        public string NAME { get; set; }
        public string SLUG { get; set; }
        public string DESCRIPTION { get; set; }
        public string IMAGE { get; set; }
        public string LANG { get; set; }

        public TermContent()
        { }
    }

    public static class TermServices
    {
        #region Articles services interface
        public static TermContent GetTermContentByLang(Dictionary<string, TermContent> lstTermContent, string lang = "vi")
        {
            try
            {
                if (lstTermContent != null && lstTermContent.ContainsKey(lang))
                {
                    return lstTermContent[lang];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static Dictionary<string, TermContent> GetContentTranslates(int TID)
        {
            try
            {
                return TermDAL.Instance.GetContentTranslates(TID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TermViewModel> GetTerms(int taxonomy = 0, string lang = "vi")
        {
            try
            {
                return TermDAL.Instance.GetTerms(taxonomy, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TermViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText = "", string dateFrom = "", string dateTo = "")
        {
            try
            {
                return TermDAL.Instance.GetSearchContent(parrent, page, perpage, searchText, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return TermDAL.Instance.GetCountSearchContent(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static TermViewModel GetById(int id)
        {
            try
            {
                return TermDAL.Instance.GetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static TermViewModel GetBySlug(string slug, int taxonomy = 0)
        {
            try
            {
                return TermDAL.Instance.GetBySlug(slug, taxonomy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static TermViewModel GetTermContentById(int id, string lang = "")
        {
            try
            {
                return TermDAL.Instance.GetTermContentById(id, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static TermViewModel AdminGetById(int id)
        {
            try
            {
                return TermDAL.Instance.AdminGetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(TermViewModel article)
        {
            try
            {
                return TermDAL.Instance.Update(article);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int id, int status)
        {
            try
            {
                return TermDAL.Instance.UpdateStatus(id, status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang)
        {
            try
            {
                return TermDAL.Instance.GetCount(parentId, fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<TermViewModel> GetByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return TermDAL.Instance.GetByPaging(parentId, dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert new article
        /// </summary>
        /// <param name="viewModel">Article View Model to create new article</param>
        /// <returns>The inserted article ID</returns>
        public static int Insert(TermViewModel viewModel)
        {
            try
            {
                return TermDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                return TermDAL.Instance.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteList(List<int> lstId)
        {
            try
            {
                return TermDAL.Instance.DeleteList(lstId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckSlug(string slug, string lang, int parent = 0)
        {
            try
            {
                return TermDAL.Instance.CheckSlug(slug, lang, parent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public class TermDAL
        {
            private static TermDAL _instance;

            static public TermDAL Instance { 
                get {
                    if (_instance == null)
                        _instance = new TermDAL();

                    return _instance;
                }
            }

            #region constructor
            public TermDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
           
            public Dictionary<string, TermContent> GetContentTranslates(int TID)
            {
                try
                {
                    Dictionary<string, TermContent> result = new Dictionary<string, TermContent>();
                    using (var context = new DBContext())
                    {
                        var node_contents = (from ct in context.WZ_TERM_CONTENT
                                    where ct.TID == TID
                                    orderby ct.LANG descending
                                    select new TermContent
                                    {
                                        TID = (int)ct.TID,
                                        NAME = ct.NAME,
                                        DESCRIPTION = ct.DESCRIPTION,
                                        SLUG = ct.SLUG,
                                        LANG = ct.LANG
                                    }).ToList();

                        if (node_contents != null)
                        {
                            foreach (var node_content in node_contents)
                            {
                                result.Add(node_content.LANG, node_content);
                            }
                        }
                    }
                    return result;
                }
                catch { }
                return null;
            }

            public List<TermViewModel> GetSearchContent(int parrent, int page = 1, int perpage = 10, string searchText="", string dateFrom="", string dateTo="")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query = from m in context.WZ_TERM
                                    from mc in context.WZ_TERM_CONTENT
                                    where mc.TID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                    orderby m.PRIORITY ascending, m.CREATED descending
                                    select new TermViewModel
                                    {
                                        ID = (int)m.ID,
                                        NAME = mc.NAME,
                                        DESCRIPTION = mc.DESCRIPTION,
                                        SLUG = mc.SLUG,
                                        CREATED = m.CREATED,
                                        MODIFIED = m.MODIFIED
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception){}
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            query = from a in query
                                    where SqlMethods.Like(a.NAME.ToLower(), "%" + searchText + "%")
                                    select a;
                        }

                        if (query.Count() > 0 && page > 0)
                        {
                            return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                        }

                        
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            public int GetCountSearchContent(int parentId, string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var result = from m in context.WZ_TERM
                                     from mc in context.WZ_TERM_CONTENT
                                     where mc.TID == m.ID && mc.LANG == currentLang && m.STATUS == Constants.SHOW
                                     select m;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_TERM_CONTENT
                                     where a.ID == ac.TID && ac.LANG == lang && SqlMethods.Like(ac.NAME.ToLower(), "%" + searchText + "%")
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            public List<TermViewModel> GetTerms(int taxonomy, string lang)
            {
                List<TermViewModel> result = new List<TermViewModel>();

                try
                {
                    using (var dbContext = new DBContext())
                    {
                        var query = from t in dbContext.WZ_TERM
                                    from c in dbContext.WZ_TERM_CONTENT
                                    from tx in dbContext.WZ_TAXONOMY.Where(tx=>t.TAXONOMY_ID == tx.ID).DefaultIfEmpty()
                                    where c.TID == t.ID && t.STATUS == 1 && c.LANG == lang
                                    orderby t.PRIORITY ascending, t.CREATED descending
                                    select new TermViewModel
                                    {
                                        ID = (int)t.ID,
                                        TAXONOMY_ID = (int)t.TAXONOMY_ID,
                                        REFERANCE_ID = tx != null ? (int)tx.REFERANCE_ID : 0,
                                        NAME = c.NAME,
                                        SLUG = c.SLUG,
                                        DESCRIPTION = c.DESCRIPTION,
                                        SERVICES = t.SERVICES,
                                        CREATED = t.CREATED,
                                        MODIFIED = t.MODIFIED
                                    };
                        if (taxonomy > 0)
                        {
                            query = query.Where(t => t.TAXONOMY_ID == taxonomy);
                        }

                        return query.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public TermViewModel GetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        var node = (from n in context.WZ_TERM
                                    where n.ID == id && n.STATUS == Constants.SHOW
                                    select new TermViewModel
                                    {
                                        ID = (int)n.ID,
                                        TAXONOMY_ID = (int)n.TAXONOMY_ID,
                                        SERVICES = n.SERVICES,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public TermViewModel GetBySlug(string slug, int taxonomy)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = StringHelper.language(); //Get the current language
                        if (taxonomy > 0)
                        {
                            var node = (from n in context.WZ_TERM
                                        from nc in context.WZ_TERM_CONTENT
                                        where nc.TID == n.ID && nc.SLUG == slug && n.STATUS == Constants.SHOW && n.TAXONOMY_ID == taxonomy
                                        select new TermViewModel
                                        {
                                            ID = (int)n.ID,
                                            TAXONOMY_ID = (int)n.TAXONOMY_ID,
                                            SERVICES = n.SERVICES,
                                            CREATED = n.CREATED,
                                            MODIFIED = n.MODIFIED,
                                            PRIORITY = (int)n.PRIORITY,
                                            STATUS = (int)n.STATUS,
                                            NAME = nc.NAME,
                                            SLUG = nc.SLUG
                                        }).FirstOrDefault();

                            if (node != null)
                            {
                                //node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                            }
                            return node;
                        }
                        else
                        {
                            var node = (from n in context.WZ_TERM
                                        from nc in context.WZ_TERM_CONTENT
                                        where nc.SLUG == slug && n.STATUS == Constants.SHOW
                                        select new TermViewModel
                                        {
                                            ID = (int)n.ID,
                                            TAXONOMY_ID = (int)n.TAXONOMY_ID,
                                            SERVICES = n.SERVICES,
                                            CREATED = n.CREATED,
                                            MODIFIED = n.MODIFIED,
                                            PRIORITY = (int)n.PRIORITY,
                                            STATUS = (int)n.STATUS,
                                            NAME = nc.NAME,
                                            SLUG = nc.SLUG
                                        }).FirstOrDefault();

                            if (node != null)
                            {
                                //node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                            }
                            return node;
                        }
                    }
                }
                catch
                {
                    return null;
                }
            }
            /// <summary>
            /// GET ARTICLE DETAIL
            /// </summary>
            /// <param name="id">ID ARTICLE</param>
            /// <returns></returns>
            public TermViewModel GetTermContentById(int id, string lang = "")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string current_lang = lang == "" ? StringHelper.language() : lang; //Get the current language
                        var node = (from n in context.WZ_TERM
                                    from nc in context.WZ_TERM_CONTENT
                                    where n.ID == id && nc.TID == n.ID && n.STATUS == Constants.SHOW && nc.LANG == current_lang
                                    select new TermViewModel
                                    {
                                        ID = (int)n.ID,
                                        NAME = nc.NAME,
                                        DESCRIPTION = nc.DESCRIPTION,
                                        SLUG = nc.SLUG,
                                        SERVICES = n.SERVICES,
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        PRIORITY = (int)n.PRIORITY,
                                        STATUS = (int)n.STATUS
                                    }).FirstOrDefault();

                        if (node != null)
                        {
                            node.ContentTranslates = this.GetContentTranslates((int)node.ID);
                        }
                        return node;
                    }
                }
                catch
                {
                    return null;
                }
            }

            /********* ADMIN AREA ********/
            public TermViewModel AdminGetById(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = (from a in context.WZ_TERM
                                       where (a.ID == id)
                                       select new TermViewModel
                                       {
                                           ID = (int)a.ID,
                                           STATUS = (int)a.STATUS,
                                           MODIFIED = a.MODIFIED,
                                           CREATED = a.CREATED,
                                           PRIORITY = (int)a.PRIORITY,
                                       }).FirstOrDefault();

                        if (article != null)
                            article.ContentTranslates = this.GetContentTranslates(article.ID);

                        return article;
                    }
                }
                catch
                {
                    return null;
                }
            }

            //update article
            public bool Update(TermViewModel term)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_TERM.Where(a => a.ID == term.ID).FirstOrDefault();
                        if (node != null)
                        {
                            node.STATUS = term.STATUS;
                            node.TAXONOMY_ID = term.TAXONOMY_ID;
                            node.SERVICES = term.SERVICES;
                            node.PRIORITY = term.PRIORITY;
                            node.MODIFIED = DateTime.Now;
                            context.SaveChanges();

                            //update content translates
                            if (term.ContentTranslates != null)
                            {
                                foreach (KeyValuePair<string, TermContent> item in term.ContentTranslates)
                                {
                                    var content_translate = item.Value;
                                    var node_content_lang = context.WZ_TERM_CONTENT.Where(a => a.TID == term.ID && a.LANG == content_translate.LANG).FirstOrDefault();
                                    if (node_content_lang != null && !string.IsNullOrEmpty(content_translate.NAME))
                                    {
                                        node_content_lang.NAME = content_translate.NAME;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;

                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        node_content_lang = new WZ_TERM_CONTENT();
                                        node_content_lang.TID = term.ID;
                                        node_content_lang.LANG = content_translate.LANG;
                                        node_content_lang.NAME = content_translate.NAME;
                                        node_content_lang.SLUG = content_translate.SLUG;
                                        node_content_lang.DESCRIPTION = content_translate.DESCRIPTION;
                                        context.WZ_TERM_CONTENT.AddObject(node_content_lang);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //update article status
            public bool UpdateStatus(int id, int status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_TERM.Where(a => a.ID == id).FirstOrDefault();

                        if (article != null)
                        {
                            article.STATUS = status;
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return false;
            }

            //count article for paging
            public int GetCount(int parentId, string fromDate, string toDate, string searchText, string lang="vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_TERM
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     from ac in context.WZ_TERM_CONTENT
                                     where a.ID == ac.TID && ac.LANG == lang && ac.NAME.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception)
                {
                }
                return 0;
            }

            //get list nodes by paging
            public List<TermViewModel> GetByPaging(int parentId, string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<TermViewModel> lstArticlles = new List<TermViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_TERM
                                     join ac in context.WZ_TERM_CONTENT on a.ID equals ac.TID
                                     where ac.LANG == lang
                                     select new TermViewModel {
                                        ID = (int)a.ID,
                                        TAXONOMY_ID = (int)a.TAXONOMY_ID,
                                        TAXONOMY_NAME = "",
                                        CREATED = a.CREATED,
                                        MODIFIED = a.MODIFIED,
                                        PRIORITY = (int)a.PRIORITY,
                                        SERVICES = a.SERVICES,
                                        NAME = ac.NAME,
                                        SLUG = ac.SLUG,
                                        DESCRIPTION = ac.DESCRIPTION,
                                        STATUS = (int)a.STATUS
                                    };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     //from ac in context.WZ_TERM_CONTENT
                                     where a.NAME.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        var temp = result.ToList();

                        var query = from n in temp
                                    join ac in context.WZ_TERM_CONTENT on n.ID equals ac.TID
                                    join t in context.WZ_TAXONOMY on n.TAXONOMY_ID equals t.ID into taxonomy
                                    from tx in taxonomy.DefaultIfEmpty()
                                    where n.ID == ac.TID && ac.LANG == lang
                                    select new TermViewModel {
                                        ID = (int)n.ID,
                                        TAXONOMY_ID = (int)n.TAXONOMY_ID,
                                        TAXONOMY_NAME = tx != null ? tx.NAME : "",
                                        CREATED = n.CREATED,
                                        MODIFIED = n.MODIFIED,
                                        PRIORITY = (int)n.PRIORITY,
                                        NAME = ac.NAME,
                                        SLUG = ac.SLUG,
                                        DESCRIPTION = ac.DESCRIPTION,
                                        STATUS = (int)n.STATUS,
                                    };

                        var data = query.ToList();
                        if (data != null)
                        {
                            foreach (var item in data) {
                                item.ContentTranslates = this.GetContentTranslates(item.ID);
                            }
                        }

                        return data;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            //Insert new article
            public int Insert(TermViewModel termView)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_TERM term = new WZ_TERM();
                        if (termView != null)
                        {
                            term.ID = this._getNextID();
                            term.STATUS = termView.STATUS;
                            term.TAXONOMY_ID = termView.TAXONOMY_ID;
                            term.PRIORITY = termView.PRIORITY;
                            term.SERVICES = termView.SERVICES;
                            term.CREATED = DateTime.Now;
                            term.MODIFIED = DateTime.Now;

                            context.WZ_TERM.AddObject(term);
                            context.SaveChanges();

                            if (termView.ContentTranslates.Count > 0)
                            {
                                foreach (KeyValuePair<string, TermContent> item in termView.ContentTranslates)
                                {
                                    var node_content = item.Value;
                                    WZ_TERM_CONTENT content = new WZ_TERM_CONTENT();
                                    content.TID = term.ID;
                                    content.NAME = node_content.NAME;
                                    content.SLUG = node_content.SLUG;
                                    content.DESCRIPTION = node_content.DESCRIPTION;
                                    content.LANG = node_content.LANG;

                                    context.WZ_TERM_CONTENT.AddObject(content);
                                    context.SaveChanges();
                                }
                            }
                        }
                        return (int)term.ID;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return Constants.ACTION_FAULT;
            }

            /// <summary>
            /// Get the next article ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "TERM" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "TERM";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        var check = context.WZ_TERM.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();

                        return newID;
                    }
                }
                catch { }
                return 0;
            }

            /// <summary>
            /// Delete article
            /// </summary>
            /// <param name="id">Aritle ID</param>
            /// <returns></returns>
            public bool Delete(int id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var article = context.WZ_TERM.Where(a => a.ID == id).FirstOrDefault();

                        if (article != null)
                        {
                            context.WZ_TERM.DeleteObject(article);
                            context.SaveChanges();

                            //delete article content translate
                            var content_translates = context.WZ_TERM_CONTENT.Where(ac=>ac.TID == id).ToList();
                            if (content_translates != null)
                            {
                                foreach (var node_translate in content_translates)
                                {
                                    context.WZ_TERM_CONTENT.DeleteObject(node_translate);
                                    context.SaveChanges();
                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            //delete list articles
            public bool DeleteList(List<int> lstId)
            {
                try
                {
                    if (lstId != null)
                    {
                        foreach (int id in lstId)
                        {
                            this.Delete(id);
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            /// <summary>
            /// Check if article slug already exist following language
            /// </summary>
            /// <param name="slug">Input slug</param>
            /// <param name="lang">Input language</param>
            /// <returns>True is exist</returns>
            public bool CheckSlug(string slug, string lang, int parent = 0)
            {
                try
                {
                    lang = string.IsNullOrEmpty(lang) ? "vi" : lang;
                    using (var context = new DBContext())
                    {
                        if (parent > 0)
                        {
                            var node = (from a in context.WZ_TERM
                                        from ac in context.WZ_TERM_CONTENT
                                        where ac.TID == a.ID && ac.LANG == lang && ac.SLUG == slug && a.TAXONOMY_ID == parent
                                        select a).FirstOrDefault();
                            if (node != null) return true;
                        }
                        else
                        {
                            var node = (from a in context.WZ_TERM
                                        from ac in context.WZ_TERM_CONTENT
                                        where ac.TID == a.ID && ac.LANG == lang && ac.SLUG == slug
                                        select a).FirstOrDefault();
                            if (node != null) return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return false;
            }

            #endregion
        }
    }
}