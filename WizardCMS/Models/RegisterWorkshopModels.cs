﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using System.Collections;

namespace WizardCMS.Models
{
    public class RegWorkshopViewModel
    {
        public int ID { get; set; }
        public int NID { get; set; }
        public int STATUS { get; set; }

        public string WORKSHOP { get; set; }
        public string FULLNAME { get; set; }
        public string CMND { get; set; }
        public string CMND_NOICAP { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string BANK_CARD { get; set; }
        public string BANK_CARD_CN { get; set; }
        public string MBS_ACCOUNT { get; set; }

        public DateTime? CMND_NGAYCAP { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }
    public static class RegWorkshopServices
    {
        #region service interface
        public static RegWorkshopViewModel Get(int Id)
        {
            try
            {
                return RegWSDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(RegWorkshopViewModel viewModel)
        {
            try
            {
                return RegWSDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return RegWSDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(RegWorkshopViewModel viewModel)
        {
            try
            {
                return RegWSDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return RegWSDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<RegWorkshopViewModel> GetData(Hashtable searchParams, out int total_rows)
        {
            try
            {
                return RegWSDAL.Instance.GetData(searchParams, out total_rows);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi", int ws = 0)
        {
            try
            {
                return RegWSDAL.Instance.GetCount(fromDate, toDate, searchText, lang, ws);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<RegWorkshopViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi", int ws = 0)
        {
            try
            {
                return RegWSDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang, ws);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        public class RegWSDAL
        {
            #region the DAL Instance
            private static RegWSDAL _instance;

            static public RegWSDAL Instance 
            {
                get
                {
                    if (_instance == null)
                        _instance = new RegWSDAL();
                    return _instance;
                }
            }
            #endregion

            #region DAL
            public RegWorkshopViewModel Get(int Id)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_WORKSHOP_REGISTER
                                from w in context.WZ_ARTICLES_CONTENT
                                where a.ID == Id && a.NID == w.NID && w.LANG == "vi"
                                select new RegWorkshopViewModel
                                {
                                    ID = (int)a.ID,
                                    NID = (int)a.NID,
                                    FULLNAME = a.FULLNAME,
                                    WORKSHOP = w.TITLE,
                                    CMND = a.CMND,
                                    CMND_NOICAP = a.CMND_NOICAP,
                                    CMND_NGAYCAP = a.CMND_NGAYCAP,
                                    EMAIL = a.EMAIL,
                                    PHONE = a.PHONE,
                                    BANK_CARD = a.BANK_CARD,
                                    BANK_CARD_CN = a.BANK_CARD_CN,
                                    MBS_ACCOUNT = a.MBS_ACCOUNT,
                                    STATUS = (int)a.STATUS,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(RegWorkshopViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = context.WZ_WORKSHOP_REGISTER.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.NID = viewModel.NID;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CMND = viewModel.CMND;
                        node.CMND_NGAYCAP = viewModel.CMND_NGAYCAP;
                        node.CMND_NOICAP = viewModel.CMND_NOICAP;
                        node.EMAIL = viewModel.EMAIL;
                        node.PHONE = viewModel.PHONE;
                        node.BANK_CARD = viewModel.BANK_CARD;
                        node.BANK_CARD_CN = viewModel.BANK_CARD_CN;
                        node.MBS_ACCOUNT = viewModel.MBS_ACCOUNT;
                        node.MODIFIED = DateTime.Now;
                        node.STATUS = viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(RegWorkshopViewModel viewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_WORKSHOP_REGISTER node = new WZ_WORKSHOP_REGISTER();
                        node.ID = this._getNextID();
                        node.NID = viewModel.NID;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CMND = viewModel.CMND;
                        node.CMND_NGAYCAP = viewModel.CMND_NGAYCAP;
                        node.CMND_NOICAP = viewModel.CMND_NOICAP;
                        node.EMAIL = viewModel.EMAIL;
                        node.PHONE = viewModel.PHONE;
                        node.BANK_CARD = viewModel.BANK_CARD;
                        node.BANK_CARD_CN = viewModel.BANK_CARD_CN;
                        node.MBS_ACCOUNT = viewModel.MBS_ACCOUNT;
                        node.STATUS = viewModel.STATUS;
                        node.MODIFIED = DateTime.Now;
                        node.CREATED = DateTime.Now;

                        context.WZ_WORKSHOP_REGISTER.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_WORKSHOP_REGISTER.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_WORKSHOP_REGISTER.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var node = (context.WZ_WORKSHOP_REGISTER.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        node.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi", int ws = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_WORKSHOP_REGISTER
                                     select a;

                        if (ws > 0)
                        {
                            result = result.Where(a => a.NID == ws);
                        }

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.FULLNAME.ToLower().Contains(searchText) || a.EMAIL.ToLower().Contains(searchText) || a.PHONE.ToLower().Contains(searchText) || a.CMND.ToLower().Contains(searchText) || a.BANK_CARD.ToLower().Contains(searchText) || a.MBS_ACCOUNT.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            public List<RegWorkshopViewModel> GetData(Hashtable searchParams, out int total_rows)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        string currentLang = StringHelper.language();
                        var query =  from a in context.WZ_WORKSHOP_REGISTER
                                     from ws in context.WZ_ARTICLES_CONTENT
                                     where (ws.NID == a.NID && ws.LANG == "vi")
                                     select new RegWorkshopViewModel
                                     {
                                         ID = (int)a.ID,
                                         NID = (int)a.NID,
                                         WORKSHOP = ws != null ? ws.TITLE : "",
                                         FULLNAME = a.FULLNAME,
                                         CMND = a.CMND,
                                         CMND_NOICAP = a.CMND_NOICAP,
                                         CMND_NGAYCAP = a.CMND_NGAYCAP,
                                         EMAIL = a.EMAIL,
                                         PHONE = a.PHONE,
                                         BANK_CARD = a.BANK_CARD,
                                         BANK_CARD_CN = a.BANK_CARD_CN,
                                         MBS_ACCOUNT = a.MBS_ACCOUNT,
                                         STATUS = (int)a.STATUS,
                                         MODIFIED = a.MODIFIED,
                                         CREATED = a.CREATED
                                     };
                        if (searchParams.ContainsKey("nid"))
                        {
                            int parent = (int)searchParams["nid"];
                            if (parent > 0)
                                query = query.Where(a => a.NID == parent);
                        }

                        if (searchParams.ContainsKey("not_ins"))
                        {
                            int[] not_ins = searchParams["not_ins"] as int[];
                            if (not_ins != null && not_ins.Count() > 0)
                                query = query.Where(a => !not_ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("ins"))
                        {
                            int[] ins = searchParams["ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.ID));
                        }

                        if (searchParams.ContainsKey("parent_ins"))
                        {
                            int[] ins = searchParams["parent_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => ins.Contains((int)a.NID));
                        }

                        if (searchParams.ContainsKey("parent_not_ins"))
                        {
                            int[] ins = searchParams["parent_not_ins"] as int[];
                            if (ins != null && ins.Count() > 0)
                                query = query.Where(a => !ins.Contains((int)a.NID));
                        }

                        if (searchParams.ContainsKey("from") && !string.IsNullOrEmpty((string)searchParams["from"]))
                        {
                            string dateFrom = (string)searchParams["from"];
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                query = query.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("to") && !string.IsNullOrEmpty((string)searchParams["to"]))
                        {
                            string dateTo = (string)searchParams["to"];
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                query = query.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception) { }
                        }

                        if (searchParams.ContainsKey("searchText"))
                        {
                            string searchText = (string)searchParams["searchText"];
                            if (!string.IsNullOrEmpty(searchText))
                                query = from a in query
                                        where a.FULLNAME.ToLower().Contains(searchText) || a.EMAIL.ToLower().Contains(searchText) || a.CMND.ToLower().Contains(searchText) || a.BANK_CARD.ToLower().Contains(searchText) || a.MBS_ACCOUNT.ToLower().Contains(searchText)
                                        select a;
                        }

                        //Get Total Record
                        total_rows = query.Count();

                        query = query.OrderByDescending(a => a.CREATED);

                        if (searchParams.ContainsKey("page") && searchParams.ContainsKey("perpage"))
                        {
                            int page = (int)searchParams["page"];
                            int perpage = (int)searchParams["perpage"];

                            if (query.Count() > 0 && page > 0)
                            {
                                return query.Skip((page - 1) * perpage).Take(perpage).ToList();
                            }
                        }
                        else
                        {
                            return query.ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }

                total_rows = 0;
                return null;
            }

            //get list article by paging
            public List<RegWorkshopViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi", int ws = 0)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_WORKSHOP_REGISTER
                                     from w in context.WZ_ARTICLES_CONTENT
                                     where a.NID == w.NID && w.LANG == "vi"
                                     select new RegWorkshopViewModel
                                     {
                                         ID = (int)a.ID,
                                         NID = (int)a.NID,
                                         FULLNAME = a.FULLNAME,
                                         WORKSHOP = w.TITLE,
                                         CMND = a.CMND,
                                         CMND_NOICAP = a.CMND_NOICAP,
                                         CMND_NGAYCAP = a.CMND_NGAYCAP,
                                         EMAIL = a.EMAIL,
                                         PHONE = a.PHONE,
                                         BANK_CARD = a.BANK_CARD,
                                         BANK_CARD_CN = a.BANK_CARD_CN,
                                         MBS_ACCOUNT = a.MBS_ACCOUNT,
                                         STATUS = (int)a.STATUS,
                                         MODIFIED = a.MODIFIED,
                                         CREATED = a.CREATED
                                     };
                        if (ws > 0) {
                            result = result.Where(a => a.NID == ws);
                        }
                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.FULLNAME.ToLower().Contains(searchText) || a.EMAIL.ToLower().Contains(searchText) || a.CMND.ToLower().Contains(searchText) || a.BANK_CARD.ToLower().Contains(searchText) || a.MBS_ACCOUNT.ToLower().Contains(searchText)
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_WORKSHOP_REGISTER_SEQ;
                        //    sequence.WZ_WORKSHOP_REGISTER_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "WORKSHOP_REGISTER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "WORKSHOP_REGISTER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        var check = context.WZ_WORKSHOP_REGISTER.Where(a => a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
    }
    
}