﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class UserMemberViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string FULNAME { get; set; }
        public string RELATIVE { get; set; }
        public string DEGREE { get; set; }
        public string YOB { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReUserMemberServices
    {
        #region service interface
        public static int Insert(UserMemberViewModel viewModel)
        {
            try
            {
                return UserMemberDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(UserMemberViewModel viewModel)
        {
            try
            {
                return UserMemberDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return UserMemberDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static UserMemberViewModel Get(int id)
        {
            try
            {
                return UserMemberDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<UserMemberViewModel> GetAll(int uid)
        {
            try
            {
                return UserMemberDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class UserMemberDAL
        {
            #region static instance
            private static UserMemberDAL _instance;

            static public UserMemberDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new UserMemberDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public UserMemberViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_USER_MEMBER
                                where a.ID == Id
                                select new UserMemberViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    FULNAME = a.FULNAME,
                                    DEGREE = a.DEGREE,
                                    RELATIVE = a.RELATIVE,
                                    YOB = a.YOB,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(UserMemberViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_USER_MEMBER.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        //node.PASSWORD = viewModel.PASSWORD;
                        node.FULNAME = viewModel.FULNAME;
                        node.DEGREE = viewModel.DEGREE;
                        node.RELATIVE = viewModel.RELATIVE;
                        node.YOB = viewModel.YOB;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<UserMemberViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_USER_MEMBER
                                     where a.USER_ID == uid
                                     select new UserMemberViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         FULNAME = a.FULNAME,
                                         DEGREE = a.DEGREE,
                                         RELATIVE = a.RELATIVE,
                                         YOB = a.YOB,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_USER_MEMBER.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_USER_MEMBER.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(UserMemberViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_USER_MEMBER node = new WZ_RE_USER_MEMBER();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.FULNAME = viewModel.FULNAME;
                        node.RELATIVE = viewModel.RELATIVE;
                        node.YOB = viewModel.YOB;
                        node.DEGREE = viewModel.DEGREE;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_USER_MEMBER.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_USER_MEMBER" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_USER_MEMBER";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_USER_MEMBER.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}