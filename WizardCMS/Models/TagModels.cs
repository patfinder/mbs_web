﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.domain;
using WizardCMS.Const;
using WizardCMS.Helpers;
using WizardCMS.DTO;
using System.Web.Configuration;
using System.Transactions;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class TagModel
    {
        public int ARTICLE_ID { get; set; }

        public int PARENT_ID { get; set; }

        // TODO
        public bool IS_MAIN_TAG { get; set; }
    }

    public static class TagServices
    {
        #region Articles services interface
        public static List<TagModel> GetTagList(int artricleId)
        {
            try
            {
                return TagDAL.Instance.GetTagList(artricleId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public class TagDAL
        {
            private static TagDAL _instance;

            static public TagDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new TagDAL();

                    return _instance;
                }
            }

            #region constructor
            public TagDAL() : base()
            {
            }
            #endregion

            #region data access layer impliment
            public List<TagModel> GetTagList(int articleId)
            {
                using (var context = new DBContext())
                {
                    return (
                        from ct in context.WZ_TAG
                        where ct.ARTICLE_ID == articleId
                        select new TagModel
                        {
                            ARTICLE_ID = articleId,
                            PARENT_ID = (int)ct.PARENT_ID,
                        }).ToList();
                }
            }
            #endregion
        }
    }
}