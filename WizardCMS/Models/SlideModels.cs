﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;

namespace WizardCMS.Models
{
    public class SlideViewModel
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public string LINK { get; set; }
        public string LANG { get; set; }
        public string IMAGE { get; set; }
        public string IMAGE_TABLET { get; set; }
        public string IMAGE_MOBILE { get; set; }
        public int PRIORITY { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class SlideServices
    {
        #region service interface
        public static SlideViewModel Get(int Id)
        {
            try
            {
                return SlideDAL.Instance.Get(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(SlideViewModel slideViewModel)
        {
            try
            {
                return SlideDAL.Instance.Update(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateStatus(int ID, int Status)
        {
            try
            {
                return SlideDAL.Instance.UpdateStatus(ID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int Insert(SlideViewModel slideViewModel)
        {
            try
            {
                return SlideDAL.Instance.Insert(slideViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return SlideDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
        {
            try
            {
                return SlideDAL.Instance.GetCount(fromDate, toDate, searchText, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SlideViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
        {
            try
            {
                return SlideDAL.Instance.GetList(dateFrom, dateTo, searchText, paging, sortBy, sortType, lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<SlideViewModel> GetList(string lang = "vi")
        {
            try
            {
                return SlideDAL.Instance.GetList(lang);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion service interface

        #region data access layer
        public class SlideDAL
        {
            #region static instance
            private static SlideDAL _instance;

            static public SlideDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new SlideDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public SlideViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new DBContext())
                    {
                        return (from a in context.WZ_SLIDES
                                where a.ID == Id
                                select new SlideViewModel
                                {
                                    ID = (int)a.ID,
                                    NAME = a.NAME,
                                    LINK = a.LINK,
                                    LANG = a.LANG,
                                    IMAGE = a.IMAGE,
                                    IMAGE_TABLET = a.IMAGE_TABLET,
                                    IMAGE_MOBILE = a.IMAGE_MOBILE,
                                    STATUS = (int)a.STATUS,
                                    PRIORITY = (int)a.PRIORITY,
                                    MODIFIED = a.MODIFIED,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(SlideViewModel slideViewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var slide = context.WZ_SLIDES.Where(a => a.ID == slideViewModel.ID).FirstOrDefault();
                        if (slide == null) return false;

                        slide.NAME = slideViewModel.NAME;
                        slide.LINK = slideViewModel.LINK;
                        slide.LANG = slideViewModel.LANG;
                        slide.IMAGE = slideViewModel.IMAGE;
                        slide.IMAGE_TABLET = slideViewModel.IMAGE_TABLET;
                        slide.IMAGE_MOBILE = slideViewModel.IMAGE_MOBILE;
                        slide.PRIORITY = slideViewModel.PRIORITY;
                        slide.STATUS = slideViewModel.STATUS;
                        slide.MODIFIED = DateTime.Now;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(SlideViewModel slideViewModel)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        WZ_SLIDES slide = new WZ_SLIDES();
                        slide.ID = this._getNextID();
                        slide.NAME = slideViewModel.NAME;
                        slide.LINK = slideViewModel.LINK;
                        slide.LANG = slideViewModel.LANG;
                        slide.IMAGE = slideViewModel.IMAGE;
                        slide.IMAGE_TABLET = slideViewModel.IMAGE_TABLET;
                        slide.IMAGE_MOBILE = slideViewModel.IMAGE_MOBILE;
                        slide.PRIORITY = slideViewModel.PRIORITY;
                        slide.STATUS = slideViewModel.STATUS;
                        slide.MODIFIED = DateTime.Now;
                        slide.CREATED = DateTime.Now;

                        context.WZ_SLIDES.AddObject(slide);
                        context.SaveChanges();

                        return (int)slide.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var slide = (context.WZ_SLIDES.Where(a=>a.ID == ID)).FirstOrDefault();

                        if (slide == null) return false;

                        context.WZ_SLIDES.DeleteObject(slide);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public bool UpdateStatus(int ID, int Status)
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var slide = (context.WZ_SLIDES.Where(a => a.ID == ID)).FirstOrDefault();

                        if (slide == null) return false;

                        slide.STATUS = Status;
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int GetCount(string fromDate, string toDate, string searchText, string lang = "vi")
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_SLIDES
                                     select a;

                        if (String.IsNullOrWhiteSpace(fromDate) == false)
                        {
                            try
                            {
                                DateTime dateFrom = DatetimeHelper.StringToDate(fromDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateFrom = DatetimeHelper.GetStartOfDay(dateFrom);
                                result = result.Where(x => x.CREATED >= dateFrom);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(toDate) == false)
                        {
                            try
                            {
                                DateTime dateTo = DatetimeHelper.StringToDate(toDate.Trim(), DatetimeHelper.DD_MM_YYYY);
                                dateTo = DatetimeHelper.GetEndOfDay(dateTo);
                                result = result.Where(x => x.CREATED <= dateTo);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where a.NAME.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText)
                                     select a;
                        }
                        return result.Count();
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
                return 0;
            }

            //get list article by paging
            public List<SlideViewModel> GetList(string dateFrom, string dateTo, string searchText, Paging paging, string sortBy = "", string sortType = "", string lang = "vi")
            {
                try
                {
                    List<SlideViewModel> lsSlideViewModels = new List<SlideViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_SLIDES
                                     select new SlideViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME  = a.NAME,
                                         LINK = a.LINK,
                                         IMAGE = a.IMAGE,
                                         IMAGE_TABLET = a.IMAGE_TABLET,
                                         IMAGE_MOBILE = a.IMAGE_MOBILE,
                                         LANG = a.LANG,
                                         PRIORITY = (int)a.PRIORITY,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        if (String.IsNullOrWhiteSpace(dateFrom) == false)
                        {
                            try
                            {
                                DateTime fromDate = DatetimeHelper.StringToDate(dateFrom.Trim(), DatetimeHelper.DD_MM_YYYY);
                                fromDate = DatetimeHelper.GetStartOfDay(fromDate);
                                result = result.Where(x => x.CREATED >= fromDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(dateTo) == false)
                        {
                            try
                            {
                                DateTime toDate = DatetimeHelper.StringToDate(dateTo.Trim(), DatetimeHelper.DD_MM_YYYY);
                                toDate = DatetimeHelper.GetEndOfDay(toDate);
                                result = result.Where(x => x.CREATED <= toDate);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (String.IsNullOrWhiteSpace(searchText) == false)
                        {
                            searchText = searchText.ToLower();
                            result = from a in result
                                     where (a.NAME.ToLower().Contains(searchText) || a.LINK.ToLower().Contains(searchText))
                                     select a;
                        }

                        if (sortBy == "") sortBy = Const.Constants.SORT_PORPERTY_DEFAULT;
                        if (sortType == "") sortType = Const.Constants.SORT_DESC;
                        result = result.OrderBy(sortBy.ToUpper() + " " + sortType);

                        if (paging.Perpage > 0)
                        {
                            result = result.Skip(paging.Offset).Take(paging.Perpage);
                        }

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public List<SlideViewModel> GetList(string lang = "vi")
            {
                try
                {
                    List<SlideViewModel> lsSlideViewModels = new List<SlideViewModel>();

                    using (var context = new DBContext())
                    {
                        var result = from a in context.WZ_SLIDES
                                     where a.LANG == lang && a.STATUS == 1
                                     select new SlideViewModel
                                     {
                                         ID = (int)a.ID,
                                         NAME = a.NAME,
                                         LINK = a.LINK,
                                         IMAGE = a.IMAGE,
                                         IMAGE_TABLET = a.IMAGE_TABLET,
                                         IMAGE_MOBILE = a.IMAGE_MOBILE,
                                         LANG = a.LANG,
                                         PRIORITY = (int)a.PRIORITY,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED,
                                         MODIFIED = a.MODIFIED,
                                     };

                        result = result.OrderBy(Const.Constants.SORT_PORPERTY_DEFAULT.ToUpper() + " " + Const.Constants.SORT_DESC);

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            /// <summary>
            /// Get the next ID
            /// </summary>
            /// <returns></returns>
            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        //var sequence = (from s in context.ADMIN_WZ_SEQUENCES select s).FirstOrDefault();
                        //decimal newID = 1;
                        //if (sequence != null)
                        //{
                        //    newID = sequence.WZ_SLIDES_SEQ;
                        //    sequence.WZ_SLIDES_SEQ += 1;
                        //    context.SaveChanges();
                        //}
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "SLIDE" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "SLIDE";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                        }

                        //check exist ID
                        var check = context.WZ_SLIDES.Where(a=>a.ID == newID).FirstOrDefault();
                        if (check != null)
                            return this._getNextID();
                        return newID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            #endregion
        }
        #endregion
    }
}