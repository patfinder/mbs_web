﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using WizardCMS.Helpers;
using WizardCMS.domain;
using WizardCMS.Const;
using System.Data.Linq.SqlClient;

namespace WizardCMS.Models
{
    public class AttestorViewModel
    {
        public int ID { get; set; }
        public int USER_ID { get; set; }
        public string FULLNAME { get; set; }
        public string CAREER { get; set; }
        public string ADDRESS_PHONE_FAX { get; set; }
        public string MAJOR { get; set; }
        public int STATUS { get; set; }
        public DateTime MODIFIED { get; set; }
        public DateTime CREATED { get; set; }
    }

    public static class ReAttestorServices
    {
        #region service interface
        public static int Insert(AttestorViewModel viewModel)
        {
            try
            {
                return AttestorDAL.Instance.Insert(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Update(AttestorViewModel viewModel)
        {
            try
            {
                return AttestorDAL.Instance.Update(viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int ID)
        {
            try
            {
                return AttestorDAL.Instance.Delete(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static AttestorViewModel Get(int id)
        {
            try
            {
                return AttestorDAL.Instance.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AttestorViewModel> GetAll(int uid)
        {
            try
            {
                return AttestorDAL.Instance.GetAll(uid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion service interface

        #region data access layer
        public class AttestorDAL
        {
            #region static instance
            private static AttestorDAL _instance;

            static public AttestorDAL Instance
            {
                get
                {
                    if (_instance == null)
                        _instance = new AttestorDAL();
                    return _instance;
                }
            }
            #endregion static instance

            #region data access layer
            public AttestorViewModel Get(int Id)
            {
                try
                { 
                    using (var context = new RecruitmentContext())
                    {
                        return (from a in context.WZ_RE_ATTESTOR
                                where a.ID == Id
                                select new AttestorViewModel
                                {
                                    ID = (int)a.ID,
                                    USER_ID = (int)a.USER_ID,
                                    FULLNAME = a.FULLNAME,
                                    CAREER = a.CAREER,
                                    ADDRESS_PHONE_FAX = a.ADDRESS_PHONE_FAX,
                                    MAJOR = a.MAJOR,
                                    STATUS = (int)a.STATUS,
                                    CREATED = a.CREATED
                                }).FirstOrDefault();
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return null;
            }

            public bool Update(AttestorViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = context.WZ_RE_ATTESTOR.Where(a => a.ID == viewModel.ID).FirstOrDefault();
                        if (node == null) return false;

                        node.USER_ID = viewModel.USER_ID;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CAREER = viewModel.CAREER;
                        node.ADDRESS_PHONE_FAX = viewModel.ADDRESS_PHONE_FAX;
                        node.MAJOR = viewModel.MAJOR;
                        node.STATUS = (int)viewModel.STATUS;

                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public List<AttestorViewModel> GetAll(int uid)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var result = from a in context.WZ_RE_ATTESTOR
                                     where a.USER_ID == uid
                                     select new AttestorViewModel
                                     {
                                         ID = (int)a.ID,
                                         USER_ID = (int)a.USER_ID,
                                         FULLNAME = a.FULLNAME,
                                         CAREER = a.CAREER,
                                         ADDRESS_PHONE_FAX = a.ADDRESS_PHONE_FAX,
                                         MAJOR = a.MAJOR,
                                         STATUS = (int)a.STATUS,
                                         CREATED = a.CREATED
                                     };

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                return null;
            }

            public bool Delete(int ID)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        var node = (context.WZ_RE_ATTESTOR.Where(a => a.ID == ID)).FirstOrDefault();

                        if (node == null) return false;

                        context.WZ_RE_ATTESTOR.DeleteObject(node);
                        context.SaveChanges();

                        return true;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return false;
            }

            public int Insert(AttestorViewModel viewModel)
            {
                try
                {
                    using (var context = new RecruitmentContext())
                    {
                        WZ_RE_ATTESTOR node = new WZ_RE_ATTESTOR();
                        node.ID = this._getNextID();
                        node.USER_ID = viewModel.USER_ID;
                        node.FULLNAME = viewModel.FULLNAME;
                        node.CAREER = viewModel.CAREER;
                        node.ADDRESS_PHONE_FAX = viewModel.ADDRESS_PHONE_FAX;
                        node.MAJOR = viewModel.MAJOR;
                        node.STATUS = (int)viewModel.STATUS;
                        node.CREATED = DateTime.Now;

                        context.WZ_RE_ATTESTOR.AddObject(node);
                        context.SaveChanges();

                        return (int)node.ID;
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return Constants.ACTION_FAULT;
            }

            private decimal _getNextID()
            {
                try
                {
                    using (var context = new DBContext())
                    {
                        var sequence = (from s in context.ADMIN_WZ_SEQUENCE where s.TABLE_NAME == "RE_ATTESTOR" select s).FirstOrDefault();
                        decimal newID = 1;
                        if (sequence != null)
                        {
                            newID = sequence.SEQ;
                            sequence.SEQ += 1;
                            context.SaveChanges();
                        }
                        else
                        {
                            sequence = new ADMIN_WZ_SEQUENCE();
                            sequence.TABLE_NAME = "RE_ATTESTOR";
                            sequence.SEQ = 2;

                            context.ADMIN_WZ_SEQUENCE.AddObject(sequence);
                            context.SaveChanges();
                        }

                        //check exist ID
                        using (RecruitmentContext reContext = new RecruitmentContext())
                        {
                            var check = reContext.WZ_RE_ATTESTOR.Where(a => a.ID == newID).FirstOrDefault();
                            if (check != null)
                                return this._getNextID();
                            return newID;
                        }
                    }
                }
                catch (Exception ex) { Logs.WriteLogError(ex.StackTrace); }
                return 0;
            }
            
            #endregion
        }
        #endregion
    }
}