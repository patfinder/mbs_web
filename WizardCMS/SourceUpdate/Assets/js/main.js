var checkScroll = true;
var checkScroll_act = true;
var data;
var w_windown = $(window).width();
var h_windown = $(window).height();
var h_ct =  $('#menu-right').height();
var mySwiper_mn;
var slideNumber = 0;
var plus = true;
var mser = true;
function Page(){
	var self= this;
	this.init= function(){
		self.jquery();
		self.fixMenuMobile();
		self.nganhang();
		$(window).load(function(){
			self.scrollrightmenu();
			if ($('.banner-child').length > 0){
				var w_img = $('.banner-child img').width();
				$('.banner-child img').css({'margin-left':-(w_img/2)+'px'});
			}
			if($(".banking-business-all .banking-business-it").length > 0){
				var h = 0;
				$(".banking-business-all .banking-business-it").attr('style','');
				$(".banking-business-all .banking-business-it").each(function(){
					if($(this).height() > h){
						h = $(this).height();
					}
				});
				$(".banking-business-all .banking-business-it").css({
					height: h
				});
			}
			if(w_windown == 768){
				$(".info-hover ul.link li").css({
					"margin-top": "3px"
				});
			}else{
				$(".info-hover ul.link li").css({
					"margin-top": "7px"
				});
			}
			if($(".more-service").length > 0){
				$(".more-service").css({
					top: $(".search-scc").offset().top + 20
				});
			}
			if($(".box-blue").length > 0){
				var hh = 0;
				$(".box-blue").attr('style','');
				$(".box-blue").each(function(){
					if(hh < $(this).height()){
						hh = $(this).height();
					}
				});
				$(".box-blue").css({
					height: hh + 18
				});		
			}
			$(window).resize(function(){
				w_windown = $(window).width();
				if($(".banking-business-all .banking-business-it").length > 0){
					var h = 0;
					$(".banking-business-all .banking-business-it").attr('style','');
					$(".banking-business-all .banking-business-it").each(function(){
						if($(this).height() > h){
							h = $(this).height();
						}
					});
					$(".banking-business-all .banking-business-it").css({
						height: h
					});
				}
				if(w_windown == 768){
					$(".info-hover ul.link li").css({
						"margin-top": "3px"
					});
				}else{
					$(".info-hover ul.link li").css({
						"margin-top": "7px"
					});
				}
				if($(".more-service").length > 0){
					$(".more-service").css({
						top: $(".search-scc").offset().top + 20
					});
				}
				if($(".box-blue").length > 0){
					var hh = 0;
					$(".box-blue").attr('style','');
					$(".box-blue").each(function(){
						if(hh < $(this).height()){
							hh = $(this).height();
						}
					});
					$(".box-blue").css({
						height: hh + 18
					});		
				}
			});

			if( 
				navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
			){
				$(".mocsk .itel .year").css({
					"line-height": "36px"
				});
			}

			if( 
				navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
			){
				$(".item-home .tag-home a p:first-child").css({
					"margin-top": "-10px"
				});
				$(".banner-child .wrapper").css({
					top: "-6px"
				});
			}

			if(navigator.userAgent.match(/Windows Phone/i)){
				$(".mocsk .itel .year").css({
					"line-height": "26px"
				});
				$(".banner-child .wrapper").css({
					top: "0"
				});
			}
		});
		
		$(".list-flink").on("click", function() {
			alert(1);
		});
	};

	this.cotmoc = function(){
		if($('.mocsk').length>0){
			var h_l = 0;
			if(w_windown >= 768){
				var h_last = $('.mocsk li.itel:last-child').height();
				$('.line-col').css({'bottom': h_last-40});
				$('.mocsk li.itel').each(function(index){
					if(index>0){
						var $this = $(this);
						h_l = h_l+$this.height()-65;
						// if(w_windown>=768){
						// 	h_l = h_l+$this.height()-65;
						// }else{
						// 	h_l = h_l+$this.height()+20;
						// }
						$('.line-col').height(h_l);
					}
				});
			}else{
				$('.line-col').css({'bottom': 0});
				$('.mocsk li.itel').each(function(index){
					var $this = $(this);
					h_l = h_l+$this.height()+20;
					$('.line-col').height(h_l);
				});
			}
		}
	}

	this.jquery = function(){
		// alert(w_windown);

		self.topMenu();
		self.animateHome();
		self.animateCSKH();
		self.thanhtuu();
		self.cotmoc();
		self.attSelect();
		if($('.q-lines .q-check input').length>0){
			$('.q-lines .q-check input').uniform();
		}

		$(window).resize(function(){
			w_windown = $(window).width();
			self.cotmoc();
			self.topMenu();
			self.attSelect();
			// self.fixMenuMobile();
			var w = $(window).width();
			if(w < 1024){
				$(".huongdan-popup").css({
					width: w
				});
			}else{
				$(".huongdan-popup").css({
					width: 960
				});
			}
			if(w_windown > 1024){
				plus = true;
				mser = true;
			}else{
				plus = false;
				mser = false;
			}
			console.log(plus);

			if(w > 1024){
				if($(".wrap-content").hasClass("active")){
					$(".wrap-content").stop().animate({
						right: 0
					}, function(){
						$(".wrap-content").removeClass("active");
						$(".box-menu-mobile *").removeClass("active");
						$(".box-menu-mobile *").attr("style","");
						$(".box-menu-mobile").attr("style", "");
						$(".box-menu-mobile").removeClass("active");
					});
					$(".fix-menu").css({
						"padding-right": "0",
						"background": "#01294d"
					});
				}
			}
		});

		if(w_windown > 1024){
			plus = true;
			mser = true;
		}else{
			plus = false;
			mser = false;
		}

		// news-content-minheight
		$('.wrap-home').css({'min-height': h_ct+48+10});
		// end news-content-minheight

		/*nav mobile*/
		$(".nav-mobile .control-menu").click(function(e){
			e.preventDefault();
			// alert(1);
			if($(".wrap-content").hasClass("active")){
				$(".wrap-content").stop().animate({
					right: 0
				}, function(){
					$(".wrap-content").removeClass("active");
					$(".box-menu-mobile *").removeClass("active");
					$(".box-menu-mobile *").attr("style","");
					// $(".box-menu-mobile .level-1").attr("style", "");
					// $(".box-menu-mobile .level-1").removeClass("active");
					// $(".box-menu-mobile .ul-level-1").attr("style", "");
					// $(".box-menu-mobile .level-2").attr("style", "");
					// $(".box-menu-mobile .level-2").removeClass("active");
					// $(".box-menu-mobile .ul-level-2").attr("style", "");
					$(".box-menu-mobile").attr("style", "");
					$(".box-menu-mobile").removeClass("active");
				});
				$(".fix-menu").css({
					"padding-right": "0",
					"background": "#01294d"
				});
			}else{
				$(".wrap-content").addClass("active");
				$(".wrap-content").stop().animate({
					right: 200
				});
				$(".box-menu-mobile").addClass("active");
				$(".fix-menu").css({
					"padding-right": "200px",
					"background": "none"
				});
			}ss	
		});

		$(".menu-mobile .level-1").click(function(event){
			event.stopPropagation();
			if($(this).find(".ul-level-1").length > 0){
				$(this).addClass("active");
				$(".wrap-content").stop().animate({
					right: 240
				});
				$(".box-menu-mobile").stop().animate({
					width: 240,
					right: -240
				});

				$(this).parent(".menu-mobile").css({
					"visibility": "hidden"
				});
				$(this).find(".bg-holder-1").css({
					"visibility": "visible"
				});

				$(this).find(".ul-level-1").css({
					"visibility": "visible"
				});
				$(this).find(".ul-level-1").stop().animate({
					right: -40
				});
			}
		});

		$(".menu-mobile .level-2").click(function(event){
			event.stopPropagation();
			if($(this).hasClass("active")){

			}else{
				if($(this).find(".ul-level-2").length > 0){
					$(this).addClass("active");
					$(".wrap-content").stop().animate({
						right: 280
					});
					$(".box-menu-mobile").stop().animate({
						width: 280,
						right: -280
					});

					$(this).parent(".ul-level-1").css({
						"visibility": "hidden"
					});
					$(this).find(".bg-holder-2").css({
						"visibility": "visible"
					});

					$(this).find(".ul-level-2").css({
						"visibility": "visible"
					});
					$(this).find(".ul-level-2").stop().animate({
						right: -40
					});
				}
			}
		});

		$(".menu-mobile .bg-holder-2").click(function(event){
			event.stopPropagation();

			$(this).css({
				"visibility": "hidden"
			});
			$(this).parent(".level-2").removeClass("active");
			$(this).parents(".ul-level-1").css({
				"visibility": "visible"
			});	

			$(".wrap-content").stop().animate({
				right: 240
			});
			$(".box-menu-mobile").stop().animate({
				width: 240,
				right: -240
			});

			$(this).parent(".level-2").find(".ul-level-2").stop().animate({
				right: -280
			});
		});


		$(".menu-mobile .bg-holder-1").click(function(event){
			event.stopPropagation();

			$(this).css({
				"visibility": "hidden"
			});
			$(this).parent(".level-1").removeClass("active");
			$(this).parents(".menu-mobile").css({
				"visibility": "visible"
			});	

			$(".wrap-content").stop().animate({
				right: 200
			});
			$(".box-menu-mobile").stop().animate({
				width: 200,
				right: -200
			});

			$(this).parent(".level-1").find(".ul-level-1").stop().animate({
				right: -280
			});
		});

		/*layer one mobile*/
		$(".layer-one .box-mobile .row-net").each(function(){
			if($(this).hasClass("active")){
				$(this).find(".info-net").slideDown();
			}
		});

		$(".layer-one .box-mobile .row-net").click(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
				$(this).find(".info-net").slideUp();
			}else{
				$(".layer-one .box-mobile .row-net").removeClass("active");
				$(".layer-one .box-mobile .row-net .info-net").slideUp();
				$(this).addClass("active");
				$(this).find(".info-net").slideDown();
			}
		});

		/*Tabmenu bao cao tai chinh */
		$(".bctc-page .tab-years ul li").click(function(e){
			var ww = $(window).width();
			if(ww > 767){
				$(".bctc-page .tab-years ul li").removeClass("active");
				$(this).addClass("active");
			}else{
				$(".bctc-page .tab-years ul li").removeClass("active");
				$(this).addClass("active");
				$(".bctc-page .tab-years ul").slideUp(function(){
					
				});
				$(".bctc-page .tab-years ul li").each(function(){
			    	if($(this).hasClass("active")){
			    		$(".tab-ymobile").html($(this).text());
			    	}
			    });
			}
	    });

	    $(".tab-ymobile").click(function(){
	    	$(".bctc-page .tab-years ul").slideDown();
	    	
	    });

	    $(".bctc-page .tab-years ul li").each(function(){
	    	if($(this).hasClass("active")){
	    		$(".tab-ymobile").html($(this).text());
	    	}
	    });

	    /*Tabmenu doi ngu */
		$(".doingu-page .tab-doingu ul li").click(function(e){
			var ww = $(window).width();
			if(ww > 767){
				$(".doingu-page .tab-doingu ul li").removeClass("active");
				$(this).addClass("active");
				var link = $(this).index();
				$('.all-boss .all-boss-it').hide();
				$('.all-boss .all-boss-it').eq(link).show();
				// $(".doingu-page .tab-doingu-mobile ul li").removeClass('active');
				// $(".doingu-page .tab-doingu-mobile ul li").eq(link).addClass('active');
				self.scrollrightmenu();
			}else{
				$(".doingu-page .tab-doingu ul li").removeClass("active");
				$(this).addClass("active");
				var link = $(this).index();
				$('.all-boss .all-boss-it').hide();
				$('.all-boss .all-boss-it').eq(link).show();
				$(".doingu-page .tab-doingu ul").slideUp(function(){});
				$(".doingu-page .tab-doingu ul li").each(function(){
			    	if($(this).hasClass("active")){
			    		$(".tab-ydoingu").html($(this).text());
			    	}
			    });
			}
	    });

	    $(".tab-ydoingu").click(function(){
	    	$(".doingu-page .tab-doingu ul").slideDown();
	    	
	    });

	    $(".doingu-page .tab-doingu ul li").each(function(){
	    	if($(this).hasClass("active")){
	    		$(".tab-ydoingu").html($(this).text());
	    	}
	    });

	    /*Tabmenu doi ngu mobile*/
		$(".doingu-page .tab-doingu-mobile ul li").click(function(e){
			if($(this).hasClass("active")){
				// $(".doingu-page .tab-doingu-mobile ul li").css("display","block");
				// $(".doingu-page .tab-doingu-mobile ul li").removeClass("active");
				// $(this).addClass("active");
				// $(this).css({
				// 	"display":"block",
				// 	"background-image": "url('images/btn/btn-up.png')",
				// 	"background-position":"90% center",
				// 	"background-repeat": "no-repeat"
				// });
			}else{
				// $(".doingu-page .tab-doingu-mobile ul li").css("display","none");
				// $(this).addClass("active");
				// var link = $(this).index();
				// $('.all-boss .all-boss-it').hide();
				// $('.all-boss .all-boss-it').eq(link).show();
				// $(".doingu-page .tab-doingu ul li").removeClass('active');
				// $(".doingu-page .tab-doingu ul li").eq(link).addClass('active');
				// $(this).css({
				// 	"display":"block",
				// 	"background-image":"url('images/btn/btn-down.png')",
				// 	"background-position":"90% center",
				// 	"background-repeat": "no-repeat"
				// });
			}
	    });
		if ($('.fancybox').length > 0){
			$('.fancybox').fancybox({
				margin: 0,
				padding: 0
			});
		}
		if ($('.fancyimg').length > 0){
			$('.fancyimg').fancybox({
				margin: 0,
				padding: 0
			});
		}
		if ($('.fancyvideo').length > 0){
			$('.fancyvideo').fancybox({
				margin: 0,
				padding: [0,0,50,0],
				helpers: {
			    overlay: {
			      locked: false
			    }
			  }
			});
		}

	    /*title home*/
		// $(".infso-hover").each(function(){
		// 	var titHeight = $(this).find(".title-rbt").height();
		// 	if(titHeight > 0){
		// 		$(this).css({
		// 			top: 349 - titHeight - 18 - 25
		// 		});
		// 	}
		// });

		/*banner home*/
		if($(".banner-tablet").length > 0){
			$(".banner-tablet").owlCarousel({
				items: 1,
				autoPlay: 2000
			});
		}
		if($(".banner-mobile").length > 0){
			$(".banner-mobile").owlCarousel({
				items: 1,
				loop: true
			});
			var owl = $(".banner-mobile").data('owlCarousel');
			setInterval(function(){
				owl.next();
			}, 2000);
		}
		if($("#owl-example").length>0){
			$("#owl-example").owlCarousel({
				nav: true,
				pagination: false,
				navText: ["<span></span>","<span></span>"],
				dots: false,
				loop:true,
			    responsiveClass:true,
			    responsive:{
			        0:{
			            items:2,
			            nav:true,
			            loop: false
			        },
			        768:{
			            items:3,
			            nav:true,
			            loop:false
			        },
			        1025:{
			            items:4,
			            nav:true,
			            loop:false
			        }
			    }
			});

			$('.item-list').hover(function(){
				var $this = $(this);
				$this.find('.bd-text_hv').stop().animate({opacity: 1}, 500)
				$this.find('.bd-text_nm').stop().animate({opacity: 0}, 500)
				$this.find('.bg').stop().animate({opacity: 1}, 200)
				$this.find('.img').stop().animate({opacity: 1}, 200, function(){
					TweenMax.to($this.find('.img'), 1, {
						css:{
							scaleX: 1.1,
	                    	scaleY: 1.1
						}
					});
				})

			}, function(){
				var $this = $(this);
				$this.find('.bd-text_hv').stop().animate({opacity: 0}, 200)
				$this.find('.bd-text_nm').stop().animate({opacity: 1}, 200)
				$this.find('.bg').stop().animate({opacity: 0}, 50)
				$this.find('.img').stop().animate({opacity: 0}, 200)
				TweenMax.to($this.find('.img'), 1, {
					css:{
						scaleX: 1,
                    	scaleY: 1
					}
				});
			})
		}
		if($('.swiper-container').length>0){
			mySwiper = new Swiper('.swiper-container',{
				progress:true,
				pagination: '.pagination',
				autoplayDisableOnInteraction: false,
				loop:true,
				autoplay: 5000,
				grabCursor: true,
				paginationClickable: true,
				onSwiperCreated:function(){
					// $('.slide1 .dothi').stop().delay(300).animate({'width':387},1000, 'linear')
					// $('.slide1 .text1').stop().delay(800).animate({'opacity':1},300);
					// $('.slide1 .so1').stop().delay(700).animate({'opacity':1,'top':68},300);
					// $('.slide1 .so2').stop().delay(700).animate({'opacity':1,'top':170},300);
					// $('.slide1 .text2').stop().delay(1200).animate({'opacity':1},300);
					// $('.slide1 .btn-more2').stop().delay(1500).animate({'opacity':1},400);
					$('.slide4 .hand-sl4').stop().delay(300).animate({'opacity':1},800);
					$('.slide4 .hc-sl4').stop().delay(600).animate({'opacity':1},1000);
					// TweenMax.to($('.slide4 .hc-sl4'), 2, {delay: 0.6, rotationY:360, transformOrigin:"50% 50%", opacity: 1});
					$('.slide4 .bg-light').stop().delay(1300).animate({'opacity':1},800);
					$('.slide4 .text-sl4').stop().delay(900).animate({'opacity':1},200);
					$('.slide4 .btn-timhieu').stop().delay(1200).animate({'opacity':1},800);
				},
				onProgressChange: function(swiper){
					for (var i = 0; i < swiper.slides.length; i++){
						var slide = swiper.slides[i];
						var progress = slide.progress;
						var translate = progress*swiper.width;
						var opacity = 1 - Math.min(Math.abs(progress),1);
						slide.style.opacity = opacity;
						swiper.setTransform(slide,'translate3d('+translate+'px,0,0)');
					}
				},
				onSlideChangeStart: function(swiper,direction){
					var demslide = swiper.activeLoopIndex ;
					// if( demslide == 0){
					// 	$('.slide1 .text1').css({'opacity':'0'});
					// 	$('.slide1 .text2').css({'opacity':'0'});
					// 	$('.slide1 .btn-more2').css({'opacity':'0'});
					// 	$('.slide1 .so1').css({'opacity':'0','top':480});
					// 	$('.slide1 .so2').css({'opacity':'0','top':480});
					// 	$('.slide1 .dothi').css({'width':'0'});

					// }
					if( demslide == 1){
						$('.slide2 .text1').css({'opacity':'0'});
						$('.slide2 .item1').css({'opacity':'0'});
						$('.slide2 .item2').css({'opacity':'0'});
						$('.slide2 .item3').css({'opacity':'0'});
						$('.slide2 .btn-timhieu').css({'opacity':'0'});

					}
					if( demslide == 2){
						$('.slide3 .phi').css({'opacity':'0'});
						$('.slide3 .text2').css('opacity','0');
						$('.slide3 .appstore').css('opacity','0');
						$('.slide3 .gplay').css('opacity','0');
						$('.slide3 .btn-timhieu').css('opacity','0');
						$('.slide3 .btn-tai').css('opacity','0');
					}
					if( demslide == 0){
						$('.slide4 .hc-sl4').css('opacity','0');
						$('.slide4 .hand-sl4').css('opacity','0');
						$('.slide4 .text-sl4').css({'opacity':'0'});
						$('.slide4 .btn-timhieu').css('opacity','0');
						$('.slide4 .bg-light').css('opacity','0');
					}
				},
				onSlideChangeEnd: function(swiper,direction){
					var demslide = swiper.activeLoopIndex ;
					// if( demslide == 0){
					// 	$('.slide1 .dothi').stop().delay(300).animate({'width':387},1000, 'linear')
					// 	$('.slide1 .text1').stop().delay(800).animate({'opacity':1},300);
					// 	$('.slide1 .so1').stop().delay(700).animate({'opacity':1,'top':68},300);
					// 	$('.slide1 .so2').stop().delay(700).animate({'opacity':1,'top':170},300);
					// 	$('.slide1 .text2').stop().delay(1200).animate({'opacity':1},300);
					// 	$('.slide1 .btn-more2').stop().delay(1500).animate({'opacity':1},400)
					// }
					if( demslide == 1){
						$('.slide2 .text1').stop().animate({'opacity':1},400);
						$('.slide2 .item1').stop().delay(1100).animate({'opacity':1},200);
						$('.slide2 .item2').stop().delay(950).animate({'opacity':1},200);
						$('.slide2 .item3').stop().delay(800).animate({'opacity':1},200);
						$('.slide2 .btn-timhieu').stop().delay(1300).animate({'opacity':1},400);
					}
					if( demslide == 2){
						$('.slide3 .phi').stop().delay(500).animate({'opacity':1},200);
						$('.slide3 .text2').stop().delay(1000).animate({'opacity':1},800);
						$('.slide3 .appstore').stop().delay(1000).animate({'opacity':1},2000);
						$('.slide3 .gplay').stop().delay(1000).animate({'opacity':1},2000);
						$('.slide3 .btn-timhieu').stop().delay(1200).animate({'opacity':1},2000);
						$('.slide3 .btn-tai').stop().delay(1200).animate({'opacity':1},2000);
					}
					if( demslide == 0){
						$('.slide4 .hand-sl4').stop().delay(300).animate({'opacity':1},800);
						$('.slide4 .hc-sl4').stop().delay(600).animate({'opacity':1},1000);
						// TweenMax.to($('.slide4 .hc-sl4'), 2, {delay: 0.6, rotationY:360, transformOrigin:"50% 50%", opacity: 1});
						$('.slide4 .bg-light').stop().delay(1300).animate({'opacity':1},800);
						$('.slide4 .text-sl4').stop().delay(900).animate({'opacity':1},200);
						$('.slide4 .btn-timhieu').stop().delay(1200).animate({'opacity':1},800);
					}
				},
				onTouchStart:function(swiper){
					for (var i = 0; i < swiper.slides.length; i++){
						swiper.setTransition(swiper.slides[i], 0);
					}
				},
				onSetWrapperTransition: function(swiper, speed) {
					for (var i = 0; i < swiper.slides.length; i++){
						swiper.setTransition(swiper.slides[i], speed);
					}
				}
			});
		}
		self.scrollMenu();
		if($(".wrap-owl-menu").length > 0){
			mySwiper_mn = new Swiper('.wrap-owl-menu',{
				paginationClickable: true,
				slidesPerView: 'auto',
				nextButton: '.swiper-button-next',
        		prevButton: '.swiper-button-prev'
			});
			
			$('.wrap-owl-menu-m .owl-prev').click(function(){
				mySwiper_mn.swipePrev();
				return false;
			});
			
			$('.wrap-owl-menu-m .owl-next').click(function(){
				mySwiper_mn.swipeNext();
				return false;
			});

			$(".item-plus").click(function(){
				if($(this).hasClass("notadd")){
					return false;
				}else{
					$(this).addClass("notadd");
					var url = $(this).data("url");
					var txt = $(this).data("text");
					var link = $(this).data("link");
					var id = $(this).data("id");
					if(url != null && txt != null){
						self.loadNewSlides(url, txt, link);

						$.post(root_lang+'set-fastlink', {id:id}, function(e){
							return true;
						});
					}
				}
			});
		}
		if($(".btn-control").hasClass("hide-btn")){
			$(".fix-menu").css({
				bottom: -62
			});
		}
		$(".btn-control").click(function(){
			if($(this).hasClass("hide-btn")){
				$(this).removeClass("hide-btn");
				$(this).parent().parent(".fix-menu").animate({
					bottom: 0
				});
				$(this).find("span").html("");
				$(this).find("span").html("Ẩn");
				checkScroll_act = true;
			}else{
				$(this).addClass("hide-btn");
				$(this).parent().parent(".fix-menu").animate({
					bottom: -62
				});
				$(this).find("span").html("");
				$(this).find("span").html("Liên kết nhanh");
				checkScroll_act = false;				
			}
			$(".btn-control.hide-btn .kln span").attr("style","");
			TweenMax.to($(".btn-control.hide-btn .kln span"), 1, {
				css:{
					left: 0
				},
				repeat: -1, 
				repeatDelay: 3
			});
			checkScroll = false;
			self.scrollMenu();
		});
		if($(".scroll").length > 0){
			$(".scroll").mCustomScrollbar({
				scrollInertia: 200,
				scrollEasing: "linear"
			});
		}
		if(plus){
			$(".icon-plus").hover(
				function(){
					$(this).find(".fix-sub-plus").show(10, function(){
						if($(".item-plus").length > 0){
							var h = 0;
							$(".item-plus").attr('style','');
							$(".item-plus").each(function(){
								if($(this).height() > h){
									h = $(this).height();
								}
							});
							$(".item-plus").css({
								height: h + 10
							});
						}
					});
					var plus = $(this).find(".icon-plus-rt");
					TweenMax.to(plus, 0.3,
				        {css:{rotation: 45}
				  	});
				}, function(){
					$(this).find(".fix-sub-plus").hide();
					var plus = $(this).find(".icon-plus-rt");
					TweenMax.to(plus, 0.3,
				        {css:{rotation: 0}
				  	});
				}
			);
		}else{
			$(".icon-plus").click(function(){
				if($(this).hasClass("clicked")){
					$(this).removeClass("clicked")
					$(this).find(".fix-sub-plus").hide();
					var plus = $(this).find(".icon-plus-rt");
					TweenMax.to(plus, 0.3,
				        {css:{rotation: 0}
				  	});
				}else{
					$(this).addClass("clicked")
					$(this).find(".fix-sub-plus").show(10, function(){
						if($(".item-plus").length > 0){
							var h = 0;
							$(".item-plus").attr('style','');
							$(".item-plus").each(function(){
								if($(this).height() > h){
									h = $(this).height();
								}
							});
							$(".item-plus").css({
								height: h + 10
							});
						}
					});
					var plus = $(this).find(".icon-plus-rt");
					TweenMax.to(plus, 0.3,
				        {css:{rotation: 45}
				  	});
				}
			});
		}
		if(!mser){
			$( document ).on( "click", ".more-service", function() {
				if($(this).hasClass("active")){
					$(this).removeClass("active");
					$(this).find(".tb-service").css({
						"display": "none"
					});
				}else{
					$(this).addClass("active");
					$(this).find(".tb-service").css({
						"display": "block"
					});
				}
			});
		}
		$(".item-box.item-hover").hover(
			function(){
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var title = $(this).find(".info-hover .title");
				var img = $(this).find(".img");
				if($(this).hasClass("tall")){
					title.stop().animate({
						"padding-top": "16px"
					}, 600);
					title.addClass("bg-top");
				}else{
					$(this).find(".info-hover").stop().animate({
						top: 0
					}, 600, function(){
						TweenMax.staggerTo(link, 0.15 , {left: 0, opacity: 1, ease:Linear.easeNone}, 0.15, allDone);
					});
				}
				bgFull.stop().animate({
					opacity: 1
				}, 300, function(){
				});
				TweenMax.to(img, 1, {
					css:{
						scaleX: 1.1,
                    	scaleY: 1.1
					}
				});
			}, function(){
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var btnMore = $(".btn-more.in");
				var title = $(this).find(".info-hover .title");
				var img = $(this).find(".img");
				btnMore.stop().animate({
					opacity: 0
				});
				if($(this).hasClass("tall")){
					title.stop().animate({
						"padding-top": "223px"
					}, 600);
					title.removeClass("bg-top");
				}else{
					$(this).find(".info-hover").stop().animate({
						top: 242
					}, 600, function(){
						link.css({
							left: -50,
							opacity: 0
						});
					});
				}
				bgFull.stop().animate({
					opacity: 0
				}, 300);
				TweenMax.to(img, 0.2, {
					css:{
						scaleX: 1,
                    	scaleY: 1
					}
				});
			}
		);
		$(".item-nav").hover(
			function(){
				$(this).find(".sub-nav").stop().slideDown(350);
			}, function(){
				$(this).find(".sub-nav").stop().slideUp(200);
			}
		);
		$("#menu-right-mobile .menu-right > ul > li").each(function(){
			if($(this).hasClass("active")){
				var ttone = $(this).children(".ttone").text();
				$(".text-active a.one").text(ttone);
				$(this).find(".sub-right li").each(function(){
					var sub = $(this);
					if(sub.hasClass("active")){
						// var tttwo = "—" + sub.children("a").text();
						var tttwo = sub.children("a").text();
						$(".text-active a.two").text(tttwo);
					}
				});
			}
		});
		$(".text-active").click(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
				$("#menu-right-mobile .menu-right").hide();
			}else{
				$(this).addClass("active");
				$("#menu-right-mobile .menu-right").show();
			}
		});
		$("#menu-right-mobile .menu-right li").click(function(){
			$("#menu-right-mobile .menu-right").hide();
		});
		$("#menu-right-mobile .menu-right a").click(function(){
			$("#menu-right-mobile .menu-right").hide();
		});
		var _w = $(window).width();
		if(_w < 768){
			$(".it-gui .icon").click(function(){
				var _this = $(this);
				if($(this).hasClass("active")){
					$(this).removeClass("active");
					if($(this).hasClass("prev")){
						$(this).parent(".it-gui").find("a").hide();
						$(this).parent(".it-gui").find("a").animate({
							left: "-100%"
						}, 500, function(){

						});
						$(".it-gui.it-gui-next").show();
						$(".type-show").show();
					}else{
						$(this).parent(".it-gui").find("a").hide();
						$(this).parent(".it-gui").find("a").animate({
							right: "-200%"
						}, 500);	
						$(".it-gui.it-gui-prev").show();	
						$(".type-show").show();			
					}
					$(".type-show").show();
				}else{
					$(".it-gui").removeClass("active");
					$(this).parent(".it-gui").addClass("active");
					$(".it-gui .icon").removeClass("active");
					$(this).addClass("active");
					if($(this).hasClass("prev")){
						$(this).parent(".it-gui").find("a").show();
						$(this).parent(".it-gui").find("a").animate({
							left: 0
						}, 500);
						$(".it-gui.it-gui-next").hide();
						$(".type-show").hide();
					}else{
						$(this).parent(".it-gui").find("a").show();
						$(this).parent(".it-gui").find("a").animate({
							right: 0
						}, 500);	
						$(".it-gui.it-gui-prev").hide();	
						$(".type-show").hide();			
					}
				}
			});
			// $(".type-show").click(function(){
			// 	$(this).show();
			// });
		}
		
		$(document).on("click", "#header .menu-ws-item-register", function(){
            console.log('click register workshop');
            $('#header .menu-ws-item').removeClass('active');
            $(this).addClass('active');

            $('.tab-ctr').eq(1).trigger('click');
        });
		
		$(".box-menu-mobile .menu-ws-item-register").click(function(e){
            e.preventDefault();
            $('.box-menu-mobile .menu-ws-item').removeClass('active');
            $(this).addClass('active');

            $('.tab-ctr').eq(1).trigger('click');

            $('.nav-mobile .control-menu').trigger('click');
        });
		
		$(document).on("click",".tab-ctr",function(){
			// alert(1)
			console.log('click tab item');
			if (!$(this).hasClass('active')){
				$(".tab-ctr").removeClass('active');
				$(this).addClass('active');
				var link = $(this).index();
				$('.wrap-tab-wk .tab-wk').hide();
				$('.wrap-tab-wk .tab-wk').eq(link).show();

				console.log(link);
				//$('.register-ws-menu').eq(link).show();
				$('.wrap-home .register-ws-menu').parent().find('li').removeClass('active');
            	$('.wrap-home .register-ws-menu').eq(link).addClass('active');
				$('#menu-right-mobile .register-ws-menu').parent().find('li').removeClass('active');
            	$('#menu-right-mobile .register-ws-menu').eq(link).addClass('active');
				
				$('#header .menu-ws-item').removeClass('active');
				$('#header .menu-ws-item').eq(link).addClass('active');
            	
				
				$('.wrap-home .hash-sub').parent().find('li').removeClass('active');
            	$('.wrap-home .hash-sub').eq(link).addClass('active');
				$('#menu-right-mobile .hash-sub').parent().find('li').removeClass('active');
            	$('#menu-right-mobile .hash-sub').eq(link).addClass('active');
				
				var parent = $(this).data("pid");
				var slug = $(this).data("slug");
				//console.log(parent);
				/*
				if (typeof parent != 'undefined') {
					SearchContent(parent, 1);
				}
				*/

				if (typeof slug != 'undefined') {
					window.location.hash = slug;
				}

				self.scrollrightmenu();

				$("#menu-right-mobile .menu-right > ul > li").each(function(){
					if($(this).hasClass("active")){
						var ttone = $(this).children(".ttone").text();
						$(".text-active a.one").text(ttone);
						$(this).find(".sub-right li").each(function(){
							var sub = $(this);
							if(sub.hasClass("active")){
								// var tttwo = "—" + sub.children("a").text();
								var tttwo = sub.children("a").text();
								$(".text-active a.two").text(tttwo);
							}
						});
					}
				});
			}
		}) 
		$(".tab-ctr").click(function(){
			
			// var wk = $(this).data("wk");
			// if($(this).hasClass("active")){
			// 	return false;
			// }else{
			// 	$(".tab-ctr").removeClass("active");
			// 	$(this).addClass("active");
			// 	$(".tab-wk").each(function(){
			// 		if($(this).hasClass(wk)){
			// 			$(".tab-wk").removeClass("active");
			// 			$(this).addClass("active");
			// 		}
			// 	});
			// }
		});

		$(".list-faq .it-supp").each(function(){
			if($(this).hasClass("active")){
				$(this).find(".ans").slideDown();
			}
		});
		
		$(document).on("click", ".it-supp", function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
				$(this).find(".ans").slideUp();
			}else{
				$(".list-faq .it-supp").removeClass("active");
				$(".it-supp .ans").slideUp();
				$(this).addClass("active");
				$(this).find(".ans").slideDown();				
			}
		})

		var w = $(window).width();
		if(w < 1024){
			$(".huongdan-popup").css({
				width: w
			});
		}else{
			$(".huongdan-popup").css({
				width: 960
			});
		}
		$('html,body').click(function(e){
			if ($(e.target).is('.ui-datepicker, .ui-datepicker *, .hasDatepicker')) { 
				return;
			}else{
				$(".hasDatepicker").datepicker("hide");
			}
		}); 
		TweenMax.to($(".title-flink div > span"), 1, {
			css:{
				left: 30
			},
			repeat: -1, 
			repeatDelay: 3
		});
		TweenMax.to($(".tit-fm > div"), 1, {
			css:{
				left: 0
			},
			repeat: -1, 
			repeatDelay: 3
		});
		$(".btn-control.hide-btn .kln span").attr("style","");
		TweenMax.to($(".btn-control.hide-btn .kln span"), 1, {
			css:{
				left: 0
			},
			repeat: -1, 
			repeatDelay: 3
		});
		if($(".cb-ck").length > 0){
			$(".cb-ck input").uniform();
		}
	};

	this.thanhtuu = function(){
		
	}

	this.nganhang = function(){
		$(window).load(function(){
			if ($('.banking-advisory').length > 0){
				$(document).on('click', '.item-advisory a', function(){
					var sctop = $(window).scrollTop();
					var bank_index = $(this).parent().index();
					var bank_top = $('.banking-counsel-it').eq(bank_index).addClass('111').offset().top;
					$("html, body").animate({ scrollTop: bank_top }, 500);
				});
			}
		});
	}

	this.animateHome = function(){
		$(".home-page .item-hover").hover(
			function(){
				var _this = $(this);
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var title = $(this).find(".info-hover .title-rbt");
				var img = $(this).find(".img");
				var btn = $(this).find(".btn-find-more");

				var titleNews = $(this).find(".title-news");
				var descNews = $(this).find(".desc-news");
				var infoNews = $(this).find(".desc-news .info");
				if($(this).hasClass("tall")){
					descNews.stop().animate({
						"bottom": 368 - descNews.height() - 100
					}, 600);
					infoNews.css({
						opacity: 1
					});
					titleNews.addClass("bg-bottom");
				}else{
					title.addClass("bg-bottom");
					$(this).find(".info-hover").stop().animate({
						top: 0
					}, 600, function(){
						// TweenMax.staggerTo(link, 0.15 , {left: 0, opacity: 1, ease:Linear.easeNone}, 0.15);
						var tl = new TimelineLite();
						tl.to(_this.find(".info-hover ul.link a.one"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.two"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.three"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.four"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						if(link.length == 5){
							tl.to(_this.find(".info-hover ul.link a.five"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						}
						tl.to(btn, 0.15, {opacity:1});
						tl.play();
					});
				}
				bgFull.stop().animate({
					opacity: 1
				}, 300, function(){
				});
				TweenMax.to(img, 1, {
					css:{
						scaleX: 1.1,
                    	scaleY: 1.1
					}
				});
			}, function(){
				var _this = $(this);
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var btn = $(this).find(".btn-find-more");
				var title = $(this).find(".info-hover .title-rbt");
				var img = $(this).find(".img");

				var titleNews = $(this).find(".title-news");
				var descNews = $(this).find(".desc-news");
				var infoNews = $(this).find(".desc-news .info");
				btn.css({
					opacity: 0
				});
				if($(this).hasClass("tall")){
					descNews.stop().animate({
						"bottom": "0px"
					}, 600);
					infoNews.css({
						opacity: 0
					});
					titleNews.removeClass("bg-bottom");
				}else{
					$(this).find(".info-hover").stop().animate({
						top: 242
					}, 600, function(){
						link.css({
							left: -50,
							opacity: 0
						}, 10);
						title.removeClass("bg-bottom");
					});
				}
				bgFull.stop().animate({
					opacity: 0
				}, 300);
				TweenMax.to(img, 0.2, {
					css:{
						scaleX: 1,
                    	scaleY: 1
					}
				});
			}
		);
	};

	this.animateCSKH = function(){
		$(".exchange-guide .item-hover").hover(
			function(){
				var _this = $(this);
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var title = $(this).find(".info-hover .title-rbt");
				var img = $(this).find(".img");
				var btn = $(this).find(".btn-find-more");

				var titleNews = $(this).find(".title-news");
				var descNews = $(this).find(".desc-news");
				var infoNews = $(this).find(".desc-news .info");
				if($(this).hasClass("tall")){
					descNews.stop().animate({
						"padding-top": "16px"
					}, 600);
					infoNews.css({
						opacity: 1
					});
					titleNews.addClass("bg-bottom");
				}else{
					title.addClass("bg-bottom");
					$(this).find(".info-hover").stop().animate({
						top: 0
					}, 600, function(){
						// TweenMax.staggerTo(link, 0.15 , {left: 0, opacity: 1, ease:Linear.easeNone}, 0.15);
						var tl = new TimelineLite();
						tl.to(_this.find(".info-hover ul.link a.one"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.two"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.three"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						tl.to(_this.find(".info-hover ul.link a.four"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						if(link.length == 5){
							tl.to(_this.find(".info-hover ul.link a.five"), 0.15, {left:0, opacity:1, ease:Linear.easeNone});
						}
						tl.to(btn, 0.15, {opacity:1});
						tl.play();
					});
				}
				bgFull.stop().animate({
					opacity: 1
				}, 300, function(){
				});
				TweenMax.to(img, 1, {
					css:{
						scaleX: 1.1,
                    	scaleY: 1.1
					}
				});
			}, function(){
				var _this = $(this);
				var bgFull = $(this).find(".bg-hv-full");
				var link = $(this).find(".info-hover ul.link li a");
				var btn = $(this).find(".btn-find-more");
				var title = $(this).find(".info-hover .title-rbt");
				var img = $(this).find(".img");

				var titleNews = $(this).find(".title-news");
				var descNews = $(this).find(".desc-news");
				var infoNews = $(this).find(".desc-news .info");
				btn.css({
					opacity: 0
				});
				if($(this).hasClass("tall")){
					descNews.stop().animate({
						"padding-top": "208px"
					}, 600);
					infoNews.css({
						opacity: 0
					});
					titleNews.removeClass("bg-bottom");
				}else{
					$(this).find(".info-hover").stop().animate({
						top: 177
					}, 600, function(){
						link.css({
							left: -50,
							opacity: 0
						}, 10);
						title.removeClass("bg-bottom");
					});
				}
				bgFull.stop().animate({
					opacity: 0
				}, 300);
				TweenMax.to(img, 0.2, {
					css:{
						scaleX: 1,
                    	scaleY: 1
					}
				});
			}
		);

		$(window).load(function(){
			if ($('.faq-main-all').length > 0){
				var h_faq = 0, h_faq_fn;
				$('.faq-main-all .faq-main-item').attr('style','');
				$('.faq-main-all .faq-main-item').each(function(){
					h_faq_fn = $(this).height();
					if (h_faq < h_faq_fn){
						h_faq = h_faq_fn;
					}
				});
				$('.faq-main-all .faq-main-item').css({'height':h_faq});
			}

			if ($('.incentive-program').length > 0){
				var h_pro = 0, h_pro_fn;
				$('.incentive-program .incentive-item').attr('style','');
				$('.incentive-program .incentive-item').each(function(){
					h_pro_fn = $(this).height();
					if (h_pro < h_pro_fn){
						h_pro = h_pro_fn;
					}
				});
				$('.incentive-program .incentive-item').css({'height':h_pro});
			}

			if ($('.time-picker').length > 0){
				$( "#from-1" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#to-1" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				$( "#to-1" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#from-1" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
				$( "#from-2" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#to-2" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				$( "#to-2" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#from-2" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
				$( "#from-3" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#to-3" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				$( "#to-3" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#from-3" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
				$( "#from-4" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#to-4" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				$( "#to-4" ).datepicker({
					showOn: "button",
					buttonImage: "images/icon/calendar.png",
					buttonImageOnly: true,
					buttonText: "Select date",
					defaultDate: "+1w",
					changeMonth: false,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						$( "#from-4" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			}
		});

		$(window).resize(function(){
			if ($('.faq-main-all').length > 0){
				var h_faq = 0, h_faq_fn;
				$('.faq-main-all .faq-main-item').attr('style','');
				$('.faq-main-all .faq-main-item').each(function(){
					h_faq_fn = $(this).height();
					if (h_faq < h_faq_fn){
						h_faq = h_faq_fn;
					}
				});
				$('.faq-main-all .faq-main-item').css({'height':h_faq});
			}

			if ($('.incentive-program').length > 0){
				var h_pro = 0, h_pro_fn;
				$('.incentive-program .incentive-item').attr('style','');
				$('.incentive-program .incentive-item').each(function(){
					h_pro_fn = $(this).height();
					if (h_pro < h_pro_fn){
						h_pro = h_pro_fn;
					}
				});
				$('.incentive-program .incentive-item').css({'height':h_pro});
			}
		});
	}

	function allDone(){
    	$(".btn-find-more").stop().animate({
    		opacity: 1
    	});
	}

	this.topMenu = function(){
		var currWidth = 0;
		$(".top-menu a").each(function(){
			currWidth = currWidth + $(this).width();
		});
		var topWidth = $(".top-menu").width();
		if(currWidth + 60 > topWidth){
			$(".top-menu a.link").hide();
		}else{
			$(".top-menu a.link").show();
		}
	};

	this.scrollrightmenu = function(){
		if($("#menu-right").length > 0){
			var rightmenu_top = $('#menu-right').offset().top;
			var h_rmenu = $('#menu-right').height();
			var footer_top = $('#footer').offset().top;
			// console.log(rightmenu_top, h_rmenu, footer_top);

			var st = $(window).scrollTop();
			// console.log(st, footer_top-h_rmenu);

			if(st>=rightmenu_top && st<=footer_top-h_rmenu){
				$('#menu-right').addClass('fixed');
				$('#menu-right').css({top: 0, bottom: 'auto' })
			}else if(st<=rightmenu_top){
				$('#menu-right').removeClass('fixed');
				$('#menu-right').css({top: 55, bottom: 'auto' })
			}else if(st>=footer_top-h_rmenu){
				$('#menu-right').removeClass('fixed');
				$('#menu-right').css({top: 'auto', bottom: 0 })
			}

			$(window).scroll(function(){
				var st = $(window).scrollTop();
				// console.log(st, footer_top-h_rmenu);

				if(st>=rightmenu_top && st<=footer_top-h_rmenu){
					$('#menu-right').addClass('fixed');
					$('#menu-right').css({top: 0, bottom: 'auto' })
				}else if(st<=rightmenu_top){
					$('#menu-right').removeClass('fixed');
					$('#menu-right').css({top: 55, bottom: 'auto' })
				}else if(st>=footer_top-h_rmenu){
					$('#menu-right').removeClass('fixed');
					$('#menu-right').css({top: 'auto', bottom: 0 })
				}
			});
		}
	}

	this.scrollMenu = function(){
		var wh = $(window).height();
		var st = $(this).scrollTop();
		var sctop = $(window).scrollTop();
		var off_set_ft = $('.footer-menu').offset().top;
		if(sctop > 100){
			$(".fix-menu").css({'bottom': 0});
		}else{
			$(".fix-menu").css({'bottom': '-62px'});
		}
		if(sctop+wh > off_set_ft){
			$(".btn-control").hide();
			if(checkScroll_act){
				$(".fix-menu").removeClass('fix');
			}else{
				$(".fix-menu").addClass('fix');
			}
		}else{
			$(".btn-control").show();
			$(".fix-menu").removeClass('fix');
		}
		
		$(window).scroll(function(event){
			wh = $(window).height();
			st = $(this).scrollTop();
			sctop = $(window).scrollTop();
			off_set_ft = $('.footer-menu').offset().top;
			if(sctop > 100){
				if(checkScroll){
					$(".fix-menu").stop().animate({
						bottom: 0
					});
					$(".fix-menu").find(".btn-control").removeClass("hide-btn");
					$(".fix-menu").find(".btn-control span").html("");
					$(".fix-menu").find(".btn-control span").html("Ẩn");
				}
			}else{
				if(checkScroll){
					$(".fix-menu").stop().animate({
						bottom: -62
					});
					$(".fix-menu").find(".btn-control").addClass("hide-btn");
					$(".fix-menu").find(".btn-control span").html("");
					$(".fix-menu").find(".btn-control span").html("Liên kết nhanh");
				}
			}
			
			if(sctop+wh > off_set_ft){
				$(".btn-control").hide();
				if(checkScroll_act){
					$(".fix-menu").removeClass('fix');
				}else{
					$(".fix-menu").addClass('fix');
				}
			}else{
				$(".btn-control").show();
				$(".fix-menu").removeClass('fix');
			}
		});
	};

	// this.fixMenuMobile = function(){
	// 	var h_windown = $(window).height();
	// 	var fixMenu = $(".footer-menu-mobile");
	// 	fixMenu.data("top", h_windown - 39);

	// 	$(".footer-menu-mobile .title-flink").click(function(){
	// 		var ftMenu = $(this).parent(".footer-menu-mobile");
	// 		var top = fixMenu.data("top");
	// 		if(ftMenu.hasClass("active")){
	// 			ftMenu.removeClass("active");
	// 			ftMenu.stop().animate({
	// 				top: top
	// 			});
	// 		}else{
	// 			ftMenu.addClass("active");
	// 			ftMenu.stop().animate({
	// 				top: 0
	// 			});
	// 		}
	// 	});

	// 	fixMenu.css({
	// 		top: h_windown - 39
	// 	});

	// 	$(".list-flink").css({
	// 		height: fixMenu.height() - 40 -10
	// 	});

	// 	if($(".list-flink").length > 0){
	// 		$(".list-flink").mCustomScrollbar({
	// 			scrollInertia: 100,
	// 			scrollEasing: "linear",
	// 			advanced:{
	// 			    updateOnContentResize: true
	// 		  	}
	// 		});
	// 		$(".list-flink").mCustomScrollbar("update");
	// 	}

	// 	$(".btn-control-fmm").click(function(){
	// 		var $this = $(this);
	// 		if($this.hasClass("active")){
	// 			$this.removeClass("active");
	// 			$this.find(".txt").html("");
	// 			$this.find(".txt").html("Ẩn");
	// 			$(".footer-menu-mobile").show();		
	// 		}else{
	// 			$this.addClass("active");
	// 			$this.find(".txt").html("");
	// 			$this.find(".txt").html("Hiện");
	// 			$(".footer-menu-mobile").hide();
	// 		}
	// 	});

	// 	$(window).resize(function(){
	// 		var h_windown = $(window).height();
	// 		var fixMenu = $(".footer-menu-mobile");
	// 		fixMenu.data("top", h_windown - 39);
	// 		fixMenu.css({
	// 			top: h_windown - 39
	// 		});
	// 		$(".list-flink").css({
	// 			height: fixMenu.height() - 40 -10
	// 		});
	// 		$(".list-flink").mCustomScrollbar("update");
	// 	});

	// };

	this.fixMenuMobile = function(){
		var h_windown = $(window).height();
		var w_windown = $(window).width();
		var fixMenu = $(".footer-menu-mobile");
		fixMenu.css({
			top: h_windown - 39
		});
		$(".footer-menu-mobile .title-flink").click(function(){
			var ftMenu = $(this).parent(".footer-menu-mobile");

			if(ftMenu.hasClass("active")){
				ftMenu.removeClass("active");
				ftMenu.stop().animate({
					height: 39,
					top: h_windown - 39
				});
				$("html, body").removeClass("ovhd");
			}else{
				ftMenu.addClass("active");
				ftMenu.stop().animate({
					height: h_windown,
					top: 0,
					bottom: "auto"
				}, function(){
					$(".list-flink").css({
						height: fixMenu.height() - 40 - 20
					});
					if($(".list-flink").length > 0){
						var nice = $(".list-flink").getNiceScroll()[0];
						if(nice){
							nice.resize();
						}else{
							$(".list-flink").niceScroll({cursorborder:"",background:"#292b2c", cursorwidth:"6px", cursorborderradius:"4px",cursorcolor:"#3c3c3d",boxzoom:false, autohidemode: false, dblclickzoom: false, horizrailenabled:false});
						}
					}
					$("html, body").addClass("ovhd");
					if($(".it-flink").length > 0){
						var h = 0;
						$(".it-flink").attr('style','');
						$(".it-flink").each(function(){
							if($(this).height() > h){
								h = $(this).height();
							}
						});
						$(".it-flink").css({
							height: h
						});
					}
				});
			}
		});

		$(window).resize(function(){
			var ftMenu = $(".footer-menu-mobile");
			h_windown = $(window).height();
			w_windown = $(window).width();
			if(w_windown > 767){

			}else{
				if(ftMenu.hasClass("active")){
					ftMenu.css({
						top: 0
					});
					ftMenu.stop().animate({
						height: h_windown
					}, function(){
						$(".list-flink").css({
							height: fixMenu.height() - 40 - 20
						});
						if($(".list-flink").length > 0){
							if($(".list-flink").length > 0){
								var nice = $(".list-flink").getNiceScroll()[0];
								if(nice){
									nice.resize();
								}else{
									$(".list-flink").niceScroll({cursorborder:"",background:"#292b2c", cursorwidth:"6px", cursorborderradius:"4px",cursorcolor:"#3c3c3d",boxzoom:false, autohidemode: false, dblclickzoom: false, horizrailenabled:false});
								}
							}
						}
					});
				}else{
					ftMenu.css({
						top: h_windown - 39,
						height: 39
					});
				}
			}	
		});

		$(window).scroll(function(){
			if(w_windown < 768){
				var ftMenu = $(".footer-menu-mobile");
				var sctop = $(window).scrollTop();
				var off_set_ft = $('.footer-copy').offset().top;
				var ww = $(window).width();
				h_windown = $(window).height();
				if(ftMenu.hasClass("active")){
					ftMenu.css({
						top: 0
					});
				}else{
					if( 
						navigator.userAgent.match(/iPhone/i)
						|| navigator.userAgent.match(/iPod/i)
					){
						ftMenu.css({
							top: "auto",
							bottom: 0
						});
					}else{
						ftMenu.css({
							top: h_windown - 39
						});
					}
					if(sctop+h_windown > off_set_ft){
						// $(".btn-control-fmm").hide();
						if(ww < 768){
							$("#footer").css({
								"padding-bottom": 39
							});
						}
					}else{
						$(".btn-control-fmm").show();
						if(ww < 768){
							$("#footer").css({
								"padding-bottom": 0
							});
						}
					}
				}
			}
		});

		$(".btn-control-fmm").click(function(){
			var $this = $(this);
			if($this.hasClass("active")){
				$this.removeClass("active");
				$this.find(".txt").html("");
				$this.find(".txt").html("Ẩn");
				$(".footer-menu-mobile").show();	
			}else{
				$this.addClass("active");
				$this.find(".txt").html("");
				$this.find(".txt").html("Hiện");
				$(".footer-menu-mobile").hide();
				$("html, body").removeClass("ovhd");	
			}
		});
	};

	this.attSelect = function(){
		var w = $(window).width();
		if($(".bselect").length > 0){
			$(".bselect").selectbox({
				onChange: function(val, inst){
					if ($('#faq_term').length > 0) {
						SearchContent();
					}
					if ($('#guide_term').length > 0) {
						SearchContent();
					}
					var sid = $(this).attr('id');
					if ($('#'+sid+'_hidden').length > 0) 
					{
						$('#'+sid+'_hidden').val(val);
					}
				}
			});
		}
		if(w > 767){
			if($(".bselect").length > 0){
				console.log('attach');
				$(".bselect").selectbox("attach");
			}
		}else{
			if($(".bselect").length > 0){
			console.log('detach');
				$(".bselect").selectbox("detach");
			}
		}
	}

	this.loadNewSlides = function(urlImg, textSlide, linkTo){
		mySwiper_mn.prependSlide('<a target="_blank" href='+linkTo+' title=""><span class="img"><span><img src='+urlImg+' title=""></span></span><span class="text"><span>'+textSlide+'</span></span><span class="line"></span></a>')

		//Release interactions and set wrapper
		mySwiper_mn.setWrapperTranslate(0,0,0);
		mySwiper_mn.params.onlyExternal=false;

		//Update active slide
		mySwiper_mn.updateActiveSlide(0);

		slideNumber++;
	}
}
Page= new Page();
$(function(){
	Page.init();
});

function scrollToDiv(id) {
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },'slow', function(){
    	Page.scrollrightmenu();
    });
}

function scrollToDiv2(ele) {
    ele = ele.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("." + ele).offset().top
    },'slow', function(){
    	Page.scrollrightmenu();
    });
}

function scrollToDiv3(ele) {
    // Scroll
    $('html,body').animate({
        scrollTop: $("." + ele).offset().top
    },'slow', function(){
    	Page.scrollrightmenu();
    });
}

function checkInp(val,error){
	if(val==''){
		
	}else{
		//$('input[name="'+error+'"], textarea[name="'+error+'"]').removeClass('error-fr');
		if(error=='cmnd'){
			if(isNaN(val)){
				//$('#msg_fr').html('CMND không hợp lệ');
				return false;
			}else if(val.length<9 || val.length>10){
				//$('#msg_fr').html('CMND không hợp lệ');
				console.log(val);
				return false;
			}
			return true;
		}
		
		if(error=='email'){
			var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			if (!val.match(filter)){
				//$('#msg_fr').html('Ðịa chỉ Email không hợp lệ');
				return false;
			}
			return true;
		}
		if(error=='password'){
			if(val.length < 6)
				return false;
			return true;
		}

		if(error=='username'){
			if(val.length < 6)
				return false;
			return true;
		}
				
		if(error=='phone'){
			if(isNaN(val)){
				//$('#msg_fr').html('Số điện thoại không hợp lệ');
				return false;
			}else{
				if(val.substr(0,2)==='01'){
					if(val.length<11 || val.length>11){
						//$('#msg_fr').html('Số điện thoại không hợp lệ');
						return false;
					}
				}else{
					if(val.length<10 || val.length>10){
						//$('#msg_fr').html('Số điện thoại không hợp lệ');
						return false;
					}
				}
			}
			return true;
		}

		return true;
	}
}
function ValidateInput(evt)
{
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;

	var keychar = String.fromCharCode(key);
	//alert(keychar);
	var keycheck = /[0-9]/;  

	if(  
	!(key == 8  ||  key == 27  || key == 46  || key == 37 || key == 39 ) ) // backspace delete  escape arrows
	{
		if( !keycheck.test(keychar) )
		{       
			theEvent.returnValue = false;//for IE
			if (theEvent.preventDefault) theEvent.preventDefault();//Firefox
		}
	}
}