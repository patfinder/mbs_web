//
var MaxAllowSize = '';

$(document).ready(function () {
    $().UItoTop({
        easingType: "easeOutQuart"
    });
    $("#caledar_from").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "1930:2050"
    });
    $("#caledar_to").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "1930:2050"
    });

    $(".custom_chk").jqTransCheckBox();
    $(".custom_rd").jqTransRadio();
    $(".custom_select").jqTransSelect();
    $(".fancyboxClick").fancybox();
    $("#saveContent").click(function () {
        $("#loader").fadeIn()
    });
    var a = $.url(document.location.href);
    if (a.segment(-1) != "update" && a.segment(-2) != "update") {
        if (a.fsegment(1) == "back" || a.fsegment(1) == "save") {
            if (a.fsegment(1) == "save") {
                show_perm_success()
            }
            //            if ($("#start").val() == "") {
            //                $("#start").val(0)
            //            }
            //            searchContent($("#start").val(), 10)
        } else {
            //            if (a.segment(-1) != "update_profile") {
            //                if (module != "admincp") {
            //                    searchContent(0, 10)
            //                }
            //            }
        }
    }
    $(".gr_perm_error").width($(".right_content").width() - 2);
    $(".gr_perm_success").width($(".right_content").width() - 2);
    $("#frmManagement input").keypress(function (b) {
        if (b.which == 13) {
            save();
            return false;
        }
    })



});
function show_perm_denied(msg) {
    console.log(msg);
    if (msg != "") {
        $('#txt_error').html(msg);
    }
    $(".gr_perm_error").fadeIn(500);
    $("#loader").fadeOut(300);
    $(".table").css("marginTop", 4);
    setTimeout("$('.gr_perm_error').fadeOut(300); $('.table').css('marginTop',0);", 5000);
}
function show_perm_success() {
    $(".gr_perm_success").fadeIn(500);
    $("#loader").fadeOut(300);
    $(".table").css("marginTop", 4);
    setTimeout("$('.gr_perm_success').fadeOut(300); $('.table').css('marginTop',0);", 5000);
}

function enterSearch(a) {
    if (a.keyCode == 13) {
        searchContent(0)
    }
}
function sort(b) {
    var a = $("#func_sort").val();
    var c = $("#type_sort").val();
    if (b == a) {
        if (c == "DESC") {
            $("#type_sort").val("ASC")
        } else {
            $("#type_sort").val("DESC")
        }
    } else {
        $("#func_sort").val(b);
        $("#type_sort").val("DESC")
    }
    searchContent(0, $("#per_page").val())
}
function updateStatus(d, a, c, loadmenu) {
    var b = root + "" + c + "/ajaxUpdateStatus";
    $.post(b, {
        id: d,
        status: a
    }, function (e) {
        $("#loadStatusID_" + d).html(e);
        if (loadmenu != null && loadmenu == 'true') {
            //$("#ajax_loadMenuAdmin").load(ajaxUrlMenu);
        }
    })
}
function updateReview(d, a, c, loadmenu) {
    var b = root + "" + c + "/ajaxUpdateReview";
    //alert(b);
    $.post(b, {
        id: d,
        review: a
    }, function (e) {
        if (d == "permission-denied") {
            show_perm_denied();
        } else {
            window.location.href = root + 'admincp/review';
            //searchContent($("#start").val(), $("#per_page").val());
        }
        //        $("#loadStatusID_" + d).html(e);
        //        if (loadmenu != null && loadmenu == 'true') {
        //            //$("#ajax_loadMenuAdmin").load(ajaxUrlMenu);
        //        }
    })
}
function selectItem(b) {
    var a = document.getElementById("item" + b);
    if (a.checked == false) {
        $(".item_row" + b).addClass("row_active");

        //Set check All
        var check = true;
        $("#ajax_loadContent .custom_chk").each(function (index) {
            if (($(this).attr('id') != "selectAllItems") && ($(this).attr('id') != $(a).attr('id'))) {
                if ($(this).attr('checked') == null || $(this).attr('checked') == false) {
                    check = false;
                }
            }
        });
        if (check) {
            $("#selectAllItems").prev("a").addClass("jqTransformChecked");
            $("#selectAllItems").attr("checked", "checked");
        }
    } else {
        $(".item_row" + b).removeClass("row_active");

        //Remove check All
        $("#selectAllItems").prev("a").removeClass("jqTransformChecked");
        $("#selectAllItems").removeAttr("checked");
    }
}
function selectAllItems(a) {
    if (document.getElementById("selectAllItems").checked == false) {
        $(".jqTransformCheckboxWrapper a").addClass("jqTransformChecked");
        for (var b = 0; b < a; b++) {
            if (document.getElementById("item" + b) != null) {
                $(".item_row" + b).addClass("row_active");
                itemCheck = document.getElementById("item" + b);
                itemCheck.checked = true
            }
        }
    } else {
        $(".jqTransformCheckboxWrapper a").removeClass("jqTransformChecked");
        for (var b = 0; b < a; b++) {
            if (document.getElementById("item" + b) != null) {
                $(".item_row" + b).removeClass("row_active");
                itemCheck = document.getElementById("item" + b);
                itemCheck.checked = false
            }
        }
    }
}
function showStatusAll() {
    var a = $("#per_page").val();
    for (var b = 0; b < a; b++) {
        if (document.getElementById("item" + b) != null) {
            if (document.getElementById("item" + b).checked == true) {
                updateStatus($("#item" + b).val(), 0, module)
            }
        }
    }
}
function showReviewAll() {
    var a = $("#per_page").val();
    for (var b = 0; b < a; b++) {
        if (document.getElementById("item" + b) != null) {
            if (document.getElementById("item" + b).checked == true) {
                updateReview($("#item" + b).val(), 0, module)
            }
        }
    }
}
function hideStatusAll() {
    var a = $("#per_page").val();
    for (var b = 0; b < a; b++) {
        if (document.getElementById("item" + b) != null) {
            if (document.getElementById("item" + b).checked == true) {
                updateStatus($("#item" + b).val(), 1, module)
            }
        }
    }
}
function deleteItem(c) {
    var a = confirm("Are you sure delete item?");
    if (a) {
        var b = root + "admincp/" + module + "/delete";
        $.post(b, {
            id: c
        }, function (d) {
            if (d == "permission-denied") {
                show_perm_denied()
            } else {
                searchContent($("#start").val(), $("#per_page").val())
            }
        })
    }
}
function deleteAll() {
    var b = confirm("Are you sure delete item selected?");
    if (b) {
        var a = $("#per_page").val();
        var isChecked = false;
        for (var d = 0; d < a; d++) {
            if (document.getElementById("item" + d) != null) {
                if (document.getElementById("item" + d).checked == true) {
                    isChecked = true;
                    id = $("#item" + d).val();
                    var c = root + module + "/delete";
                    //alert(c);
                    $.post(c, {
                        id: id
                    }, function (e) {
                        if (e == "permission-denied") {
                            show_perm_denied()
                        }
                        else if (e == "fail") {
                            alert("Delete item fail");
                        } else {
                            searchContent($("#start").val(), $("#per_page").val())
                        }
                    })
                }
            }
        }
        if (isChecked == false) {
            alert("Please select item(s) to do action.");
        }
    }
}
function chk_perm(b, a) {
    if (a != "no_access") {
        if (a == "read") {
            if ($("#read" + b).attr("checked") == "checked") {
                $("#noaccess" + b).attr("checked", true);
                $("#write" + b).attr("checked", false);
                $("#delete" + b).attr("checked", false);
                $(".custom_noaccess" + b).addClass("jqTransformChecked");
                $(".custom_write" + b).removeClass("jqTransformChecked");
                $(".custom_delete" + b).removeClass("jqTransformChecked")
            } else {
                $("#noaccess" + b).attr("checked", false);
                $(".custom_noaccess" + b).removeClass("jqTransformChecked")
            }
        } else {
            $("#read" + b).attr("checked", true);
            $("#noaccess" + b).attr("checked", false);
            $(".custom_read" + b).addClass("jqTransformChecked");
            $(".custom_noaccess" + b).removeClass("jqTransformChecked")
        }
    } else {
        if ($("#noaccess" + b).attr("checked") == "checked") {
            $("#read" + b).attr("checked", true);
            $(".custom_read" + b).addClass("jqTransformChecked")
        } else {
            $(".perm_access" + b).attr("checked", false);
            $(".custom_read" + b).removeClass("jqTransformChecked");
            $(".custom_write" + b).removeClass("jqTransformChecked");
            $(".custom_delete" + b).removeClass("jqTransformChecked")
        }
    }
};
$(function () {
    $('input[data-type="image"]').change(function () {
        var value = $(this).val();
        if (value.trim() != "") {
            var ext = value.substr((value.lastIndexOf('.') + 1));
            var in_ext = $.inArray(ext, ['jpg', 'png']);
            if (in_ext == -1) {
                $('#txt_error').html('Định dạng file không hợp lệ (.jpg, .png,)');
                show_error();
                $(this).val('');
                return false;
            }
        }

        var imgbytes = this.files[0].size; // Size returned in bytes.
        var imgkbytes = Math.round(parseInt(imgbytes) / 1024);
        var imgmbytes = Math.round(parseInt(imgkbytes) / 1024);
        if (MaxAllowSize == '') {
            MaxAllowSize = 2; //default max allow upload size
        }
        //alert(MaxAllowSize);
        if (imgmbytes > MaxAllowSize) {
            alert('File upload có kích thước quá lớn (hơn ' + MaxAllowSize + ' MB), vui lòng chọn file khác.');
            $(this).val('');
        }

    });
    $('input[data-type="pdf"]').change(function () {
        var value = $(this).val();
        if (value.trim() != "") {
            var ext = value.substr((value.lastIndexOf('.') + 1));
            var in_ext = $.inArray(ext, ['pdf']);
            if (in_ext == -1) {
                $('#txt_error').html('Định dạng file không hợp lệ (.pdf) ');
                show_error();
                $(this).val('');
                return false;
            }
        }
    });
    $('input[data-type="word"]').change(function () {
        var value = $(this).val();
        if (value.trim() != "") {
            var ext = value.substr((value.lastIndexOf('.') + 1));
            var in_ext = $.inArray(ext, ['doc', 'docx']);
            if (in_ext == -1) {
                $('#txt_error').html('Định dạng file không hợp lệ (.doc, .docx) ');
                show_error();
                $(this).val('');
            }
        }
    });
});
function checkInp(val, error) {
    if (val == '') { }
    else {
        $('input[name="' + error + '"], textarea[name="' + error + '"]').removeClass('error-fr');
        if (error == 'cmnd') {
            if (isNaN(val)) {
                $('#msg_fr').html('CMND không hợp lệ');
                return false;
            } else if ($('#info_cmnd').val().length < 9 || $('#info_cmnd').val().length > 10) {
                $('#msg_fr').html('CMND không hợp lệ');
                return false;
            }
            return true;
        }
        if (error == 'email') {
            var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (!val.match(filter)) {
                $('#msg_fr').html('Địa chỉ Email không hợp lệ');
                return false;
            }
            return true;
        }
        if (error == 'phone') {
            if (isNaN(val)) {
                $('#msg_fr').html('Số điện thoại không hợp lệ');
                return false;
            } else {
                if (val.substr(0, 2) === '01') {
                    if (val.length < 11 || val.length > 11) {
                        $('#msg_fr').html('Số điện thoại không hợp lệ');
                        return false;
                    }
                } else {
                    if (val.length < 10 || val.length > 10) {
                        $('#msg_fr').html('Số điện thoại không hợp lệ');
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
function checkSel(val, error) {
    if (val != 0) {
        $('.' + error).fadeOut();
    } else {
        $('.' + error).fadeIn();
    }
}
function scrollTo(element, speed) {
    $('html, body').animate({
        scrollTop: $(element).offset().top
    }, 500);
}


function getContentAjax(ajaxUrl, page, sortBy, sortType) {
    var perpage = $("#per_page").val();
    if (perpage != null && parseInt(perpage) > 0) {

        var fromDate = $.trim($("#caledar_from").val());
        var toDate = $.trim($("#caledar_to").val());
        var content = $.trim($("#search_content").val());
        var lang = $.trim($("#language_filter").val());
		var ws = $.trim($("#workshop_filter").val());

        if (fromDate != null) {
            fromDate = $.trim(fromDate);
        }
        if (toDate != null) {
            toDate = $.trim(toDate);
        }
        if (content != null) {
            content = $.trim(content);
            if (content == "type here...") {
                content = "";
            }
        }

        var parentId = $('#parentId').val();
        if (parentId == null) {
            parentId = 0;
        }

        $("#loader").show();
        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: { Page: page, PerPage: perpage, parentId: parentId, sortBy: sortBy, sortType: sortType, fromDate: fromDate, toDate: toDate, searchText: content, hasLayout: false, lang: lang, ws:ws },
            success: function (content) {
                $("#loader").hide();
                if (content != null && content != "") {
                    $("#ajax_loadContent").html(content);
                }
            }
        });
    }
}

function searchContentAjax(ajaxUrl, page) {
    var sortBy = $("#by_sort").val();
    var sortType = $("#type_sort").val();
    getContentAjax(ajaxUrl, page, sortBy, sortType);
}

function ColumnSort(ajaxUrl, newSortBy) {
    var sortBy = $("#by_sort").val();
    var sortType = $("#type_sort").val();
    if (newSortBy.toLowerCase() == sortBy.toLowerCase()) {
        if (sortType == "DESC") {
            sortType = "ASC";
        } else {
            sortType = "DESC";
        }
    } else {
        sortBy = newSortBy;
        sortType = "ASC";
    }

    var page = $("#currentPage").val();

    getContentAjax(ajaxUrl, page, sortBy, sortType);
}

function getSelectedItem() {
    var perpage = $("#per_page").val();
    var listId = "[";
    if (perpage != null && parseInt(perpage) > 0) {
        var element = null;
        for (var index = 0; index < perpage; index++) {
            element = document.getElementById("item" + index);
            if (element != null) {
                if (element.checked == true) {
                    listId += "," + element.value;
                }
            }
        }
    }
    listId = listId.replace("[,", "").replace("[", "");
    return listId;
}

function doAppoveReviewListItem(listId) {
    $("#loader").show();
    ajaxUrl = root + "review/UpdateListReview/";
    $.ajax({
        type: 'POST',
        url: ajaxUrl,
        data: { listId: listId },
        success: function (d) {
            if (d == "permission-denied") {
                $("#loader").hide();
                show_perm_denied();
            } else {
                window.location.href = root + 'admincp/review';
                //searchContent($("#start").val(), $("#per_page").val());
            }
            //$("#ajax_loadContent").html(content);
        }

    });
}

function doActionForSeleted(ajaxUrl, listId, status) {
    var perpage = $("#per_page").val();
    if (perpage != null && parseInt(perpage) > 0) {
        var page = $("#currentPage").val();
        var sortBy = $("#by_sort").val();
        var sortType = $("#type_sort").val();

        var fromDate = $.trim($("#caledar_from").val());
        var toDate = $.trim($("#caledar_to").val());
        var content = $.trim($("#search_content").val());

        if (fromDate != null) {
            fromDate = $.trim(fromDate);
        }
        if (toDate != null) {
            toDate = $.trim(toDate);
        }
        if (content != null) {
            content = $.trim(content);
            if (content == "type here...") {
                content = "";
            }
        }
        var parentId = $('#parentId').val();
        if (parentId == null) {
            parentId = 0;
        }

        $("#loader").show();
        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: { listId: listId, status: status, Page: page, PerPage: perpage, parentId: parentId, sortBy: sortBy, sortType: sortType, fromDate: fromDate, toDate: toDate, searchText: content, hasLayout: false },
            success: function (content) {
                //if (content == null || content == "") {
                //    alert("Have an error when update status of Bloc.Please try again.");
                //}

                $("#loader").hide();
                $("#ajax_loadContent").html(content);
            }

        });
    }
}
function approveSelectedReview() {
    var listId = getSelectedItem();
    if (listId == "") {
        alert("Please select item(s) to do action.");
    } else {
        doAppoveReviewListItem(listId);

    }
}
function showSelectedStatus() {
    var listId = getSelectedItem();
    if (listId == "") {
        alert("Please select item(s) to do action.");
    } else {
        updateSelectedStatus(listId, 1);
    }
}

function hideSelectedStatus() {
    var listId = getSelectedItem();
    if (listId == "") {
        alert("Please select item(s) to do action.");
    } else {
        updateSelectedStatus(listId, 0);
    }
}

function deleteSelected() {
    var conf = confirm("Are you sure delete item?");
    if (conf) {
        var listId = getSelectedItem();
        //alert(listId);
        if (listId == "") {
            alert("Please select item(s) to do action.");
        } else {
            deleteSelectedImplement(listId);
        }
    }
}

function update_ck() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}