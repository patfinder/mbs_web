﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using WizardCMS.Const;
using System.Web.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using WizardCMS.domain;

namespace WizardCMS.Controllers
{
    public class TermController : Controller
    {
        string nameModule = "taxonomy";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Term/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            ViewBag.module = "Term";

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);
                    ViewBag.dv_canhan = ArticlesServices.GetMenuByListId(new int[] { 113 }, 1);
                    ViewBag.dv_tochuc = ArticlesServices.GetMenuByListId(new int[] { 114, 115 }, 1);
                    ViewBag.Taxonomies = TaxonomyServices.GetAll();
                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            TermViewModel node = TermServices.GetById(id);

                            return PartialView("../BackEnd/Term/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Term/Update", null);
                    }
                }
                else
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    TermViewModel viewModel = new TermViewModel();

                    viewModel.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    viewModel.PRIORITY = StringHelper.IsNumber(Request["priority"]);
                    viewModel.TAXONOMY_ID = StringHelper.IsNumber(Request["taxonomy"]);
                    viewModel.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    viewModel.SERVICES = Request["checkbox"];

                    foreach (var lang in Constants.Langs)
                    {
                        var name = Request["name_" + lang];
                        var description = Request["description_" + lang];
                        var slug = StringHelper.GenerateSlug(name);

                        //Validate data input
                        if (name == string.Empty) continue;
                        
                        TermContent content = new TermContent();
                        content.NAME = name;
                        content.SLUG = slug;
                        content.DESCRIPTION = CommonHelper.CutText(description, 250);
                        content.LANG = lang;

                        viewModel.ContentTranslates.Add(lang, content);
                    }

                    if (viewModel.ID > 0)
                    {
                        //update
                        TermViewModel viewModelOld = TermServices.AdminGetById(viewModel.ID);
                        if (viewModelOld.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, TermContent> item in viewModelOld.ContentTranslates)
                            {
                                TermContent old_content = item.Value;

                                if (viewModel.ContentTranslates != null)
                                {
                                    if (viewModel.ContentTranslates.ContainsKey(old_content.LANG))
                                    {
                                        if (viewModel.ContentTranslates[old_content.LANG].SLUG != old_content.SLUG)
                                        {
                                            if (TermServices.CheckSlug(viewModel.ContentTranslates[old_content.LANG].SLUG, old_content.LANG, viewModel.TAXONOMY_ID))
                                                return "Tên đã có trong hệ thống.";
                                        }
                                    }
                                    else
                                    {
                                        viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                    }
                                }
                                else
                                {
                                    viewModel.ContentTranslates = new Dictionary<string, TermContent>();
                                    viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                }
                            }
                        }

                        if (TermServices.Update(viewModel)) return "success";
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                    else
                    {

                        if (viewModel.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, TermContent> item in viewModel.ContentTranslates)
                            {
                                TermContent content = item.Value;
                                if (TermServices.CheckSlug(content.SLUG, content.LANG, viewModel.TAXONOMY_ID))
                                    return "Tên đã có trong hệ thống.";
                            }
                        }
                        else
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = TermServices.Insert(viewModel);
                        if (newID >= Constants.ACTION_SUCCESS)
                            return "success";
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (TermServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        TermServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    TermViewModel node = TermServices.GetById(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        TermServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = TermServices.GetById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    TermServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "term";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Term/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            ViewBag.module = "Term";
            int totalItem = TermServices.GetCount(0, fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Term/Search/";

            List<TermViewModel> terms = TermServices.GetByPaging(0, fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Term/Index", terms);
            }
            else
            {
                return PartialView("../BackEnd/Term/AjaxContent", terms);
            }
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    TermViewModel node = TermServices.GetById(id);
                    if (order == 0)
                    {
                        node.PRIORITY -= 1;
                        if (node.PRIORITY < 0)
                        {
                            node.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (node.PRIORITY + 1 < int.MaxValue)
                        {
                            node.PRIORITY += 1;
                        }
                    }
                    TermServices.Update(node);
                    return node.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }

        public string ExportEnglish()
        {
            XmlTextWriter writer = new XmlTextWriter(System.Web.HttpContext.Current.Server.MapPath("/App_Data/term_english.xml"), System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("root");

            using (var context = new WizardCMS.domain.DBContext())
            {
                var result = context.WZ_TERM.ToList();
                if (result != null)
                {
                    foreach (var node in result)
                    {
                        var node_content = context.WZ_TERM_CONTENT.Where(n => n.TID == node.ID && n.LANG == Constants.LANG_EN).FirstOrDefault();
                        if (node_content != null)
                        {
                            writer.WriteStartElement("item");
                            writer.WriteStartElement("tid"); writer.WriteString(node_content.TID.ToString()); writer.WriteEndElement();
                            writer.WriteStartElement("name"); writer.WriteString(node_content.NAME); writer.WriteEndElement();
                            writer.WriteStartElement("slug"); writer.WriteString(node_content.SLUG); writer.WriteEndElement();
                            writer.WriteStartElement("description"); writer.WriteString(node_content.DESCRIPTION); writer.WriteEndElement();
                            writer.WriteStartElement("lang"); writer.WriteString(node_content.LANG); writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            return "DONE";
        }

        public string ImportEnglish()
        {
            XDocument xml = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("/App_Data/term_english.xml"));
            var node_contents = xml.Elements("root").Elements("item").ToList();

            if (node_contents != null)
            {
                foreach (var temp in node_contents)
                {
                    WZ_TERM_CONTENT node_content = new WZ_TERM_CONTENT();
                    node_content.TID = StringHelper.IsNumber(temp.Element("nid").Value.ToString());
                    node_content.LANG = temp.Element("lang").Value.ToString();
                    node_content.NAME = temp.Element("name").Value.ToString();
                    node_content.SLUG = temp.Element("slug").Value.ToString();
                    node_content.DESCRIPTION = temp.Element("description").Value.ToString();

                    using (DBContext context = new DBContext())
                    {
                        var check_parent = context.WZ_TERM.Where(n=>n.ID == node_content.TID).FirstOrDefault();
                        if (check_parent == null) continue; //BỎ QUA NẾU KO CÓ NODE CHA

                        var node_check = context.WZ_TERM_CONTENT.Where(nc => nc.LANG == node_content.LANG && nc.TID == node_content.TID).FirstOrDefault();
                        if (node_check != null)
                        {
                            node_check.NAME = temp.Element("title").Value.ToString();
                            node_check.SLUG = temp.Element("slug").Value.ToString();
                            node_check.DESCRIPTION = temp.Element("description").Value.ToString();

                            context.SaveChanges();
                        }
                        else
                        {
                            context.WZ_TERM_CONTENT.AddObject(node_content);
                            context.SaveChanges();
                        }
                    }
                }
            }
            else
            {
                return "";
            }
            return "DONE";
        }
    }
}
