﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardCMS.Models;
using WizardCMS.Helpers;
using WizardCMS.Const;
using System.Web.Configuration;
using System.IO;

namespace WizardCMS.Controllers
{
    public class JobController : Controller
    {
        string nameModule = "taxonomy";
        static int IdModule = 0;

        AdmincpController admin = new AdmincpController();

        string maxAllowConfig = WebConfigurationManager.AppSettings["MaxAllowSize"].ToString();
        int maxAllow = 0;

        public PartialViewResult Index()
        {
            string curentPath = Request.Url.AbsoluteUri;
            string function = CommonHelper.GetSegment(2, curentPath);
            if (string.IsNullOrEmpty(function) == false)
            {
                ModuleModel module = ModuleServices.GetModuleByFunction(function);
                if (module != null)
                {
                    IdModule = module.ID;
                    if (admin.GetPermission(IdModule, "r") == Constants.PERMISSION_DENIED || module.STATUS == 0)
                    {
                        ViewBag.permission = Constants.PERMISSION_DENIED;
                        return PartialView("../BackEnd/Job/Index", null);
                    }
                    else
                    {
                        ViewBag.PermissionWrite = admin.GetPermission(IdModule, "w");
                        ViewBag.PermissionDelete = admin.GetPermission(IdModule, "d");
                    }
                }
            }

            Paging paging = new Paging();
            paging.Perpage = Constants.DEFAULT_ITEM;
            paging.Page = 1;

            string nameAction = CommonHelper.GetSegment(3, curentPath);

            if (!int.TryParse(maxAllowConfig, out maxAllow)) maxAllow = 3;
            ViewBag.MaxAllowSize = maxAllow;

            ViewBag.module = "Job";

            if (!string.IsNullOrEmpty(nameAction))
            {
                if (nameAction == "update") //go to create or edit page
                {
                    string nodeId = CommonHelper.GetSegment(4, curentPath);
                    ViewBag.categories = TermServices.GetTerms(3);
                    ViewBag.terms = TermServices.GetTerms(4);
                    ViewBag.Taxonomies = TaxonomyServices.GetAll();
                    ViewBag.branchs = BranchServices.GetList();
                    ViewBag.provinces = BranchServices.GetProvinces();

                    if (!string.IsNullOrEmpty(nodeId))
                    {
                        try
                        {
                            int id = int.Parse(nodeId);
                            JobViewModel node = JobServices.AdminGetById(id);

                            return PartialView("../BackEnd/Job/Update", node);
                        }
                        catch (Exception)
                        {
                            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                        }
                    }
                    else
                    {
                        return PartialView("../BackEnd/Job/Update", null);
                    }
                }
                else
                {
                    try
                    {
                        string nodeId = CommonHelper.GetSegment(4, curentPath);
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                    catch (Exception)
                    {
                        return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
                    }
                }
            }
            else// manage article
            {
                return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_DESC, true);
            }
        }

        [HttpPost, ValidateInput(false)]
        public string Save()
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "w");
                if (permission == "w")
                {
                    string url = Request.Url.AbsoluteUri;
                    string curentPath = Request.Url.AbsoluteUri;
                    JobViewModel viewModel = new JobViewModel();

                    viewModel.ID = Request["hiddenIdAdmincp"] == string.Empty ? 0 : int.Parse(Request["hiddenIdAdmincp"]);
                    viewModel.PRIORITY = StringHelper.IsNumber(Request["priority"]);
                    viewModel.CID = StringHelper.IsNumber(Request["cid"]);
                    viewModel.TERM_ID = StringHelper.IsNumber(Request["term"]);
                    viewModel.NUM = StringHelper.IsNumber(Request["num"]);
                    viewModel.STATUS = Request["statusAdmincp"] == "on" ? 1 : 0;

                    string provinces = Request["checkbox"];

                    string strDeadline = Request["dead_line"];

                    if (!string.IsNullOrEmpty(Request["dead_line"]))
                    {
                        strDeadline = strDeadline.Replace('-', '/');
                        viewModel.DEADLINE = DatetimeHelper.StringToDate(Request["dead_line"], "dd-MM-yyyy");
                    }

                    if (!string.IsNullOrEmpty(Request["post_day"]))
                    {
                        strDeadline = strDeadline.Replace('-', '/');
                        viewModel.POST_DAY = DatetimeHelper.StringToDate(Request["post_day"], "dd-MM-yyyy");
                    }

                    foreach (var lang in Constants.Langs)
                    {
                        var name = Request["name_" + lang];
                        var description = Request["description_" + lang];
                        var require = Request["require_" + lang];
                        var benefit = Request["benefit_" + lang];
                        var doc_require = Request["doc_require_" + lang];
                        var deadline = Request["deadline_" + lang];
                        var place_cv = Request["place_cv_" + lang];
                        var slug = StringHelper.CreateSlug(name);

                        //Validate data input
                        if (name == string.Empty) continue;

                        JobContent content = new JobContent();
                        content.TITLE = name;
                        content.REQUIRE = require;
                        content.BENEFIT = benefit;
                        content.DOC_REQUIRE = doc_require;
                        content.DEADLINE = deadline;
                        content.PLACE_CV = place_cv;
                        content.SLUG = slug;
                        content.DESCRIPTION = description;
                        content.LANG = lang;

                        viewModel.ContentTranslates.Add(lang, content);
                    }

                    if (viewModel.ID > 0)
                    {
                        //update
                        JobViewModel viewModelOld = JobServices.AdminGetById(viewModel.ID);
                        if (viewModelOld.ContentTranslates != null)
                        {
                            foreach (KeyValuePair<string, JobContent> item in viewModelOld.ContentTranslates)
                            {
                                JobContent old_content = item.Value;

                                if (viewModel.ContentTranslates != null)
                                {
                                    if (viewModel.ContentTranslates.ContainsKey(old_content.LANG))
                                    {
                                        //if (viewModel.ContentTranslates[old_content.LANG].SLUG != old_content.SLUG)
                                        //{
                                        //    if (JobServices.CheckSlug(viewModel.ContentTranslates[old_content.LANG].SLUG, old_content.LANG, viewModel.TAXONOMY_ID))
                                        //        return "Tên đã có trong hệ thống.";
                                        //}
                                    }
                                    else
                                    {
                                        viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                    }
                                }
                                else
                                {
                                    viewModel.ContentTranslates = new Dictionary<string, JobContent>();
                                    viewModel.ContentTranslates.Add(old_content.LANG, old_content);
                                }
                            }
                        }

                        if (JobServices.Update(viewModel)) {
                            JobServices.InsertJobProvinces(provinces, viewModel.ID);
                            return "success"; 
                        }
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                    else
                    {

                        if (viewModel.ContentTranslates != null)
                        {
                            //foreach (KeyValuePair<string, JobContent> item in viewModel.ContentTranslates)
                            //{
                            //    JobContent content = item.Value;
                            //    if (JobServices.CheckSlug(content.SLUG, content.LANG, viewModel.TERM_ID))
                            //        return "Tên đã có trong hệ thống.";
                            //}
                        }
                        else
                        {
                            return "Vui lòng nhập nội dung bài viết.";
                        }

                        int newID = JobServices.Insert(viewModel);
                        if (newID >= Constants.ACTION_SUCCESS) {
                            JobServices.InsertJobProvinces(provinces, newID);
                            return "success";
                        }
                        else
                            return Constants.PERMISSION_DENIED;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                string permission = admin.GetPermission(IdModule, "d");
                if (permission == "d")
                {
                    if (id > 0)
                    {
                        if (JobServices.Delete(id))
                        {
                            return "success";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Constants.PERMISSION_DENIED;
        }

        [HttpPost]
        public PartialViewResult DeleteList(string listId, string fromDate, string toDate, string searchText, int parentId, Paging paging, string sortBy, string sortType)
        {
            try
            {
                string[] arrayId = listId.Split(',');
                if (arrayId.Count() > 0)
                {
                    List<int> listArtId = new List<int>();
                    foreach (string item in arrayId)
                    {
                        int id = int.Parse(item);
                        JobServices.Delete(id);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.WriteLogError(ex.StackTrace);
            }
            return Search(fromDate, toDate, searchText, paging, sortBy, sortType, false);
        }

        [HttpPost]
        public PartialViewResult UpdateStatus(int id, Paging paging, int parentId)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    string curentPath = Request.Url.AbsoluteUri;
                    JobViewModel node = JobServices.GetById(id);
                    if (node != null)
                    {
                        node.STATUS = (int)(node.STATUS == 1 ? 0 : 1);
                        JobServices.UpdateStatus(id, node.STATUS);
                    }
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            ViewBag.MessagePermssion = permission;
            return Search("", "", "", paging, Constants.SORT_PORPERTY_DEFAULT, Constants.SORT_ASC, false);
        }

        [HttpPost]
        public PartialViewResult ajaxUpdateStatus(int id, int status)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                var ar = JobServices.GetById(id);
                if (ar != null)
                {
                    status = status == 0 ? 1 : 0;
                    ar.STATUS = status;
                    JobServices.UpdateStatus(id, ar.STATUS);
                }
            }
            ViewBag.Id = id;
            ViewBag.NameModule = "Job";
            ViewBag.Status = status;
            ViewBag.MessagePermssion = permission;
            return PartialView("../BackEnd/Job/AjaxUpdateStatus");
        }

        [HttpPost]
        public PartialViewResult Search(string fromDate, string toDate, string searchText, Paging paging, string sortBy, string sortType, bool? hasLayout, string lang = "vi")
        {
            ViewBag.module = "Job";
            int totalItem = JobServices.GetCount(0, fromDate, toDate, searchText, lang);
            paging.TotalItems = totalItem;
            paging.TotalPage = totalItem / paging.Perpage;
            if (totalItem % paging.Perpage > 0)
            {
                paging.TotalPage++;
            }
            if (paging.Page - 1 < 0)
            {
                paging.Page = 1;
            }
            paging.Offset = (paging.Page - 1) * paging.Perpage;
            ViewBag.Paging = paging;
            ViewBag.SortBy = sortBy.ToUpper();
            ViewBag.SortType = sortType.ToUpper();
            ViewBag.AjaxUrl = Url.Content("~") + "Job/Search/";

            List<JobViewModel> terms = JobServices.GetByPaging(0, fromDate, toDate, searchText, paging, sortBy, sortType, lang);

            if (hasLayout != null && hasLayout == true)
            {
                return PartialView("../BackEnd/Job/Index", terms);
            }
            else
            {
                return PartialView("../BackEnd/Job/AjaxContent", terms);
            }
        }

        [HttpPost]
        public string UpdateOrder(int id, int order)
        {
            string permission = admin.GetPermission(IdModule, "w");
            if (permission == "w")
            {
                try
                {
                    JobViewModel node = JobServices.GetById(id);
                    if (order == 0)
                    {
                        node.PRIORITY -= 1;
                        if (node.PRIORITY < 0)
                        {
                            node.PRIORITY = 0;
                        }
                    }
                    else
                    {
                        if (node.PRIORITY + 1 < int.MaxValue)
                        {
                            node.PRIORITY += 1;
                        }
                    }
                    JobServices.Update(node);
                    return node.PRIORITY.ToString();
                }
                catch (Exception ex)
                {
                    Logs.WriteLogError(ex.StackTrace);
                }
            }
            return permission;
        }
    }
}
