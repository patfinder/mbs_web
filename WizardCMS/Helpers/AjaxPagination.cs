﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.Helpers
{
    public class AjaxPagination
    {
        public string BaseUrl { get; set; }

        public int Parrent { get; set; }
        public int Perpage { get; set; }
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int Numlinks { get; set; }

        public string FirstLink { get; set; }
        public string LastLink { get; set; }
        public string NextLink { get; set; }
        public string PrevLink { get; set; }
        public string FunctionAjax { get; set; }

        public AjaxPagination()
        {
            Parrent = 0;
            Perpage = 5;
            Numlinks = 4;

            FirstLink = string.Empty;
            LastLink = string.Empty;
            NextLink = Resources.Resources.NEXT_LINK;
            PrevLink = string.Empty;//Resources.Resources.PREV_LINK;
        }

        public AjaxPagination(int parrent, int total, string baseUrl, int page, string function)
            : this()
        {
            Parrent = parrent;
            TotalRows = total;
            BaseUrl = baseUrl;
            CurrentPage = page;
            FunctionAjax = function;
        }

        public string CreateLinks()
        {
            if (TotalRows == 0 || Perpage == 0)
            {
                return string.Empty;
            }

            int num_page = (int)Math.Ceiling(TotalRows / (double)Perpage);
            if (num_page == 1)
            {
                return string.Empty;
            }

            if (CurrentPage > num_page)
            {
                CurrentPage = num_page;
            }
            else
            {
 
            }

            int start = (CurrentPage - num_page > 0) ? CurrentPage - num_page : 1;
            //int end = num_page;

            string output = string.Empty;
            //previous link
            if(this.PrevLink != string.Empty && this.CurrentPage != 1)
            {
                output += String.Format("<a href='javascript:void(0)' onclick='{0}({1},{2},{3})'>{4}</a>", this.FunctionAjax, this.Parrent, 1, this.Perpage, this.PrevLink);
                //output += "<a href='javascript:void(0)' onclick='" + this.FunctionAjax + "(1," + this.Perpage + ")'>" + StringHelper.lang("text_page_first") + "</a>";
            }
            //calculate start, end
            int begin = CurrentPage - Numlinks > 0 ? CurrentPage - Numlinks : 1;
            int end = CurrentPage + Numlinks < num_page ? CurrentPage + Numlinks : num_page;

            if (begin > 1)
            {
                output += "<span class='no-bd'>...</span>";
            }

            //loop page number
            for (int i = begin; i <= end; i++)
            {
                if (i == CurrentPage)
                {
                    output += "<span class='page'>" + i + "</span>";
                }
                else
                {
                    output += String.Format("<a href='javascript:void(0)' onclick='{0}({1},{2},{3})'>{4}</a>", this.FunctionAjax, this.Parrent, i, this.Perpage, i);
                    //output += "<a href='javascript:void(0)' onclick='"+this.FunctionAjax+"("+i+","+this.Perpage+")'>" + i + "</a>";
                }

            }

            if (end < num_page)
            {
                output += "<span class='no-bd'>...</span>";
            }

            //next link
            if (this.NextLink != string.Empty && this.CurrentPage < num_page)
            {
                output += String.Format("<a class='no-bd pag_end' href='javascript:void(0)' onclick='{0}({1},{2},{3})'>{4}</a>", this.FunctionAjax, this.Parrent, num_page, this.Perpage, this.NextLink);
                //output += "<a class='pag_end' href='javascript:void(0)' onclick='"+this.FunctionAjax+"("+num_page+","+this.Perpage+")' >" + StringHelper.lang("text_page_end") + "</a>";
            }

            string pre = "<span class='p-tt'>" + StringHelper.lang("text_page") + " </span>";
            return pre + "<span class='page'>" + output + "</span>";
        }


    }
}