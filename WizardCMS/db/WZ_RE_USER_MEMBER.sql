/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 11:00:45
*/


-- ----------------------------
-- Table structure for WZ_RE_USER_MEMBER
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_USER_MEMBER";
CREATE TABLE "MBS_DB"."WZ_RE_USER_MEMBER" (
"ID" NUMBER NOT NULL ,
"USER_ID" NUMBER NOT NULL ,
"FULNAME" NVARCHAR2(128) NULL ,
"RELATIVE" NVARCHAR2(128) NULL ,
"DEGREE" NVARCHAR2(128) NULL ,
"YOB" NVARCHAR2(64) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Indexes structure for table WZ_RE_USER_MEMBER
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_USER_MEMBER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_USER_MEMBER" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER_MEMBER" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER_MEMBER" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_USER_MEMBER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_USER_MEMBER" ADD PRIMARY KEY ("ID");
