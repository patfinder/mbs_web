/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-06 15:36:58
*/


-- ----------------------------
-- Table structure for WZ_RE_FORM
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_FORM";
CREATE TABLE "MBS_DB"."WZ_RE_FORM" (
"ID" NUMBER NOT NULL ,
"PRIORITY" NUMBER DEFAULT 0  NULL ,
"STATUS" NUMBER DEFAULT 1  NULL ,
"MODIFIED" TIMESTAMP(6)  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_FORM
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_FORM" VALUES ('1', '0', '1', TO_TIMESTAMP(' 2015-04-25 14:55:46:897815', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-04-25 14:55:46:897815', 'YYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "MBS_DB"."WZ_RE_FORM" VALUES ('2', '0', '1', TO_TIMESTAMP(' 2015-04-25 15:06:41:895279', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-04-25 15:06:41:895279', 'YYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Indexes structure for table WZ_RE_FORM
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_FORM
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_FORM" ADD CHECK ("MODIFIED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_FORM" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_FORM
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_FORM" ADD PRIMARY KEY ("ID");
