/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-06 16:06:57
*/


-- ----------------------------
-- Table structure for WZ_RE_JOB
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_JOB";
CREATE TABLE "MBS_DB"."WZ_RE_JOB" (
"ID" NUMBER NOT NULL ,
"NUM" NUMBER DEFAULT 1  NOT NULL ,
"STATUS" NUMBER DEFAULT 1  NOT NULL ,
"MODIFIED" TIMESTAMP(6)  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL ,
"TERM_ID" NUMBER DEFAULT 0  NOT NULL ,
"DEADLINE" TIMESTAMP(6)  NOT NULL ,
"PRIORITY" NUMBER DEFAULT 0  NOT NULL ,
"CID" NUMBER DEFAULT 0  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_JOB
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_JOB" VALUES ('1', '1', '1', TO_TIMESTAMP(' 2015-04-25 16:29:38:151904', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-04-25 16:29:38:151904', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0', TO_TIMESTAMP(' 2015-04-30 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0', '0');
INSERT INTO "MBS_DB"."WZ_RE_JOB" VALUES ('2', '1', '1', TO_TIMESTAMP(' 2015-04-25 16:30:14:541986', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-04-25 16:30:14:230968', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0', TO_TIMESTAMP(' 2015-04-30 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0', '0');
INSERT INTO "MBS_DB"."WZ_RE_JOB" VALUES ('3', '1', '1', TO_TIMESTAMP(' 2015-05-05 17:54:20:203494', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-04-25 16:41:15:631798', 'YYYY-MM-DD HH24:MI:SS:FF6'), '12', TO_TIMESTAMP(' 2015-04-30 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0', '9');

-- ----------------------------
-- Indexes structure for table WZ_RE_JOB
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_JOB
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("NUM" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("MODIFIED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("TERM_ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("DEADLINE" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD CHECK ("CID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_JOB
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_JOB" ADD PRIMARY KEY ("ID");
