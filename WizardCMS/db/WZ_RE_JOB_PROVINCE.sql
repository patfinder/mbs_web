/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-08 17:22:15
*/


-- ----------------------------
-- Table structure for WZ_RE_JOB_PROVINCE
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_JOB_PROVINCE";
CREATE TABLE "MBS_DB"."WZ_RE_JOB_PROVINCE" (
"JOB_ID" NUMBER NOT NULL ,
"PROVINCE_ID" NVARCHAR2(10) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_JOB_PROVINCE
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_JOB_PROVINCE" VALUES ('9', '701');
INSERT INTO "MBS_DB"."WZ_RE_JOB_PROVINCE" VALUES ('10', '101');
INSERT INTO "MBS_DB"."WZ_RE_JOB_PROVINCE" VALUES ('10', '701');

-- ----------------------------
-- Indexes structure for table WZ_RE_JOB_PROVINCE
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table WZ_RE_JOB_PROVINCE
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_JOB_PROVINCE" ADD PRIMARY KEY ("JOB_ID", "PROVINCE_ID");
