/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-04-13 23:14:32
*/


-- ----------------------------
-- Table structure for ADMIN_WZ_GROUPS
-- ----------------------------
DROP TABLE "MBS_DB"."ADMIN_WZ_GROUPS";
CREATE TABLE "MBS_DB"."ADMIN_WZ_GROUPS" (
"ID" NUMBER NOT NULL ,
"NAME" VARCHAR2(255 BYTE) NULL ,
"PERMISSION" VARCHAR2(500 BYTE) NULL ,
"STATUS" NUMBER DEFAULT 1  NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of ADMIN_WZ_GROUPS
-- ----------------------------
INSERT INTO "MBS_DB"."ADMIN_WZ_GROUPS" VALUES ('1', 'Root', '0|rwd,5|rwd,2|rwd,1|rwd,4|rwd,9|rwd,10|rwd,8|rwd,13|rwd,7|rwd,11|rwd,12|rwd,3|rwd,6|rwd,14|rwd', '1', TO_TIMESTAMP(' 2012-08-28 14:51:26:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "MBS_DB"."ADMIN_WZ_GROUPS" VALUES ('2', 'admin', '0|rwd,5|---,2|rwd,1|rwd,4|rwd,9|rwd,10|rwd,8|rwd,6|rwd,3|rwd,7|rwd', '1', TO_TIMESTAMP(' 2012-12-03 17:23:58:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Indexes structure for table ADMIN_WZ_GROUPS
-- ----------------------------

-- ----------------------------
-- Checks structure for table ADMIN_WZ_GROUPS
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table ADMIN_WZ_GROUPS
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_GROUPS" ADD PRIMARY KEY ("ID");
