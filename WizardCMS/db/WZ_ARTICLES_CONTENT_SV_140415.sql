/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.4.0

Source Server         : MBS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-04-13 20:14:27
*/


-- ----------------------------
-- Table structure for WZ_ARTICLES_CONTENT
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_ARTICLES_CONTENT";
CREATE TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" (
"NID" NUMBER DEFAULT 0  NOT NULL ,
"TITLE" NVARCHAR2(255) NOT NULL ,
"SLUG" NVARCHAR2(255) NULL ,
"DESCRIPTION" NCLOB NULL ,
"LANG" NVARCHAR2(20) NOT NULL ,
"IMAGE" NVARCHAR2(255) NULL ,
"LINK" NVARCHAR2(383) NULL ,
"CONTENT" NCLOB NULL ,
"SUB_TITLE" NVARCHAR2(128) NULL ,
"ATTACHMENT" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_ARTICLES_CONTENT
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('20', 'Executive Board', 'executive-board', empty_clob(), 'en', null, 'about/leadership/executive-board', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('21', 'Ban Kiểm Soát', 'ban-kiem-soat', empty_clob(), 'vi', null, 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-kiem-soat', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('21', 'Key Personnel', 'key-personnel', empty_clob(), 'en', null, 'about/leadership/key-personnel', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('22', 'Cao Thị Thúy Nga', 'cao-thi-thuy-nga', 'Chủ tịch HĐQT', 'vi', 'uploads/article/2015/3/02032015_431103_AM1.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri/cao-thi-thuy-nga', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma; font-size: 12px; line-height: normal; text-align: justify;">Bà Cao Thị Thuý Nga hiện là Chủ tịch HĐQT Công ty CP Chứng khoán&nbsp;MB (MBS), nguyên Phó Tổng Giám đốc Ngân hàng TMCP Quân Đội (MB), phụ trách Phòng Truyền thông, khối Quản lý mạng lưới và kênh phân phối. Bà có 11 năm kinh nghiệm về lĩnh vực tín dụng tại Ngân hàng Đầu tư và Phát triển Việt Nam, hơn 12 năm là Kế toán trưởng kiêm Phụ trách nhân sự tại Ngân hàng liên doanh VID PUBLIC. Bà Nga tốt nghiệp Thạc sỹ Đại học Tài chính - Kế toán và đã tham gia các khoá học Kiểm toán quốc tế tại Công ty Kiểm toán Việt Nam. Bà Nga là thành viên HĐQT đại diện vốn của MB tại MBS.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('22', 'Cao Thi Thuy Nga', 'cao-thi-thuy-nga', 'Chairman of the Board', 'en', 'uploads/article/2015/3/02032015_431103_AM1.jpg', 'about/leadership/board-of-directors/cao-thi-thuy-nga', '<p><span class="des">Bà Cao Thị Thuý Nga hiện là Chủ tịch HĐQT Công ty CP Chứng khoán MB (MBS), nguyên Phó Tổng Giám đốc Ngân hàng TMCP Quân Đội (MB), phụ trách Phòng Truyền thông, khối Quản lý mạng lưới và kênh phân phối. Bà có 11 năm kinh nghiệm về lĩnh vực tín dụng tại Ngân hàng Đầu tư và Phát triển Việt Nam, hơn 12 năm là Kế toán trưởng kiêm Phụ trách nhân sự tại Ngân hàng liên doanh VID PUBLIC. Bà Nga tốt nghiệp Thạc sỹ Đại học Tài chính - Kế toán và đã tham gia các khoá học Kiểm toán quốc tế tại Công ty Kiểm toán Việt Nam. Bà Nga là thành viên HĐQT đại diện vốn của MB tại MBS.</span></p>
', 'Mrs', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('23', 'Hoàng Minh Tuấn', 'hoang-minh-tuan', 'Phó Chủ tịch HĐQT', 'vi', 'uploads/article/2015/3/02032015_361151_AM1.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri/hoang-minh-tuan', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 12px; line-height: 13.8000001907349px; text-align: justify;">Ông Hoàng Minh Tuấn gia nhập Ngân hàng TMCP Quân đội (MB) từ tháng 12/2010 và hiện là Phó Giám đốc Khối Khách hàng cá nhân. Ông Tuấn tốt nghiệp trường Đại học Kinh tế Quốc dân năm 1998, và có 14 năm kinh nghiệm trong lĩnh vực tài chính - ngân hàng, hơn 5 năm giữ chức vụ Giám đốc chi nhánh tại Sacombank.&nbsp;</span></p>
', 'Ông', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('65', 'Công bố thông tin', 'cong-bo-thong-tin', empty_clob(), 'vi', null, 'quan-he-co-dong/cong-bo-thong-tin', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('218', 'Trần Hải Hà', 'tran-hai-ha', null, 'vi', '/uploads/images/doi-ngu/dn-h3.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri/tran-hai-ha', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 13.3333330154419px; line-height: 16.8666667938232px; text-align: justify;">Ông Trần Hải Hà có nhiều năm kinh nghiệm chuyên môn trong lĩnh vực tài chính ngân hàng, từng giữ nhiều vị trí quan trọng tại Ngân hàng TMCP Quân đội (MB) như Phó Giám đốc Khối Khách hàng lớn và Định chế tài chính, Giám đốc Chi nhánh MB tại Hải Phòng. Hiện tại, Ông Trần Hải Hà là Thành viên HĐQT kiêm Tổng Giám đốc MBS, trực tiếp phụ trách hoạt động dịch vụ ngân hàng đầu tư của MBS.</span></p>
', 'Ông', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('23', 'Hoang Minh Tuan', 'hoang-minh-tuan', 'Vice president', 'en', 'uploads/article/2015/3/02032015_361151_AM1.jpg', 'about/leadership/board-of-directors/hoang-minh-tuan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>
', 'Mr', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('24', 'Trần Hải Hà', 'tran-hai-ha', 'Tổng Giám đốc', 'vi', 'uploads/article/2015/3/02032015_401138_AM1.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-tong-giam-doc/tran-hai-ha', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 13.3333330154419px; line-height: 18px; text-align: justify;">Ông Trần Hải Hà có nhiều năm kinh nghiệm chuyên môn trong lĩnh vực tài chính ngân hàng, từng giữ nhiều vị trí quan trọng tại Ngân hàng TMCP Quân đội (MB) như Phó Giám đốc Khối Khách hàng lớn và Định chế tài chính, Giám đốc Chi nhánh MB tại Hải Phòng. Hiện tại, Ông Trần Hải Hà là Thành viên HĐQT kiêm Tổng Giám đốc MBS, trực tiếp phụ trách hoạt động dịch vụ ngân hàng đầu tư của MBS.</span></p>
', 'Ông', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('24', 'Trần Hải Hà', 'tran-hai-ha', 'Chủ tịch HĐQT', 'en', 'uploads/article/2015/3/02032015_401138_AM1.jpg', 'about/leadership/executive-board/tran-hai-ha', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>
', 'Mr', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('25', 'Đặng Thuý Dung', 'dang-thuy-dung', 'Thành viên HĐQT', 'vi', 'uploads/article/2015/3/02032015_411130_AM1.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri/dang-thuy-dung', '<p><span style="color: rgb(1, 1, 1); text-align: justify; font-size: 9pt; line-height: 13.8000001907349px; font-family: tahoma, sans-serif;">Bà Đặng Thúy Dung gia nhập Ngân hàng TMCP Quân đội (MB) từ năm 1995. Trải qua gần 20 năm kinh nghiệm gắn bó phát triển cùng MB, bà Dung đã được giao nhiều trọng trách như: Trưởng phòng hỗ trợ Treasury, Trưởng phòng Kế toán Hội sở, Thành viên Văn phòng triển khai chiến lược, Phó Giám đốc Khối Nguồn vốn và kinh doanh tiền tệ. Bà Dung tốt nghiệp Thạc sỹ chuyên ngành Tài chính tại Học viện Tài chính, là<span class="apple-converted-space">&nbsp;</span></span><span style="color: rgb(1, 1, 1); text-align: justify; font-size: 9pt; line-height: 13.8000001907349px; font-family: tahoma, sans-serif; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0% 0%; background-repeat: repeat;">thành viên HĐQT đại diện vốn của MB tại MBS.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('25', 'Đặng Thuý Dung', 'dang-thuy-dung', 'Chủ tịch HĐQT', 'en', 'uploads/article/2015/3/02032015_411130_AM1.jpg', 'about/leadership/board-of-directors/dang-thuy-dung', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>
', 'Mrs', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('26', 'Đoàn Kim Dung', 'doan-kim-dung', 'Thành viên HĐQT', 'vi', 'uploads/article/2015/3/02032015_421143_AM1.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri/doan-kim-dung', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 12px; line-height: 13.8000001907349px; text-align: justify;">Bà Đoàn Kim Dung tốt nghiệp Học viện Tài chính năm 1996 và gia nhập Ngân hàng TMCP Quân đội (MB) từ năm 1997. Với 16 năm kinh nghiệm trong lĩnh vực tài chính, bà Dung hiện đang là Phó Giám đốc Tài chính – Công ty CP Quản lý Quỹ đầu tư MB (MB Capital). Trước đó bà Dung đã được giao nhiều trọng trách tại MB như: Phó Phòng Kế toán Hội sở, Phụ trách Trung tâm Thẻ, Phụ trách Phòng Kế toán Chi nhánh Điện Biên Phủ.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('64', 'Financial statements', 'financial-statements', empty_clob(), 'en', null, 'investor-relations/financial-statements', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('2', 'Organazation customer', 'organazation-customer', null, 'en', null, 'organazation-customer', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('1', 'Khách hàng cá nhân', 'khach-hang-ca-nhan', null, 'vi', 'uploads/article/2014/12/26122014_341653_PM1.jpg', 'khach-hang-ca-nhan', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('1', 'Individual customer', 'individual-customer', null, 'en', null, 'individual-customer', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('3', 'Trung tâm phân tích', 'trung-tam-phan-tich', null, 'vi', null, 'trung-tam-phan-tich', '<p>Trung tâm phân tích</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('4', 'Chăm sóc khách hàng', 'cham-soc-khach-hang', empty_clob(), 'vi', null, 'cham-soc-khach-hang', '<p>Chăm sóc khách hàng</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('4', 'Customer Care', 'customer-care', empty_clob(), 'en', null, 'customer-care', '<p>Customer Care</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('5', 'About', 'about', null, 'en', null, 'about', '<p>Content is being updating...</p>
', 'About MBS', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('6', 'Liên hệ', 'lien-he', null, 'vi', null, 'lien-he', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('6', 'Contact', 'contact', null, 'en', null, 'contact', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('7', 'Tầm nhìn - sứ mệnh', 'tam-nhin-su-menh', null, 'vi', null, 'gioi-thieu-chung/tam-nhin-su-menh', '<p>Nội dung đang cập nhật...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('7', 'Vision - Mission', 'vision-mission', null, 'en', null, 'about/vision-mission', '<p>Content is being update...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('8', 'Core values', 'core-values', empty_clob(), 'en', null, 'about/core-values', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('9', 'Đội ngũ lãnh đạo', 'doi-ngu-lanh-dao', 'Đội ngũ lãnh đạo', 'vi', null, 'gioi-thieu-chung/doi-ngu-lanh-dao', '<p>Nội dung đang cập nhật...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('9', 'Leadership', 'leadership', empty_clob(), 'en', null, 'about/leadership', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('10', 'Sơ đồ tổ chức', 'so-do-to-chuc', null, 'vi', null, 'gioi-thieu-chung/so-do-to-chuc', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('10', 'Organizational chart', 'organizational-chart', null, 'en', null, 'about/organizational-chart', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('11', 'Accomplish', 'accomplish', 'During operation, MBS has made spectacular progress with the typical achievements as:', 'en', null, 'about/accomplish', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('12', 'Văn hóa tổ chức', 'van-hoa-to-chuc', empty_clob(), 'vi', null, 'gioi-thieu-chung/van-hoa-to-chuc', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('12', 'Cultural institutions', 'cultural-institutions', empty_clob(), 'en', null, 'about/cultural-institutions', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('13', 'Mạng lưới', 'mang-luoi', empty_clob(), 'vi', null, 'gioi-thieu-chung/mang-luoi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('13', 'Network', 'network', empty_clob(), 'en', null, 'about/network', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('14', 'Sáng tạo đi kèm thực tiễn', 'sang-tao-di-kem-thuc-tien', null, 'vi', 'uploads/article/2015/2/26022015_271150_AM1.jpg', 'gioi-thieu-chung/gia-tri-cot-loi/sang-tao-di-kem-thuc-tien', '<p>Thực tiễn là nơi kiểm nghiệm các sáng tạo của MBS.</p>

<p>Sáng tạo được thể hiện trong từng quy trình thực hiện công việc và nghiên cứu đưa ra giải pháp, từ việc không ngừng tìm hiểu, học tập, lắng nghe và áp dụng tất cả những thay đổi phù hợp nhất để đạt được những giải pháp tối ưu nhất với từng khách hàng riêng biệt. Sáng tạo góp phần quan trọng đảm bảo sự phục vụ tốt nhất đối với từng nhóm đối tượng khách hàng của MBS.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('14', 'Creativity accompanied by practices', 'creativity-accompanied-by-practices', null, 'en', 'uploads/article/2015/2/26022015_271150_AM1.jpg', 'about/core-values/creativity-accompanied-by-practices', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('15', 'Tôn trọng và học tập', 'ton-trong-va-hoc-tap', null, 'vi', 'uploads/article/2015/2/26022015_271126_AM1.jpg', 'gioi-thieu-chung/gia-tri-cot-loi/ton-trong-va-hoc-tap', '<p>MBS đề cao saự tôn trọng các ý tưởng sáng tạo, các ý kiến đóng góp của từng cá nhân; đồng thời, đánh giá cao tinh thần học tập lẫn nhau giữa các cá nhân và từ yêu cầu của từng khách hàng.</p>

<p>Tôn trọng còn được thể hiện qua việc đáp ứng nhanh nhất nhu cầu khách hàng thông qua việc lắng nghe, hỏi sâu để thấu đáo vấn đề. Đó được coi là Giá trị lớn nhất, là trách nhiệm trọng yếu của từng cá nhân thuộc MBS, tạo nên sự khác biệt về phong cách phục vụ của MBS.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('15', 'Tespecting and learning', 'tespecting-and-learning', null, 'en', 'uploads/article/2015/2/26022015_271126_AM1.jpg', 'about/core-values/tespecting-and-learning', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('16', 'Mẫn cán', 'man-can', null, 'vi', 'uploads/article/2015/2/26022015_221123_AM1.jpg', 'gioi-thieu-chung/gia-tri-cot-loi/man-can', '<p>Để thành công và được khách hàng và đối tác tin cậy và lựa chọn, Người MBS cần phải làm việc mẫn cán tức là chủ động làm việc, luôn nghĩ ra cách thực hiện công việc tốt hơn, nhanh hơn và hiệu quả hơn.</p>

<p>MBS đồng thời duy trì kỷ luật cao để triển khai nhanh, nhất quán các quy trình công việc, ý tưởng sáng tạo và các cam kết, đặc biệt là các cam kết liên quan đến an toàn tài chính với khách hàng và nhân viên MBS.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('16', 'Diligence', 'diligence', null, 'en', 'uploads/article/2015/2/26022015_221123_AM1.jpg', 'about/core-values/diligence', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('17', 'Tinh thần đồng đội', 'tinh-than-dong-doi', null, 'vi', 'uploads/article/2015/2/26022015_261141_AM1.jpg', 'gioi-thieu-chung/gia-tri-cot-loi/tinh-than-dong-doi', '<p>Một tổ chức một mục tiêu - không có một cá nhân nào là quá quan trọng tại MBS.</p>

<p>Để thống nhất mục tiêu, hành động, các cán bộ quản lý MBS cần là những người nắm bắt được chiến lược của công ty, biết giao việc theo tiêu chuẩn SMART, định hướng cách giải quyết công việc và giám sát kết quả của các công việc đó.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('17', 'Teamwork', 'teamwork', null, 'en', 'uploads/article/2015/2/26022015_261141_AM1.jpg', 'about/core-values/teamwork', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('18', 'Trách nhiệm và phát triển cộng đồng', 'trach-nhiem-va-phat-trien-cong-dong', null, 'vi', 'uploads/article/2015/2/26022015_271104_AM1.jpg', 'gioi-thieu-chung/gia-tri-cot-loi/trach-nhiem-va-phat-trien-cong-dong', '<p>MBS cam kết chia sẻ kiến thức, kinh nghiệm và lợi nhuận cho cộng đồng xã hội, giúp tạo ra một thị trường vốn phục vụ hiệu quả nhất phát triển kinh tế đất nước.</p>

<p>Đồng thời, MBS mang đến cho các gia đình và doanh nghiệp các giá trị đầu tư và tài chính tối ưu.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('18', 'Responsibility and community development', 'responsibility-and-community-development', null, 'en', 'uploads/article/2015/2/26022015_271104_AM1.jpg', 'about/core-values/responsibility-and-community-development', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('8', 'Giá trị cốt lõi', 'gia-tri-cot-loi', empty_clob(), 'vi', null, 'gioi-thieu-chung/gia-tri-cot-loi', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('19', 'Hội đồng quản trị', 'hoi-dong-quan-tri', empty_clob(), 'vi', null, 'gioi-thieu-chung/doi-ngu-lanh-dao/hoi-dong-quan-tri', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('19', 'Board of Directors', 'board-of-directors', empty_clob(), 'en', null, 'about/leadership/board-of-directors', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('20', 'Ban Tổng Giám Đốc', 'ban-tong-giam-doc', empty_clob(), 'vi', null, 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-tong-giam-doc', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('91', 'Khai trương chi nhánh tại TP.HCM', 'khai-truong-chi-nhanh-tai-tphcm', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2003/khai-truong-chi-nhanh-tai-tphcm', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('65', 'Disclosure of information', 'disclosure-of-information', empty_clob(), 'en', null, 'investor-relations/disclosure-of-information', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('66', 'Thông tin quản trị', 'thong-tin-quan-tri', empty_clob(), 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('66', 'Management Information', 'management-information', empty_clob(), 'en', null, 'investor-relations/management-information', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('73', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', empty_clob(), 'en', null, 'investor-relations/shareholder-news/lorem-ipsum-dolor-sit-amet', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('74', 'Morbi in sem quis dui placerat ornare', 'morbi-in-sem-quis-dui-placerat-ornare', empty_clob(), 'en', null, 'investor-relations/shareholder-news/morbi-in-sem-quis-dui-placerat-ornare', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('75', 'Phasellus ultrices nulla quis nibh', 'phasellus-ultrices-nulla-quis-nibh', empty_clob(), 'en', null, 'investor-relations/shareholder-news/phasellus-ultrices-nulla-quis-nibh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('76', 'Tầm nhìn', 'tam-nhin', empty_clob(), 'vi', null, 'gioi-thieu-chung/tam-nhin-su-menh/tam-nhin', '<div class="tt-gtcl first">
<p>MBS nhận thức rằng: Mỗi Khách hàng là một đối tác riêng biệt, có điều kiện tài chính, mục tiêu đầu tư khác nhau và đều hướng đến một sự tăng trưởng tài chính bền vững.</p>

<p>Do đó, chúng tôi sáng tạo và nỗ lực không ngừng cùng khách hàng tìm ra các Giải pháp đầu tư và tài chính tối ưu được may đo riêng cho từng gia đình và doanh nghiệp.</p>

<ul>
	<li>Khách hàng luôn là trung tâm của mọi hoạt động.</li>
	<li>Nhân lực tại MBS là tài sản quý giá và luôn luôn cần có thử thách.</li>
	<li>Tăng trưởng bền vững là mục tiêu trọng yếu và lâu dài của MBS.</li>
</ul>

<p>Trên cơ sở đó, MBS phấn đấu trở thành:</p>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('76', 'Vision', 'vision', empty_clob(), 'en', null, 'about/vision-mission/vision', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('77', 'Sứ mệnh', 'su-menh', empty_clob(), 'vi', null, 'gioi-thieu-chung/tam-nhin-su-menh/su-menh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('77', 'Mission', 'mission', empty_clob(), 'en', null, 'about/vision-mission/mission', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('78', 'Tầm nhìn 1', 'tam-nhin-1', null, 'vi', null, 'gioi-thieu-chung/tam-nhin-su-menh/tam-nhin/tam-nhin-1', '<p>Công ty có <span>dịch vụ <strong>thuận tiện nhất</strong></span> cho khách hàng cá nhân, và <strong><span>chuyên nghiệp nhất</span></strong> trong các nhà cung cấp dịch vụ M&amp;A và thị trường vốn tại Việt Nam</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('78', 'vision 1', 'vision-1', null, 'en', null, 'about/vision-mission/vision/vision-1', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('79', 'Sứ mệnh 1', 'su-menh-1', null, 'vi', null, 'gioi-thieu-chung/tam-nhin-su-menh/su-menh/su-menh-1', '<p>Sáng tạo và nỗ lực không ngừng để mang đến <strong><span>cơ hội đầu tư</span></strong> và <strong><span>tăng trưởng tài chính</span></strong> cho từng Khách hàng và Đối tác.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('79', 'Mission 1', 'mission-1', null, 'en', null, 'about/vision-mission/mission/mission-1', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('85', 'Tăng vốn điều lệ hơn 1,221 tỷ đồng', 'tang-von-dieu-le-hon-1221-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2013/tang-von-dieu-le-hon-1221-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('91', 'Khai trương chi nhánh tại TP.HCM', 'khai-truong-chi-nhanh-tai-tphcm', null, 'en', null, 'about/the-important-milestones/2003/khai-truong-chi-nhanh-tai-tphcm', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('92', '2006', '2006', null, 'vi', 'uploads/article/2015/3/19032015_430529_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2006', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('60', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', 'uploads/article/2015/3/06032015_041623_PM1.jpg', 'goc-truyen-thong/tin-mbs/lorem-ipsum-dolor-sit-amet', '<div class="content" id="short-para">
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>

<p>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('92', '2006', '2006', null, 'en', 'uploads/article/2015/3/19032015_430529_AM1.jpg', 'about/the-important-milestones/2006', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('26', 'Đoàn Kim Dung', 'doan-kim-dung', 'Kế toán trưởng', 'en', 'uploads/article/2015/3/02032015_421143_AM1.jpg', 'about/leadership/board-of-directors/doan-kim-dung', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>
', 'Mrs', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('27', '2008', '2008', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2008', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('27', '2008', '2008', null, 'en', null, 'about/accomplish/2008', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('28', '2009', '2009', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('28', '2009', '2009', null, 'en', null, 'about/accomplish/2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('29', '2010', '2010', null, 'en', null, 'about/accomplish/2010', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('30', '2012', '2012', null, 'en', null, 'about/accomplish/2012', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('31', 'Công ty chứng khoán tiêu biểu', 'cong-ty-chung-khoan-tieu-bieu', 'của HNX năm 2008', 'en', null, 'about/accomplish/2008/cong-ty-chung-khoan-tieu-bieu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('32', 'Danh hiệu Tin và dùng Việt Nam năm 2009', 'danh-hieu-tin-va-dung-viet-nam-nam-2009', 'do Thời báo Kinh tế Việt Nam bình chọn', 'en', null, 'about/accomplish/2009/danh-hieu-tin-va-dung-viet-nam-nam-2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('33', 'Công ty chứng khoán được yêu thích nhất năm 2009', 'cong-ty-chung-khoan-duoc-yeu-thich-nhat-nam-2009', 'tại HNX do Báo Đầu tư kết hợp với Sở GDCK Hà Nội bình chọn', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2009/cong-ty-chung-khoan-duoc-yeu-thich-nhat-nam-2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('33', 'Công ty chứng khoán được yêu thích nhất năm 2009', 'cong-ty-chung-khoan-duoc-yeu-thich-nhat-nam-2009', 'tại HNX do Báo Đầu tư kết hợp với Sở GDCK Hà Nội bình chọn', 'en', null, 'about/accomplish/2009/cong-ty-chung-khoan-duoc-yeu-thich-nhat-nam-2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('34', 'Đứng đầu thị phần môi giới', 'dung-dau-thi-phan-moi-gioi', 'tại Sở GDCK TP. HCM và Sở GDCK Hà Nội', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2009/dung-dau-thi-phan-moi-gioi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('34', 'Đứng đầu thị phần môi giới', 'dung-dau-thi-phan-moi-gioi', 'tại Sở GDCK TP. HCM và Sở GDCK Hà Nội', 'en', null, 'about/accomplish/2009/dung-dau-thi-phan-moi-gioi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('35', 'Thương hiệu mạnh Việt Nam 2010', 'thuong-hieu-manh-viet-nam-2010', null, 'en', null, 'about/accomplish/2010/thuong-hieu-manh-viet-nam-2010', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('36', 'Thương hiệu chứng khoán uy tín', 'thuong-hieu-chung-khoan-uy-tin', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/thuong-hieu-chung-khoan-uy-tin', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('36', 'Thương hiệu chứng khoán uy tín', 'thuong-hieu-chung-khoan-uy-tin', null, 'en', null, 'about/accomplish/2010/thuong-hieu-chung-khoan-uy-tin', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('37', '“Doanh nghiệp tiêu biểu”', 'doanh-nghiep-tieu-bieu', 'trong chương trình "Trí tuệ Thăng Long - Hà Nội"', 'en', null, 'about/accomplish/2010/doanh-nghiep-tieu-bieu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('38', 'Cúp Thăng Long', 'cup-thang-long', 'do UBND thành phố Hà Nội trao tặng', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/cup-thang-long', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('38', 'Cúp Thăng Long', 'cup-thang-long', 'do UBND thành phố Hà Nội trao tặng', 'en', null, 'about/accomplish/2010/cup-thang-long', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('39', 'Bằng khen của UBND TP. Hà Nội', 'bang-khen-cua-ubnd-tp-ha-noi', 'về thành tích đạt được trong 10 năm thành lập', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/bang-khen-cua-ubnd-tp-ha-noi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('39', 'Bằng khen của UBND TP. Hà Nội', 'bang-khen-cua-ubnd-tp-ha-noi', 'về thành tích đạt được trong 10 năm thành lập', 'en', null, 'about/accomplish/2010/bang-khen-cua-ubnd-tp-ha-noi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('40', 'Bằng khen của Thủ tướng Chính phủ', 'bang-khen-cua-thu-tuong-chinh-phu', 'cho doanh nghiệp đã có nhiều thành tích trong xây dựng và phát triển thị trường chứng khoán giai đoạn 2000 - 2010', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/bang-khen-cua-thu-tuong-chinh-phu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('40', 'Bằng khen của Thủ tướng Chính phủ', 'bang-khen-cua-thu-tuong-chinh-phu', 'cho doanh nghiệp đã có nhiều thành tích trong xây dựng và phát triển thị trường chứng khoán giai đoạn 2000 - 2010', 'en', null, 'about/accomplish/2010/bang-khen-cua-thu-tuong-chinh-phu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('41', 'Đứng đầu thị phần môi giới', 'dung-dau-thi-phan-moi-gioi', 'tại sở GDCK TP.HCM và Sở GDCK Hà Nội', 'en', null, 'about/accomplish/2010/dung-dau-thi-phan-moi-gioi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('42', 'Giải thưởng M&A', 'giai-thuong-ma', 'Thương vụ Diageo - Halico được bình chọn là Thương vụ M&A tiêu biểu năm 2011 - 2012', 'en', null, 'about/accomplish/2012/giai-thuong-ma', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('11', 'Thành tựu', 'thanh-tuu', 'Trong quá trình hoạt động, MBS đã có những bước phát triển ngoạn mục với những thành tích tiêu biểu như:', 'vi', null, 'gioi-thieu-chung/thanh-tuu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('29', '2010', '2010', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('30', '2012', '2012', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2012', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('42', 'Giải thưởng M&A', 'giai-thuong-ma', 'Thương vụ Diageo - Halico được bình chọn là Thương vụ M&A tiêu biểu năm 2011 - 2012', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2012/giai-thuong-ma', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('31', 'Công ty chứng khoán tiêu biểu', 'cong-ty-chung-khoan-tieu-bieu', 'của HNX năm 2008', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2008/cong-ty-chung-khoan-tieu-bieu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('35', 'Thương hiệu mạnh Việt Nam 2010', 'thuong-hieu-manh-viet-nam-2010', null, 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/thuong-hieu-manh-viet-nam-2010', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('41', 'Đứng đầu thị phần môi giới', 'dung-dau-thi-phan-moi-gioi', 'tại sở GDCK TP.HCM và Sở GDCK Hà Nội', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/dung-dau-thi-phan-moi-gioi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('32', 'Danh hiệu Tin và dùng Việt Nam năm 2009', 'danh-hieu-tin-va-dung-viet-nam-nam-2009', 'do Thời báo Kinh tế Việt Nam bình chọn', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2009/danh-hieu-tin-va-dung-viet-nam-nam-2009', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('5', 'Giới thiệu chung', 'gioi-thieu-chung', null, 'vi', null, 'gioi-thieu-chung', '<div class="img_top_ab right"><img alt="" src="/Assets/images/img/ab-img1.jpg" /> <img alt="" src="/Assets/images/img/ab-img2.jpg" /></div>

<div class="text_top_ab left">
<p>Được thành lập từ tháng 5 năm 2000 bởi Ngân hàng TMCP Quân đội (MB), Công ty CP Chứng khoán MB (MBS) là một trong 5 công ty chứng khoán đầu tiên tại Việt Nam. Sau 14 năm không ngừng phát triển, MBS đã trở thành một trong những công ty chứng khoán hàng đầu Việt Nam. Trong hai năm liên tiếp 2009 và 2010, MBS đã vươn lên dẫn đầu thị phần môi giới tại cả hai sở giao dịch: Sở GDCK Hà Nội (HNX) và Sở GDCK TP. HCM (HOSE) và liên tục đứng trong Top 10 thị phần tại cả hai sở.</p>

<p>Với thế mạnh là công ty thành viên của Tập đoàn MB, MBS liên tục vươn lên đạt các mốc phát triển mới cả về quy mô và nghiệp vụ. Hiện nay, MBS tập trung phát triển hai mảng hoạt động chính là Dịch vụ chứng khoán và Dịch vụ ngân hàng đầu tư được bổ trợ bởi hoạt động nghiên cứu chuyên sâu.</p>

<p>Mạng lưới chi nhánh và các phòng giao dịch của MBS đã được mở rộng và hoạt động có hiệu quả tại nhiều thành phố trọng điểm. MBS đã thu hút và phát triển được một đội ngũ nhân sự năng động trong đó có hàng chục chuyên gia phân tích và hàng trăm chuyên viên quan hệ khách hàng cá nhân và tổ chức được đào tạo bài bản, có tư cách đạo đức tốt, không ngừng phấn đấu để cung cấp dịch vụ và các giải pháp kinh doanh cho khách hàng với sự cẩn trọng, chuyên nghiệp và trách nhiệm cao nhất.</p>
</div>
', 'Về chứng khoán MBS', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('43', 'Lợi thế cạnh tranh', 'loi-the-canh-tranh', empty_clob(), 'vi', null, 'gioi-thieu-chung/loi-the-canh-tranh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('43', 'Competitive Advantage', 'competitive-advantage', empty_clob(), 'en', null, 'about/competitive-advantage', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('44', 'Các mốc sự kiện quan trọng', 'cac-moc-su-kien-quan-trong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('44', 'The important milestones', 'the-important-milestones', null, 'en', null, 'about/the-important-milestones', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('45', 'Nền tảng khách hàng lớn', 'nen-tang-khach-hang-lon', null, 'vi', 'uploads/article/2015/3/05032015_391747_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/nen-tang-khach-hang-lon', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('45', 'Large customer base', 'large-customer-base', null, 'en', 'uploads/article/2015/3/05032015_391747_PM1.jpg', 'about/competitive-advantage/large-customer-base', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('46', 'Nguồn nhân lực được đào tạo bài bản, có kinh nghiệm quốc tế và kỷ luật cao', 'nguon-nhan-luc-duoc-dao-tao-bai-ban-co-kinh-nghiem-quoc-te-va-ky-luat-cao', null, 'vi', 'uploads/article/2015/3/05032015_401747_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/nguon-nhan-luc-duoc-dao-tao-bai-ban-co-kinh-nghiem-quoc-te-va-ky-luat-cao', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('46', 'Human resources are trained , experienced and disciplined international', 'human-resources-are-trained-experienced-and-disciplined-international', null, 'en', 'uploads/article/2015/3/05032015_401747_PM1.jpg', 'about/competitive-advantage/human-resources-are-trained-experienced-and-disciplined-international', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('47', 'Dịch vụ tài chính đa dạng, linh hoạt và an toàn', 'dich-vu-tai-chinh-da-dang-linh-hoat-va-an-toan', null, 'vi', 'uploads/article/2015/3/05032015_411724_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/dich-vu-tai-chinh-da-dang-linh-hoat-va-an-toan', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('47', 'Financial Services diverse , flexible and secure', 'financial-services-diverse-flexible-and-secure', null, 'en', 'uploads/article/2015/3/05032015_411724_PM1.jpg', 'about/competitive-advantage/financial-services-diverse-flexible-and-secure', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('48', 'Công nghệ đồng bộ, hiện dại', 'cong-nghe-dong-bo-hien-dai', null, 'vi', 'uploads/article/2015/3/05032015_421704_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/cong-nghe-dong-bo-hien-dai', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('48', 'Synchronous technology , modern', 'synchronous-technology-modern', null, 'en', 'uploads/article/2015/3/05032015_421704_PM1.jpg', 'about/competitive-advantage/synchronous-technology-modern', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('49', 'Tính chuyên nghiệp và chăm sóc khách hàng tốt', 'tinh-chuyen-nghiep-va-cham-soc-khach-hang-tot', null, 'vi', 'uploads/article/2015/3/05032015_421741_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/tinh-chuyen-nghiep-va-cham-soc-khach-hang-tot', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('49', 'The professionalism and good customer care', 'the-professionalism-and-good-customer-care', null, 'en', 'uploads/article/2015/3/05032015_421741_PM1.jpg', 'about/competitive-advantage/the-professionalism-and-good-customer-care', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('50', 'Chi phí thấp', 'chi-phi-thap', null, 'vi', 'uploads/article/2015/3/05032015_431715_PM1.jpg', 'gioi-thieu-chung/loi-the-canh-tranh/chi-phi-thap', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('50', 'Low cost', 'low-cost', null, 'en', 'uploads/article/2015/3/05032015_431715_PM1.jpg', 'about/competitive-advantage/low-cost', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('53', 'Thành lập bởi Ngân hàng TMCP Quân Đội với vốn điều lệ 9 tỷ đồng', 'thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2000/thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('53', 'Established by the Military Commercial Joint Stock Bank with capital of 9 billion', 'established-by-the-military-commercial-joint-stock-bank-with-capital-of-9-billion', null, 'en', null, 'about/the-important-milestones/2000/established-by-the-military-commercial-joint-stock-bank-with-capital-of-9-billion', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('54', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2010/lorem-ipsum-dolor-sit-amet', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('54', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', null, 'en', null, 'about/the-important-milestones/2010/lorem-ipsum-dolor-sit-amet', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('55', 'Góc truyền thông', 'goc-truyen-thong', null, 'vi', null, 'goc-truyen-thong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('55', 'Communications', 'communications', null, 'en', null, 'communications', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('56', 'Quan hệ cổ đông', 'quan-he-co-dong', null, 'vi', null, 'quan-he-co-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('56', 'Investor relations', 'investor-relations', null, 'en', null, 'investor-relations', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('57', 'Cơ hội nghề nghiệp', 'co-hoi-nghe-nghiep', null, 'vi', null, 'co-hoi-nghe-nghiep', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('57', 'Job opportunities', 'job-opportunities', null, 'en', null, 'job-opportunities', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('58', 'Tin MBS', 'tin-mbs', empty_clob(), 'vi', null, 'goc-truyen-thong/tin-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('58', 'MBS news', 'mbs-news', empty_clob(), 'en', null, 'communications/mbs-news', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('59', 'MBS & Báo chí', 'mbs-bao-chi', empty_clob(), 'vi', null, 'goc-truyen-thong/mbs-bao-chi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('59', 'MBS & Press', 'mbs-press', empty_clob(), 'en', null, 'communications/mbs-press', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('84', 'Hợp nhất với Công ty CP Chứng khoán VIT thành Công ty hợp nhất với tên gọi là Công ty CP Chứng khoán MB (MBS)', 'hop-nhat-voi-cong-ty-cp-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-la-cong-ty-cp-chung-khoan-mb-mbs', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2013/hop-nhat-voi-cong-ty-cp-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-la-cong-ty-cp-chung-khoan-mb-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('62', 'Fusce accumsan mollis eros', 'fusce-accumsan-mollis-eros', null, 'vi', 'uploads/article/2015/3/06032015_041659_PM1.jpg', 'goc-truyen-thong/tin-mbs/fusce-accumsan-mollis-eros', '<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante. Vivamus imperdiet nibh feugiat est.</p>

<p>Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed augue. Integer eu magna sit amet metus fermentum posuere. Morbi sit amet nulla sed dolor elementum imperdiet. Quisque fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque adipiscing eros ut libero. Ut condimentum mi vel tellus. Suspendisse laoreet. Fusce ut est sed dolor gravida convallis. Morbi vitae ante. Vivamus ultrices luctus nunc. Suspendisse et dolor. Etiam dignissim. Proin malesuada adipiscing lacus. Donec metus. Curabitur gravida.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('62', 'Fusce accumsan mollis eros', 'fusce-accumsan-mollis-eros', null, 'en', 'uploads/article/2015/3/06032015_041659_PM1.jpg', 'communications/mbs-news/fusce-accumsan-mollis-eros', '<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante. Vivamus imperdiet nibh feugiat est.</p>

<p>Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed augue. Integer eu magna sit amet metus fermentum posuere. Morbi sit amet nulla sed dolor elementum imperdiet. Quisque fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque adipiscing eros ut libero. Ut condimentum mi vel tellus. Suspendisse laoreet. Fusce ut est sed dolor gravida convallis. Morbi vitae ante. Vivamus ultrices luctus nunc. Suspendisse et dolor. Etiam dignissim. Proin malesuada adipiscing lacus. Donec metus. Curabitur gravida.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('60', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'en', 'uploads/article/2015/3/06032015_041623_PM1.jpg', 'communications/mbs-news/lorem-ipsum-dolor-sit-amet', '<div class="content" id="short-para">
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>

<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</p>

<p>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('87', '2000', '2000', empty_clob(), 'vi', 'uploads/article/2015/3/19032015_370513_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2000', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('84', 'Hợp nhất với Công ty CP Chứng khoán VIT thành Công ty hợp nhất với tên gọi là Công ty CP Chứng khoán MB (MBS)', 'hop-nhat-voi-cong-ty-cp-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-la-cong-ty-cp-chung-khoan-mb-mbs', null, 'en', null, 'about/the-important-milestones/2013/hop-nhat-voi-cong-ty-cp-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-la-cong-ty-cp-chung-khoan-mb-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('88', 'Thành lập bởi Ngân hàng TMCP Quân Đội với vốn điều lệ 9 tỷ đồng', 'thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2000/thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('87', '2000', '2000', empty_clob(), 'en', null, 'about/the-important-milestones/2000', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('88', 'Thành lập bởi Ngân hàng TMCP Quân Đội với vốn điều lệ 9 tỷ đồng', 'thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', null, 'en', null, 'about/the-important-milestones/2000/thanh-lap-boi-ngan-hang-tmcp-quan-doi-voi-von-dieu-le-9-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('89', '2003', '2003', empty_clob(), 'vi', 'uploads/article/2015/3/19032015_400556_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2003', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('89', '2003', '2003', empty_clob(), 'en', 'uploads/article/2015/3/19032015_400556_AM1.jpg', 'about/the-important-milestones/2003', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('90', 'Tăng vốn điều lệ lên 43 tỷ đồng', 'tang-von-dieu-le-len-43-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2003/tang-von-dieu-le-len-43-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('90', 'Tăng vốn điều lệ lên 43 tỷ đồng', 'tang-von-dieu-le-len-43-ty-dong', null, 'en', null, 'about/the-important-milestones/2003/tang-von-dieu-le-len-43-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('124', 'Tư vấn phát hành cổ phiếu (ECM)', 'tu-van-phat-hanh-co-phieu-ecm', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_1.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-co-phieu-ecm', '<p>MBS tư vấn cho khách hàng các phương án, thủ tục và yêu cầu pháp lý trong việc phát hành các sản phẩm chứng khoán có tính chất cổ phiếu như cổ phiếu phổ thông, cổ phiếu ưu đãi và trái phiếu chuyển đổi. Là một trong số ít các công ty chứng khoán có thị phần môi giới cổ phiếu lớn tại Việt Nam, MBS đã xây dựng được một mạng lưới khách hàng tổ chức và khách hàng cá nhân đa dạng, tạo nên lợi thế để MBS giúp khách hàng huy động vốn thành công thông qua chào bán riêng lẻ hoặc chào bán ra công chúng. MBS cũng cung cấp dịch vụ bảo lãnh phát hành để đảm bảo đợt phát hành của khách hàng diễn ra thành công với một mức giá được hai bên thỏa thuận trước.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('126', 'Tư vấn M&A', 'tu-van-ma', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_3.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-ma', '<p>Dịch vụ tư vấn M&amp;A của MBS giúp khách hàng (bên mua hoặc bên bán hoặc cả hai) trong việc tìm kiếm đối tác phù hợp nhất, kết nối bên mua và bên bán trên tinh thần đôi bên cùng có lợi. MBS cung cấp các dịch vụ trọn gói cho bên bán hoặc bên mua từ các khâu lên kế hoạch cấu trúc giao dịch, xác định bên mua hoặc bên bán mục tiêu, thẩm định đặc biệt (DD), định giá, thương lượng mua bán, và hỗ trợ các vấn đề thủ tục pháp lý cần thiết để đảm bảo giao dịch thành công.</p>

<p>Danh mục khách hàng sử dụng dịch vụ tư vấn tài chính doanh nghiệp của MBS không ngừng được mở rộng. Trên cơ sở đó, MBS đã xây dựng và liên tục cập nhật được một cơ sở dữ liệu phong phú về các cơ hội đầu tư trên nhiều lĩnh vực khác nhau tại các tỉnh thành lớn của Việt Nam, từ chế biến thực phẩm, chăm sóc sức khỏe, giáo dục, bất động sản, dịch vụ tài chính, khu công nghiệp, sản xuất công nghiệp, năng lượng, trồng và chế biến mủ cao su đến xây dựng và khai thác cảng biển v.v… Kinh nghiệm làm việc với các nhà đầu tư đến từ các quốc gia khác nhau và sự hiểu biết sâu sắc của đội ngũ nhân sự của MBS về các yêu cầu của nhà đầu tư sẽ giúp cho quá trình mua bán hay sáp nhập của khách hàng trở nên dễ dàng hơn rất nhiều.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('136', 'Bảo lãnh phát hành cổ phiếu', 'bao-lanh-phat-hanh-co-phieu', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-co-phieu-ecm/bao-lanh-phat-hanh-co-phieu', '<p>Tùy theo yêu cầu của khách hàng, MBS có thể cung cấp dịch vụ bảo lãnh phát hành để đảm bảo đợt phát hành của khách hàng diễn ra thành công với một mức giá thỏa thuận tối thiểu.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('137', 'Phân phối các sản phẩm cổ phiếu', 'phan-phoi-cac-san-pham-co-phieu', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-co-phieu-ecm/phan-phoi-cac-san-pham-co-phieu', '<p>MBS có một mạng lưới khách hàng tổ chức và khách hàng cá nhân đa dạng, tạo nên lợi thế để MBS hỗ trợ khách hàng huy động vốn thành công thông qua chào bán riêng lẻ hoặc chào bán ra công chúng. MBS có thể giúp khách hàng dựng sổ (book building) nhu cầu đăng ký nhằm lượng hóa khả năng hấp thụ của thị trường cũng như mức giá có thể phát hành thành công.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('137', 'Equity distribution', 'equity-distribution', empty_clob(), 'en', null, 'institutional-clients/investment-banking/equity-capital-market-ecm/equity-distribution', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('61', 'Morbi in sem quis dui placerat ornare', 'morbi-in-sem-quis-dui-placerat-ornare', null, 'vi', 'uploads/article/2015/3/06032015_041640_PM1.jpg', 'goc-truyen-thong/tin-mbs/morbi-in-sem-quis-dui-placerat-ornare', '<p>Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus. Sed vel lacus. Mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. Suspendisse ac urna. Etiam pellentesque mauris ut lectus. Nunc tellus ante, mattis eget, gravida vitae, ultricies ac, leo. Integer leo pede, ornare a, lacinia eu, vulputate vel, nisl.</p>

<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante. Vivamus imperdiet nibh feugiat est.</p>

<p>Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed augue. Integer eu magna sit amet metus fermentum posuere. Morbi sit amet nulla sed dolor elementum imperdiet. Quisque fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque adipiscing eros ut libero. Ut condimentum mi vel tellus. Suspendisse laoreet. Fusce ut est sed dolor gravida convallis. Morbi vitae ante. Vivamus ultrices luctus nunc. Suspendisse et dolor. Etiam dignissim. Proin malesuada adipiscing lacus. Donec metus. Curabitur gravida.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('61', 'Morbi in sem quis dui placerat ornare', 'morbi-in-sem-quis-dui-placerat-ornare', null, 'en', 'uploads/article/2015/3/06032015_041640_PM1.jpg', 'communications/mbs-news/morbi-in-sem-quis-dui-placerat-ornare', '<p>Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus. Sed vel lacus. Mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. Suspendisse ac urna. Etiam pellentesque mauris ut lectus. Nunc tellus ante, mattis eget, gravida vitae, ultricies ac, leo. Integer leo pede, ornare a, lacinia eu, vulputate vel, nisl.</p>

<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante. Vivamus imperdiet nibh feugiat est.</p>

<p>Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed augue. Integer eu magna sit amet metus fermentum posuere. Morbi sit amet nulla sed dolor elementum imperdiet. Quisque fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque adipiscing eros ut libero. Ut condimentum mi vel tellus. Suspendisse laoreet. Fusce ut est sed dolor gravida convallis. Morbi vitae ante. Vivamus ultrices luctus nunc. Suspendisse et dolor. Etiam dignissim. Proin malesuada adipiscing lacus. Donec metus. Curabitur gravida.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('63', 'Tin cổ đông', 'tin-co-dong', empty_clob(), 'vi', null, 'quan-he-co-dong/tin-co-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('63', 'Shareholder news', 'shareholder-news', empty_clob(), 'en', null, 'investor-relations/shareholder-news', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('64', 'Báo cáo tài chính', 'bao-cao-tai-chinh', empty_clob(), 'vi', null, 'quan-he-co-dong/bao-cao-tai-chinh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('93', 'Tăng vốn điều lệ lên 120 tỷ đồng', 'tang-von-dieu-le-len-120-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2006/tang-von-dieu-le-len-120-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('93', 'Tăng vốn điều lệ lên 120 tỷ đồng', 'tang-von-dieu-le-len-120-ty-dong', null, 'en', null, 'about/the-important-milestones/2006/tang-von-dieu-le-len-120-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('98', 'Tăng vốn điều lệ lên 120 tỷ đồng', 'tang-von-dieu-le-len-120-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2008/tang-von-dieu-le-len-120-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('98', 'Tăng vốn điều lệ lên 120 tỷ đồng', 'tang-von-dieu-le-len-120-ty-dong', null, 'en', null, 'about/the-important-milestones/2008/tang-von-dieu-le-len-120-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('99', '2009', '2009', null, 'vi', 'uploads/article/2015/3/19032015_500510_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2009', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('99', '2009', '2009', null, 'en', 'uploads/article/2015/3/19032015_500510_AM1.jpg', 'about/the-important-milestones/2009', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('107', 'Đổi tên thành Công ty CP Chứng khoán MB (MBS)', 'doi-ten-thanh-cong-ty-cp-chung-khoan-mb-mbs', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2012/doi-ten-thanh-cong-ty-cp-chung-khoan-mb-mbs', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('107', 'Đổi tên thành Công ty CP Chứng khoán MB (MBS)', 'doi-ten-thanh-cong-ty-cp-chung-khoan-mb-mbs', null, 'en', null, 'about/the-important-milestones/2012/doi-ten-thanh-cong-ty-cp-chung-khoan-mb-mbs', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('108', 'Thay đổi logo và hệ thống nhận diện mới', 'thay-doi-logo-va-he-thong-nhan-dien-moi', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2012/thay-doi-logo-va-he-thong-nhan-dien-moi', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('108', 'Thay đổi logo và hệ thống nhận diện mới', 'thay-doi-logo-va-he-thong-nhan-dien-moi', null, 'en', null, 'about/the-important-milestones/2012/thay-doi-logo-va-he-thong-nhan-dien-moi', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('109', '2013', '2013', null, 'vi', 'uploads/article/2015/3/19032015_570551_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2013', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('109', '2013', '2013', null, 'en', 'uploads/article/2015/3/19032015_570551_AM1.jpg', 'about/the-important-milestones/2013', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('110', 'Hợp nhất với Công ty Chứng khoán VIT thành Công ty hợp nhất với tên gọi Công ty CP Chứng khoán MB (MBS)', 'hop-nhat-voi-cong-ty-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-cong-ty-cp-chung-khoan-mb-mbs', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2013/hop-nhat-voi-cong-ty-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-cong-ty-cp-chung-khoan-mb-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('110', 'Hợp nhất với Công ty Chứng khoán VIT thành Công ty hợp nhất với tên gọi Công ty CP Chứng khoán MB (MBS)', 'hop-nhat-voi-cong-ty-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-cong-ty-cp-chung-khoan-mb-mbs', null, 'en', null, 'about/the-important-milestones/2013/hop-nhat-voi-cong-ty-chung-khoan-vit-thanh-cong-ty-hop-nhat-voi-ten-goi-cong-ty-cp-chung-khoan-mb-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('111', 'Tăng vốn điều lệ lên 1.221 tỷ đồng', 'tang-von-dieu-le-len-1221-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2013/tang-von-dieu-le-len-1221-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('111', 'Tăng vốn điều lệ lên 1.221 tỷ đồng', 'tang-von-dieu-le-len-1221-ty-dong', null, 'en', null, 'about/the-important-milestones/2013/tang-von-dieu-le-len-1221-ty-dong', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('100', 'Tăng vốn điều lệ lên 800 tỷ đồng', 'tang-von-dieu-le-len-800-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2009/tang-von-dieu-le-len-800-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('100', 'Tăng vốn điều lệ lên 800 tỷ đồng', 'tang-von-dieu-le-len-800-ty-dong', null, 'en', null, 'about/the-important-milestones/2009/tang-von-dieu-le-len-800-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('101', 'Khai trương Chi Nhánh Hải Phòng', 'khai-truong-chi-nhanh-hai-phong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2009/khai-truong-chi-nhanh-hai-phong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('101', 'Khai trương Chi Nhánh Hải Phòng', 'khai-truong-chi-nhanh-hai-phong', null, 'en', null, 'about/the-important-milestones/2009/khai-truong-chi-nhanh-hai-phong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('102', 'Trở thành công ty chứng khoán có thị phần môi giới số 1 tại cả hai sở giao dịch HNX và HSX', 'tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2009/tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('102', 'Trở thành công ty chứng khoán có thị phần môi giới số 1 tại cả hai sở giao dịch HNX và HSX', 'tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, 'en', null, 'about/the-important-milestones/2009/tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('103', '2010', '2010', null, 'vi', 'uploads/article/2015/3/19032015_530535_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2010', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('103', '2010', '2010', null, 'en', 'uploads/article/2015/3/19032015_530535_AM1.jpg', 'about/the-important-milestones/2010', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('104', 'Tăng vốn điều lệ lên 1.200 tỷ đồng', 'tang-von-dieu-le-len-1200-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2010/tang-von-dieu-le-len-1200-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('104', 'Tăng vốn điều lệ lên 1.200 tỷ đồng', 'tang-von-dieu-le-len-1200-ty-dong', null, 'en', null, 'about/the-important-milestones/2010/tang-von-dieu-le-len-1200-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('105', 'Trở thành công ty chứng khoán có thị phần môi giới số 1 tại cả hai sở giao dịch HNX và HSX', 'tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2010/tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('105', 'Trở thành công ty chứng khoán có thị phần môi giới số 1 tại cả hai sở giao dịch HNX và HSX', 'tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, 'en', null, 'about/the-important-milestones/2010/tro-thanh-cong-ty-chung-khoan-co-thi-phan-moi-gioi-so-1-tai-ca-hai-so-giao-dich-hnx-va-hsx', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('106', '2012', '2012', null, 'vi', 'uploads/article/2015/3/19032015_550550_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2012', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('106', '2012', '2012', null, 'en', 'uploads/article/2015/3/19032015_550550_AM1.jpg', 'about/the-important-milestones/2012', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('94', '2007', '2007', null, 'vi', 'uploads/article/2015/3/19032015_440556_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2007', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('94', '2007', '2007', null, 'en', 'uploads/article/2015/3/19032015_440556_AM1.jpg', 'about/the-important-milestones/2007', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('95', 'Cổ phần hoá', 'co-phan-hoa', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2007/co-phan-hoa', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('95', 'Cổ phần hoá', 'co-phan-hoa', null, 'en', null, 'about/the-important-milestones/2007/co-phan-hoa', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('96', 'Tăng vốn điều lệ lên 300 tỷ đồng', 'tang-von-dieu-le-len-300-ty-dong', null, 'vi', null, 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2007/tang-von-dieu-le-len-300-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('96', 'Tăng vốn điều lệ lên 300 tỷ đồng', 'tang-von-dieu-le-len-300-ty-dong', null, 'en', null, 'about/the-important-milestones/2007/tang-von-dieu-le-len-300-ty-dong', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('97', '2008', '2008', null, 'vi', 'uploads/article/2015/3/19032015_460553_AM1.jpg', 'gioi-thieu-chung/cac-moc-su-kien-quan-trong/2008', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('97', '2008', '2008', null, 'en', 'uploads/article/2015/3/19032015_460553_AM1.jpg', 'about/the-important-milestones/2008', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('37', '“Doanh nghiệp tiêu biểu”', 'doanh-nghiep-tieu-bieu', 'trong chương trình "Trí tuệ Thăng Long - Hà Nội"', 'vi', null, 'gioi-thieu-chung/thanh-tuu/2010/doanh-nghiep-tieu-bieu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('112', 'Khách hàng tổ chức', 'khach-hang-to-chuc', null, 'vi', null, 'khach-hang-to-chuc', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('112', 'Institutional clients', 'institutional-clients', null, 'en', null, 'institutional-clients', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('113', 'Dịch vụ chứng khoán', 'dich-vu-chung-khoan', empty_clob(), 'vi', null, 'khach-hang-ca-nhan/dich-vu-chung-khoan', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('113', 'Securities services', 'securities-services', empty_clob(), 'en', null, 'individual-customer/securities-services', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('114', 'Dịch vụ chứng khoán', 'dich-vu-chung-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/1.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan', '<p>MBS cung cấp đa dạng các dịch vụ ngân hàng đầu tư cho khách hàng và tổ chức; đặc biệt dẫn đầu thị trường trong tư vấn M &amp; A.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('114', 'Securities services', 'securities-services', empty_clob(), 'en', '/uploads/images/dich-vu/1.jpg', 'institutional-clients/securities-services', '<p>Content is being updating...</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('115', 'Dịch vụ ngân hàng đầu tư', 'dich-vu-ngan-hang-dau-tu', empty_clob(), 'vi', '/uploads/images/dich-vu/2.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('115', 'Investment banking', 'investment-banking', empty_clob(), 'en', '/uploads/images/dich-vu/2.jpg', 'institutional-clients/investment-banking', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('116', 'Open Account', 'open-account', empty_clob(), 'en', null, 'institutional-clients/securities-services/open-account', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('117', 'Brokerage Service', 'brokerage-service', empty_clob(), 'en', null, 'institutional-clients/securities-services/brokerage-service', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('118', 'Custody service', 'custody-service', empty_clob(), 'en', null, 'institutional-clients/securities-services/custody-service', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('119', 'Financing products', 'financing-products', empty_clob(), 'en', null, 'institutional-clients/securities-services/financing-products', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('120', 'Dịch vụ điện tử', 'dich-vu-dien-tu', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_5.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/dich-vu-dien-tu', '<p>Với nền tảng tài chính vững chắc và được sự hỗ trợ mạnh mẽ từ Ngân hàng mẹ MB, MBS luôn đáp ứng tốt nhất nhu cầu kinh doanh của đối tác.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('120', 'Online services', 'online-services', empty_clob(), 'en', null, 'institutional-clients/securities-services/online-services', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('121', 'Investment Advisory', 'investment-advisory', empty_clob(), 'en', null, 'institutional-clients/securities-services/investment-advisory', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('122', 'Custodian services', 'custodian-services', empty_clob(), 'en', null, 'institutional-clients/securities-services/custodian-services', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('123', 'Biểu phí', 'bieu-phi', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_8.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/bieu-phi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('123', 'Services Fees', 'services-fees', empty_clob(), 'en', null, 'institutional-clients/securities-services/services-fees', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('124', 'Equity Capital Market (ECM)', 'equity-capital-market-ecm', empty_clob(), 'en', null, 'institutional-clients/investment-banking/equity-capital-market-ecm', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('125', 'Tư vấn phát hành trái phiếu (DCM)', 'tu-van-phat-hanh-trai-phieu-dcm', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_2.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-trai-phieu-dcm', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('125', 'Dept Capital Market', 'dept-capital-market', empty_clob(), 'en', null, 'institutional-clients/investment-banking/dept-capital-market', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('126', 'M&A advisory', 'ma-advisory', empty_clob(), 'en', null, 'institutional-clients/investment-banking/ma-advisory', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('127', 'Tư vấn tài chính doanh nghiệp', 'tu-van-tai-chinh-doanh-nghiep', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_4.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-tai-chinh-doanh-nghiep', '<p>MBS tư vấn cho khách hàng các phương án, thủ tục và yêu cầu pháp lý trong việc phát hành các công cụ nợ như trái phiếu doanh nghiệp.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('127', 'Financial advisory', 'financial-advisory', empty_clob(), 'en', null, 'institutional-clients/investment-banking/financial-advisory', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('128', 'Open Account', 'open-account', empty_clob(), 'en', null, 'individual-customer/securities-services/open-account', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('129', 'Brokerage Service', 'brokerage-service', empty_clob(), 'en', null, 'individual-customer/securities-services/brokerage-service', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('130', 'Custody service', 'custody-service', empty_clob(), 'en', null, 'individual-customer/securities-services/custody-service', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('131', 'Financing products', 'financing-products', empty_clob(), 'en', null, 'individual-customer/securities-services/financing-products', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('132', 'Dịch vụ điện tử', 'dich-vu-dien-tu', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_5.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/dich-vu-dien-tu', '<p>Với nền tảng tài chính vững chắc và được sự hỗ trợ mạnh mẽ từ Ngân hàng mẹ MB, MBS luôn đáp ứng tốt nhất nhu cầu kinh doanh của đối tác.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('132', 'Online services', 'online-services', empty_clob(), 'en', null, 'individual-customer/securities-services/online-services', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('133', 'Tư vấn đầu tư', 'tu-van-dau-tu', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_6.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/tu-van-dau-tu', '<div class="fs14 c_0066b3 mt20"><strong>Tư vấn đầu tư là một trong những thế mạnh cạnh tranh của MBS. </strong></div>

<div class="wrap clearfix">
<div class="it-pro fl">Đội ngũ trên 200 chuyên viên quan hệ khách hàng được đào tạo bài bản.</div>

<div class="it-pro fr">Đội ngũ chuyên viên quan hệ khách hàng luôn bám sát thị trường, thường xuyên đưa ra các nhận định và tư vấn cho khách hàng ngay trong phiên giao dịch.</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Được hỗ trợ bởi Trung tâm Nghiên cứu MBS với các sản phẩm nghiên cứu chuyên nghiệp, độc lập và đa dạng</div>
</div>

<div class="fs14 c_555 mt15">Đến với MBS, Quý khách sẽ được cung cấp dịch vụ tư vấn đầu tư hiệu quả, trên cơ sở trung thực, tin cậy và kịp thời.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('133', 'Investment Advisory', 'investment-advisory', empty_clob(), 'en', null, 'individual-customer/securities-services/investment-advisory', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('134', 'Ủy thác quản lý tài khoản', 'uy-thac-quan-ly-tai-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_7.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/uy-thac-quan-ly-tai-khoan', '<div class="fs14 c_555 mt15">Ủy thác quản lý tài khoản là một dịch vụ được MBS chú trọng phát triển nhằm đem lại những lợi ích tối đa cho khách hàng:</div>

<div class="wrap clearfix">
<div class="it-pro fl">Giúp các khách hàng không có thời gian bám sát thị trường mà vẫn có thể đầu tư hiệu quả vào thị trường chứng khoán Việt Nam.</div>

<div class="it-pro fr">Được làm việc với những chuyên gia tư vấn đầu tư được đào tạo bài bản và chuyên sâu, có nhiều năm kinh nghiệm thực tế trên thị trường,</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Đem đến cơ hội cho những nhà đầu tư chưa có nhiều kinh nghiệm.</div>
</div>

<div class="fs14 c_555 mt15">Sử dụng dịch vụ ủy thác quản lý tài khoản của MBS đem đến những cơ hội đầu tư hiệu quả, là lựa chọn tốt nhất đối với những nhà đầu tư.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('134', 'Custodian services', 'custodian-services', empty_clob(), 'en', null, 'individual-customer/securities-services/custodian-services', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('135', 'Biểu phí', 'bieu-phi', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_8.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/bieu-phi', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem libero, sodales id efficitur cursus, efficitur at tortor. Nam volutpat, ipsum et gravida placerat.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('135', 'Services Fees', 'services-fees', empty_clob(), 'en', '/uploads/images/dich-vu/img_dvck_8.jpg', 'individual-customer/securities-services/services-fees', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('117', 'Môi giới chứng khoán', 'moi-gioi-chung-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_2.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/moi-gioi-chung-khoan', '<div class="fs14 c_555 mt15">MBS cung cấp dịch vụ tư vấn cho Quý khách mua, bán các loại chứng khoán đã niêm yết và chưa niêm yết. MBS luôn chú trọng việc thu hút nhân tài, đầu tư vào công nghệ hiện đại và cung cấp cho khách hàng những sản phẩm phân tích và tư vấn đầu tư với chất lượng tốt nhất trên thị trường.</div>

<div class="box-mbs-ck clearfix mt25">
<div class="img fl"><strong>MBS cam kết</strong></div>

<div class="text fl">
<ul>
	<li>Cung cấp dịch vụ với chất lượng tốt nhất;</li>
	<li>Đi đầu về công nghệ;</li>
	<li>An toàn bảo mật;</li>
	<li>Sản phẩm dịch vụ đa dạng, thỏa mãn mọi nhu cầu của khách hàng.</li>
</ul>
</div>
</div>

<div class="fs14 c_0066b3 mt20"><strong>Hệ thống giao dịch môi giới</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Tốc độ nhanh, chính xác, mức phí môi giới hợp lý;</div>

<div class="it-pro fr">Cập nhật thông tin thị trường liên tục đầy đủ</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện, bảo mật tuyệt đối</div>

<div class="it-pro fr">Trao đổi thông tin và chia sẻ kiến thức với nhà đầu tư thông qua các buổi hội thảo, tọa đàm</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Tra cứu thông tin tài khoản về các giao dịch tiền và chứng khoán qua nhiều hình thức: Trực tiếp tại sàn, qua điện thoại, internet</div>

<div class="it-pro fr">Nhiều hình thức ưu đãi hấp dẫn</div>
</div>

<div class="fs14 c_555 mt15">Lựa chọn MBS là nhà môi giới chứng khoán tại thị trường chứng khoán Việt Nam là lựa chọn tốt nhất cho những nhà đầu tư chuyên nghiệp.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('116', 'Mở tài khoản', 'mo-tai-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_1.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/mo-tai-khoan', '<div class="fs14 c_555 mt15">MBS giúp bạn mở tài khoản giao dịch chứng khoán nhanh chóng, chính xác và thuận tiện nhất. Chúng tôi có thể đáp ứng các nhu cầu sau đây của quý khách</div>

<div class="fs14 c_0066b3 mt20"><strong>Mở tài khoản mới tại MBS</strong></div>

<div class="it-pro mt20">Quý khách có nhu cầu mở tài khoản giao dịch chứng khoán tại MBS chỉ cần mang chứng minh thư nhân dân bản gốc đến các chi nhánh/ phòng giao dịch của MBS trên toàn quốc để làm thủ tục mở tài khoản giao dịch chứng khoán (Danh sách các CN/PGD MBS <a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">tại đây</a>)</div>

<div class="it-pro mt10">Quý khách có thể đăng ký mở tài khoản Online thông qua website:<br />
<a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">http://open24.mbs.com.vn/Presentation/Welcome.aspx</a></div>

<div class="fs14 c_0066b3 mt20"><strong>Chuyển giao dịch về MBS</strong></div>

<div class="it-pro mt20">Theo quy định của Ủy ban Chứng khoán Nhà nước, một nhà đầu tư có thể có nhiều tài khoản giao dịch tại các công ty chứng khoán khác nhau. Do đó, Quý khách có thể mở thêm tài khoản giao dịch chứng khoán tại MBS đồng thời chuyển tiền và số dư chứng khoán sang tài khoản tại MBS để thực hiện giao dịch và sử dụng sản phẩm dịch vụ tốt nhất. (Chi tiết vui lòng tham khảo <a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">tại đây</a>).</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('147', 'Hỗ trợ trong việc thực hiện thẩm định đặc biệt (DD)', 'ho-tro-trong-viec-thuc-hien-tham-dinh-dac-biet-dd', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-ma/ho-tro-trong-viec-thuc-hien-tham-dinh-dac-biet-dd', '<p>Việc thực hiện các quy trình thẩm định, rà soát (về tài chính, về thuế, về thương mại và pháp lý) thường gặp không ít khó khăn và có thể ảnh hưởng tới thành công của thương vụ. Thực hiện rà soát kiểm toán FCPA (Foreign Corrupt Practices Act) mà bên mua là các công ty đã niêm yết tại Thị trường chứng khoán New York hay Thị trường chứng khoán Luân Đôn thậm chí còn khó khăn hơn rất nhiều. Sự hiểu biết sâu sắc về văn hóa của bên bán, khả năng lập kế hoạch hợp lý cho việc rà soát sẽ tạo điều kiện thuận lợi hơn rất nhiều cho toàn bộ quá trình và giúp hai bên dễ dàng hợp tác hơn. Kinh nghiệm thực tế của MBS trong nhiều thương vụ gần đây sẽ đem lại giá trị thực sự cho các khách hàng.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('147', 'Assistance in conducting due diligence', 'assistance-in-conducting-due-diligence', empty_clob(), 'en', null, 'institutional-clients/investment-banking/ma-advisory/assistance-in-conducting-due-diligence', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('148', 'Tư vấn về các thủ tục pháp lý tại Việt Nam', 'tu-van-ve-cac-thu-tuc-phap-ly-tai-viet-nam', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-ma/tu-van-ve-cac-thu-tuc-phap-ly-tai-viet-nam', '<p>Việc đầu tư vào công ty đại chúng Việt Nam hay các công ty cổ phần chưa đại chúng, công ty trách nhiệm hữu hạn, công ty mà Nhà nước nắm giữ từ 50% trở lên sẽ phải đáp ứng rất nhiều các yêu cầu tuân thủ pháp lý khác nhau như yêu cầu về báo cáo, thuế, đăng ký, tài khoản ngân hàng, ngoại hối, và các yêu cầu pháp lý đặc thù khác. Đội ngũ tư vấn của chúng tôi, với hiểu biết vững chắc về hệ thống luật pháp Việt Nam và kinh nghiệm thực tế phong phú, sẽ luôn luôn sẵn sàng hỗ trợ nhà đầu tư trong việc hoạch định kế hoạch và thực hiện thương vụ theo đúng các quy định pháp lý của Việt Nam.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('148', 'Advice on Vietnamese legal requirements', 'advice-on-vietnamese-legal-requirements', empty_clob(), 'en', null, 'institutional-clients/investment-banking/ma-advisory/advice-on-vietnamese-legal-requirements', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('149', 'Giới thiệu và hỗ trợ đánh giá các cơ hội đầu tư', 'gioi-thieu-va-ho-tro-danh-gia-cac-co-hoi-dau-tu', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-ma/gioi-thieu-va-ho-tro-danh-gia-cac-co-hoi-dau-tu', '<p>Thách thức chính đối với nhà đầu tư nước ngoài là tìm kiếm các cơ hội đầu tư phù hợp. Quy mô sản xuất kinh doanh là một trong những nguyên nhân gây ra thách thức đó. Trong khu vực kinh tế tư nhân, rất nhiều doanh nghiệp mà các nhà đầu tư quan tâm có quy mô quá nhỏ hoặc không có nhu cầu cấp thiết về vốn, một số doanh nghiệp khác thì vẫn còn cần phải được cải tiến rất nhiều về quản trị doanh nghiệp nói chung hoặc hệ thống kiểm soát nội bộ nói riêng. Ngoài ra, với tư cách là người mua, nhà đầu tư còn có thể gặp phải các vấn đề khó khăn về cơ cấu của doanh nghiệp. Cụ thể, nhiều công ty Việt Nam đồng thời tiến hành các hoạt động kinh doanh trong các lĩnh vực rất khác nhau, trong khi đó nhà đầu tư lại chỉ muốn đầu tư vào một lĩnh vực. Trong ngành bất động sản, ngoài một số dự án tốt (nhưng phần lớn lại không có nhu cầu tài trợ vốn) thì các dự án khác vẫn chưa hoàn tất được các thủ tục pháp lý liên quan, chưa xin được quyền sử dụng đất dài hạn…</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('149', 'Introduction and assistance in assessment of opportunities', 'introduction-and-assistance-in-assessment-of-opportunities', empty_clob(), 'en', null, 'institutional-clients/investment-banking/ma-advisory/introduction-and-assistance-in-assessment-of-opportunities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('152', 'Công ty CP Xây dựng Hà tầng Sông Đà (SDH)', 'cong-ty-cp-xay-dung-ha-tang-song-da-sdh', 'Tư vấn phát hành cổ phiếu<br/>Vốn huy động: 250 tỷ VNĐ<br/>Năm thục hiện 2009', 'vi', '/uploads/images/thuong-vu/img-tv8.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-xay-dung-ha-tang-song-da-sdh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('152', 'Công ty CP Xây dựng Hà tầng Sông Đà (SDH)', 'cong-ty-cp-xay-dung-ha-tang-song-da-sdh', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-xay-dung-ha-tang-song-da-sdh', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('153', 'Công ty CP Tập đoàn Tân Mai', 'cong-ty-cp-tap-doan-tan-mai', 'Tư vấn và BLPH trái phiếu<br/>Vốn thu xếp: 300 tỷ VNĐ<br/>Năm thục hiện 2009', 'vi', '/uploads/images/thuong-vu/img-tv7.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-tap-doan-tan-mai', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('153', 'Công ty CP Tập đoàn Tân Mai', 'cong-ty-cp-tap-doan-tan-mai', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-tap-doan-tan-mai', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('154', 'Công ty CP Đầu tư Tổng hợp Hà Nội (SHN)', 'cong-ty-cp-dau-tu-tong-hop-ha-noi-shn', 'Tư vấn phát hành cổ phiếu <br/>Vốn huy động: 238 tỷ VNĐ<br/>Năm thục hiện: 2010', 'vi', '/uploads/images/thuong-vu/img-tv6.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-dau-tu-tong-hop-ha-noi-shn', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('154', 'Công ty CP Đầu tư Tổng hợp Hà Nội (SHN)', 'cong-ty-cp-dau-tu-tong-hop-ha-noi-shn', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-dau-tu-tong-hop-ha-noi-shn', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('155', 'Công ty CP Ống thép Việt Đức (VGS)', 'cong-ty-cp-ong-thep-viet-duc-vgs', 'Tư vấn phát hành cổ phiếu<br/>Vốn huy động: 250 tỷ VNĐ<br/>Năm thục hiện 2009', 'vi', '/uploads/images/thuong-vu/img-tv5.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-ong-thep-viet-duc-vgs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('155', 'Công ty CP Ống thép Việt Đức (VGS)', 'cong-ty-cp-ong-thep-viet-duc-vgs', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-ong-thep-viet-duc-vgs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('156', 'Cty CP Tài Chính và phát triển doanh nghiệp FBS', 'cty-cp-tai-chinh-va-phat-trien-doanh-nghiep-fbs', 'Tư vấn và BLPH trái phiếu<br/>Vốn thu xếp: 300 tỷ VNĐ<br/>Năm thục hiện 2009', 'vi', '/uploads/images/thuong-vu/img-tv4.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cty-cp-tai-chinh-va-phat-trien-doanh-nghiep-fbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('156', 'Cty CP Tài Chính và phát triển doanh nghiệp FBS', 'cty-cp-tai-chinh-va-phat-trien-doanh-nghiep-fbs', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cty-cp-tai-chinh-va-phat-trien-doanh-nghiep-fbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('157', 'Công ty CP Cơ điện và Xây dựng Việt Nam (MCG)', 'cong-ty-cp-co-dien-va-xay-dung-viet-nam-mcg', empty_clob(), 'vi', '/uploads/images/thuong-vu/img-tv3.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-co-dien-va-xay-dung-viet-nam-mcg', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('157', 'Công ty CP Cơ điện và Xây dựng Việt Nam (MCG)', 'cong-ty-cp-co-dien-va-xay-dung-viet-nam-mcg', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-co-dien-va-xay-dung-viet-nam-mcg', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('119', 'Hỗ trợ tài chính', 'ho-tro-tai-chinh', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_4.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/ho-tro-tai-chinh', '<div class="fs14 c_555 mt15">Với nền tảng tài chính vững chắc và được sự hỗ trợ mạnh mẽ từ Ngân hàng mẹ MB, MBS luôn đáp ứng tốt nhất nhu cầu kinh doanh của đối tác và khách hàng bằng các dịch vụ hỗ trợ tài chính đa dạng. MBS đã xây dựng được một mạng lưới quan hệ với các đối tác cung cấp vốn để hỗ trợ tài chính cho các khách hàng có nhu cầu đầu tư chứng khoán.</div>

<div class="fs14 c_0066b3 mt20"><strong>Dịch vụ hỗ trợ tài chính của MBS bao gồm:</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Ứng trước tiền bán</div>

<div class="it-pro fr">Cho vay thanh toán tiền mua chứng khoán đã khớp lệnh và/hoặc các mục đích tiêu dùng (Margin+)</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Sức mua ứng trước</div>

<div class="it-pro fr">Và một số dịch vụ hỗ trợ tài chính khác.</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Giao dịch ký quỹ (Margin)</div>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('118', 'Lưu ký chứng khoán', 'luu-ky-chung-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_3.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/luu-ky-chung-khoan', '<div class="fs14 c_555 mt15">MBS phối hợp với Trung tâm Lưu ký Chứng khoán Việt Nam (VSD) cung cấp dịch vụ lưu giữ, bảo quản chứng khoán của khách hàng, làm thủ tục chuyển chứng khoán vào tài khoản giao dịch chứng khoán, giúp khách hàng dễ dàng thực hiện giao dịch và nhận đầy đủ mọi quyền lợi liên quan của chứng khoán.</div>

<div class="fs14 c_0066b3 mt20"><strong>Tiện ích</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện;</div>

<div class="it-pro fr">Tránh rủi ro cho khách hàng khi tự lưu giữ chứng khoán</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Đội ngũ nhân viên chuyên nghiệp, đảm bảo thời gian hồ sơ được chấp thuận nhanh nhất;</div>

<div class="it-pro fr">Giúp khách hàng nhanh chóng nhận được mọi quyền lợi về chứng khoán</div>
</div>

<div class="fs14 c_0066b3 mt20"><strong>Đối tượng</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện;</div>

<div class="it-pro fr">Tránh rủi ro cho khách hàng khi tự lưu giữ chứng khoán</div>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('121', 'Tư vấn đầu tư', 'tu-van-dau-tu', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_6.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/tu-van-dau-tu', '<div class="fs14 c_0066b3 mt20"><strong>Tư vấn đầu tư là một trong những thế mạnh cạnh tranh của MBS. </strong></div>

<div class="wrap clearfix">
<div class="it-pro fl">Đội ngũ trên 200 chuyên viên quan hệ khách hàng được đào tạo bài bản.</div>

<div class="it-pro fr">Đội ngũ chuyên viên quan hệ khách hàng luôn bám sát thị trường, thường xuyên đưa ra các nhận định và tư vấn cho khách hàng ngay trong phiên giao dịch.</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Được hỗ trợ bởi Trung tâm Nghiên cứu MBS với các sản phẩm nghiên cứu chuyên nghiệp, độc lập và đa dạng</div>
</div>

<div class="fs14 c_555 mt15">Đến với MBS, Quý khách sẽ được cung cấp dịch vụ tư vấn đầu tư hiệu quả, trên cơ sở trung thực, tin cậy và kịp thời.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('122', 'Ủy thác quản lý tài khoản', 'uy-thac-quan-ly-tai-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_7.jpg', 'khach-hang-to-chuc/dich-vu-chung-khoan/uy-thac-quan-ly-tai-khoan', '<div class="fs14 c_555 mt15">Ủy thác quản lý tài khoản là một dịch vụ được MBS chú trọng phát triển nhằm đem lại những lợi ích tối đa cho khách hàng:</div>

<div class="wrap clearfix">
<div class="it-pro fl">Giúp các khách hàng không có thời gian bám sát thị trường mà vẫn có thể đầu tư hiệu quả vào thị trường chứng khoán Việt Nam.</div>

<div class="it-pro fr">Được làm việc với những chuyên gia tư vấn đầu tư được đào tạo bài bản và chuyên sâu, có nhiều năm kinh nghiệm thực tế trên thị trường,</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Đem đến cơ hội cho những nhà đầu tư chưa có nhiều kinh nghiệm.</div>
</div>

<div class="fs14 c_555 mt15">Sử dụng dịch vụ ủy thác quản lý tài khoản của MBS đem đến những cơ hội đầu tư hiệu quả, là lựa chọn tốt nhất đối với những nhà đầu tư.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('128', 'Mở tài khoản', 'mo-tai-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_1.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/mo-tai-khoan', '<div class="fs14 c_555 mt15">MBS giúp bạn mở tài khoản giao dịch chứng khoán nhanh chóng, chính xác và thuận tiện nhất. Chúng tôi có thể đáp ứng các nhu cầu sau đây của quý khách</div>

<div class="fs14 c_0066b3 mt20"><strong>Mở tài khoản mới tại MBS</strong></div>

<div class="it-pro mt20">Quý khách có nhu cầu mở tài khoản giao dịch chứng khoán tại MBS chỉ cần mang chứng minh thư nhân dân bản gốc đến các chi nhánh/ phòng giao dịch của MBS trên toàn quốc để làm thủ tục mở tài khoản giao dịch chứng khoán (Danh sách các CN/PGD MBS <a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">tại đây</a>)</div>

<div class="it-pro mt10">Quý khách có thể đăng ký mở tài khoản Online thông qua website:<br />
<a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">http://open24.mbs.com.vn/Presentation/Welcome.aspx</a></div>

<div class="fs14 c_0066b3 mt20"><strong>Chuyển giao dịch về MBS</strong></div>

<div class="it-pro mt20">Theo quy định của Ủy ban Chứng khoán Nhà nước, một nhà đầu tư có thể có nhiều tài khoản giao dịch tại các công ty chứng khoán khác nhau. Do đó, Quý khách có thể mở thêm tài khoản giao dịch chứng khoán tại MBS đồng thời chuyển tiền và số dư chứng khoán sang tài khoản tại MBS để thực hiện giao dịch và sử dụng sản phẩm dịch vụ tốt nhất. (Chi tiết vui lòng tham khảo <a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" target="_blank" title="">tại đây</a>).</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('131', 'Hỗ trợ tài chính', 'ho-tro-tai-chinh', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_4.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/ho-tro-tai-chinh', '<div class="fs14 c_555 mt15">Với nền tảng tài chính vững chắc và được sự hỗ trợ mạnh mẽ từ Ngân hàng mẹ MB, MBS luôn đáp ứng tốt nhất nhu cầu kinh doanh của đối tác và khách hàng bằng các dịch vụ hỗ trợ tài chính đa dạng. MBS đã xây dựng được một mạng lưới quan hệ với các đối tác cung cấp vốn để hỗ trợ tài chính cho các khách hàng có nhu cầu đầu tư chứng khoán.</div>

<div class="fs14 c_0066b3 mt20"><strong>Dịch vụ hỗ trợ tài chính của MBS bao gồm:</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Ứng trước tiền bán</div>

<div class="it-pro fr">Cho vay thanh toán tiền mua chứng khoán đã khớp lệnh và/hoặc các mục đích tiêu dùng (Margin+)</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Sức mua ứng trước</div>

<div class="it-pro fr">Và một số dịch vụ hỗ trợ tài chính khác.</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Giao dịch ký quỹ (Margin)</div>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('130', 'Lưu ký chứng khoán', 'luu-ky-chung-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_3.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/luu-ky-chung-khoan', '<div class="fs14 c_555 mt15">MBS phối hợp với Trung tâm Lưu ký Chứng khoán Việt Nam (VSD) cung cấp dịch vụ lưu giữ, bảo quản chứng khoán của khách hàng, làm thủ tục chuyển chứng khoán vào tài khoản giao dịch chứng khoán, giúp khách hàng dễ dàng thực hiện giao dịch và nhận đầy đủ mọi quyền lợi liên quan của chứng khoán.</div>

<div class="fs14 c_0066b3 mt20"><strong>Tiện ích</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện;</div>

<div class="it-pro fr">Tránh rủi ro cho khách hàng khi tự lưu giữ chứng khoán</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Đội ngũ nhân viên chuyên nghiệp, đảm bảo thời gian hồ sơ được chấp thuận nhanh nhất;</div>

<div class="it-pro fr">Giúp khách hàng nhanh chóng nhận được mọi quyền lợi về chứng khoán</div>
</div>

<div class="fs14 c_0066b3 mt20"><strong>Đối tượng</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện;</div>

<div class="it-pro fr">Tránh rủi ro cho khách hàng khi tự lưu giữ chứng khoán</div>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('129', 'Môi giới chứng khoán', 'moi-gioi-chung-khoan', empty_clob(), 'vi', '/uploads/images/dich-vu/img_dvck_2.jpg', 'khach-hang-ca-nhan/dich-vu-chung-khoan/moi-gioi-chung-khoan', '<div class="fs14 c_555 mt15">MBS cung cấp dịch vụ tư vấn cho Quý khách mua, bán các loại chứng khoán đã niêm yết và chưa niêm yết. MBS luôn chú trọng việc thu hút nhân tài, đầu tư vào công nghệ hiện đại và cung cấp cho khách hàng những sản phẩm phân tích và tư vấn đầu tư với chất lượng tốt nhất trên thị trường.</div>

<div class="box-mbs-ck clearfix mt25">
<div class="img fl"><strong>MBS cam kết</strong></div>

<div class="text fl">
<ul>
	<li>Cung cấp dịch vụ với chất lượng tốt nhất;</li>
	<li>Đi đầu về công nghệ;</li>
	<li>An toàn bảo mật;</li>
	<li>Sản phẩm dịch vụ đa dạng, thỏa mãn mọi nhu cầu của khách hàng.</li>
</ul>
</div>
</div>

<div class="fs14 c_0066b3 mt20"><strong>Hệ thống giao dịch môi giới</strong></div>

<div class="wrap clearfix mt5">
<div class="it-pro fl">Tốc độ nhanh, chính xác, mức phí môi giới hợp lý;</div>

<div class="it-pro fr">Cập nhật thông tin thị trường liên tục đầy đủ</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Thủ tục đơn giản, thuận tiện, bảo mật tuyệt đối</div>

<div class="it-pro fr">Trao đổi thông tin và chia sẻ kiến thức với nhà đầu tư thông qua các buổi hội thảo, tọa đàm</div>
</div>

<div class="wrap clearfix">
<div class="it-pro fl">Tra cứu thông tin tài khoản về các giao dịch tiền và chứng khoán qua nhiều hình thức: Trực tiếp tại sàn, qua điện thoại, internet</div>

<div class="it-pro fr">Nhiều hình thức ưu đãi hấp dẫn</div>
</div>

<div class="fs14 c_555 mt15">Lựa chọn MBS là nhà môi giới chứng khoán tại thị trường chứng khoán Việt Nam là lựa chọn tốt nhất cho những nhà đầu tư chuyên nghiệp.</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('150', 'Thương vụ điển hình', 'thuong-vu-dien-hinh', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_5.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('150', 'Highlight deals', 'highlight-deals', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('151', 'Công ty CP Cao su Đồng Phú (DPR)', 'cong-ty-cp-cao-su-dong-phu-dpr', 'Tư vấn chuyển đổi, Tư vấn IPO<br/>Giá trị IPO: 626 tỷ VNĐ<br/>Năm thục hiện 2006', 'vi', '/uploads/images/thuong-vu/img-tv9.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-cao-su-dong-phu-dpr', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('151', 'Công ty CP Cao su Đồng Phú (DPR)', 'cong-ty-cp-cao-su-dong-phu-dpr', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-cao-su-dong-phu-dpr', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('138', 'Tư vấn về các thủ tục pháp lý', 'tu-van-ve-cac-thu-tuc-phap-ly', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-co-phieu-ecm/tu-van-ve-cac-thu-tuc-phap-ly', '<p>Việc phát hành cổ phiếu đối với các công ty đại chúng liên quan đến nhiều thủ tục pháp lý và các yêu cầu tối thiểu về năng lực tài chính cũng như quản trị doanh nghiệp. MBS sẽ giúp doanh nghiệp rà soát các yêu cầu pháp lý cần thiết để tuân thủ pháp luật.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('138', 'Advice on legal requirements', 'advice-on-legal-requirements', empty_clob(), 'en', null, 'institutional-clients/investment-banking/equity-capital-market-ecm/advice-on-legal-requirements', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('139', 'Tư vấn phương án phát hành', 'tu-van-phuong-an-phat-hanh', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-co-phieu-ecm/tu-van-phuong-an-phat-hanh', '<p>Việc lựa chọn một phương án phát hành chứng khoán tối ưu có thể mang lại nhiều lợi ích cho doanh nghiệp. Bên cạnh việc huy động vốn phục vụ nhu cầu kinh doanh, doanh nghiệp cần cân đối các yếu tố khác như khả năng tiết kiệm thuế, xây dựng một cơ cấu vốn phù hợp an toàn trong con mắt của các nhà tài trợ vốn cũng như các đối tác, khả năng pha loãng cổ phiếu so với tốc độ tăng trưởng về thu nhập, cơ cấu cổ đông, điều kiện thị trường tại thời điểm dự kiến phát hành. Đội ngũ tư vấn của MBS có thể giúp các doanh nghiệp đưa ra một phương án phát hành vốn hiệu quả trong từng giai đoạn nhất định và phù hợp với điều kiện cụ thể của thị trường.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('139', 'Deal planning', 'deal-planning', empty_clob(), 'en', null, 'institutional-clients/investment-banking/equity-capital-market-ecm/deal-planning', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('140', 'Bảo lãnh phát hành trái phiếu', 'bao-lanh-phat-hanh-trai-phieu', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-trai-phieu-dcm/bao-lanh-phat-hanh-trai-phieu', '<p>Tùy theo yêu cầu của khách hàng, MBS có thể cung cấp dịch vụ bảo lãnh phát hành để đảm bảo đợt phát hành của khách hàng diễn ra thành công với một mức giá thỏa thuận tối thiểu.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('140', 'Debt underwriting', 'debt-underwriting', empty_clob(), 'en', null, 'institutional-clients/investment-banking/dept-capital-market/debt-underwriting', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('141', 'Phân phối các sản phẩm trái phiếu', 'phan-phoi-cac-san-pham-trai-phieu', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-trai-phieu-dcm/phan-phoi-cac-san-pham-trai-phieu', '<p>MBS có một mạng lưới khách hàng tổ chức và khách hàng cá nhân đa dạng, tạo nên lợi thế để MBS hỗ trợ khách hàng huy động vốn thành công thông qua chào bán riêng lẻ hoặc chào bán ra công chúng. MBS có thể giúp khách hàng dựng sổ (book building) nhu cầu đăng ký nhằm lượng hóa khả năng hấp thụ của thị trường cũng như mức giá có thể phát hành thành công.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('141', 'Debt distribution', 'debt-distribution', empty_clob(), 'en', null, 'institutional-clients/investment-banking/dept-capital-market/debt-distribution', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('142', 'Tư vấn về các thủ tục pháp lý', 'tu-van-ve-cac-thu-tuc-phap-ly', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-trai-phieu-dcm/tu-van-ve-cac-thu-tuc-phap-ly', '<p>Việc phát hành cổ phiếu đối với các công ty đại chúng liên quan đến nhiều thủ tục pháp lý và các yêu cầu tối thiểu về năng lực tài chính cũng như quản trị doanh nghiệp. MBS sẽ giúp doanh nghiệp rà soát các yêu cầu pháp lý cần thiết để tuân thủ pháp luật.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('142', 'Advice on legal requirements', 'advice-on-legal-requirements', empty_clob(), 'en', null, 'institutional-clients/investment-banking/dept-capital-market/advice-on-legal-requirements', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('143', 'Tư vấn phương án phát hành', 'tu-van-phuong-an-phat-hanh', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-phat-hanh-trai-phieu-dcm/tu-van-phuong-an-phat-hanh', '<p>Việc lựa chọn một phương án phát hành chứng khoán tối ưu có thể mang lại nhiều lợi ích cho doanh nghiệp. Bên cạnh việc huy động vốn phục vụ nhu cầu kinh doanh, doanh nghiệp cần cân đối các yếu tố khác như khả năng tiết kiệm thuế, xây dựng một cơ cấu vốn phù hợp an toàn trong con mắt của các nhà tài trợ vốn cũng như các đối tác, khả năng pha loãng cổ phiếu so với tốc độ tăng trưởng về thu nhập, cơ cấu cổ đông, điều kiện thị trường tại thời điểm dự kiến phát hành. Đội ngũ tư vấn của MBS có thể giúp các doanh nghiệp đưa ra một phương án phát hành vốn hiệu quả trong từng giai đoạn nhất định và phù hợp với điều kiện cụ thể của thị trường.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('143', 'Deal planning', 'deal-planning', empty_clob(), 'en', null, 'institutional-clients/investment-banking/dept-capital-market/deal-planning', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('144', 'Tư vấn niêm yết', 'tu-van-niem-yet', empty_clob(), 'vi', 'uploads/article/2015/3/27032015_431841_PM1.png', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-tai-chinh-doanh-nghiep/tu-van-niem-yet', '<p>MBS tư vấn cho khách hàng trong tất cả các bước cần thực hiện để niêm yết trên HNX và HSX, bao gồm chuẩn bị tiền niêm yết, định giá doanh nghiệp, chuẩn bị hồ sơ, xin cấp phép, tổ chức road show, thực hiện các yêu cầu báo cáo, giải quyết các vấn đề nảy sinh trong quá trình thực hiện niêm yết và hậu niêm yết.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('144', 'Securities listing', 'securities-listing', empty_clob(), 'en', null, 'institutional-clients/investment-banking/financial-advisory/securities-listing', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('145', 'Tư vấn cổ phần hóa', 'tu-van-co-phan-hoa', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-tai-chinh-doanh-nghiep/tu-van-co-phan-hoa', '<p>Cổ phần hóa là một hình thức chuyển đổi doanh nghiệp đặc thù. Để đảm bảo doanh nghiệp hoạt động thành công sau khi cổ phần hóa, cần có những bước chuẩn bị cụ thể, kỹ lưỡng, giải quyết các vấn đề về lao động, quyền sở hữu tài sản, trong nhiều trường hợp cần phải tái cơ cấu nợ. MBS đã cung cấp dịch vụ tư vấn và trợ giúp cho nhiều doanh nghiệp nhà nước và các doanh nghiệp tư nhân thực hiện cổ phần hóa thành công.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('145', 'Equitisation', 'equitisation', empty_clob(), 'en', null, 'institutional-clients/investment-banking/financial-advisory/equitisation', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('146', 'Trợ giúp trong đàm phán', 'tro-giup-trong-dam-phan', empty_clob(), 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/tu-van-ma/tro-giup-trong-dam-phan', '<p>Sự khác biệt về văn hóa và cách thực hành kinh doanh có thể khiến cho quá trình đàm phán khó khăn hơn. Phần lớn đội ngũ nhân sự của MBS được đào tạo ở nước ngoài và có nhiều kinh nghiệm làm việc tại các công ty đa quốc gia. Do vậy, chúng tôi có khả năng giúp tháo gỡ bớt các rào cản đó trong quá trình đàm phán.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('146', 'Assistance in conducting valuation', 'assistance-in-conducting-valuation', empty_clob(), 'en', null, 'institutional-clients/investment-banking/ma-advisory/assistance-in-conducting-valuation', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('189', 'Nộp chuyền tiền vào tài khoản chung', 'nop-chuyen-tien-vao-tai-khoan-chung', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/nop-chuyen-tien-vao-tai-khoan-chung', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('189', 'Paying money into the ball joint account', 'paying-money-into-the-ball-joint-account', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/paying-money-into-the-ball-joint-account', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('190', 'Lưu ký chứng khoán', 'luu-ky-chung-khoan', 'Khách hàng mở tài khoản giao dịch chứng khoán tại MBS có thể thực hiện lưu ký chứng khoán tại các điểm giao dịch của MBS.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/luu-ky-chung-khoan', '<p>Khách hàng mở tài khoản giao dịch chứng khoán tại MBS có thể thực hiện lưu ký chứng khoán tại các điểm giao dịch của MBS.</p>

<p><strong>1.&nbsp;&nbsp; &nbsp;Hồ sơ lưu ký chứng khoán:</strong></p>

<p>Để lưu ký chứng khoán, khách hàng cần bị đầy đủ các giấy tờ cần thiết sau:</p>

<p style="margin-left: 40px;"><strong>Đối với Khách hàng là cá nhân:</strong></p>


<p><ul>
	<li>Sổ/Giấy chứng nhận sở hữu cổ phần (bản gốc);</li>
	<li>CMND còn hiệu lực (01 bản photo và bản gốc để đối chiếu);</li>
	<li>Phiếu gửi chứng khoán (mẫu đính kèm): 03 bản gốc.</li>
</ul></p>



<p style="margin-left: 40px;"><strong>Đối với Khách hàng là tổ chức:</strong></p>


<p><ul>
	<li>Sổ/Giấy chứng nhận sở hữu cổ phần (bản gốc);</li>
	<li>Giấy chứng nhận đăng ký kinh doanh (01 bản sao công chứng);</li>
	<li>CMND còn hiệu lực của người đại diện theo pháp luật (người ký tên, đóng dấu trên phiếu gửi chứng khoán) (01 bản photo công chứng);</li>
	<li>Giấy giới thiệu người được ủy quyền đến làm thủ tục lưu ký;</li>
	<li>CMND của người được ủy quyền đến làm thủ tục lưu ký (01 bản photo);</li>
	<li>Phiếu gửi chứng khoán (mẫu đính kèm): 03 bản gốc.</li>
</ul></p>



<p><strong>2.&nbsp;&nbsp; &nbsp;Thực hiện lưu ký chứng khoán:</strong></p>

<p>Khách hàng mang bộ hồ sơ lưu ký chứng khoán đến các điểm giao dịch của MBS để thực hiện thủ tục lưu ký.</p>

<p><strong>Lưu ý:</strong> Việc lưu ký chứng khoán của khách hàng chỉ thành công khi thông tin (họ tên, số và ngày cấp CMND/ Giấy ĐKKD) của khách hàng trên Sổ/ Giấy chứng nhận sở hữu chứng khoán khớp với thông tin trên CMND/ Giấy ĐKKD và khớp với thông tin khách hàng đã đăng ký tại MBS.&nbsp;<br />
Trường hợp có bất kỳ sai khác thông tin nào, khách hàng liên hệ với MBS &nbsp;để được hướng dẫn và thực hiện.</p>
', null, '/uploads/files/LuuKy_Giay-de-thay-doi-thong-tin.doc');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('190', 'Securities Depository', 'securities-depository', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/securities-depository', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('158', 'Công ty CP cao su Phước Hòa (PHR)', 'cong-ty-cp-cao-su-phuoc-hoa-phr', empty_clob(), 'vi', '/uploads/images/thuong-vu/img-tv2.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/cong-ty-cp-cao-su-phuoc-hoa-phr', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('158', 'Công ty CP cao su Phước Hòa (PHR)', 'cong-ty-cp-cao-su-phuoc-hoa-phr', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/cong-ty-cp-cao-su-phuoc-hoa-phr', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('159', 'Ngân hàng TMCP Quân đội', 'ngan-hang-tmcp-quan-doi', 'Tư vấn phát hành trái phiếu<br/>Giá trị thu xếp: 1.430 tỷ VNĐ<br/>Tư vấn phát hành cổ phiếu<br/>Vốn huy động: 1.360 tỷ VNĐ<br/>Năm thục hiện: 2010', 'vi', '/uploads/images/thuong-vu/img-tv1.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/thuong-vu-dien-hinh/ngan-hang-tmcp-quan-doi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('159', 'Ngân hàng TMCP Quân đội', 'ngan-hang-tmcp-quan-doi', empty_clob(), 'en', null, 'institutional-clients/investment-banking/highlight-deals/ngan-hang-tmcp-quan-doi', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('160', 'DV tra cứu thông tin giao dịch qua tin nhắn SMS', 'dv-tra-cuu-thong-tin-giao-dich-qua-tin-nhan-sms', empty_clob(), 'vi', '/uploads/images/dich-vu/dt6.png', 'khach-hang-to-chuc/dich-vu-chung-khoan/dich-vu-dien-tu/dv-tra-cuu-thong-tin-giao-dich-qua-tin-nhan-sms', '<div class="text-dtshow fs14 c_555">
<p>MBS cung cấpdịch vụ thông báo kết quả khớp lệnh, tra cứu kết quả khớp lệnh và tra cứu số dư tài khoản, tra cứu thông tin thị trường và thông tin chứng khoán qua tin nhắn SMS.</p>

<p>Để biết thêm thông tin chi tiết Quý khách vui lòng xem Hướng dẫn sử dụng tại: <a href="https://mbs.com.vn/Upload/HDSD_SMS24_moinhat.pdf" target="_blank">https://mbs.com.vn/Upload/HDSD_SMS24_moinhat.pdf</a></p>
</div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('160', 'DV tra cứu thông tin giao dịch qua tin nhắn SMS', 'dv-tra-cuu-thong-tin-giao-dich-qua-tin-nhan-sms', empty_clob(), 'en', null, 'institutional-clients/securities-services/online-services/dv-tra-cuu-thong-tin-giao-dich-qua-tin-nhan-sms', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('161', 'Hướng dẫn giao dịch', 'huong-dan-giao-dich', empty_clob(), 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('161', 'Transactions guide', 'transactions-guide', empty_clob(), 'en', null, 'customer-care/transactions-guide', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('162', 'Hội thảo đào tạo', 'hoi-thao-dao-tao', null, 'vi', null, 'cham-soc-khach-hang/hoi-thao-dao-tao', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('162', 'Hội thảo đào tạo', 'hoi-thao-dao-tao', null, 'en', null, 'customer-care/hoi-thao-dao-tao', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('163', 'Chương trình ưu đãi', 'chuong-trinh-uu-dai', null, 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('163', 'Promotion', 'promotion', null, 'en', null, 'customer-care/promotion', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('164', 'Câu hỏi thường gặp', 'cau-hoi-thuong-gap', null, 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('164', 'FAQs', 'faqs', null, 'en', null, 'customer-care/faqs', null, null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('165', 'Hướng dẫn giao dịch', 'huong-dan-giao-dich', empty_clob(), 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('165', 'Trading Guide', 'trading-guide', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('166', 'Biểu phí', 'bieu-phi', empty_clob(), 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/bieu-phi', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', 'Biểu phí giao dịch chứng khoán tại MBS', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('166', 'Services Fees', 'services-fees', empty_clob(), 'en', null, 'customer-care/transactions-guide/services-fees', empty_clob(), 'Stock trading fees at MBS', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('167', 'Video hướng dẫn', 'video-huong-dan', empty_clob(), 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/video-huong-dan', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('167', 'Guide videos', 'guide-videos', empty_clob(), 'en', null, 'customer-care/transactions-guide/guide-videos', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('168', 'M.Stock24', 'mstock24', 'M.Stock24 là sản phẩm giao dịch chứng khoán ưu việt với nhiều tiện ích nổi trội, là trợ thủ đắc lực của các khách hàng thực hiện mọi giao dịch về tiền tệ và chứng khoán mọi lúc mọi nơi...', 'vi', '/uploads/images/video-thumb/img-hd-2.jpg', 'cham-soc-khach-hang/huong-dan-giao-dich/video-huong-dan/mstock24', '<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/5ieyTW77BYo" width="560"></iframe></p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('168', 'M.Stock24', 'mstock24', empty_clob(), 'en', null, 'customer-care/transactions-guide/guide-videos/mstock24', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('169', 'Sản phẩm chiến lượt đầu tư', 'san-pham-chien-luot-dau-tu', 'Mở tài khoản giao dịch chúng khoán tại MBS nhanh chóng, chính xác và thuận tiện nhất. Mở tài khoản mới tại MBS. Quý khách có nhu cầu mở tài khoản chứng khoán tại MBS chỉ cần mang chứng minh thư đến MBS.', 'vi', '/uploads/images/video-thumb/img-hd-1.jpg', 'cham-soc-khach-hang/huong-dan-giao-dich/video-huong-dan/san-pham-chien-luot-dau-tu', '<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/jwQLOlwxX_8" width="420"></iframe></p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('169', 'Sản phẩm chiến lượt đầu tư', 'san-pham-chien-luot-dau-tu', empty_clob(), 'en', null, 'customer-care/transactions-guide/guide-videos/san-pham-chien-luot-dau-tu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('170', 'Thực hiện quền trên stock24', 'thuc-hien-quen-tren-stock24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/thuc-hien-quen-tren-stock24', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('170', 'Implementation of permission granted in Stock24', 'implementation-of-permission-granted-in-stock24', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/implementation-of-permission-granted-in-stock24', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('171', 'Thay đổi thông tin, đăng ký, hủy dịch vụ', 'thay-doi-thong-tin-dang-ky-huy-dich-vu', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/thay-doi-thong-tin-dang-ky-huy-dich-vu', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('171', 'Change the information , registration , cancellation', 'change-the-information-registration-cancellation', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/change-the-information-registration-cancellation', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('172', 'Sử dụng chiến lược đầu tư', 'su-dung-chien-luoc-dau-tu', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/su-dung-chien-luoc-dau-tu', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('172', 'Use the investment strategy', 'use-the-investment-strategy', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/use-the-investment-strategy', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('173', 'Sử dụng báo cáo trên stock24', 'su-dung-bao-cao-tren-stock24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/su-dung-bao-cao-tren-stock24', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('173', 'Using reports on Stock24', 'using-reports-on-stock24', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/using-reports-on-stock24', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('174', 'Sử dụng bảng giá trên stock24', 'su-dung-bang-gia-tren-stock24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/su-dung-bang-gia-tren-stock24', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('174', 'Use the price table on Stock24', 'use-the-price-table-on-stock24', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/use-the-price-table-on-stock24', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('175', 'Change the stock status', 'change-the-stock-status', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/change-the-stock-status', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('176', 'Chuyển khoản chứng khoán', 'chuyen-khoan-chung-khoan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/chuyen-khoan-chung-khoan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('176', 'Transfer of securities', 'transfer-of-securities', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/transfer-of-securities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('177', 'Bán chứng khoán lô lẻ', 'ban-chung-khoan-lo-le', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/ban-chung-khoan-lo-le', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('177', 'Odd-lot sale securities', 'odd-lot-sale-securities', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/odd-lot-sale-securities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('178', 'Dịch vụ hợp tác kinh doanh chứng khoán', 'dich-vu-hop-tac-kinh-doanh-chung-khoan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/dich-vu-hop-tac-kinh-doanh-chung-khoan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('178', 'Services business cooperation stock', 'services-business-cooperation-stock', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/services-business-cooperation-stock', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('179', 'Giao dịch ký quỹ', 'giao-dich-ky-quy', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/giao-dich-ky-quy', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('179', 'Margin trading', 'margin-trading', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/margin-trading', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('180', 'Ứng trước tiền bán', 'ung-truoc-tien-ban', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/ung-truoc-tien-ban', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('180', 'Cash advances', 'cash-advances', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/cash-advances', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('181', 'Đăng nhập stock24', 'dang-nhap-stock24', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/dang-nhap-stock24', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('181', 'Loggin to stock24', 'loggin-to-stock24', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/loggin-to-stock24', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('182', 'Thực hiện quyền cho người sở hữu', 'thuc-hien-quyen-cho-nguoi-so-huu', 'Để thuận tiện và đảm bảo quyền lợi cho khách hàng trong việc thực hiện quyền cho nhà đầu tư, MBS xin hướng dẫn Quý khách hàng  thực hiện quyền cho người sở hữu chứng khoán như sau:
', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/thuc-hien-quyen-cho-nguoi-so-huu', '<p style="line-height: 20.7999992370605px; text-align: justify;">Để thuận tiện và đảm bảo quyền lợi cho khách hàng trong việc thực hiện quyền cho nhà đầu tư, MBS xin hướng dẫn Quý khách hàng &nbsp;thực hiện quyền cho người sở hữu chứng khoán như sau:</p>

<p style="line-height: 20.7999992370605px;"><strong>1.&nbsp;&nbsp; &nbsp;Quyền đại hội cổ đông</strong></p>

<p style="line-height: 20.7999992370605px;">Khách hàng sẽ được nhận thông báo từ Tổ chức Phát hành</p>

<p style="line-height: 20.7999992370605px;"><strong>2.&nbsp;&nbsp; &nbsp;Quyền Cổ tức bằng tiền, Quyền cổ tức bằng cổ phiếu/cổ phiếu thưởng</strong></p>

<p style="line-height: 20.7999992370605px;">Quyền nhận cổ tức bằng tiền, quyền cổ tức bằng cổ phiếu/cổ phiếu thưởng: Vào ngày chi trả cổ tức hoặc ngày giao dịch cổ tức bằng cổ phiếu, cổ phiếu thưởng, MBS sẽ phân bổ tự động vào tài khoản giao dịch chứng khoán của khách hàng.</p>

<p style="line-height: 20.7999992370605px;"><strong>3.&nbsp;&nbsp; &nbsp;Quyền mua cổ phiếu phát hành thêm</strong></p>

<p style="line-height: 20.7999992370605px;">-&nbsp;&nbsp; &nbsp;Quyền mua cổ phiếu phát hành thêm: Khi có phát sinh quyền mua chứng khoán phát hành thêm, MBS sẽ gửi thông báo về việc sở hữu quyền mua của &nbsp;từng khách hàng thông qua SMS hoặc email mà khách hàng đã đăng ký với MBS. Vì vậy, Quý khách hàng lưu ý thông báo và cập nhật cho MBS các thông tin liên lạc khi có sự thay đổi.&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Thủ tục đăng ký mua chứng khoán:<br />
•&nbsp;&nbsp; &nbsp;KH có thể thực hiện đăng ký mua trực tuyến cổ phiếu phát hành thêm qua https://stock24.mbs.com.vn. (nghiệp vụ khác\thực hiện quyền). &nbsp;<br />
•&nbsp;&nbsp; &nbsp;Hoặc &nbsp;đến các Phòng giao dịch/Chi nhánh gần nhất của MBS để thực hiện.</p>

<p style="line-height: 20.7999992370605px;">Mọi thắc mắc về dịch vụ xin vui lòng liên hệ số hotline 1900 9088 nhấn phím 4 để được hướng dẫn và tư vấn.</p>

<p style="line-height: 20.7999992370605px;">&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('183', 'Change of ownership of securities', 'change-of-ownership-of-securities', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/change-of-ownership-of-securities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('184', 'Chuyển khoản tất toán tài khoản chứng khoán', 'chuyen-khoan-tat-toan-tai-khoan-chung-khoan', 'Khách hàng có chứng khoán lưu ký tại MBS khi có nhu cầu chuyển khoản/tất toán tài khoản tại MBS có thể thực hiện với điều kiện sau:', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/chuyen-khoan-tat-toan-tai-khoan-chung-khoan', '<p style="text-align: justify;">Khách hàng có chứng khoán lưu ký tại MBS khi có nhu cầu chuyển khoản/tất toán tài khoản tại MBS có thể thực hiện với điều kiện sau:<br />
-&nbsp;&nbsp; &nbsp;Khách hàng không có nghĩa vụ tài chính tại MBS;<br />
-&nbsp;&nbsp; &nbsp;Chỉ thực hiện chuyển khoản chứng khoán ở tài khoản giao dịch chứng khoán thường;<br />
-&nbsp;&nbsp; &nbsp;Khách hàng chỉ được thực hiện chuyển khoản tất toán tài khoản hoặc chuyển khoản chứng khoán trong phạm vi toàn bộ số lượng các loại chứng khoán có trên tài khoản và các quyền phát sinh (nếu có) đã xác định thuộc sở hữu của khách hàng.</p>

<p style="text-align: justify;"><strong>1.&nbsp;&nbsp; &nbsp;Hồ sơ chuyển khoản chứng khoán/tất toán tài khoản bao gồm:</strong></p>
<p>-&nbsp;&nbsp; &nbsp;Giấy đề nghị tất toán tài khoản Mẫu 25/LK (trong trường hợp chuyển khoản tất toán tài khoản), hoặc &nbsp;Giấy đề nghị chuyển khoản chứng khoán Mẫu 26/LK &nbsp;(trong trường hợp chuyển khoản không tất toán tài khoản);&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Bản sao có đóng dấu treo của thành viên lưu ký bên nhận chuyển khoản văn bản chứng minh việc mở tài khoản của nhà đầu tư.<br />
-&nbsp;&nbsp; &nbsp;Bản sao xác nhận thay đổi thành viên lưu ký đối với nhà đầu tư nước ngoài (áp dụng với trường hợp nhà đầu tư nước ngoài chuyển khoản tất toán chứng khoán);<br />
-&nbsp;&nbsp; &nbsp;Chứng minh thư nhân dân bản sao và chứng minh thư nhân dân bản gốc để đối chiếu đối với khách hàng cá nhân;<br />
-&nbsp;&nbsp; &nbsp;Bản sao công chứng Giấy đăng ký kinh doanh đối với khách hàng tổ chức;<br />
Nếu tài khoản của nhà đầu tư phát sinh thêm quyền trong thời gian hồ sơ đang được xử lý, MBS sẽ thông báo đến khách hàng để ký bổ sung hồ sơ theo Quy chế của Trung tâm Lưu ký Chứng khoán Việt Nam (VSD).</p>

<p style="text-align: justify;"><strong>2.&nbsp;&nbsp; &nbsp;Thực hiện chuyển khoản chứng khoán/tất toán tài khoản chứng khoán:</strong></p>
<p>Khi có nhu cầu chuyển khoản chứng khoán/tất toán tài khoản chứng khoán, khách hàng đến trực tiếp tại các Chi nhánh/Phòng giao dịch của MBS để làm thủ tục và được hướng dẫn.</p>

<p style="text-align: justify;"><em><strong>Lưu ý:</strong></em>&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Thời gian hoàn tất việc chuyển khoản chứng khoán/tất toán tài khoản chứng khoán dự kiến &nbsp;là 03 ngày làm việc kể từ ngày nhận được hồ sơ đầy đủ, hợp lệ.<br />
-&nbsp;&nbsp; &nbsp;Chuyển khoản chứng khoán tức là chuyển toàn bộ danh mục chứng khoán sang tài khoản giao dịch chứng khoán của khách hàng mở tại thành viên lưu ký khác. Không chuyển một phần danh mục.&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Tất toán tài khoản là khách hàng thực hiện đóng tài khoản.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('184', 'Transfer all securities accounts', 'transfer-all-securities-accounts', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/transfer-all-securities-accounts', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('185', 'Thay đổi thông tin nhà đầu tư', 'thay-doi-thong-tin-nha-dau-tu', 'Khi có phát sinh thay đổi các thông tin cá nhân như số chứng minh thư nhân dân (CMTND), địa chỉ liên hệ, số điện thoại… và các thông tin khác, khách hàng cần liên hệ với MBS để thực hiện thay đổi thông tin.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/thay-doi-thong-tin-nha-dau-tu', '<ul>
	<li>Khi có phát sinh thay đổi các thông tin cá nhân như số chứng minh thư nhân dân (CMTND), địa chỉ liên hệ, số điện thoại… và các thông tin khác, khách hàng cần liên hệ với MBS để thực hiện thay đổi thông tin.</li>
	<li>Đối với các thông tin thay đổi liên quan đến họ tên, số chứng minh thư nhân dân, ngày cấp, nơi cấp, địa chỉ liên hệ, khách hàng cần làm thủ tục thay đổi thông tin với MBS và Trung tâm Lưu ký Chứng khoán Việt Nam (VSD).</li>
</ul>

<p>Một số hướng dẫn chuẩn bị hồ sơ thay đổi thông tin:</p>

<p><strong>1. &nbsp;Thay đổi số CMTND: </strong>hồ sơ chuẩn bị:</p>

<p>- &nbsp;Giấy đề nghị điều chỉnh thông tin theo mẫu của MBS có ghi đầy đủ lý do điều chỉnh, có chữ ký xác nhận của khách hàng, nhân viên giao dịch và kiểm soát của MBS.<br />
- &nbsp;CMTND mới bản gốc và 1 trong số các giấy tờ sau:</p>

<ul>
	<li>Xác nhận của cơ quan có thẩm quyền (công an/quân đội…) về việc thay đổi số CMTND cũ và mới của khách hàng;</li>
	<li>Sổ hộ khẩu (có ghi số CMTND cũ và CMTND thay đổi);&nbsp;</li>
	<li>CMTND cũ (bản có công chứng)/hộ chiếu có ghi số CMTND cũ.</li>
</ul>

<p><strong>2. &nbsp;Thay đổi ngày cấp CMTND: </strong>hồ sơ chuẩn bị:</p>

<p>- &nbsp;Giấy đề nghị điều chỉnh thông tin theo mẫu của MBS có ghi đầy đủ lý do điều chỉnh, có chữ ký xác nhận của khách hàng, nhân viên giao dịch và kiểm soát của MBS.<br />
- &nbsp;CMTND mới bản gốc của khách hàng.</p>

<p><strong>3. Thay đổi họ và tên khách hàng:</strong>&nbsp;<span style="line-height: 1.6em;">hồ sơ chuẩn bị:</span></p>

<p><span style="line-height: 1.6em;">- &nbsp;Giấy đề nghị điều chỉnh thông tin theo mẫu của MBS có ghi đầy đủ lý do điều chỉnh, có chữ ký xác nhận của khách hàng, nhân viên giao dịch và kiểm soát của MBS.</span></p>

<p>- &nbsp;CMTND mới bản gốc của khách hàng.&nbsp;<br />
- &nbsp;Giấy xác nhận của cơ quan chính quyền địa phương hoặc công an xác nhận việc đổi họ tên của khách hàng.</p>

<p><strong>4.&nbsp;Thay đổi địa chỉ liên hệ hoặc địa chỉ thường trú của khách hàng:</strong> hồ sơ chuẩn bị:</p>

<p>- &nbsp;Giấy đề nghị điều chỉnh thông tin theo mẫu của MBS có ghi đầy đủ lý do điều chỉnh, có chữ ký xác nhận của khách hàng, nhân viên giao dịch và kiểm soát của MBS.<br />
- &nbsp;CMTND bản gốc của khách hàng.</p>

<p><strong>5.&nbsp;Thay đổi họ tên và thông tin CMTND của khách hàng:</strong> hồ sơ chuẩn bị:</p>

<p>- &nbsp;Giấy đề nghị điều chỉnh thông tin (Mẫu 05/LK) ghi đầy đủ thông tin và lý do điều chỉnh của khách hàng.<br />
- &nbsp;CMTND cũ và mới của khách hàng.<br />
- &nbsp;Giấy xác nhận của cơ quan chính quyền địa phương hoặc công an xác nhận việc đổi họ tên của khách hàng.<br />
- &nbsp;Bản sao y bản chính hợp đồng mở tài khoản của khách hàng (01 bản).</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('185', 'Change the information investors', 'change-the-information-investors', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/change-the-information-investors', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('186', 'Withdrawal of securities', 'withdrawal-of-securities', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/withdrawal-of-securities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('187', 'Stock Exchange', 'stock-exchange', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/stock-exchange', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('188', 'Rút chuyển tiền tại MBS', 'rut-chuyen-tien-tai-mbs', 'Quý khách có thể rút/chuyển tiền trực tiếp tại quầy giao dịch của MBS hoặc chuyển khoản qua giao dịch trực tuyến (Stock24)', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/rut-chuyen-tien-tai-mbs', '<p style="text-align: justify;">Quý khách có thể rút/chuyển tiền trực tiếp tại quầy giao dịch của MBS hoặc chuyển khoản qua giao dịch trực tuyến (Stock24)</p>

<p><strong>1.&nbsp;&nbsp; &nbsp;Thực hiện rút &nbsp;tiền/chuyển tiền tại quầy giao dịch&nbsp;</strong><br />
Khách hàng là chủ tài khoản hoặc người được uỷ quyền hợp pháp chỉ cần mang theo CMTND đến các địa điểm Giao dịch của MBS để thực hiện giao dịch rút/chuyển tiền.&nbsp;</p>

<p><strong>2.&nbsp;&nbsp; &nbsp; Chuyển khoản qua Stock24</strong></p>

<p>Với độ an toàn, bảo mật cao, nhanh chóng và hiệu quả, chỉ cần một máy tính kết nối internet, Stock24 cho phép người sử dụng có tài khoản giao dịch chứng khoán tại MBS thực hiện chuyển tiền từ tài khoản chứng khoán của mình sang một tài khoản chứng khoán khác mở tại MBS hoặc sang bất kỳ tài khoản cá nhân mở tại ngân hàng.<br />
Chức năng chuyển khoản qua Stock24 áp dụng cho toàn bộ khách hàng mở tài khoản chứng khoán tại MBS và có đăng ký sử dụng dịch vụ này. Quý khách có thể lựa chọn các hình thức chuyển tiền như:</p>

<ul>
	<li style="text-align: justify;">Chuyển khoản nội bộ: là hình thức chuyển tiền giữa các tài khoản chứng khoán mở tại MBS với nhau. Tiền sẽ về tài khoản nhận ngay sau khi bút toán chuyển tiền được thực hiện thành công.</li>
	<li style="text-align: justify;">Chuyển khoản ra MB: chuyển tiền trực tiếp sang tài khoản cá nhân mở tại MB. Hiện tại MBS đã kết nối trực tiếp với MB nên việc chuyển tiền được thực hiện 24/7.</li>
	<li style="text-align: justify;">Chuyển khoản ra ngoài: khách hàng có thể thực hiện chuyển tiền từ tài khoản chứng khoán của mình tại MBS sang các tài khoản mở tại các ngân hàng khác nhau trên lãnh thổ Việt Nam.</li>
</ul>

<p style="text-align: justify;">Sau khi đăng nhập vào Stock24, Quý khách hàng chọn Giao dịch tiền để thực hiện chuyển tiền.</p>

<p>Chi tiết, Quý khách hàng có thể xem tại đây.</p>

<p>&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('188', 'Withdrawing, transfer money at MBS', 'withdrawing-transfer-money-at-mbs', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/withdrawing-transfer-money-at-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('195', 'Trong trường hợp chờ chứng khoán về tài khoản để giao dịch, tôi có thể xem thông tin ở đâu?', 'trong-truong-hop-cho-chung-khoan-ve-tai-khoan-de-giao-dich-toi-co-the-xem-thong-tin-o-dau', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/trong-truong-hop-cho-chung-khoan-ve-tai-khoan-de-giao-dich-toi-co-the-xem-thong-tin-o-dau', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('195', 'In case pending on account of securities to trading , I can see where the information ?', 'in-case-pending-on-account-of-securities-to-trading-i-can-see-where-the-information-', empty_clob(), 'en', null, 'customer-care/faqs/in-case-pending-on-account-of-securities-to-trading-i-can-see-where-the-information-', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('196', 'Tôi đã lưu ký chứng khoán, tại sao chưa có trong tài khoản?', 'toi-da-luu-ky-chung-khoan-tai-sao-chua-co-trong-tai-khoan', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-da-luu-ky-chung-khoan-tai-sao-chua-co-trong-tai-khoan', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('196', 'I have custody of securities , why not take in account ?', 'i-have-custody-of-securities-why-not-take-in-account-', empty_clob(), 'en', null, 'customer-care/faqs/i-have-custody-of-securities-why-not-take-in-account-', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('197', 'Tôi đang ở nước ngoài, tôi muốn chuyển tiền vào tài khoản thì làm thế nào?', 'toi-dang-o-nuoc-ngoai-toi-muon-chuyen-tien-vao-tai-khoan-thi-lam-the-nao', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-dang-o-nuoc-ngoai-toi-muon-chuyen-tien-vao-tai-khoan-thi-lam-the-nao', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('197', 'I am abroad , I want to transfer money to your account , then how?', 'i-am-abroad-i-want-to-transfer-money-to-your-account-then-how', empty_clob(), 'en', null, 'customer-care/faqs/i-am-abroad-i-want-to-transfer-money-to-your-account-then-how', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('198', 'Vậy tôi có thể chuyển tiền tới tài khoản chứng khoán của tôi bằng hình thức nào?', 'vay-toi-co-the-chuyen-tien-toi-tai-khoan-chung-khoan-cua-toi-bang-hinh-thuc-nao', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/vay-toi-co-the-chuyen-tien-toi-tai-khoan-chung-khoan-cua-toi-bang-hinh-thuc-nao', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('209', 'Thông báo thay đổi lịch học khóa đào tạo Phân tích đầu tư chứng khoán', 'thong-bao-thay-doi-lich-hoc-khoa-dao-tao-phan-tich-dau-tu-chung-khoan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai/thong-bao-thay-doi-lich-hoc-khoa-dao-tao-phan-tich-dau-tu-chung-khoan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('209', 'Thông báo thay đổi lịch học khóa đào tạo Phân tích đầu tư chứng khoán', 'thong-bao-thay-doi-lich-hoc-khoa-dao-tao-phan-tich-dau-tu-chung-khoan', empty_clob(), 'en', null, 'customer-care/promotion/thong-bao-thay-doi-lich-hoc-khoa-dao-tao-phan-tich-dau-tu-chung-khoan', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('198', 'Can I transfer money to my securities account in any way?', 'can-i-transfer-money-to-my-securities-account-in-any-way', empty_clob(), 'en', null, 'customer-care/faqs/can-i-transfer-money-to-my-securities-account-in-any-way', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('199', 'Tôi có thể chuyển tiền từ ATM không?', 'toi-co-the-chuyen-tien-tu-atm-khong', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-co-the-chuyen-tien-tu-atm-khong', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('199', 'I can not transfer money from an ATM ?', 'i-can-not-transfer-money-from-an-atm-', empty_clob(), 'en', null, 'customer-care/faqs/i-can-not-transfer-money-from-an-atm-', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('200', 'Hiện tôi đang ở xa, không thể đến trực tiếp MBS được, làm thế nào để lấy mật khẩu truy cập?', 'hien-toi-dang-o-xa-khong-the-den-truc-tiep-mbs-duoc-lam-the-nao-de-lay-mat-khau-truy-cap', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/hien-toi-dang-o-xa-khong-the-den-truc-tiep-mbs-duoc-lam-the-nao-de-lay-mat-khau-truy-cap', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('200', 'I am currently away , unable to directly MBS is , how to get access password?', 'i-am-currently-away-unable-to-directly-mbs-is-how-to-get-access-password', empty_clob(), 'en', null, 'customer-care/faqs/i-am-currently-away-unable-to-directly-mbs-is-how-to-get-access-password', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('201', 'Đã lâu tôi không vào tài khoản, hiện tài khoản bị khóa, anh chị có thể mở tài khoản cho tôi được không?', 'da-lau-toi-khong-vao-tai-khoan-hien-tai-khoan-bi-khoa-anh-chi-co-the-mo-tai-khoan-cho-toi-duoc-khong', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/da-lau-toi-khong-vao-tai-khoan-hien-tai-khoan-bi-khoa-anh-chi-co-the-mo-tai-khoan-cho-toi-duoc-khong', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('201', 'I no longer have to account , the account is locked , you can open his account for me?', 'i-no-longer-have-to-account-the-account-is-locked-you-can-open-his-account-for-me', empty_clob(), 'en', null, 'customer-care/faqs/i-no-longer-have-to-account-the-account-is-locked-you-can-open-his-account-for-me', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('202', 'Tôi có thể bán khống chứng khoán được không?', 'toi-co-the-ban-khong-chung-khoan-duoc-khong', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-co-the-ban-khong-chung-khoan-duoc-khong', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('202', 'I can not short sell securities?', 'i-can-not-short-sell-securities', empty_clob(), 'en', null, 'customer-care/faqs/i-can-not-short-sell-securities', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('203', 'Vì sao tôi kê lệnh không được?', 'vi-sao-toi-ke-lenh-khong-duoc', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/vi-sao-toi-ke-lenh-khong-duoc', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('203', 'Why should I not be ordered?', 'why-should-i-not-be-ordered', empty_clob(), 'en', null, 'customer-care/faqs/why-should-i-not-be-ordered', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('204', 'Tối muốn ứng trước tiền bán chứng khoán MBS thì có phải đến trực tiếp MBS không?', 'toi-muon-ung-truoc-tien-ban-chung-khoan-mbs-thi-co-phai-den-truc-tiep-mbs-khong', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-muon-ung-truoc-tien-ban-chung-khoan-mbs-thi-co-phai-den-truc-tiep-mbs-khong', '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('204', 'I want cash advance MBS securities shall have no right to direct MBS?', 'i-want-cash-advance-mbs-securities-shall-have-no-right-to-direct-mbs', empty_clob(), 'en', null, 'customer-care/faqs/i-want-cash-advance-mbs-securities-shall-have-no-right-to-direct-mbs', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('205', 'MBS tổ chức Hội thảo MBS’s Talk 5', 'mbs-to-chuc-hoi-thao-mbs’s-talk-5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.', 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai/mbs-to-chuc-hoi-thao-mbs’s-talk-5', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('205', 'MBS open Workshop MBS ''s Talk 5', 'mbs-open-workshop-mbs-s-talk-5', empty_clob(), 'en', null, 'customer-care/promotion/mbs-open-workshop-mbs-s-talk-5', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('206', 'MBS tổ chức thành công Hội thảo MBS’s Talk 5', 'mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai/mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('206', 'MBS tổ chức thành công Hội thảo MBS’s Talk 5', 'mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', empty_clob(), 'en', null, 'customer-care/promotion/mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('207', 'MBS tổ chức Hội thảo MBS’s Talk 6 với chủ để "Cơ hội trong khủng hoảng"', 'mbs-to-chuc-hoi-thao-mbs’s-talk-6-voi-chu-de-co-hoi-trong-khung-hoang', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai/mbs-to-chuc-hoi-thao-mbs’s-talk-6-voi-chu-de-co-hoi-trong-khung-hoang', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('207', 'MBS tổ chức Hội thảo MBS’s Talk 6 với chủ để "Cơ hội trong khủng hoảng"', 'mbs-to-chuc-hoi-thao-mbs’s-talk-6-voi-chu-de-co-hoi-trong-khung-hoang', empty_clob(), 'en', null, 'customer-care/promotion/mbs-to-chuc-hoi-thao-mbs’s-talk-6-voi-chu-de-co-hoi-trong-khung-hoang', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('208', 'MBS thông báo thay đổi địa điểm đào tạo buổi thứ 3 của lớp A1', 'mbs-thong-bao-thay-doi-dia-diem-dao-tao-buoi-thu-3-cua-lop-a1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', null, 'cham-soc-khach-hang/chuong-trinh-uu-dai/mbs-thong-bao-thay-doi-dia-diem-dao-tao-buoi-thu-3-cua-lop-a1', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('208', 'MBS thông báo thay đổi địa điểm đào tạo buổi thứ 3 của lớp A1', 'mbs-thong-bao-thay-doi-dia-diem-dao-tao-buoi-thu-3-cua-lop-a1', empty_clob(), 'en', null, 'customer-care/promotion/mbs-thong-bao-thay-doi-dia-diem-dao-tao-buoi-thu-3-cua-lop-a1', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('191', 'Mở tài khoản online', 'mo-tai-khoan-online', 'Open24 là dịch vụ mở tài khoản trực tuyếncủa MBS dành cho khách hàng cá nhân trong nước. Với giao diện thân thiện, Quý khách hàng sẽ dễ dàng hoàn tất việc khai báo thông tin trực tuyến chỉ trong 1 bước thông qua trang web http://open24.mbs.com.vn.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/mo-tai-khoan-online', '<p style="text-align: justify;"><span style="">Open24 là dịch vụ mở tài khoản trực tuyếncủa MBS dành cho khách hàng cá nhân trong nước. Với giao diện thân thiện, Quý khách hàng sẽ dễ dàng hoàn tất việc khai báo thông tin trực tuyến chỉ trong 1 bước thông qua trang web</span><a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx" style=""> http://open24.mbs.com.vn</a><span style="">.. Sử dụng Open24, Quý khách hàng có thể:</span></p>

<ul style="margin-left: 40px;">
	<li style="text-align: justify;">Mở tài khoản chứng khoán một cách nhanh chóng, thuận tiện</li>
	<li style="text-align: justify;">Tự chọn số tài khoản giao dịch &nbsp;</li>
	<li style="text-align: justify;">Đăng ký dịch vụ giao dịch chứng khoán, giao dịch tiền và quản lý tài khoản trực tuyến để nhận mức phí giao dịch ưu đãi</li>
	<li style="text-align: justify;">Đăng ký dịch vụ giao dịch chứng khoán qua điện thoại</li>
	<li style="text-align: justify;">Đăng ký sử dụng dịch vụ chứng khoán và tiện ích khác của MBS&nbsp;</li>
	<li style="text-align: justify;">Hưởng dịch vụ chăm sóc khách hàng chuyên nghiệp qua Trung tâm Khách hàng Bán lẻ &nbsp;của MBS</li>
</ul>

<p><u style="text-align: justify; line-height: 1.6em;"><strong>Hướng dẫn mở tài khoản trực tuyến:&nbsp;</strong></u></p>

<ul style="margin-left: 40px;">
	<li><span style="text-align: justify; line-height: 1.6em;">Khách hàng đăng nhập vào Open24 qua địa chỉ: http://open24.mbs.com.vn</span></li>
	<li><span style="text-align: justify; line-height: 1.6em;">Sau khi hoàn tất việc khai báo thông tin mở tài khoản, nhập chuỗi ngẫu nhiên xác thực tài khoản, tích chọn mục “Tôi đồng ý với các điều khoản và điều kiện ở trên”, Quý khách hàng bấm “Cập nhật”. Số tài khoản của Quý khách hàng sẽ được ghi nhận thành công trên hệ thống của MBS và Quý khách hàng sẽ nhận được thông tin về số tài khoản, mật khẩu và PIN bằng SMS qua số điện thoại di động đã đăng ký.</span></li>
	<li><span style="text-align: justify; line-height: 1.6em;">Quý khách hàng in 2 bộ “Yêu cầu mở tài khoản và đăng ký sử dụng dịch vụ giao dịch chứng khoán” &nbsp;và mang tới các Điểm giao dịch trực tuyến hoặc Phòng giao dịch của MBS &nbsp;để được kích hoạt tài khoản.&nbsp;</span></li>
</ul>

<p style="text-align: justify;">Trong trường hợp Quý khách hàng muốn lấy lại &nbsp;bộ “Yêu cầu mở tài khoản và đăng ký sử dụng dịch vụ giao dịch chứng khoán”, tại trang web: <a href="http://open24.mbs.com.vn/Presentation/Welcome.aspx">http://open24.mbs.com.vn</a>, Quý khách hàng chọn “Tìm kiếm thông tin tài khoản”, nhập số CMTND và PIN khi thực hiện mở tài khoản qua Open24, bấm “Tìm kiếm” hệ thống sẽ kiểm tra và hiển thị số tài khoản Quý khách hàng đã mở thành công tại Open24. Quý khách hàng có thể sửa lại các thông tin khi mở tài khoản, và in lại hồ sơ.&nbsp;</p>

<p style="text-align: justify;"><strong><em>Lưu ý:</em></strong>&nbsp;<br />
Trong trường hợp tài khoản đã được kích hoạt tại MBS, Quý khách hàng sẽ không thể sửa lại thông tin cá nhân tại Open24. Nếu muốn thay đổi thông tin, Quý khách hàng vui lòng đến các điểm giao dịch của MBS để thực hiện thay đổi.<br />
Để tìm kiếm lại thông tin tài khoản và in lại hồ sơ mở tài khoản, Quý khách hàng cần lưu lại mã PIN. &nbsp;</p>
', null, '/uploads/files/84f93e6a35f34ccd8cd0c8c71447e9c4_1383535933(1).pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('191', 'Register account online', 'register-account-online', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/register-account-online', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('192', 'Open an account assets', 'open-an-account-assets', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/open-an-account-assets', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('73', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', 'uploads/article/2015/3/06032015_132046_PM1.jpg', 'quan-he-co-dong/tin-co-dong/lorem-ipsum-dolor-sit-amet', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('74', 'Morbi in sem quis dui placerat ornare', 'morbi-in-sem-quis-dui-placerat-ornare', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/morbi-in-sem-quis-dui-placerat-ornare', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('193', 'Tôi thực hiện chuyển tiền từ tài khoản chứng khoán sang MB tại sao không được?', 'toi-thuc-hien-chuyen-tien-tu-tai-khoan-chung-khoan-sang-mb-tai-sao-khong-duoc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-thuc-hien-chuyen-tien-tu-tai-khoan-chung-khoan-sang-mb-tai-sao-khong-duoc', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('193', 'I made an transfer money from the stock account to the MB why not ?', 'i-made-an-transfer-money-from-the-stock-account-to-the-mb-why-not-', empty_clob(), 'en', null, 'customer-care/faqs/i-made-an-transfer-money-from-the-stock-account-to-the-mb-why-not-', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('194', 'Tôi muốn có báo cáo cổ phiếu theo yêu cầu thì có được không?', 'toi-muon-co-bao-cao-co-phieu-theo-yeu-cau-thi-co-duoc-khong', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.', 'vi', null, 'cham-soc-khach-hang/cau-hoi-thuong-gap/toi-muon-co-bao-cao-co-phieu-theo-yeu-cau-thi-co-duoc-khong', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('194', 'I wanted to share reports on demand , there is not ?', 'i-wanted-to-share-reports-on-demand-there-is-not-', empty_clob(), 'en', null, 'customer-care/faqs/i-wanted-to-share-reports-on-demand-there-is-not-', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('217', 'MBS giới thiệu Sản phẩm Chiến lược đầu tư tại Hội thảo MBS’s Talk 3', 'mbs-gioi-thieu-san-pham-chien-luoc-dau-tu-tai-hoi-thao-mbs’s-talk-3', 'Tiếp nối thành công của hội thảo MBS’s Talk 1 và 2, ngày 31/7 và 1/8/2013, MBS đã tổ chức Hội thảo MBS’s Talk 3 với chủ đề “Thị trường và cơ hội đầu tư 6 tháng cuối năm 2013” tại TP. HCM và Hà Nội. Như thường lệ, hội  thảo đã nhận được sự quan tâm của nhiều nhà đầu tư tại cả hai thị trường phía bắc và phía nam.', 'vi', '/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk3.JPG', 'cham-soc-khach-hang/hoi-thao-dao-tao/mbs-gioi-thieu-san-pham-chien-luoc-dau-tu-tai-hoi-thao-mbs’s-talk-3', '<p style="text-align: justify;">Tiếp nối thành công của hội thảo MBS’s Talk 1 và 2, ngày 31/7 và 1/8/2013, MBS đã tổ chức Hội thảo MBS’s Talk 3 với chủ đề “Thị trường và cơ hội đầu tư 6 tháng cuối năm 2013” tại TP. HCM và Hà Nội. Như thường lệ, hội &nbsp;thảo đã nhận được sự quan tâm của nhiều nhà đầu tư tại cả hai thị trường phía bắc và phía nam.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Tại hội thảo, MBS đã giới thiệu Sản phẩm Chiến lược đầu tư. Đây là một trong những sản phẩm nghiên cứu riêng và quan trọng của MBS phù hợp với từng mức độ rủi ro của Nhà đầu tư. Đặc biệt, sản phẩm này được phát triển sau khi MBS giới thiệu với Nhà đầu tư hệ thống giao dịch Stock24 - hệ thống giao dịch do MBS phát triển và thường xuyên nâng cấp, bổ sung các tính năng tiện ích cho Nhà đầu tư thuận tiện trong giao dịch. Hệ thống này giúp Nhà đầu tư không chỉ tìm hiểu và tra cứu thông tin thị trường/giao dịch mà còn được tư vấn trực tiếp bởi các chuyên gia của MBS. Nhận định thị trường Quý III/2013, MBS cho rằng thị trường sẽ đi ngang trong khoảng 490 (+/-) 5% với độ rộng dao động 470-530 và đến thời điểm cuối năm cơ hội đầu tư sẽ rõ ràng hơn.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">MBS''s Talk 4 dự kiến sẽ tiếp tục được tổ chức trong quý IV/2013 tại Hà Nội và TP. HCM. Đây là hội thảo định kỳ hàng quý dành cho Nhà đầu tư của MBS. Tại mỗi kỳ hội thảo, MBS cam kết sẽ giới thiệu tới các Nhà đầu tư các sản phẩm mới hoặc ý tưởng chiến lược đầu tư mới.</p>

<p style="text-align: justify;">Thông qua đây, MBS muốn thể hiện rõ quyết tâm khẳng định vị thế của mình thông qua chất lượng sản phẩm nghiên cứu và dịch vụ chứng khoán cung cấp tới Khách hàng trong bối cảnh cạnh tranh ngày càng khốc liệt giữa các Công ty chứng khoán tại Việt Nam</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk3.JPG" style="width: 490px; height: 325px;" />.</p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk3-1.JPG" style="width: 490px; height: 368px;" /></p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('226', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2014', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2014', '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('227', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2014', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('227', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2014', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', empty_clob(), 'en', null, 'investor-relations/financial-statements/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2014', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('210', 'MBS mở lớp đào tạo chứng khoán miễn phí cho nhà đầu tư', 'mbs-mo-lop-dao-tao-chung-khoan-mien-phi-cho-nha-dau-tu', 'Chương trình đầu tiên sẽ thực hiện tại Hà Nội cho các nhà đầu tư phía Bắc. Chương trình đào tạo cho các nhà đầu tư khu vực phía Nam dự kiến sẽ tổ chức sau khu vực phía Bắc một tuần. Điểm khác biệt so với các đơn vị khác đó là MBS là công ty chứng khoán đầu tiên tổ chức đào tạo phân tích chuyên sâu với nội dung mang tính thực tiễn cao và dễ dàng được áp dụng, được trình bày...', 'vi', '/uploads/images/uu-dai/work-3.jpg', 'cham-soc-khach-hang/chuong-trinh-uu-dai/mbs-mo-lop-dao-tao-chung-khoan-mien-phi-cho-nha-dau-tu', '<p><img alt="" src="/uploads/images/uu-dai/work-3.jpg" style="width: 745px; height: 260px;" /></p>

<p>&nbsp;</p>

<p>Công ty chứng khoán MB (MBS) cho biết, sẽ tổ chức khóa đào tạo phân tích chứng khoán cơ bản và chuyên sâu dành cho nhà đầu tư toàn quốc.</p>

<p>Chương trình đầu tiên sẽ thực hiện tại Hà Nội cho các nhà đầu tư khu vực phía Bắc. Chương trình đào tạo cho các nhà đầu tư khu vực phía Nam dự kiến sẽ tổ chức sau khu vực phía Bắc 1 tuần. Điểm khác biệt so với các đơn vị khác đó là MBS là công ty chứng khoán đầu tiên tổ chức đào tạo phân tích chuyên sâu với nội dung mang tính thực tiễn cao và dễ dàng áp dụng, được trình bày bởi các chuyên gia nghiên cứu cao cấp của MBS có trình độ, nhiều kinh nghiệm và trực tiếp tham gia thị trường. Đặc biệt, tại các buổi đào tạo thứ Bảy hàng tuần, các chuyên gia MBS sẽ thảo luận về thị trường và phân tích thực tế về một số công ty niêm yết khi thời điểm công bố báo cáo quý II đang đến gần.</p>

<p>Thời gian đào tạo vào thứ Bảy và Chủ nhật hàng tuần, bắt đầu từ 18/7/2014 tại Hội trường Tầng 11, Tòa nhà MB, số 3 Liễu Giai, Ba Đình, Hà Nội. Các nhà đầu tư muốn tham dự khóa học có thể đăng ký trước với MBS qua tổng đài 1900 9088.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('210', 'MBS mở lớp đào tạo chứng khoán miễn phí cho nhà đầu tư', 'mbs-mo-lop-dao-tao-chung-khoan-mien-phi-cho-nha-dau-tu', empty_clob(), 'en', null, 'customer-care/promotion/mbs-mo-lop-dao-tao-chung-khoan-mien-phi-cho-nha-dau-tu', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('211', 'MBS tổ chức Hội thảo MBS’s Talk 7', 'mbs-to-chuc-hoi-thao-mbs’s-talk-7', 'Mở đầu năm mới 2015, MBS đã tổ chức thành công hội thảo MBS’s Talk 7 với chủ đề “Thị trường chứng khoán 2015 – Cơ hội và thách thức” tại cả hai địa điểm Hà Nội (07/01/2015) và TP. Hồ Chí Minh (08/01/2015).', 'vi', '/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk7-1.JPG', 'cham-soc-khach-hang/hoi-thao-dao-tao/mbs-to-chuc-hoi-thao-mbs’s-talk-7', '<p>Mở đầu năm mới 2015, MBS đã tổ chức thành công hội thảo MBS’s Talk 7 với chủ đề <strong>“Thị trường chứng khoán 2015 – Cơ hội và thách thức”</strong> tại cả hai địa điểm Hà Nội (07/01/2015) và TP. Hồ Chí Minh (08/01/2015).</p>

<p>&nbsp;</p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk7-1.JPG" style="width: 100%;" /></p>

<p>&nbsp;</p>

<p style="text-align: justify;">Tại hội thảo, các chuyên gia nghiên cứu cao cấp của MBS đã mang đến cho nhà đầu tư tham dự một cái nhìn tổng quan về tình hình kinh tế Thế giới và Việt Nam, đồng thời phân tích những triển vọng và thách thức với thị trường chứng khoán. MBS nhận định kinh tế Việt Nam đang trong xu hướng đi lên vững chắc với tăng trưởng GDP cao hơn và lạm phát thấp hơn. Chuyên gia MBS dự báo 6 tháng đầu năm 2015, Vn-Index chinh phục thành công ngưỡng 600 điểm và hình thành xu hướng hồi phục rõ nét, điểm mua vào trong vùng 520 điểm là khá an toàn. Ngoài ra, cổ phiếu BĐS được MBS đánh giá là nhóm cổ phiếu tâm điểm đầu tư năm 2015. Cùng với sự hồi phục của nhóm bất động sản, nhóm cổ phiếu xây dựng và vật liệu xây dựng cũng sẽ hồi phục mạnh theo. Ô tô và phụ tùng cũng là nhóm ngành được khuyến nghị trong năm tới với triển vọng sẽ được hưởng lợi từ việc giá dầu giảm.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk7-2.jpg" style="width: 100%;" /></p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Cũng trong buổi hội thảo, các chuyên gia MBS đã giải đáp thắc mắc của nhiều Nhà đầu tư về các mã cổ phiếu ngân hàng, chứng khoán và sự ảnh hưởng của thông tư 36 tới thị trường trong 2 tháng tới.<br />
Chuỗi hội thảo MBS’s Talk đã trở thành một trong những hoạt động định kỳ nhằm gia tăng tiện ích cho khách hàng tại MBS và nhận được sự đón nhận, đánh giá tích cực của các nhà đầu tư chứng khoán trong suốt những năm vừa qua.<br />
Với cột mốc quan trọng đánh dấu 15 năm thành lập, trong năm 2015 sản phẩm dịch vụ của MBS sẽ ngày một được nâng cao, hoàn thiện hơn, xứng với tầm vóc và sự phát triển của thương hiệu MBS.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('211', 'MBS tổ chức Hội thảo MBS’s Talk 7', 'mbs-to-chuc-hoi-thao-mbs’s-talk-7', empty_clob(), 'en', null, 'customer-care/hoi-thao-dao-tao/mbs-to-chuc-hoi-thao-mbs’s-talk-7', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('216', ' MBS tổ chức thành công Hội thảo MBS’s Talk 5', '-mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', 'Tiếp nối thành công của chuỗi hội thảo MBS’s Talk trong năm 2013, ngày 27/02/2014 và 03/03/2014 vừa qua, Công ty CP Chứng khoán MB (MBS) đã tổ chức hội thảo MBS’s Talk 5 với chủ đề “Chứng khoán 2014 – Sự khởi sắc được dự báo trước” tại TP. Hồ Chí Minh và Hà Nội.', 'vi', '/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk5.JPG', 'cham-soc-khach-hang/hoi-thao-dao-tao/-mbs-to-chuc-thanh-cong-hoi-thao-mbs’s-talk-5', '<p style="text-align: justify;">Tiếp nối thành công của chuỗi hội thảo MBS’s Talk trong năm 2013, ngày 27/02/2014 và 03/03/2014 vừa qua, Công ty CP Chứng khoán MB (MBS) đã tổ chức hội thảo MBS’s Talk 5 với chủ đề “Chứng khoán 2014 – Sự khởi sắc được dự báo trước” tại TP. Hồ Chí Minh và Hà Nội.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Tại hội thảo, MBS đã đưa ra những nhận định về bối cảnh đầu tư tại Việt Nam hiện nay, xu hướng của TTCK trong năm 2014 nhằm giúp cho Nhà đầu tư có thêm góc nhìn đánh giá về thị trường chứng khoán năm tới. Theo nghiên cứu của MBS, kinh tế Việt Nam đang dần hồi phục, các chỉ báo về kinh tế vĩ mô như tăng trưởng, lạm phát, tỷ giá, xuất nhập khẩu, … tăng trưởng tích cực hơn so với các năm trước.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Bên cạnh đó, kinh tế thế giới tiếp tục trên đà hồi phục, kết hợp với việc Việt Nam gia nhập TPP vào cuối năm 2014 hoặc đầu 2015 sẽ giúp nguồn vốn ngoại được đẩy mạnh vào thị trường Việt Nam. Việc gia nhập TPP cũng sẽ tạo cơ hội cho các doanh nghiệp xuất khẩu đặc biệt là các ngành có thế mạnh như dệt may, đồ gỗ và da giày. Trong khi đó, ở hệ thống ngân hàng, nợ xấu của hệ thống ngân hàng tiếp tục được giải quyết dần với sự chủ động của các ngân hàng. Điều này khiến tăng trưởng tín dụng được lưu thông. Về thị trường bất động sản, chuyên gia MBS cho là đã chạm đáy và bắt đầu đi lên theo tính chu kỳ của nền kinh tế.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Kinh tế cho dấu hiệu khả quan nhưng đứng trên quan điểm đầu tư, MBS cho rằng năm 2014, dòng vốn nội sẽ chủ yếu tập trung vào thị trường chứng khoán do các lĩnh vực khác như tiết kiệm (lãi suất tiết kiệm giảm mạnh), giá vàng (diễn biến bất thường, chênh lệch với giá vàng thế giới cao), thị trường USD (giá USD biến động nhẹ) và thị trường bất động sản (mới chạm đáy tại một số phân khúc giá thấp) kém hấp dẫn. Theo đó, MBS cho rằng, thị trường chứng khoán năm 2014 sẽ tiếp tục đi lên với khả năng VN-Index sẽ chinh phục ngưỡng 655 điểm.</p>

<p style="text-align: justify;">Những dự báo xu hướng thị trường của MBS dựa trên 3 yếu tố chính là tâm lý thị trường, phân tích kỹ thuật và mô hình doanh thu – lợi nhuận. Bên cạnh đó các chuyên gia MBS cũng đưa ra những khuyến nghị lĩnh vực, cổ phiếu cần quan tâm.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">MBS’s Talk 5 nhận được nhiều quan tâm từ Nhà đầu tư thông qua những câu hỏi NĐT đặt ra cho các chuyên gia tư vấn MBS. Trong năm 2014, MBS cam kết sẽ tiếp tục đổi mới để mang lại cho khách hàng nhiều tiện ích và sản phẩm tối ưu nhất.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"><em>Dưới đây là những hình ảnh tại Hội thảo MBS''s Talk 5</em></p>

<p style="text-align: justify;"><strong>Tại Hồ Chí Minh</strong></p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6-a.jpg" style="width: 640px; height: 213px;" /></p>

<p><strong>Tại Hà Nội</strong></p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6-b.jpg" style="width: 640px; height: 213px;" /></p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('212', 'Đội ngũ', 'doi-ngu', empty_clob(), 'vi', '/uploads/images/dich-vu/dvdt_6.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/doi-ngu', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('212', 'Our team', 'our-team', empty_clob(), 'en', '/uploads/images/dich-vu/dvdt_6.jpg', 'institutional-clients/investment-banking/our-team', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('213', 'Trần Như Ngọc', 'tran-nhu-ngoc', 'Giám đốc chi nhánh Hà Nội<br/>
Điện thoại: +84 4 3726 2600<br/>
Mobile: 0909862486', 'vi', '/uploads/images/doi-ngu/dn-h5.jpg', 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/doi-ngu/tran-nhu-ngoc', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('213', 'Mrs Tran Nhu Ngoc', 'mrs-tran-nhu-ngoc', 'Giám đốc chi nhánh Hà Nội<br/>
Điện thoại: +84 4 3726 2600<br/>
Mobile: 0909862486', 'en', '/uploads/images/doi-ngu/dn-h5.jpg', 'institutional-clients/investment-banking/our-team/mrs-tran-nhu-ngoc', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('214', 'Bùi Quân', 'bui-quan', 'Giám đốc chi nhánh: TP HCM<br/>
Điện thoại: +84 884884<br/>
Mobile: 0909020202', 'vi', null, 'khach-hang-to-chuc/dich-vu-ngan-hang-dau-tu/doi-ngu/bui-quan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', 'Ông', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('214', 'Bui Quan', 'bui-quan', 'Giám đốc chi nhánh: TP HCM<br/>
Điện thoại: +84 884884<br/>
Mobile: 0909020202', 'en', null, 'institutional-clients/investment-banking/our-team/bui-quan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', 'Mr', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('175', 'Chuyển trạng thái chứng khoán', 'chuyen-trang-thai-chung-khoan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/chuyen-trang-thai-chung-khoan', '<div class="item-step-detail">
<div class="title-text">Thực hiện chuyển trạng thái chứng khoán</div>

<div class="info">
<p>Chức năng này hỗ trợ Quý khách hàng việc có thể chuyển đổi trạng thái chứng khoán từ trạng thái thường sang trạng thái Outr ( Chứng khoán ngoài danh mục) hoặc ngược lại.</p>
<img alt="" src="/Assets/images/img/photo-1.jpg" />
<p>Mục đích của Chuyển trạng thái chứng khoán nhằm hỗ trợ Quý khách hàng chủ động trong việc chuyển đổi trạng thái chứng khoán, bổ sung tài sản đảm bảo (chuyển trạng thái Outr sang thường) hoặc giảm Room (chuyển trạng thái thường sang Outr)</p>

<p>Quý khách hàng vào menu phụ Chuyển trạng thái CK</p>
</div>
</div>

<div class="item-step-detail">
<div class="title-text">Thực hiện chuyển trạng thái chứng khoán</div>

<div class="info">
<p>Chức năng này hỗ trợ Quý khách hàng việc có thể chuyển đổi trạng thái chứng khoán từ trạng thái thường sang trạng thái Outr ( Chứng khoán ngoài danh mục) hoặc ngược lại.</p>

<p>Mục đích của Chuyển trạng thái chứng khoán nhằm hỗ trợ Quý khách hàng chủ động trong việc chuyển đổi trạng thái chứng khoán, bổ sung tài sản đảm bảo (chuyển trạng thái Outr sang thường) hoặc giảm Room (chuyển trạng thái thường sang Outr)</p>

<p>Quý khách hàng vào menu phụ Chuyển trạng thái CK</p>
</div>
<img alt="" src="/Assets/images/img/photo-1.jpg" /></div>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('223', 'Nguyễn Kim Chung', 'nguyen-kim-chung', 'Thành viên Ban Kiểm soát', 'vi', '/uploads/images/doi-ngu/doingu-chiChung.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-kiem-soat/nguyen-kim-chung', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 12px; line-height: normal; text-align: justify;">Bà Nguyễn Kim Chung hiện là thành viên Ban Kiểm soát Công ty CP Chứng khoán&nbsp;MB (MBS). Bà Chung có bằng Thạc sỹ Kinh tế. Trước khi gia nhập MBS, bà đã từng làm Kế toán Công ty cổ phần Intimex Việt Nam. Tại MBS, bà Chung đã đảm nhiệm nhiều vị trí như Kế toán, Kiểm soát, Trưởng sàn giao dịch Hoàng Quốc Việt, Trưởng phòng Nghiệp vụ 16.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('224', ' MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính bán niên năm 2013', '-mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-ban-nien-nam-2013', empty_clob(), 'en', null, 'investor-relations/shareholder-news/-mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-ban-nien-nam-2013', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('215', ' MBS tổ chức thành công Hội thảo MBS''s Talk 6', '-mbs-to-chuc-thanh-cong-hoi-thao-mbss-talk-6', '“Cơ hội trong khủng hoảng” là chủ đề hội thảo MBS’s Talk 6 vừa được CTCP Chứng khoán MB (MBS) tổ chức cho các Nhà đầu tư trong 2 ngày 04/06 tại Hà Nội và 05/06 tại TP. HCM. ', 'vi', '/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6.jpg', 'cham-soc-khach-hang/hoi-thao-dao-tao/-mbs-to-chuc-thanh-cong-hoi-thao-mbss-talk-6', '<p style="text-align: justify;">“Cơ hội trong khủng hoảng” là chủ đề hội thảo MBS’s Talk 6 vừa được CTCP Chứng khoán MB (MBS) tổ chức cho các Nhà đầu tư trong 2 ngày 04/06 tại Hà Nội và 05/06 tại TP. HCM.&nbsp;<br />
Xoay quanh chủ đề nóng “Sự kiện Biển Đông”, các chuyên gia phân tích của CTCP Chứng khoán MB đã phân tích những tác động của sự kiện tranh chấp trên Biển Đông đến nền kinh tế Việt Nam và nhận định TTCK vẫn sẽ là kênh sinh lời hấp dẫn nhất trong 6 tháng cuối năm 2014. Xu hướng tăng điểm từ đầu năm là khá vững, trong khi sự dao động tiêu cực vừa qua có nguyên nhân từ việc Trung Quốc hạ đặt giàn khoan trái phép trên vùng biển đặc quyền kinh tế của Việt Nam chỉ là xáo trộn ngắn hạn.<br />
Tương tự với bối cảnh hiện tại của Việt Nam, chuyên gia phân tích MBS đưa ra so sánh về phản ứng của TTCK Philipines 2 năm trước đó: “Khi Philippines xảy ra sự việc tranh chấp lãnh thổ tương tự vào năm 2012, thị trường không có một phản ứng đáng kể nào, thậm chí còn đi lên suốt tháng, sau đó xuất hiện một nhịp điều chỉnh nhẹ rồi tiếp tục đi lên cho đến hết năm”.</p>

<p style="text-align: justify;"><br />
Cũng trong hội thảo này, các chuyên gia phân tích của MBS còn đưa ra những cơ hội của thị trường trong 6 tháng cuối năm 2014. Cụ thể, từ nay đến trung tuần tháng 7, thị trường sẽ sụt giảm nhẹ để NĐT tích lũy, sau đó bắt đầu đà phục hồi. Tuy nhiên, sự phục hồi này được dự báo là không quá lớn. Khuyến cáo về các ngành triển vọng trong năm 2014, chuyên gia của MBS lưu ý đến các ngành như dược phẩm, săm lốp, xây dựng và vật liệu xây dựng, BĐS.</p>

<p style="text-align: justify;"><br />
Hội thảo MBS’s Talk 6 kết thúc thành công và được nhiều Nhà đầu tư đánh giá rất cao về tính chuyên môn và mức độ bám sát, cập nhật so với diễn biến thị trường.<br />
Sau đây là một số hình ảnh tại Hội thảo:</p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6-1.jpg" style="width: 640px; height: 213px;" /></p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6-2.jpg" style="width: 640px; height: 213px;" /></p>

<p style="text-align: center;"><img alt="" src="/uploads/images/cham-soc-khach-hang/hoi-thao-dao-tao/talk6-3.jpg" style="width: 640px; height: 213px;" /></p>

<p>&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('219', 'Trần Khắc Hậu', 'tran-khac-hau', 'Phó Tổng Giám đốc', 'vi', '/uploads/images/doi-ngu/do-ngu-anh-Hau.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-tong-giam-doc/tran-khac-hau', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 12px; line-height: normal; text-align: justify;">Ông Trịnh Khắc Hậu, Tiến sỹ Kinh tế, hiện là Phó Tổng Giám đốc của MBS. Ông Hậu tốt nghiệp trường Đại học Quản lý Kinh tế Moscow, khoa Tổ chức và Quản lý Kinh tế trong Xây dựng và Kinh tế thành phố và có nhiều năm kinh nghiệm làm quản lý tại một số doanh nghiệp lớn, đặc biệt trong lĩnh vực tài chính và tài trợ dự án tại Liên bang Nga. Ông Hậu đã từng giữ các chức vụ quan trọng như Giám đốc Chi nhánh Ekateringburg và Chi nhánh Ural tại Tập đoàn King Lion - CH Liên bang Nga, Phó phòng Quản lý dự án tại Ngân hàng TMCP Quân đội (MB)&nbsp;và Giám đốc Kinh doanh Công ty Cổ phần Phú Quý tại Nha Trang.</span></p>
', 'Ông', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('75', 'Phasellus ultrices nulla quis nibh', 'phasellus-ultrices-nulla-quis-nibh', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, ne...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/phasellus-ultrices-nulla-quis-nibh', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('221', 'Nguyễn Thanh Bình', 'nguyen-thanh-binh', 'Trưởng Ban Kiểm Soát', 'vi', '/uploads/images/doi-ngu/doingu-chiBinh.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-kiem-soat/nguyen-thanh-binh', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma, sans-serif; font-size: 12px; line-height: normal; text-align: justify;">Bà Nguyễn Thanh Bình hiện là&nbsp;Trưởng Ban Kiểm soát Công ty CP Chứng khoán&nbsp;MB (MBS), thành viên Ban Kiểm soát Ngân hàng TMCP Quân Đội (MB). Bà Bình tốt nghiệp Trường ĐH Kinh tế Praha, Khoa Kinh tế Quốc dân, chuyên ngành Tài chính. Trước khi trở thành thành viên Ban Kiểm soát MB, bà Bình đã có 20 năm kinh nghiệm ở vị trí quản lý Khối Tài chính Doanh nghiệp.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('222', 'Nguyễn Thị Diệp Quỳnh', 'nguyen-thi-diep-quynh', 'Thành viên Ban Kiểm Soát', 'vi', '/uploads/images/doi-ngu/doingu-chiDiep.jpg', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-kiem-soat/nguyen-thi-diep-quynh', '<p><span style="color: rgb(1, 1, 1); font-family: tahoma; font-size: 12px; line-height: 13.8000001907349px; text-align: justify;">Bà Nguyễn Thị Diệp Quỳnh tốt nghiệp trường Đại học Kinh tế Quốc dân. Bà Quỳnh có 7 năm kinh tại Công ty Quản lý quỹ đầu tư MB (MB Capital) và Ngân hàng TMCP Quân đội (MB).&nbsp;</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('224', ' MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính bán niên năm 2013', '-mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-ban-nien-nam-2013', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/-mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-ban-nien-nam-2013', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('220', 'Phùng Thị Thanh Hà', 'phung-thi-thanh-ha', 'Phó Tổng Giám đốc', 'vi', '/uploads/images/doi-ngu/doi-ngu-chi-Ha.JPG', 'gioi-thieu-chung/doi-ngu-lanh-dao/ban-tong-giam-doc/phung-thi-thanh-ha', '<p><span style="color: rgb(1, 1, 1); font-family: Tahoma, Verdana; font-size: 12px; line-height: normal; text-align: justify;">Bà Phùng Thị Thanh Hà là Thạc sỹ kinh tế tại trường Đại học Méditerranée – Aix Marseille II (Pháp), khoa Quản trị doanh nghiệp. Bà Hà là người có nhiều năm kinh nghiệm chuyên môn trong lĩnh vực tài chính ngân hàng và từng giữ nhiều vị trí quản lý như Trưởng Bộ phận Thẩm định Khách hàng cá nhân, Trưởng Bộ phận Thẩm định Khách hàng Doanh nghiệp vừa và nhỏ; Phó phòng Thẩm định Hội sở tại Ngân hàng TMCP Quân đội (MB) và Trưởng Phòng Quản trị rủi ro tại Công ty CP Chứng khoán MB (MBS). Bà Phùng Thị Thanh Hà chính thức tiếp quản vị trí Phó Tổng Giám đốc MBS kể từ ngày 5/7/2013.</span></p>
', 'Bà', null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('225', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2013', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2013', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2013', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('225', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2013', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2013', empty_clob(), 'en', null, 'investor-relations/shareholder-news/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2013', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('253', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/tin-mbs/lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pedeid cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('254', 'Donec odio. Quisque volutpat mattis eros', 'donec-odio-quisque-volutpat-mattis-eros', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/tin-mbs/donec-odio-quisque-volutpat-mattis-eros', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pedeid cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('255', '"Nghịch lý GDP tăng, chứng khoán giảm"', 'nghich-ly-gdp-tang-chung-khoan-giam', 'Trong khi Chính phủ và cả nền kinh tế hào hứng với tốc độ tăng trưởng GDP quý I năm nay vượt trên 6% và cao hơn so với cùng kỳ năm trước, thì TTCK lại chứng kiến đà giảm liên tục, với thanh khoản nhiều phiên giảm tới 60-70% so với cùng kỳ. Được mệnh danh là phong vũ biểu của nền kinh tế, chứng khoán Việt đi ngược với diễn biến vĩ mô, vì đâu?', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/tin-mbs/nghich-ly-gdp-tang-chung-khoan-giam', '<p>&nbsp;</p>

<p>Trong khi Chính phủ và cả nền kinh tế hào hứng với tốc độ tăng trưởng GDP quý I năm nay vượt trên 6% và cao hơn so với cùng kỳ năm trước, thì TTCK lại chứng kiến đà giảm liên tục, với thanh khoản nhiều phiên giảm tới 60-70% so với cùng kỳ. Được mệnh danh là phong vũ biểu của nền kinh tế, chứng khoán Việt đi ngược với diễn biến vĩ mô, vì đâu?</p>

<p>&nbsp;</p>

<p>Chứng khoán giảm là không thực</p>

<p><br />
Trong khi nhiều TTCK khu vực như Nhật Bản, Trung Quốc, Singapore... tăng điểm, thì TTCK Việt Nam lại trong xu hướng giảm dài với thanh khoản giảm đến mức báo động. Diễn biến này xảy ra trong bối cảnh TTCK VIệt Nam đang có định giá khá rẻ so với nhiều TTCK. Nếu như chỉ số PE của TTCK Nhật hiện 22,45 lần, của TTCK Thượng Hải là 19,66 lần, của Mỹ là 18,35 lần, thì tại sàn HOSE, PE bình quân chỉ có 12,27 lần, còn PE tại sàn Hà Nội chỉ có 11,1 lần.</p>

<p><br />
Chưa kể đến điểm số, những tháng đầu năm 2014, thanh khoản trung bình trên TTCK đạt gần 5.000 tỷ đồng/phiên, thì hiện nay, giá trị giao dịch nhiều phiên chỉ còn từ 1.500 - 2.000 tỷ đồng, tính trên cả 2 sàn. Thanh khoản rơi sâu, thị trường ảm đạm, đặt nhà đầu tư và CTCK trong tình trạng gian khó. Trong khi nền kinh tế vĩ mô khởi sắc thì nhiều CTCK dự kiến sẽ phải báo lỗ trong quý I năm nay.</p>

<p><br />
Bình luận về hiện trạng TTCK, bà Cao Thị Thúy Nga, Chủ tịch CTCK MBS cho rằng, thị trường giảm nhiều như hiện nay là không thực. Nhìn rộng ra, các yếu tố kinh tế vĩ mô của Việt Nam và thế giới, sự chuyển động của quá trình hội nhập của Việt Nam, đều có diễn biến tích cực. Bản thân nhiều DN trên sàn hiệu quả kinh doanh vẫn vững, ngay cả khối DN ngành dầu khí, dù gặp cú sốc giảm giá dầu vừa qua, nhưng doanh thu, lợi nhuận năm 2014 ổn định, doanh thu quí 1 ảnh hưởng không nhiều, ảnh hưởng của Thông tư 36 đã phản ánh vào thị trường từ tháng 11 năm 2014.</p>

<p><br />
Đó là những lý do Chủ tịch MBS tin rằng, TTCK sẽ sớm trở lại trạng thái cân bằng và dòng tiền sẽ vẫn tìm đến kênh chứng khoán, do các kênh đầu tư khác như vàng, bất động sản hay gửi tiết kiệm... đều kém hấp dẫn hơn.</p>

<p><br />
Đà giảm của TTCK lần này, theo Chủ tịch MBS, rất khác so với diễn biến năm 2011. Nếu như năm 2011, nền kinh tế chìm trong khó khăn do ảnh hưởng của suy thoái kinh tế toàn cầu, khiến TTCK ảm đạm thì nay, chứng khoán phủ màu đỏ rực trong một nền kinh tế đang tăng trưởng và nó sẽ ảnh hưởng tới tiến trình cổ phần hóa doanh nghiệp đang đươ Chính phủ thúc đẩy tiến độ.</p>

<p><br />
Bộ trưởng Bộ Kế hoạch và Đầu tư Bùi Quang Vinh, trong dịp trò chuyện với các đối tác của Báo Đầu tư cuối tháng 3 vừa qua đã nhận định, nền kinh tế Việt Nam năm nay đủ sức tăng trưởng ở mức 6,2-6,3%, thậm chí có thể vượt lên mức cao hơn, 6,5% và trở lại đà tăng trưởng cao của những năm trước khủng hoảng tài chính quốc tế, nếu không có biến cố bất thường.</p>

<p>Bộ trưởng chia sẻ thông điệp quyết tâm cải cách nền kinh tế, cải tạo môi trường đầu tư của Chính phủ để năm 2016, Việt Nam vượt lên, xếp hạng về môi trường đầu tư tốt ngang với 3 nước phát triển nhất khu vực là Singapore, Malaysia và Thái Lan. Điểm tựa cũng như tâm điểm của nỗ lực cải cách, theo Bộ trưởng, chính là các DN, đặc biệt là khối DN thuộc khu vực kinh tế tư nhân.</p>

<p><br />
Vậy tại sao sự khởi sắc của nền kinh tế, sự nỗ lực của Chính phủ, không được thể hiện tương đồng với diễn biến của TTCK? Khảo sát của ĐTCK với nhiều CTCK cho thấy, yếu tố ảnh hưởng trực tiếp là Thông tư 36/2014/TT-NHN. Khó khăn ngày càng ngấm sâu vào TTCK, khi dòng tiền từ các ngân hàng bị khống chế quá chặt (từ mức được cho vay tối đa 20% vốn điều lệ, xuống còn 5% vốn điều lệ với đầu tư cổ phiếu). Các ngân hàng buộc phải thu hồi tiền đã cho vay mua cổ phiếu, đồng thời không thể thực hiện các khoản cho vay mới.</p>

<p><br />
Trên cương vị Phó chủ tịch Hiệp hội Kinh doanh Chứng khoán (VASB), ông Phan Quốc Huỳnh cho rằng, dòng tiền từ ngân hàng vào CTCK bị chặt lại, trong khi bản thân CTCK đang phải chịu nhiều ràng buộc, đặc biệt là không được huy động vốn từ các tổ chức, cá nhân khác, chỉ được vay vốn từ ngân hàng với mức vay không quá 3 lần vốn điều lệ, là nguyên nhân trực tiếp dẫn đến sự suy giảm của dòng chảy vốn trên TTCK. Thanh khoản chứng khoán, vì thế giảm mạnh, nhiều nhà đầu tư, nhiều CTCK rơi vào khó khăn.</p>

<p><br />
“Với quy mô vốn hóa trên 100.000 tỷ đồng, mỗi ngày TTCK giảm 1% là toàn thị trường, hay cụ thể hơn là tài sản của nhà đầu tư, cổ đông mất đi cả nghìn tỷ đồng”, ông Huỳnh nói.</p>

<p><br />
Vì sao các ngân hàng thừa tiền, ứ đọng vốn mà lại bị co hẹp không gian cho vay đầu tư cổ phiếu, trong khi đây là lĩnh vực cho vay tiềm năng, các CTCK đã có công cụ kiểm soát và thu hồi vốn tốt hơn thời kỳ trước rất nhiều và TTCK đang rất cần sôi động để tạo đà cho tiến trình cổ phần hóa năm 2015? Ngân hàng Nhà nước (NHNN), cơ quan ban hành Thông tư 36, đã giải thích nhiều lần, nhưng góc nhìn từ lãnh đạo nhiều CTCK hiện vẫn chờ đợi Chính phủ - với cái nhìn bao quát toàn bộ nền kinh tế, đứng trên Bộ Tài chính và NHNN, sẽ có quan điểm chuẩn hơn trong việc điều tiết các nguồn lực trong nền kinh tế, để đạt các mục tiêu kinh tế lớn của năm.</p>

<p><br />
Phó chủ tịch VASB nhắc lại chia sẻ của Thủ tướng Nguyễn Tấn Dũng khi đến với TTCK đầu năm Ất Mùi: phải cởi trói chính sách, tạo thuận lợi cho DN và TTCK phát triển. Chính sách nào chưa hợp lý, phải điều chỉnh cho hợp lý. “Áp lực hội nhập nền kinh tế, trong đó có thị trường tài chính đang đến rất gần, nếu không điều chỉnh chính sách theo kịp nhu cầu thị trường, chúng ta sẽ thua sớm”, ông Huỳnh nói.</p>

<p><br />
Ảnh hưởng của Thông tư 36, theo nhiều CTCK đã trực tiếp khiến thanh khoản TTCK sụt giảm. Tuy nhiên, khi thanh khoản sụt giảm quá đà đến 60-70%, còn vì nhiều nguyên nhân khác. Ngoài những khó khăn nền kinh tế vẫn phải đối mặt, thì không ít nhà đầu tư đã dần xa rời TTCK khi nhận thấy rất khó phân biệt DN tốt/xấu, có DN truyền thông quá đà để huy động vốn tràn lan, sử dụng không rõ mục đích; nhiều DN biến tướng lợi nhuận từ lỗ sang lãi, từ cá nhân sang pháp nhân và ngược lại, hay gần đây, DN lớn như GAS, PVD... công bố mua cổ phiếu quỹ nhưng thực hiện không đáng kể. Khi khả năng giám sát và chế tài với các hiện tượng tiêu cực trên TTCK không đủ mạnh, cũng sẽ làm niềm tin nhà đầu tư bị xói mòn.</p>

<p><br />
Ngành chứng khoán cần tự cởi trói cho chính mình</p>

<p><br />
Cùng với việc UBCK, Bộ Tài chính cần hoàn thiện pháp lý để chặn những hiện tượng tiêu cực trên TTCK, thì không gian kinh doanh của ngành cần phải mở rộng hơn, để các DN và nhà đầu tư được tự chủ trong kinh doanh, tự tìm cơ hội sống cho chính mình.</p>

<p><br />
Chẳng hạn, quy định về giao dịch ký quỹ. Nhiều CTCK cho rằng, quy định này đã lỗi thời khi UBCK buộc các Sở soát xét danh mục cổ phiếu cho vay ký quỹ theo thông tin quá khứ và 3 tháng soát xét một lần, trong khi đầu tư chứng khoán là đầu tư vào kỳ vọng và CTCK tiếp cận DN niêm yết trực tiếp, cập nhật hơn UBCK rất nhiều. Hơn thế, tỷ lệ bó buộc CTCK chỉ được cho vay ký quỹ tối đa 50% giá trị khoản đầu tư là không phù hợp và can thiệp quá sâu vào nguyên tắc tự chủ, tự chịu trách nhiệm trong kinh doanh của các DN. Quy định này cần được nới lỏng hơn, theo hướng tốt nhất UBCK chỉ đưa ra nguyên tắc chung để quản lý giao dịch ký quỹ, còn cho vay mã nào, tỷ lệ bao nhiêu, nên để CTCK và nhà đầu tư tự thỏa thuận, quyết định.</p>

<p><br />
Điểm thứ hai nên nới rộng không gian tìm vốn mới của CTCK từ các nhà đầu tư tổ chức và cá nhân. Với quy định hiện hành (Thông tư 210/2012/TT-BTC), CTCK chỉ được vay tối đa 3 lần vốn điều lệ và chỉ được vay từ các ngân hàng, khiến nhiều CTCK không thể xoay sở được nguồn tiền mới, đủ đáp ứng nhu cầu giao dịch của nhà đầu tư. Điểm này cần sửa đổi, theo hướng giảm bớt can thiệp hành chính, tăng quyền tự quyết cho CTCK, cho thị trường. UBCK, Bộ Tài chính cần đưa ra những nguyên tắc quản lý, giám sát hoạt động của các thành viên thị trường để giữ trật tự và an toàn hệ thống, nhưng không nên can thiệp quá sâu vào hoạt động tài chính của mỗi chủ thể. Đây là khuyến cáo cũng là mong đợi chung của các CTCK trên thị trường.</p>

<p><br />
Việc nới rộng không gian tìm vốn cho CTCK càng trở nên cần thiết hơn khi dòng tín dụng chảy vào TTCK bị hạn chế theo Thông tư 36. Trong khi chờ đợi NHNN thay đổi quan điểm và chờ Chính phủ thấu hiểu hiện trạng TTCK để chỉ đạo hướng sửa đổi Thông tư 36 (nếu có), thì tự thân ngành chứng khoán cần tự cởi trói cho chính mình.</p>

<p>Hy vọng, sự vận động của nội ngành và những quyết sách mới từ Chính phủ sẽ giúp TTCK sớm tăng trưởng trở lại, không trái ngược với diễn biến chung của nền kinh tế.</p>

<p>Theo ĐTCK</p>

<p>&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('256', 'Donec nec justo eget felis facilisis fermentum', 'donec-nec-justo-eget-felis-facilisis-fermentum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/donec-nec-justo-eget-felis-facilisis-fermentum', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('251', 'Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'suspendisse-urna-nibh-viverra-non-semper-suscipit-posuere-a-pede', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/suspendisse-urna-nibh-viverra-non-semper-suscipit-posuere-a-pede', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('252', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/tin-co-dong/lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('257', 'Aliquam porttitor mauris sit amet orci', 'aliquam-porttitor-mauris-sit-amet-orci', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/aliquam-porttitor-mauris-sit-amet-orci', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('258', 'Aenean dignissim pellentesque felis.', 'aenean-dignissim-pellentesque-felis', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/aenean-dignissim-pellentesque-felis', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('247', 'Donec odio. Quisque volutpat mattis eros', 'donec-odio-quisque-volutpat-mattis-eros', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/donec-odio-quisque-volutpat-mattis-eros', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('248', 'Donec nec justo eget felis facilisis fermentum', 'donec-nec-justo-eget-felis-facilisis-fermentum', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/donec-nec-justo-eget-felis-facilisis-fermentum', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('249', 'Aliquam porttitor mauris sit amet orci', 'aliquam-porttitor-mauris-sit-amet-orci', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/aliquam-porttitor-mauris-sit-amet-orci', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('250', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat', 'praesent-dapibus-neque-id-cursus-faucibus-tortor-neque-egestas-augue-eu-vulputate-magna-eros-eu-erat', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/praesent-dapibus-neque-id-cursus-faucibus-tortor-neque-egestas-augue-eu-vulputate-magna-eros-eu-erat', '<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('182', 'Exercising the right to property', 'exercising-the-right-to-property', empty_clob(), 'en', null, 'customer-care/transactions-guide/trading-guide/exercising-the-right-to-property', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('228', 'Báo cáo tài chính Quý IV/2014', 'bao-cao-tai-chinh-quy-iv/2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/bao-cao-tai-chinh-quy-iv/2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('228', 'Báo cáo tài chính Quý IV/2014', 'bao-cao-tai-chinh-quy-iv/2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'en', null, 'investor-relations/financial-statements/bao-cao-tai-chinh-quy-iv/2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('229', ' Báo cáo tài chính Quý III/2014', '-bao-cao-tai-chinh-quy-iii/2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/-bao-cao-tai-chinh-quy-iii/2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('230', ' Báo cáo tài chính Quý II/2014', '-bao-cao-tai-chinh-quy-ii/2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/-bao-cao-tai-chinh-quy-ii/2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('231', 'MBS CBTT Báo cáo thường niên 2013', 'mbs-cbtt-bao-cao-thuong-nien-2013', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/mbs-cbtt-bao-cao-thuong-nien-2013', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('232', ' Báo cáo tình hình quản trị MBS 6 tháng năm 2013', '-bao-cao-tinh-hinh-quan-tri-mbs-6-thang-nam-2013', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
Praesent dapibus, neque id...', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/-bao-cao-tinh-hinh-quan-tri-mbs-6-thang-nam-2013', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('232', ' Báo cáo tình hình quản trị MBS 6 tháng năm 2013', '-bao-cao-tinh-hinh-quan-tri-mbs-6-thang-nam-2013', empty_clob(), 'en', null, 'investor-relations/financial-statements/-bao-cao-tinh-hinh-quan-tri-mbs-6-thang-nam-2013', empty_clob(), null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('241', ' MBS_BCKQ giao dịch quyền mua CP của cổ đông nội bộ, người có liên quan', '-mbsbckq-giao-dich-quyen-mua-cp-cua-co-dong-noi-bo-nguoi-co-lien-quan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/cong-bo-thong-tin/-mbsbckq-giao-dich-quyen-mua-cp-cua-co-dong-noi-bo-nguoi-co-lien-quan', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('234', 'MBS Báo cáo tài chính và Báo cáo Tỷ lệ an toàn tài chính năm 2011', 'mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2011', 'Công ty CP Chứng khoán MB (MBS) trân trọng kính gửi tới Quý Khách hàng Báo cáo tài chính và Báo cáo tỷ lệ an toàn tài chính năm 2013.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/bao-cao-tai-chinh/mbs-bao-cao-tai-chinh-va-bao-cao-ty-le-an-toan-tai-chinh-nam-2011', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('235', 'Báo cáo tài chính 2008', 'bao-cao-tai-chinh-2008', 'Công ty CP Chứng khoán MB (MBS) trân trọng kính gửi tới Quý Khách hàng Báo cáo tài chính và Báo cáo tỷ lệ an toàn tài chính năm 2013.', 'vi', null, 'quan-he-co-dong/bao-cao-tai-chinh/bao-cao-tai-chinh-2008', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('242', 'MBS báo cáo tình hình quản trị công ty năm 2014', 'mbs-bao-cao-tinh-hinh-quan-tri-cong-ty-nam-2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/mbs-bao-cao-tinh-hinh-quan-tri-cong-ty-nam-2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('243', 'MBS báo cáo tình hình quản trị công ty 6 tháng đầu năm 2014', 'mbs-bao-cao-tinh-hinh-quan-tri-cong-ty-6-thang-dau-nam-2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/mbs-bao-cao-tinh-hinh-quan-tri-cong-ty-6-thang-dau-nam-2014', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('244', 'Điều lệ tổ chức và hoạt động Công ty CP Chứng khoán MB (MBS)', 'dieu-le-to-chuc-va-hoat-dong-cong-ty-cp-chung-khoan-mb-mbs', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/dieu-le-to-chuc-va-hoat-dong-cong-ty-cp-chung-khoan-mb-mbs', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('245', 'Báo cáo tình hình quản trị MBS năm 2013', 'bao-cao-tinh-hinh-quan-tri-mbs-nam-2013', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/bao-cao-tinh-hinh-quan-tri-mbs-nam-2013', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('246', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/thong-tin-quan-tri/lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit', empty_clob(), null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('236', 'MBS bổ nhiệm Phó Tổng Giám đốc', 'mbs-bo-nhiem-pho-tong-giam-doc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/cong-bo-thong-tin/mbs-bo-nhiem-pho-tong-giam-doc', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('237', ' MBS TB mời họp Đại hội đồng cổ đông thường niên năm 2015', '-mbs-tb-moi-hop-dai-hoi-dong-co-dong-thuong-nien-nam-2015', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', null, 'quan-he-co-dong/cong-bo-thong-tin/-mbs-tb-moi-hop-dai-hoi-dong-co-dong-thuong-nien-nam-2015', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('238', 'MBS CBTT Báo cáo thường niên 2014', 'mbs-cbtt-bao-cao-thuong-nien-2014', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/cong-bo-thong-tin/mbs-cbtt-bao-cao-thuong-nien-2014', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('239', ' MBS thông báo ngày chốt danh sách ĐHCĐ thường niên năm 2015', '-mbs-thong-bao-ngay-chot-danh-sach-dhcd-thuong-nien-nam-2015', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/cong-bo-thong-tin/-mbs-thong-bao-ngay-chot-danh-sach-dhcd-thuong-nien-nam-2015', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('240', ' Thông tin nhân sự có chứng chỉ hành nghề tại MBS', '-thong-tin-nhan-su-co-chung-chi-hanh-nghe-tai-mbs', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'quan-he-co-dong/cong-bo-thong-tin/-thong-tin-nhan-su-co-chung-chi-hanh-nghe-tai-mbs', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
', null, '/uploads/files/MBS_HD-chuyen-tien-MB---MBS.pdf');
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('183', 'Chuyển quyền sở hữu chứng khoán', 'chuyen-quyen-so-huu-chung-khoan', 'Để thuận tiện và đảm bảo quyền lợi cho khách hàng trong việc thực hiện quyền cho nhà đầu tư, MBS xin hướng dẫn Quý khách hàng  thực hiện quyền cho người sở hữu chứng khoán như sau:
', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/chuyen-quyen-so-huu-chung-khoan', '<p style="text-align: justify;">Để thuận tiện và đảm bảo quyền lợi cho khách hàng trong việc thực hiện quyền cho nhà đầu tư, MBS xin hướng dẫn Quý khách hàng &nbsp;thực hiện quyền cho người sở hữu chứng khoán như sau:</p>

<p><strong>1.&nbsp;&nbsp; &nbsp;Quyền đại hội cổ đông</strong></p>

<p>Khách hàng sẽ được nhận thông báo từ Tổ chức Phát hành</p>

<p><strong>2.&nbsp;&nbsp; &nbsp;Quyền Cổ tức bằng tiền, Quyền cổ tức bằng cổ phiếu/cổ phiếu thưởng</strong></p>

<p>Quyền nhận cổ tức bằng tiền, quyền cổ tức bằng cổ phiếu/cổ phiếu thưởng: Vào ngày chi trả cổ tức hoặc ngày giao dịch cổ tức bằng cổ phiếu, cổ phiếu thưởng, MBS sẽ phân bổ tự động vào tài khoản giao dịch chứng khoán của khách hàng.</p>

<p><strong>3.&nbsp;&nbsp; &nbsp;Quyền mua cổ phiếu phát hành thêm</strong></p>

<p>-&nbsp;&nbsp; &nbsp;Quyền mua cổ phiếu phát hành thêm: Khi có phát sinh quyền mua chứng khoán phát hành thêm, MBS sẽ gửi thông báo về việc sở hữu quyền mua của &nbsp;từng khách hàng thông qua SMS hoặc email mà khách hàng đã đăng ký với MBS. Vì vậy, Quý khách hàng lưu ý thông báo và cập nhật cho MBS các thông tin liên lạc khi có sự thay đổi.&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Thủ tục đăng ký mua chứng khoán:<br />
•&nbsp;&nbsp; &nbsp;KH có thể thực hiện đăng ký mua trực tuyến cổ phiếu phát hành thêm qua https://stock24.mbs.com.vn. (nghiệp vụ khác\thực hiện quyền). &nbsp;<br />
•&nbsp;&nbsp; &nbsp;Hoặc &nbsp;đến các Phòng giao dịch/Chi nhánh gần nhất của MBS để thực hiện.</p>

<p>Mọi thắc mắc về dịch vụ xin vui lòng liên hệ số hotline 1900 9088 nhấn phím 4 để được hướng dẫn và tư vấn.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('186', 'Rút chứng khoán', 'rut-chung-khoan', 'Khách hàng có thể rút một phần hoặc toàn bộ số lượng chứng khoán đang sở hữu. Sau khi  thực hiện rút chứng khoán, khách hàng nhận giấy xác nhận của Trung tâm Lưu ký Chứng khoán Việt Nam (VSD) đến tổ chức phát hành để nhận lại Sổ cổ đông.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/rut-chung-khoan', '<p>Khách hàng có chứng khoán lưu ký tại MBS có nhu cầu rút chứng khoán có thể thực hiện rút tại MBS với điều kiện sau:</p>

<p><ul>
	<li>Khách hàng không có nghĩa vụ tài chính tại MBS;</li>
	<li>Chỉ thực hiện rút chứng khoán ở tài khoản giao dịch chứng khoán thường;</li>
	<li>Khách hàng có thể rút một phần hoặc toàn bộ số lượng chứng khoán đang sở hữu.</li>
</ul></p>

<p>Sau khi &nbsp;thực hiện rút chứng khoán, khách hàng nhận giấy xác nhận của Trung tâm Lưu ký Chứng khoán Việt Nam (VSD) đến tổ chức phát hành để nhận lại Sổ cổ đông.</p>

<p><strong>1.&nbsp;&nbsp; &nbsp;Hồ sơ rút chứng khoán bao gồm:</strong></p>

<p><ul>
	<li>Đề nghị rút chứng khoán (Mẫu 17A/LK);&nbsp;</li>
	<li>Chứng minh thư bản sao và Chứng minh thư bản gốc (để đối chiếu);</li>
	<li>Bản sao công chứng Giấy Đăng ký kinh doanh đối với khách hàng tổ chức.</li>
</ul></p>

<p><strong>2.&nbsp;&nbsp; &nbsp;Các bước thực hiện:</strong></p>

<p><ul>
	<li>Khách hàng có thể thực hiện việc rút chứng khoán như sau:</li>
	<li>Khách hàng đến trực tiếp các Chi nhánh/Phòng giao dịch cua MBS để làm thủ tục;</li>
	<li>Khi VSD chấp thuận hồ sơ rút chứng khoán, MBS thông báo với&nbsp;</li>
</ul></p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('259', 'Morbi in sem quis dui placerat ornare', 'morbi-in-sem-quis-dui-placerat-ornare', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/morbi-in-sem-quis-dui-placerat-ornare', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('260', 'Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.', 'pellentesque-odio-nisi-euismod-in-pharetra-a-ultricies-in-diam-sed-arcu-cras-consequat', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/pellentesque-odio-nisi-euismod-in-pharetra-a-ultricies-in-diam-sed-arcu-cras-consequat', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('261', 'Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', 'aliquam-erat-volutpat-nam-dui-mi-tincidunt-quis-accumsan-porttitor-facilisis-luctus-metus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', 'vi', '/uploads/files/MBS_521_TinCoDong.jpg', 'goc-truyen-thong/mbs-bao-chi/aliquam-erat-volutpat-nam-dui-mi-tincidunt-quis-accumsan-porttitor-facilisis-luctus-metus', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>

<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>

<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('192', 'Mở tài khoản trực tiếp tại sàn', 'mo-tai-khoan-truc-tiep-tai-san', ' Mở tài khoản trực tiếp tại Trụ sở chính, các Chi nhánh, Phòng Giao dịch và các Điểm giao dịch trực tuyến của MBS:', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/mo-tai-khoan-truc-tiep-tai-san', '<p>Mở tài khoản trực tiếp tại Trụ sở chính, các Chi nhánh, Phòng Giao dịch và các Điểm giao dịch trực tuyến của MBS:</p>

<p><strong>1. Hồ sơ do khách hàng cung cấp:</strong></p>

<p style="margin-left: 40px;">1.1. Cá nhân:</p>

<ul style="margin-left: 80px;">
	<li>&nbsp;Bản sao CMTND còn &nbsp;thời hạn hiệu lực (không quá 15 năm kể từ ngày cấp đối với CMTND</li>
</ul>

<p style="margin-left: 40px;"><span style="line-height: 20.7999992370605px;">1.2. Tổ chức:</span></p>
<p>
<ul style="margin-left: 80px;">
	<li>Bản sao công chứng Giấy phép thành lập/Giấy chứng nhận đăng ký kinh doanh.</li>
	<li>Bản sao công chứng Quyết định bổ nhiệm người đại diện theo pháp luật.</li>
	<li>Bản sao CMTND của người đại diện theo pháp luật.</li>
	<li>Bản chính giấy ủy quyền người đại diện có xác nhận của tổ chức.</li>
	<li>Bản sao CMTND của người được ủy quyền.</li>
	<li>Các văn bản khác (nếu có).</li>
</ul>
</p>
<p><strong>2. &nbsp;Hồ sơ do MBS cung cấp:</strong></p>

<ul style="margin-left: 80px;">
	<li>Phiếu lấy thông tin khách hàng (không bắt buộc).</li>
	<li>Yêu cầu mở tài khoản và đăng ký sử dụng dịch vụ.</li>
	<li>Bộ điều khoản và điều kiện chung (T&amp;C).</li>
	<li>Các mẫu biểu khác tùy vào từng khách hàng và loại hình dịch vụ khách hàng đăng ký.</li>
</ul>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
', null, null);
INSERT INTO "MBS_DB"."WZ_ARTICLES_CONTENT" VALUES ('187', 'S24 - Giao dịch chứng khoán', 's24-giao-dich-chung-khoan', 'Quý khách có thể thực hiện giao dịch chứng khoán trực tiếp tại quầy giao dịch của MBS, hoặc giao dịch qua điện thoại Contact24, hoặc giao dịch trực tuyến qua Stock24.', 'vi', null, 'cham-soc-khach-hang/huong-dan-giao-dich/huong-dan-giao-dich/s24-giao-dich-chung-khoan', '<p>Quý khách có thể thực hiện giao dịch chứng khoán trực tiếp tại quầy giao dịch của MBS, hoặc giao dịch qua điện thoại Contact24, hoặc giao dịch trực tuyến qua Stock24.</p>

<p><strong>1. &nbsp; Thực hiện giao dịch tại sàn</strong></p>

<p>Khách hàng là chủ tài khoản hoặc người được uỷ quyền hợp pháp chỉ cần mang theo CMTND đến các điểm giao dịch của MBS để thực hiện giao dịch chứng khoán.</p>

<p><strong>2. &nbsp; Giao dịch qua Contact24</strong></p>

<p>Để thực hiện giao dịch chứng khoán tài khoản ký quỹ, khách hàng thực hiện như sau:</p>



<p><ul>
	<li>Bước1: Gọi điện đến tổng đài 1900 9088, chọn nhánh 3.</li>
	<li>Bước 2: Nhập số tài khoản và mật khẩu giao dịch qua điện thoại.</li>
	<li>Bước 3: Gặp và đưa yêu cầu lệnh đặt cho nhân viên giao dịch.</li>
</ul></p>


<p><strong>3. &nbsp;Giao dịch qua Stock 24</strong></p>

<p>Để sử dụng dịch vụ này, Quý khách cần đăng ký dịch vụ giao dịch trực tuyến Stock24. Nếu chưa đăng ký xin vui lòng liên hệ với MBS để được hướng dẫn.</p>
', null, null);

-- ----------------------------
-- Indexes structure for table WZ_ARTICLES_CONTENT
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_ARTICLES_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("TITLE" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("TITLE" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("TITLE" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("TITLE" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD CHECK ("LANG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_ARTICLES_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_ARTICLES_CONTENT" ADD PRIMARY KEY ("NID", "LANG");
