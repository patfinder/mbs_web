/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-06 16:07:13
*/


-- ----------------------------
-- Table structure for WZ_RE_JOB_CONTENT
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_JOB_CONTENT";
CREATE TABLE "MBS_DB"."WZ_RE_JOB_CONTENT" (
"NID" NUMBER NOT NULL ,
"LANG" NVARCHAR2(10) NOT NULL ,
"TITLE" NVARCHAR2(255) NULL ,
"SLUG" NVARCHAR2(255) NULL ,
"DESCRIPTION" NCLOB NULL ,
"REQUIRE" NCLOB NULL ,
"DOC_REQUIRE" NCLOB NULL ,
"DEADLINE" NVARCHAR2(255) NULL ,
"PLACE_CV" NVARCHAR2(1000) NULL ,
"BENEFIT" NCLOB NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_JOB_CONTENT
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_JOB_CONTENT" VALUES ('3', 'vi', 'Giám đốc phòng kinh doanh', 'giam-doc-phong-kinh-doanh', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.

Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.

Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.

Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.', '<ul>
	<li>Bằng cấp: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
	<li>Kinh nghiệm: 4 năm</li>
	<li>Kỹ năng mềm:
	<p>- Sáng tạo, nhiệt tình, kỹ năng giao tiếp tốt, kỹ năng xử lý tình huống linh hoạt</p>

	<p>- Kỹ năng tổ chức và quản lý thời gian hiệu quả</p>

	<p>- Kỹ năng làm việc nhóm, xử lý công việc độc lập</p>

	<p>- Kỹ năng viết phân tích và trình bày văn bản, báo cáo</p>
	</li>
	<li>Yêu cầu khác:
	<p>- Sức khỏe tốt.</p>

	<p>- Có trách nhiệm, chịu được áp lực cao trong công việc.</p>

	<p>- Trung thực, có đạo đức nghề nghiệp, có tinh thần kỷ luật cao</p>

	<p>- Siêng năng, cẩn thận, kiên trì</p>
	</li>
</ul>
', '<ul>
	<li>Đơn xin việc</li>
	<li>Mẫu thông tin ứng viên (Download tại Website MBS, mục Cơ hội nghề nghiệp/Tải hồ sơ)</li>
	<li>Sơ yếu lý lịch: có xác nhận của chính quyền địa phương</li>
	<li>Giấy chứng nhận sức khỏe trong vòng 6 tháng gần nhất</li>
	<li>Bằng cấp, Chứng chỉ (bản sao có công chứng)</li>
	<li>CMTND photo công chứng</li>
	<li>02 ảnh 4 x 6 mới chụp trong vòng 6 tháng gần nhất</li>
	<li>Ngoài hồ sơ ghi rõ vị trí dự tuyển, địa chỉ và số điện thoại liên hệ</li>
</ul>
', '(Phỏng vấn cuốn chiếu ngay khi nhận được hồ sơ đến khi tuyển được ứng viên phù hợp)', '<p style="margin-left: 30px;">Tại Hà Nội:</p>

<p style="margin-left: 30px;">Chị Phạm Thị Kim Dung</p>

<p style="margin-left: 30px;">Phòng Nhân sự</p>

<p style="margin-left: 30px;">Công ty CP chứng khoán MB</p>

<p style="margin-left: 30px;">Tầng 7, tòa nhà MB, số 3 Liễu Giai, Ba Đình, Hà Nội</p>

<p style="margin-left: 30px;">Phone: 043.7262600 – Máy lẻ: 2199</p>
', '<p>Thu nhập hấp dẫn và cạnh tranh</p>
');
INSERT INTO "MBS_DB"."WZ_RE_JOB_CONTENT" VALUES ('3', 'en', 'Business precident', 'business-precident', empty_clob(), empty_clob(), empty_clob(), null, null, empty_clob());

-- ----------------------------
-- Indexes structure for table WZ_RE_JOB_CONTENT
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table WZ_RE_JOB_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_JOB_CONTENT" ADD PRIMARY KEY ("NID", "LANG");
