/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-06 16:07:05
*/


-- ----------------------------
-- Table structure for WZ_RE_JOB_BRANCH
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_JOB_BRANCH";
CREATE TABLE "MBS_DB"."WZ_RE_JOB_BRANCH" (
"JOB_ID" NUMBER NOT NULL ,
"BRANCH_ID" NUMBER NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_JOB_BRANCH
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_JOB_BRANCH" VALUES ('3', '1');
INSERT INTO "MBS_DB"."WZ_RE_JOB_BRANCH" VALUES ('3', '2');

-- ----------------------------
-- Indexes structure for table WZ_RE_JOB_BRANCH
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table WZ_RE_JOB_BRANCH
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_JOB_BRANCH" ADD PRIMARY KEY ("JOB_ID", "BRANCH_ID");
