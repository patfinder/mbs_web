/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 10:59:42
*/


-- ----------------------------
-- Table structure for WZ_RE_EDUCATION
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_EDUCATION";
CREATE TABLE "MBS_DB"."WZ_RE_EDUCATION" (
"ID" NUMBER NOT NULL ,
"USER_ID" NUMBER NOT NULL ,
"LEVEL_NAME" NVARCHAR2(128) NULL ,
"SCHOOL" NVARCHAR2(128) NULL ,
"DATE_FROM" TIMESTAMP(6)  NOT NULL ,
"DATE_TO" TIMESTAMP(6)  NOT NULL ,
"MAJOR" NVARCHAR2(128) NULL ,
"GRADUATE_POINT" NVARCHAR2(128) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Indexes structure for table WZ_RE_EDUCATION
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_EDUCATION
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD CHECK ("DATE_FROM" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD CHECK ("DATE_TO" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_EDUCATION
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_EDUCATION" ADD PRIMARY KEY ("ID");
