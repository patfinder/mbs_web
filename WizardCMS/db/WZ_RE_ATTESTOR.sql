/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 10:59:14
*/


-- ----------------------------
-- Table structure for WZ_RE_ATTESTOR
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_ATTESTOR";
CREATE TABLE "MBS_DB"."WZ_RE_ATTESTOR" (
"ID" NUMBER NOT NULL ,
"USER_ID" NUMBER NOT NULL ,
"FULLNAME" NVARCHAR2(64) NULL ,
"CAREER" NVARCHAR2(128) NULL ,
"ADDRESS_PHONE_FAX" NVARCHAR2(255) NULL ,
"MAJOR" NVARCHAR2(255) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Indexes structure for table WZ_RE_ATTESTOR
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_ATTESTOR
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_ATTESTOR" ADD CHECK ("USER_ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_ATTESTOR" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_ATTESTOR" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_ATTESTOR
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_ATTESTOR" ADD PRIMARY KEY ("ID");
