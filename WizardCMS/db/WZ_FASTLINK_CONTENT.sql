/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.4.0

Source Server         : MBS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-04-21 18:12:57
*/


-- ----------------------------
-- Table structure for WZ_FASTLINK_CONTENT
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_FASTLINK_CONTENT";
CREATE TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" (
"NID" NUMBER DEFAULT 0  NOT NULL ,
"NAME" NVARCHAR2(255) NULL ,
"LINK" NVARCHAR2(255) NULL ,
"LANG" NVARCHAR2(8) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_FASTLINK_CONTENT
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('6', 'Bảng giá chứng khoáng trực tiếp', 'http://quote24pro.mbs.com.vn/login.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('7', 'Tra cứu thông tin giao dịch qua tin nhắn sms', 'https://mbs.com.vn/Upload/HDSD_SMS24_moinhat.pdf', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('9', 'Mở tài khoản trực tuyến', 'http://open24.mbs.com.vn/Presentation/Welcome.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('9', 'Register account online', 'http://open24.mbs.com.vn/Presentation/Welcome.aspx', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('10', 'Đặt lệnh qua điện thoại Hotline 19009088', null, 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('10', 'Đặt lệnh qua điện thoại Hotline 19009088', null, 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('11', 'Hỗ trợ trực tuyến', 'http://search24.mbs.com.vn/', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('11', 'Online support', 'http://search24.mbs.com.vn/', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('8', 'Bảng giá trực tuyến', 'http://quote24pro.mbs.com.vn/login.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('8', 'Online prices', 'http://quote24pro.mbs.com.vn/login.aspx', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('1', 'Dịch vụ mở tài khoản trực tuyến', 'http://open24.mbs.com.vn/Presentation/Welcome.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('1', 'Online transaction', 'https://stock24.mbs.com.vn/login.aspx', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('2', 'Tổng đài tra cứu và giao dịch chứng khoáng qua điện thoại', 'http://27.118.24.3:8899/vi/khach-hang-to-chuc/dich-vu-chung-khoan/dich-vu-dien-tu#item_click3', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('2', 'Online prices', 'http://quote24pro.mbs.com.vn/login.aspx', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('3', 'Giao dịch chứng khoáng qua điện thoại', null, 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('3', 'Register account online', 'http://open24.mbs.com.vn/Presentation/Welcome.aspx', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('4', 'Giao dịch chứng khoáng trực tuyến', 'https://stock24.mbs.com.vn/login.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('4', 'Online news', 'http://search24.mbs.com.vn/', 'en');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('5', 'Giao dịch trực tuyến', 'https://stock24.mbs.com.vn/login.aspx', 'vi');
INSERT INTO "MBS_DB"."WZ_FASTLINK_CONTENT" VALUES ('5', 'Online transaction', 'https://stock24.mbs.com.vn/login.aspx', 'en');

-- ----------------------------
-- Indexes structure for table WZ_FASTLINK_CONTENT
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_FASTLINK_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD CHECK ("NID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_FASTLINK_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_FASTLINK_CONTENT" ADD PRIMARY KEY ("NID", "LANG");
