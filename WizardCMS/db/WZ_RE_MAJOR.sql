/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 11:00:05
*/


-- ----------------------------
-- Table structure for WZ_RE_MAJOR
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_MAJOR";
CREATE TABLE "MBS_DB"."WZ_RE_MAJOR" (
"ID" NUMBER NOT NULL ,
"OTHER_MAJOR" NVARCHAR2(500) NULL ,
"SOCIAL_ACTIVITIES" NVARCHAR2(255) NULL ,
"ARTICLES" NVARCHAR2(500) NULL ,
"SOFTWARES" NVARCHAR2(500) NULL ,
"TALENT" NVARCHAR2(255) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL ,
"USER_ID" NUMBER NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_MAJOR
-- ----------------------------

-- ----------------------------
-- Indexes structure for table WZ_RE_MAJOR
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_MAJOR
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_MAJOR" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_MAJOR" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_MAJOR" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_MAJOR
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_MAJOR" ADD PRIMARY KEY ("ID");
