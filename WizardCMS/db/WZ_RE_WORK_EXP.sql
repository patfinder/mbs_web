/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 11:00:51
*/


-- ----------------------------
-- Table structure for WZ_RE_WORK_EXP
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_WORK_EXP";
CREATE TABLE "MBS_DB"."WZ_RE_WORK_EXP" (
"ID" NUMBER NOT NULL ,
"USER_ID" NUMBER NOT NULL ,
"OFFICE_NAME_ADDRESS" NVARCHAR2(255) NULL ,
"OFFICE_PHONE" NVARCHAR2(16) NULL ,
"WORK_FROM" NVARCHAR2(32) NULL ,
"WORK_TO" NVARCHAR2(32) NULL ,
"POSITION" NVARCHAR2(128) NULL ,
"SALARY" NVARCHAR2(65) NULL ,
"LEADER" NVARCHAR2(128) NULL ,
"SUBORDINATES" NVARCHAR2(128) NULL ,
"OUT_REASON" NVARCHAR2(128) NULL ,
"WORK_DESCRIPTION" NVARCHAR2(500) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Indexes structure for table WZ_RE_WORK_EXP
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_WORK_EXP
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_WORK_EXP" ADD CHECK ("USER_ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_WORK_EXP" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_WORK_EXP" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_WORK_EXP
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_WORK_EXP" ADD PRIMARY KEY ("ID");
