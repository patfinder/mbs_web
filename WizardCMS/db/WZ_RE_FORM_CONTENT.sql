/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-05-06 16:06:41
*/


-- ----------------------------
-- Table structure for WZ_RE_FORM_CONTENT
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_FORM_CONTENT";
CREATE TABLE "MBS_DB"."WZ_RE_FORM_CONTENT" (
"NID" NUMBER NOT NULL ,
"LANG" NVARCHAR2(10) NOT NULL ,
"TITLE" NVARCHAR2(255) NULL ,
"DESCRIPTION" NVARCHAR2(255) NULL ,
"FILE_DOWNLOAD" NVARCHAR2(255) NULL ,
"SLUG" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_FORM_CONTENT
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_FORM_CONTENT" VALUES ('1', 'vi', 'Mẫu thông tin thực tập sinh', 'Lorem ipsum dolor sit amet', '/uploads/files/84f93e6a35f34ccd8cd0c8c71447e9c4_1383535933(1).pdf', 'mau-thong-tin-thuc-tap-sinh');
INSERT INTO "MBS_DB"."WZ_RE_FORM_CONTENT" VALUES ('1', 'en', 'Internship form', null, null, 'internship-form');
INSERT INTO "MBS_DB"."WZ_RE_FORM_CONTENT" VALUES ('2', 'vi', 'Mấu thông tin ứng viên MBS', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', '/uploads/files/84f93e6a35f34ccd8cd0c8c71447e9c4_1383535933.pdf', 'mau-thong-tin-ung-vien-mbs');
INSERT INTO "MBS_DB"."WZ_RE_FORM_CONTENT" VALUES ('2', 'en', 'MBS application form', null, null, 'mbs-application-form');

-- ----------------------------
-- Indexes structure for table WZ_RE_FORM_CONTENT
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table WZ_RE_FORM_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_FORM_CONTENT" ADD PRIMARY KEY ("NID", "LANG");
