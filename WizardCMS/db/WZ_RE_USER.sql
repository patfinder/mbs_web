/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 11:00:32
*/


-- ----------------------------
-- Table structure for WZ_RE_USER
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_USER";
CREATE TABLE "MBS_DB"."WZ_RE_USER" (
"ID" NUMBER NOT NULL ,
"FULLNAME" NVARCHAR2(64) NULL ,
"NAME" NVARCHAR2(32) NULL ,
"CMND" NVARCHAR2(16) NULL ,
"DATE_PLACE_CMND" NVARCHAR2(128) NULL ,
"ADDRESS" NVARCHAR2(128) NULL ,
"ADDRESS_CONTACT" NVARCHAR2(128) NULL ,
"HOME_PHONE" NVARCHAR2(16) NULL ,
"MOBILE_PHONE" NVARCHAR2(16) NULL ,
"HOME_PHONE_CONTACT" NVARCHAR2(16) NULL ,
"FAX" NVARCHAR2(64) NULL ,
"EMAIL" NVARCHAR2(64) NULL ,
"GENDER" NUMBER DEFAULT 0  NOT NULL ,
"HEIGHT" NVARCHAR2(32) NULL ,
"WEIGHT" NVARCHAR2(32) NULL ,
"MIRITAL" NUMBER DEFAULT 0  NOT NULL ,
"MIRITAL_OTHER" NVARCHAR2(64) NULL ,
"DOB" TIMESTAMP(6)  NOT NULL ,
"POB" NVARCHAR2(64) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"MODIFIED" TIMESTAMP(6)  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL ,
"IMAGE" NVARCHAR2(255) NULL ,
"RELATE_EXP" NVARCHAR2(255) NULL ,
"CONTACT_LEADER" NVARCHAR2(255) NULL ,
"USERNAME" NVARCHAR2(64) NULL ,
"PASSWORD" NVARCHAR2(64) NULL ,
"TOKEN" NVARCHAR2(64) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_RE_USER
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_RE_USER" VALUES ('1', 'Truong Nghia', null, null, null, null, null, null, null, null, null, 'vuonchuoi@gmail.com', '0', null, null, '0', null, TO_TIMESTAMP(' 0001-01-01 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, '0', TO_TIMESTAMP(' 2015-06-04 15:36:33:370346', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2015-06-04 15:36:33:370346', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null, null, 'vuonchuoi', 'A4A45FB971AFB1CEC1EF539F62907530', null);

-- ----------------------------
-- Indexes structure for table WZ_RE_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_USER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("GENDER" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("MIRITAL" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("DOB" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("MODIFIED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_USER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_USER" ADD PRIMARY KEY ("ID");
