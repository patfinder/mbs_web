/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-04-13 23:14:42
*/


-- ----------------------------
-- Table structure for ADMIN_WZ_USERS
-- ----------------------------
DROP TABLE "MBS_DB"."ADMIN_WZ_USERS";
CREATE TABLE "MBS_DB"."ADMIN_WZ_USERS" (
"ID" NUMBER NOT NULL ,
"USERNAME" VARCHAR2(50 BYTE) NULL ,
"PASSWORD" VARCHAR2(99 BYTE) NULL ,
"GROUP_ID" NUMBER DEFAULT 0  NULL ,
"PERMISSION" VARCHAR2(500 BYTE) NULL ,
"CUSTOM_PERMISSION" NUMBER DEFAULT 0  NULL ,
"STATUS" NUMBER DEFAULT 1  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of ADMIN_WZ_USERS
-- ----------------------------
INSERT INTO "MBS_DB"."ADMIN_WZ_USERS" VALUES ('1', 'root', 'eff5b3fe8427dea01bec6c305f09cb9a', '1', '0|rwd,5|rwd,2|rwd,1|rwd,4|rwd,9|rwd,10|rwd,8|rwd,13|rwd,7|rwd,11|rwd,12|rwd,3|rwd,6|rwd,14|rwd', '1', '1', TO_TIMESTAMP(' 2012-08-28 14:52:42:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "MBS_DB"."ADMIN_WZ_USERS" VALUES ('2', 'admin', '2813ba16cd7d0b1e73f201f91393c5a1', '1', '0|rwd,2|rwd,1|rwd,4|rwd,9|rwd,8|rwd,13|rwd,7|rwd,11|rwd,12|rwd,3|rwd,6|rwd,14|rwd', '1', '1', TO_TIMESTAMP(' 2012-08-28 14:52:59:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Indexes structure for table ADMIN_WZ_USERS
-- ----------------------------

-- ----------------------------
-- Checks structure for table ADMIN_WZ_USERS
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("STATUS" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD CHECK ("CREATED" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table ADMIN_WZ_USERS
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Foreign Key structure for table "MBS_DB"."ADMIN_WZ_USERS"
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_USERS" ADD FOREIGN KEY ("GROUP_ID") REFERENCES "MBS_DB"."ADMIN_WZ_GROUPS" ("ID");
