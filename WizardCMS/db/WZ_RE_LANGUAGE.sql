/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : XE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-06-05 10:59:56
*/


-- ----------------------------
-- Table structure for WZ_RE_LANGUAGE
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_RE_LANGUAGE";
CREATE TABLE "MBS_DB"."WZ_RE_LANGUAGE" (
"ID" NUMBER NOT NULL ,
"USER_ID" NUMBER NOT NULL ,
"NAME" NVARCHAR2(128) NULL ,
"WRITE" NVARCHAR2(128) NULL ,
"LISTEN" NVARCHAR2(128) NULL ,
"READ" NVARCHAR2(128) NULL ,
"SPEAK" NVARCHAR2(128) NULL ,
"STATUS" NUMBER DEFAULT 0  NOT NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Indexes structure for table WZ_RE_LANGUAGE
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_RE_LANGUAGE
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_LANGUAGE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_LANGUAGE" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_RE_LANGUAGE" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_RE_LANGUAGE
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_RE_LANGUAGE" ADD PRIMARY KEY ("ID", "STATUS");
