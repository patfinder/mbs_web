/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : SYS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-04-27 15:46:54
*/


-- ----------------------------
-- Table structure for WZ_BRANCH_CONTENT
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_BRANCH_CONTENT";
CREATE TABLE "MBS_DB"."WZ_BRANCH_CONTENT" (
"NID" NUMBER NOT NULL ,
"NAME" NVARCHAR2(255) NULL ,
"DESCRIPTION" NVARCHAR2(500) NULL ,
"LANG" NVARCHAR2(10) NOT NULL ,
"SLUG" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_BRANCH_CONTENT
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('1', 'Hội Sở Update', '<p>Tòa nhà MB</p>

<p>Số 3 Liễu Giai, Quận Ba Đình, Hà Nội</p>

<p>&nbsp;</p>
', 'vi', 'hoi-so-update');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('2', 'Chi Nhánh Hà Nội', '<p>Tầng 5, Tòa nhà</p>

<p>98 Ngụy Như, Kon Tum</p>

<p>Quận Thanh Xuân, Hà Nội</p>
', 'vi', 'chi-nhanh-ha-noi');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('3', 'Chi nhánh Hoàn Kiếm', '<p>Tầng 5, Tòa nhà 14C Lý Nam Đế,<br />
Quận Hoàn Kiếm, Hà Nội</p>
', 'vi', 'chi-nhanh-hoan-kiem');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('4', 'Chi nhánh Hải Phòng', '<p>Tầng 3, Tòa nhà EVN,<br />
số 7B Trần Hưng Đạo,<br />
Quận Hồng Bàng, TP. Hải Phòng</p>
', 'vi', 'chi-nhanh-hai-phong');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('5', 'Chi nhánh Hồ Chí Minh', '<p>Tầng 5, Tòa nhà Sunny Tower,<br />
259 Trần Hưng Đạo,<br />
Quận 1, TP. Hồ Chí Minh</p>
', 'vi', 'chi-nhanh-ho-chi-minh');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('6', 'PGD Nguyễn Công Trứ', '<p>150 Nguyễn Công Trứ,<br />
Quận 1, TP. Hồ Chí Minh</p>
', 'vi', 'pgd-nguyen-cong-tru');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('7', 'PGD Bắc Sài Gòn', '<p>Số 3 Nguyễn Oanh, Quận Gò Vấp,<br />
TP. Hồ Chí Minh</p>
', 'vi', 'pgd-bac-sai-gon');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT" VALUES ('8', 'Điểm giao dịch Quảng Ninh', null, 'vi', 'diem-giao-dich-quang-ninh');

-- ----------------------------
-- Indexes structure for table WZ_BRANCH_CONTENT
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_BRANCH_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT" ADD CHECK ("NID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_BRANCH_CONTENT
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT" ADD PRIMARY KEY ("NID", "LANG");
