/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : MBS_DEMO
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2016-04-13 19:11:47
*/


-- ----------------------------
-- Table structure for WZ_BRANCH_CONTENT_FOOTER
-- ----------------------------
DROP TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER";
CREATE TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" (
"NID" NUMBER NOT NULL ,
"NAME" NVARCHAR2(255) NULL ,
"DESCRIPTION" NVARCHAR2(500) NULL ,
"LANG" NVARCHAR2(10) NOT NULL ,
"SLUG" NVARCHAR2(255) NULL ,
"LINK" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WZ_BRANCH_CONTENT_FOOTER
-- ----------------------------
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('1', 'Hội Sở', '<p>Tòa nhà MB<br />
Số 3 Liễu Giai, Quận Ba Đình, Hà Nội</p>

<p><span>Tel:</span> + 84 4 3726 2600</p>

<p><span>Fax:</span> + 84 4 3726 2601</p>
', 'vi', 'hoi-so', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('2', 'Chi Nhánh Hà Nội', '<p>Tầng 5, Tòa nhà Thăng Long<br />
98A Ngụy Như Kon Tum,<br />
Quận Thanh Xuân, Hà Nội</p>

<p><span>Tel:</span> + 84 4 7305 7386</p>

<p><span>&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;Máy lẻ: 6636</p>

<p><span>Fax:</span> + 84 4 3569 0257</p>
', 'vi', 'chi-nhanh-ha-noi', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('4', 'Chi nhánh Hải Phòng', '<p>Tầng 3, Tòa nhà EVN,<br/>số 7B Trần Hưng Đạo,<br/>Quận Hồng Bàng, TP. Hải Phòng</p>
		    				<p><span>Tel:</span> +84 31 382 1886</p>
		    				<p><span>Fax:</span> +84 31 374 7739</p>', 'vi', 'chi-nhanh-hai-phong', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('5', 'Chi nhánh Hồ Chí Minh', '<p>Tầng 5, Tòa nhà Sunny Tower,<br />
259 Trần Hưng Đạo,<br />
Quận 1, TP. Hồ Chí Minh</p>

<p><span>Tel:</span> +84 8 3920 3388</p>

<p><span>Fax:</span> +84 8 3838 5181</p>
', 'vi', 'chi-nhanh-ho-chi-minh', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('6', 'Chi nhánh Sài Gòn', '<p>150 Nguyễn Công Trứ,<br />
Quận 1, TP. Hồ Chí Minh</p>

<p><span>Tel:</span> +84 8 7305 7386</p>

<p><span>Fax:</span> +84 8 3914 0938</p>
', 'vi', 'chi-nhanh-sai-gon', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('1', 'Head office', '<p>MB Building<br />
No. 3 Lieu Giai Street, Ba Dinh District, Hanoi</p>

<p><span>Tel:</span> + 84 4 3726 2600</p>

<p><span>Fax:</span> + 84 4 3726 2601</p>
', 'en', 'head-office', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('2', 'MBS Ha Noi', '<p>5th Floor , Thang Long Tower,<br />
98A Nguy Nhu Kon Tum,<br />
Thanh Xuan District, Ha Noi</p>

<p><span>Tel:</span> + 84 4 7305 7386</p>

<p><span>Fax:</span> + 84 4 3569 0257</p>
', 'en', 'mbs-ha-noi', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('3', 'MBS Hoan Kiem', '<p>5th Floor, 14C Ly Nam De Building,<br />
Hoan Kiem District, Hanoi</p>

<p><span>Tel:</span> +84 4 3733 7671</p>

<p><span>Fax:</span> +84 4 3733 7890</p>
', 'en', 'mbs-hoan-kiem', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('4', 'MBS Hai Phong', '<p>3rd Floor, EVN Building,<br />
No. 7B Tran Hung Dao,<br />
Hong Bang District, Haiphong</p>

<p><span>Tel:</span> +84 31 382 1886</p>

<p><span>Fax:</span> +84 31 374 7739</p>
', 'en', 'mbs-hai-phong', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('5', 'MBS Ho Chi Minh', '<p>5th Floor, Sunny Tower,<br />
259 Tran Hung Dao,<br />
District 1, HCMC.</p>

<p><span>Tel:</span> +84 8 3920 3388</p>

<p><span>Fax:</span> +84 8 3838 5181</p>
', 'en', 'mbs-ho-chi-minh', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('6', 'MBS Nguyen Cong Tru', '<p>150 Nguyen Cong Tru,<br />
District 1, HCMC.</p>

<p><span>Tel:</span> +84 8 7305 7386</p>

<p><span>Fax:</span> +84 8 3914 0938</p>
', 'en', 'mbs-nguyen-cong-tru', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('7', 'MBS Bac Sai Gon', '<p>No.3 Nguyen Oanh, Go Vap District, HCMC.</p>

<p><span>Tel:</span> +84 8 3989 4425</p>

<p><span>Fax:</span> +84 8 3989 4428</p>
', 'en', 'mbs-bac-sai-gon', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('8', 'Other transaction locations:', '<p>Quang Ninh, Hai Dương, Ninh Binh, Bac Ninh, Nghe An, Da Nang, Khanh Hoa, Vung Tau, Binh Dương, Dong Nai, An Giang, Can Tho…</p>
', 'en', 'other-transaction-locations', '/en/about/locations#dia-diem-giao-dich-truc-tiep');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('3', 'Chi nhánh Hoàn Kiếm', '<p>Tầng 5, Tòa nhà 14C Lý Nam Đế,<br />
Quận Hoàn Kiếm, Hà Nội</p>

<p><span>Tel:</span> +84 4 3733 7671</p>

<p><span>Fax:</span> +84 4 3733 7890</p>
', 'vi', 'chi-nhanh-hoan-kiem', null);
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('8', 'Danh sách điểm giao dịch trực tuyến', '<p>Quảng Ninh, Hải Dương, Ninh Bình, Bắc Ninh, Nghệ An, Đà Nẵng, Khánh Hòa, Vũng Tàu, Bình Dương, Đồng Nai, An Giang, Cần Thơ…</p>
', 'vi', 'danh-sach-diem-giao-dich-truc-tuyen', '/vi/gioi-thieu-chung/mang-luoi#dia-diem-giao-dich-truc-tiep');
INSERT INTO "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" VALUES ('7', 'Chi nhánh Bắc Sài Gòn', '<p>Số 3 Nguyễn Oanh, Quận Gò Vấp, TP. Hồ Chí Minh</p>

<p><span>Tel:</span> +84 8 3989 4425</p>

<p><span>Fax:</span> +84 8 3989 4428</p>
', 'vi', 'chi-nhanh-bac-sai-gon', null);

-- ----------------------------
-- Indexes structure for table WZ_BRANCH_CONTENT_FOOTER
-- ----------------------------

-- ----------------------------
-- Checks structure for table WZ_BRANCH_CONTENT_FOOTER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("NID" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("LANG" IS NOT NULL);
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD CHECK ("NID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WZ_BRANCH_CONTENT_FOOTER
-- ----------------------------
ALTER TABLE "MBS_DB"."WZ_BRANCH_CONTENT_FOOTER" ADD PRIMARY KEY ("NID", "LANG");
