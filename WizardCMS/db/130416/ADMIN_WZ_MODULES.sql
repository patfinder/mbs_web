/*
Navicat Oracle Data Transfer
Oracle Client Version : 12.1.0.2.0

Source Server         : MBS_DEMO
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MBS_DB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2016-04-13 19:11:24
*/


-- ----------------------------
-- Table structure for ADMIN_WZ_MODULES
-- ----------------------------
DROP TABLE "MBS_DB"."ADMIN_WZ_MODULES";
CREATE TABLE "MBS_DB"."ADMIN_WZ_MODULES" (
"ID" NUMBER NOT NULL ,
"NAME" VARCHAR2(255 BYTE) NULL ,
"NAME_FUNCTION" VARCHAR2(255 BYTE) NULL ,
"STATUS" NUMBER DEFAULT 1  NULL ,
"CREATED" TIMESTAMP(6)  NOT NULL ,
"PRIORITY" NUMBER DEFAULT 0  NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of ADMIN_WZ_MODULES
-- ----------------------------
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('15', 'Manage Forms', 'form', '1', TO_TIMESTAMP(' 2015-05-06 16:29:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '13');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('16', 'Manage Jobs', 'job', '1', TO_TIMESTAMP(' 2015-05-06 16:29:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '14');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('17', 'Manage News', 'news', '1', TO_TIMESTAMP(' 2015-05-20 17:47:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '15');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('1', 'Manage Account Group', 'accountgroups', '1', TO_TIMESTAMP(' 2014-12-15 09:38:40:169065', 'YYYY-MM-DD HH24:MI:SS:FF6'), '1');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('2', 'Manage Account', 'accounts', '1', TO_TIMESTAMP(' 2014-12-15 09:38:46:673437', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('3', 'Manage Module', 'modules', '1', TO_TIMESTAMP(' 2014-12-15 09:38:46:677437', 'YYYY-MM-DD HH24:MI:SS:FF6'), '10');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('4', 'Manage Article', 'article', '1', TO_TIMESTAMP(' 2014-12-15 09:38:46:680437', 'YYYY-MM-DD HH24:MI:SS:FF6'), '2');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('5', 'Html', 'html', '0', TO_TIMESTAMP(' 2014-12-15 09:38:46:684437', 'YYYY-MM-DD HH24:MI:SS:FF6'), '0');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('6', 'Manage Logs', 'logs', '0', TO_TIMESTAMP(' 2014-12-15 09:38:46:695438', 'YYYY-MM-DD HH24:MI:SS:FF6'), '11');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('8', 'Manage Contact', 'contact', '1', TO_TIMESTAMP(' 2014-12-30 16:25:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '5');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('9', 'Manage Branch', 'branch', '1', TO_TIMESTAMP(' 2015-08-01 11:04:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '3');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('10', 'Manage Branch Group', 'branchgroup', '0', TO_TIMESTAMP(' 2015-03-03 16:36:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '4');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('7', 'Manage Slider', 'slide', '1', TO_TIMESTAMP(' 2014-12-30 11:49:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '7');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('11', 'Manage Taxonomy', 'taxonomy', '0', TO_TIMESTAMP(' 2015-04-02 11:28:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('12', 'Manage Term', 'term', '1', TO_TIMESTAMP(' 2015-04-02 11:29:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '9');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('13', 'Manage Workshop', 'workshop', '1', TO_TIMESTAMP(' 2015-04-08 14:43:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '6');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('14', 'Manage Fast Link', 'fastlink', '1', TO_TIMESTAMP(' 2015-04-08 16:29:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '12');
INSERT INTO "MBS_DB"."ADMIN_WZ_MODULES" VALUES ('18', 'Manage Branch Footer', 'branchfooter', '1', TO_TIMESTAMP(' 2016-04-20 16:22:13:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '4');

-- ----------------------------
-- Indexes structure for table ADMIN_WZ_MODULES
-- ----------------------------

-- ----------------------------
-- Checks structure for table ADMIN_WZ_MODULES
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("CREATED" IS NOT NULL);
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD CHECK ("PRIORITY" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table ADMIN_WZ_MODULES
-- ----------------------------
ALTER TABLE "MBS_DB"."ADMIN_WZ_MODULES" ADD PRIMARY KEY ("ID");
