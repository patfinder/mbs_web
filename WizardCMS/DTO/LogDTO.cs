﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardCMS.DTO
{
    public class LogDTO
    {
        public string NameModule { get; set; }
        public string IP { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public string Field { get; set; }
        public string Type { get; set; }
        public string IdGroup { get; set; }
        public Object OldVal { get; set; }
        public Object NewVal { get; set; }
        public int Id { get; set; }
        public string GroupName { get; set; }

        public LogDTO()
        { }

        public LogDTO(string nameModule, int id, string field, string type, string Ip, string user, Object oldVal, object newVal)
        {
            this.NameModule = nameModule;
            this.Id = id;
            this.Field = field;
            this.Type = type;
            this.IP = Ip;
            this.User = user;
            this.OldVal = oldVal;
            this.NewVal = newVal;
        }

        public LogDTO(string nameModule, int id, string field, string type, string ip, string user)
        {
            this.NameModule = nameModule;
            this.Id = id;
            this.Field = field;
            this.Type = type;
            this.IP = ip;
            this.User = user;
        }
    }
}