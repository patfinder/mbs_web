﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WizardCMS.DTO
{
    public class SelectGroup
    {
        public SelectList   SelectList { get; set; }
        public String       GroupValue { get; set; }
    }
}